package com.darmaneh.recievers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.darmaneh.requests.Utility;

/**
 * Created by pourya on 4/6/17.
 */

public class NetworkChangeListener extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.e("NetReceiver", "..................");
        Utility.check_internet_connection(context);
    }
}
