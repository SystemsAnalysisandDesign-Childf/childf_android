package com.darmaneh.utilities;

import android.support.v4.app.Fragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sepehr Abdous on 6/4/2017.
 */

public class BackStackTracer {


    public List<Fragment> backTrackStack;
    public List<String> backTrackPos, backTrackTAG;

    public BackStackTracer(){
        backTrackStack = new ArrayList<>();
        backTrackPos = new ArrayList<>();
        backTrackTAG = new ArrayList<>();
    }

    public void removeTrace(String pos){
        int index = backTrackPos.indexOf(pos);
        backTrackStack.remove(index);
        backTrackPos.remove(index);
        backTrackTAG.remove(index);
    }

    public void addTrace(Fragment frag, String pos, String TAG){
        backTrackStack.add(frag);
        backTrackPos.add(pos);
        backTrackTAG.add(TAG);
    }

    public boolean hasTag(String tag){
        return backTrackTAG.contains(tag);
    }

    public boolean isTheLastSlide(){
        return backTrackStack.size()==1;
    }

    public List<Fragment> getBackTrackStack() {
        return backTrackStack;
    }

    public List<String> getBackTrackPos() {
        return backTrackPos;
    }

    public List<String> getBackTrackTAG() {
        return backTrackTAG;
    }

    public void trace(Fragment frag, String pos, String tag){
        if (hasTag(tag)){
            removeTrace(pos);
        }
        addTrace(frag, pos, tag);
    }
}
