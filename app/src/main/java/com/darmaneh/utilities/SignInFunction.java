package com.darmaneh.utilities;

import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.widget.TextView;

import com.darmaneh.ava.App;
import com.darmaneh.ava.R;
import com.darmaneh.ava.StepProgressActivity;

/**
 * Created by alireza on 3/30/17.
 */
public class SignInFunction {

    public static void signInDialog(final Context context, String message) {
        if(Storage.getIsLogin())
            return;

        Analytics.sendScreenName("login/first");
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                switch (i){
                    case DialogInterface.BUTTON_POSITIVE:
                        signInProcess(context);
                        break;
                    case DialogInterface.BUTTON_NEGATIVE:
                        break;
                }
            }
        };
        AlertDialog dialog = new AlertDialog.Builder(context)
                .setMessage(message)
                .setPositiveButton("ورود" , dialogClickListener)
                .setNegativeButton("الان نه", dialogClickListener)
                .show();
        TextView textMessage = (TextView) dialog.findViewById(android.R.id.message);
        textMessage.setTypeface(App.getFont(3));
        TextView textPositive = (TextView) dialog.findViewById(android.R.id.button1);
        textPositive.setTypeface(App.getFont(3));
        textPositive.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
        TextView textNegative = (TextView) dialog.findViewById(android.R.id.button2);
        textNegative.setTypeface(App.getFont(3));
        textNegative.setTextColor(ContextCompat.getColor(context ,R.color.colorPrimary));
    }

    private static void signInProcess (Context context){
        Intent intent = new Intent(context , StepProgressActivity.class);
        context.startActivity(intent);
    }
}
