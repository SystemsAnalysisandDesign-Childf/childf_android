package com.darmaneh.utilities;

import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v4.content.PermissionChecker;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import com.darmaneh.ava.call.GalleryActivity;
import com.darmaneh.dialogs.DarmanehProgressDialog;
import com.darmaneh.fragments.PhotoDescriptionFragment;

import java.io.ByteArrayOutputStream;
import java.io.File;

import static android.app.Activity.RESULT_OK;

/**
 * Created by alireza on 8/13/17.
 */

public class PickImageUtility {

    private static final int CAMERA_PERMISSION = 4;
    private static final int STORAGE_PERMISSION = 5;
    private static final int TAKEPICTURECAMERA = 1, TAKEPICTUREGALLERY = 2;
    private static Uri mImageUri;
    public static String currentPicturePath;
    static DarmanehProgressDialog progressDialog;
    private final static String TAG = PickImageUtility.class.getSimpleName();

    private static void resultHandler(Context context, int requestCode, int resultCode, Intent data){
        if (requestCode == TAKEPICTURECAMERA && resultCode == RESULT_OK) {
            try {
                if (Build.VERSION.SDK_INT > 23) {
                    Bitmap takenImage = BitmapFactory.decodeFile(currentPicturePath);
                    progressDialog = new DarmanehProgressDialog(context);
                    progressDialog.show();
                    AsyncCompress compress = new AsyncCompress();
                    compress.execute(takenImage);
                } else {
                    context.getContentResolver().notifyChange(mImageUri, null);
                    ContentResolver cr = context.getContentResolver();
                    Bitmap bitmap = null;

                    bitmap = MediaStore.Images.Media.getBitmap(cr, mImageUri);
                    progressDialog = new DarmanehProgressDialog(context);
                    progressDialog.show();
                    AsyncCompress compress = new AsyncCompress();
                    compress.execute(bitmap);

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (requestCode == TAKEPICTUREGALLERY && resultCode == RESULT_OK && data != null && data.getData() != null) {
            Uri filePath = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), filePath);
                progressDialog = new DarmanehProgressDialog(context);
                progressDialog.show();
                AsyncCompress compress = new AsyncCompress();
                compress.execute(bitmap);
            } catch (OutOfMemoryError | Exception e) {
                e.printStackTrace();
            }
        }

        if (requestCode == CAMERA_PERMISSION && resultCode == RESULT_OK) {
            Log.d(TAG, "camera permission OK");
        }
        if (requestCode == STORAGE_PERMISSION && resultCode == RESULT_OK) {
            Log.d(TAG, "storage permission OK");
        }
    }

    public static void showChooseDialog(final Context context) {
        final CharSequence[] options = {"دریافت عکس از دوربین", "دریافت عکس از گالری", "لغو"};
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("افزودن عکس");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("دریافت عکس از دوربین")) {
                    try {
                        if (Build.VERSION.SDK_INT > 23 &&
                                (context.checkSelfPermission(android.Manifest.permission.CAMERA)) == PermissionChecker.PERMISSION_GRANTED) {
                            dialog.dismiss();
                        }
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        File image = Files.generatePicturePath();
                        if (Build.VERSION.SDK_INT > 23) {
//                                File path = new File(Environment.getExternalStorageDirectory(),"images");
//                                File f = new File(path, "image.jpg");
                            mImageUri = FileProvider.getUriForFile(context,
                                    "com.darmaneh.provider", image);
                            intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageUri);
                            intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                        } else {
                            File f = new File(Environment.getExternalStorageDirectory(), "image.jpg");
                            mImageUri = Uri.fromFile(f);
                            intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageUri);
                        }
                        currentPicturePath = image.getAbsolutePath();
                        ((AppCompatActivity)context).startActivityForResult(intent, TAKEPICTURECAMERA);
                    } catch (Exception e) {
                        Log.e(TAG, e.toString());
                    }
                } else if (options[item].equals("دریافت عکس از گالری")) {
                    Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    ((AppCompatActivity)context).startActivityForResult(intent, TAKEPICTUREGALLERY);
                } else if (options[item].equals("لغو")) {
                    dialog.dismiss();
                }
            }

        });

        builder.show();
    }


    private static class AsyncCompress extends AsyncTask<Bitmap, String, ImageTypes> {

        @Override
        protected ImageTypes doInBackground(Bitmap... params) {
            Bitmap bitmap = params[0];
            Bitmap.CompressFormat format;
            String base;
            if (bitmap.getByteCount() > 10000000) {
                if (bitmap.getByteCount() > 55000000) {
                    return null;
                }
                format = Bitmap.CompressFormat.JPEG;
                base = "data:image/jpg;base64,";
            } else {
                format = Bitmap.CompressFormat.PNG;
                base = "data:image/png;base64,";
            }
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            bitmap.compress(format, 100, byteArrayOutputStream);
            byte[] byteArray = byteArrayOutputStream.toByteArray();
            Log.d(TAG, String.valueOf(byteArray.length));
            String encoded = base + Base64.encodeToString(byteArray, Base64.DEFAULT);
            return new ImageTypes(encoded, byteArray);
        }

        @Override
        protected void onPostExecute(ImageTypes imageTypes) {
            if (imageTypes != null) {
                String encoded = imageTypes.getEncoded();
                Log.d("base64", String.valueOf(encoded.length()));
//                PhotoDescriptionFragment fragment = PhotoDescriptionFragment
//                        .newInstance(imageTypes.getByteArray(), imageTypes.getEncoded(), imageCategories);
//                fragment.show(getSupportFragmentManager(), "photo_description");
            } else {
//                Toast.makeText(GalleryActivity.this, "حجم عکس شما زیاد است، دقت کنید که حجم همه عکس ها باید کمتر از 8 مگابایت باشد",
//                        Toast.LENGTH_LONG).show();
            }
            progressDialog.dismiss();
        }
    }

    private static class ImageTypes {
        private String encoded;
        private byte[] byteArray;

        public ImageTypes(String encoded, byte[] byteArray) {
            this.encoded = encoded;
            this.byteArray = byteArray;
        }

        public String getEncoded() {
            return encoded;
        }

        public byte[] getByteArray() {
            return byteArray;
        }
    }
}


