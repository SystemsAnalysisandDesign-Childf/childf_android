package com.darmaneh.utilities;

import android.graphics.Paint;
import android.graphics.Typeface;
import android.text.TextPaint;
import android.text.style.MetricAffectingSpan;

import com.darmaneh.ava.App;

/**
 * Created by pourya on 5/28/17.
 */

public class TypefaceSpan extends MetricAffectingSpan {
    private Typeface mTypeface;

    public TypefaceSpan(int type) {
        mTypeface = App.getFont(type);
    }

    @Override
    public void updateMeasureState(TextPaint p) {
        p.setTypeface(mTypeface);
        p.setFlags(p.getFlags() | Paint.SUBPIXEL_TEXT_FLAG);
    }

    @Override
    public void updateDrawState(TextPaint tp) {
        tp.setTypeface(mTypeface);
        tp.setFlags(tp.getFlags() | Paint.SUBPIXEL_TEXT_FLAG);
    }
}
