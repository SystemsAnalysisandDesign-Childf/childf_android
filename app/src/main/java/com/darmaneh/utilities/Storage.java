package com.darmaneh.utilities;

import com.darmaneh.ava.App;

import java.util.Date;

/**
 * Created by pourya on 3/20/17.
 */

public class Storage {
    private static String LAUNCHED = "LAUNCHED_COUNT";
    private static String TOKEN = "TOKEN";
    private static String IS_LOGIN = "IS_LOGIN";
    private static String IS_NET = "IS_NET";
    private static String PHONE_NUM = "PHONE_NUM";
    private static String ID = "ID";
    private static String CODE_NUM = "CODE_NUM";
    private static String SC_USAGE_NUMBER_KEY = "SC_USAGE_NUMBER_KEY";
    private static String IS_RATE = "IS_RATE";
    private static String TIME_RATE = "TIME_RATE";
    private static String IS_SHARED = "IS_SHARED";
    private static String DOCTOR_CATEGORY = "DOCTOR_CATEGORY";
    private static String MAX_WAITING_TIME = "MAX_WAITING_TIME";

    private static String SENT_PHONE = "SENT_PHONE";


    public static boolean getIsLogin() {
        return App.SP.getBoolean(IS_LOGIN, false);
    }
    public static void setIsLogin(Boolean is_login) {
        App.SP.edit().putBoolean(IS_LOGIN, is_login).apply();
    }

    public static boolean getIsNet() {
        return App.SP.getBoolean(IS_NET, true);
    }
    public static void setIsNet(Boolean is_net) {
        App.SP.edit().putBoolean(IS_NET, is_net).apply();
    }

    public static String getToken() {
        return App.SP.getString(TOKEN, "");
    }
    public static void setToken(String token) {
        App.SP.edit().putString(TOKEN, "TOKEN " + token).apply();
    }


    public static String getPhoneNum() {
        return App.SP.getString(PHONE_NUM, "");
    }
    public static void setPhoneNum(String phoneNum) {
        App.SP.edit().putString(PHONE_NUM, phoneNum).apply();
    }

    public static Integer getID() {
        return Integer.parseInt(App.SP.getString(ID, ""));
    }
    public static void setID(Integer id) {
        App.SP.edit().putString(ID, id.toString()).apply();
    }

    public static String getCodeNum(){
        return App.SP.getString(CODE_NUM, "98");
    }
    public static void setCodeNum(String codeNum){
        App.SP.edit().putString(CODE_NUM, codeNum).apply();
    }

    public static String getCompletePhoneNum(String lan) {
        String code = Storage.getCodeNum().equals("98") ? "0" : Storage.getCodeNum();
        if (lan.equals("fa")) {
            return Functions.toPersian(code + Storage.getPhoneNum());
        } else {
            return code + Storage.getPhoneNum();
        }
    }

    public static Boolean canUseSC() {
        if (getIsLogin()) {
            return true;
        }
        int numberOfSCUsage = App.SP.getInt(SC_USAGE_NUMBER_KEY, 0);
        return numberOfSCUsage < 2;
    }
    public static void countSCUsage() {
        int numberOfSCUsage = App.SP.getInt(SC_USAGE_NUMBER_KEY, 0);
        numberOfSCUsage += 1;
        App.SP.edit().putInt(SC_USAGE_NUMBER_KEY, numberOfSCUsage).apply();
    }
    public static int getSCUsage() {
        return App.SP.getInt(SC_USAGE_NUMBER_KEY, 0);
    }

    public static void countLaunched() {
        int numberOfLaunched = App.SP.getInt(LAUNCHED, 0);
        numberOfLaunched += 1;
        App.SP.edit().putInt(LAUNCHED, numberOfLaunched).apply();
    }
    public static int getLaunched() {
        return App.SP.getInt(LAUNCHED, 0);
    }

    public static boolean getIsRated() {
        return App.SP.getBoolean(IS_RATE, false);
    }
    public static void setIsRated(Boolean is_rated) {
        App.SP.edit().putBoolean(IS_RATE, is_rated).apply();
    }

    public static boolean getIsShared() {
        return App.SP.getBoolean(IS_SHARED, false);
    }
    public static void setIsShared(Boolean is_shared) {
        App.SP.edit().putBoolean(IS_SHARED, is_shared).apply();
    }

    public static long getTimeRate() {
        return App.SP.getLong(TIME_RATE, 0);
    }
    public static void setTimeRate(Long time_rate) {
        App.SP.edit().putLong(TIME_RATE, time_rate).apply();
    }

    public static String getDoctorCategory(){return App.SP.getString(DOCTOR_CATEGORY, "");}
    public static void setDoctorCategory(String doctorCategory){
        App.SP.edit().putString(DOCTOR_CATEGORY, doctorCategory).apply();
    }

    public static Integer getMaxWaitingTime(){return App.SP.getInt(MAX_WAITING_TIME, 0);}
    public static void setMaxWaitingTime(Integer maxWaitingTime){
        App.SP.edit().putInt(MAX_WAITING_TIME, maxWaitingTime).apply();
    }

    public static boolean getSentPhone() {
        return App.SP.getBoolean(SENT_PHONE, false);
    }
    public static void setSentPhone(Boolean sentPhone) {
        App.SP.edit().putBoolean(SENT_PHONE, sentPhone).apply();
    }
}
