package com.darmaneh.utilities;

import android.util.Log;

import com.darmaneh.ava.App;
import com.darmaneh.ava.BuildConfig;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

/**
 * Created by pourya on 3/30/17.
 */

public class Analytics {
    public static void sendScreenName(String screenName) {
        // don't send data to analytics when we are in DebugMode
        if (BuildConfig.DEBUG)
            return;

        Tracker tracker = App.getDefaultTracker();
        tracker.setScreenName(screenName);
        tracker.enableAdvertisingIdCollection(true);
        tracker.send(new HitBuilders.ScreenViewBuilder().build());
    }
}
