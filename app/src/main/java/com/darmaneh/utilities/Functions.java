package com.darmaneh.utilities;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.text.Spannable;
import android.text.SpannableString;
import android.widget.ImageView;

import com.darmaneh.fragments.ContactUsDialog;
import com.darmaneh.requests.Variable;
import com.squareup.picasso.Picasso;

import java.security.SecureRandom;
import java.util.Random;

/**
 * Created by alireza on 3/27/17.
 */
public class Functions {

    public static String toPersian(int num) {
        String number = String.valueOf(num);
        String persian = "۰۱۲۳۴۵۶۷۸۹";
        String english = "0123456789";

        for(int i=0; i<10; i++) {
            String p = "" + persian.charAt(i), e = "" + english.charAt(i);
            number = number.replaceAll(e, p);
        }

        return number;
    }

    public static String toPersian(double num) {
        String number = String.valueOf(num);
        String persian = "۰۱۲۳۴۵۶۷۸۹";
        String english = "0123456789";

        for(int i=0; i<10; i++) {
            String p = "" + persian.charAt(i), e = "" + english.charAt(i);
            number = number.replaceAll(e, p);
        }

        return number;
    }

    public static void putInPicasso(Context context, String imageUrl, ImageView imageView, int drawableId){
        if (imageUrl!=null) {
            Picasso.with(context)
                    .load(imageUrl)
                    .placeholder(drawableId)
                    .into(imageView);
        } else{
            imageView.setImageResource(drawableId);
        }

    }

    public static String toPersian(String number) {
        String persian = "۰۱۲۳۴۵۶۷۸۹";
        String english = "0123456789";

        for(int i=0; i<10; i++) {
            String p = "" + persian.charAt(i), e = "" + english.charAt(i);
            number = number.replaceAll(e, p);
        }

        return number;
    }

    public static SpannableString setFont(String text, int fontType) {
        SpannableString s = new SpannableString(text);
        s.setSpan(new TypefaceSpan(fontType), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return s;
    }

    public static void ContactUsInToolbar(FragmentManager fm) {
        Analytics.sendScreenName("contact_us/screen");
        ContactUsDialog contactUsDialog = new ContactUsDialog();
        contactUsDialog.show(fm, "contact_us");
    }

    public static void appShareFunction(Context context) {
        Analytics.sendScreenName("broad_cast");
        Intent msg = new Intent(Intent.ACTION_SEND);
        String message = "سلام" + "\n\n" +
                "اپلیکیشن «درمانه» یک اپلیکیشن جدید در حوزه سلامت هست که با استفاده از هوش مصنوعی می تواند به تشخیص بیماری احتمالی شما کمک کند. همچنین آدرس مراکز درمانی و کلی مطالب پزشکی را هم در اختیارت می گذارد." + "\n\n" +
                "همین حالا اپلیکیشن رو نصب کن :)" + "\n" + Variable.getDownloadLink();
        msg.putExtra(Intent.EXTRA_TEXT, message);
        msg.setType("text/plain");
        context.startActivity(Intent.createChooser(msg, "درمانه را به دوستانتان معرفی کنید:"));
    }

    public static String generateInterviewId() {
        final char[] CHARSET = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();

        Random random = new SecureRandom();
        char[] result = new char[16];
        for (int i = 0; i < result.length; i++) {
            // picks a random index out of character set > random character
            int randomCharIndex = random.nextInt(CHARSET.length);
            result[i] = CHARSET[randomCharIndex];
        }
        return "drmh-" + new String(result);
    }
}
