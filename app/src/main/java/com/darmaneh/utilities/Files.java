package com.darmaneh.utilities;

import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Environment;
import android.util.Log;

import com.darmaneh.ava.App;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by alireza on 8/5/17.
 */

public class Files {

    private static final String TAG = Files.class.getSimpleName();
    public static File generatePicturePath() {
        try {
            File storageDir = getAlbumDir();
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(new Date());
            return new File(storageDir, "IMG_" + timeStamp + ".jpg");
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
        return null;
    }

    private static File getAlbumDir() {
        if (Build.VERSION.SDK_INT >= 23) {
            File dir = getCacheDir();
            if(!dir.isDirectory()){
                dir.mkdir();
            }
            return dir;
        }
        File storageDir = null;
        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
            storageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "Darmaneh");
            if (!storageDir.mkdirs()) {
                if (!storageDir.exists()){
                    Log.d(TAG,"failed to create directory");
                    return null;
                }
            }
        } else {
            Log.d(TAG,"External storage is not mounted READ/WRITE.");
        }

        return storageDir;
    }

    public static File getCacheDir() {
        String state = null;
        try {
            state = Environment.getExternalStorageState();
        } catch (Exception e) {
            Log.e(TAG,e.toString());
        }
        if (state == null || state.startsWith(Environment.MEDIA_MOUNTED)) {
            try {
                File file = App.applicationContext.getExternalCacheDir();
                if (file != null) {
                    return file;
                }
            } catch (Exception e) {
                Log.e(TAG,e.toString());
            }
        }
        try {
            File file = App.applicationContext.getCacheDir();
            if (file != null) {
                return file;
            }
        } catch (Exception e) {
            Log.e(TAG,e.toString());
        }
        return new File("");
    }

}
