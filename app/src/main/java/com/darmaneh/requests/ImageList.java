package com.darmaneh.requests;

import android.content.Context;
import android.util.Log;

import com.darmaneh.dialogs.DarmanehProgressDialog;
import com.darmaneh.dialogs.NoInternetDialog;
import com.darmaneh.models.call.GalleryModel;
import com.darmaneh.models.call.ImageModel;
import com.darmaneh.utilities.Storage;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestHandle;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.List;

import cz.msebera.android.httpclient.entity.ContentType;
import cz.msebera.android.httpclient.entity.StringEntity;
import cz.msebera.android.httpclient.Header;

/**
 * Created by Sepehr Abdous on 7/8/2017.
 */

public class ImageList {
    private final static String TAG = ImageList.class.getSimpleName();
    private static DarmanehProgressDialog progress;
    public static void post_images(Context context,
                                   final ImageModel imageModel ,
                                   final ImageUploaded callback) {

        if (!Storage.getIsNet()) {
            (new NoInternetDialog()).show(context);
            return;
        }

        progress = new DarmanehProgressDialog(context);
        progress.show();

        AsyncHttpClient client = Template.configureClient();
        client.setTimeout(30000);
        JSONObject json = new JSONObject();
        StringEntity entity;
        try {
            json.put("image", imageModel.getBase64());
            json.put("category", imageModel.getPack_name());
            json.put("description", imageModel.getDescription());
            entity = new StringEntity(json.toString(), ContentType.APPLICATION_JSON);
        } catch (JSONException e) {
            return;
        }
        client.put(context,
                Variable.SERVER_ADDRESS_V1 + "user/patient/images/",
                entity, "application/json",
                new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        super.onSuccess(statusCode, headers, response);
                        Log.e(TAG, response.toString());
                        String detail = response.optString("detail", " ");
                        if (statusCode == 201)
                            callback.onHttpResponse(true);
                        else
                            callback.onHttpResponse(false);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                        callback.onHttpResponse(false);
                    }

                    @Override
                    public void onFinish() {
                        super.onFinish();
                        progress.dismiss();
                    }
                });
    }
    public interface ImageUploaded {
        void onHttpResponse(Boolean success);
    }

    public static void get_gallery(Context context, final GalleryGot callback){
        if (!Storage.getIsNet()) {
            (new NoInternetDialog()).show(context);
            return;
        }
        progress = new DarmanehProgressDialog(context);
        progress.show();
        AsyncHttpClient client = Template.configureClient();
        client.setTimeout(Variable.TIME_OUT);
        client.get(context,
                Variable.SERVER_ADDRESS_V1 +"user/patient/images/",
                new JsonHttpResponseHandler(){
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                        super.onSuccess(statusCode, headers, response);
                        List<GalleryModel> galleryModelList = (new Gson()).fromJson(response.toString(),
                                new TypeToken<List<GalleryModel>>(){}.getType());
                        Log.d(TAG, galleryModelList.toString());
                        if (statusCode == 200)
                            callback.onHttpResponse(true, galleryModelList);
                        else
                            callback.onHttpResponse(false, null);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                        callback.onHttpResponse(false, null);
                    }

                    // handle response that isn't json array

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        super.onSuccess(statusCode, headers, response);
                        callback.onHttpResponse(false, null);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                        callback.onHttpResponse(false, null);
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, String responseString) {
                        super.onSuccess(statusCode, headers, responseString);
                        callback.onHttpResponse(false, null);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        super.onFailure(statusCode, headers, responseString, throwable);
                        callback.onHttpResponse(false, null);
                    }

                    @Override
                    public void onFinish() {
                        super.onFinish();
                        progress.dismiss();
                    }
                });
    }
    public interface GalleryGot {
        void onHttpResponse(Boolean success, List<GalleryModel> modelList);
    }

    public static void delete_image(Context context,
                                    final List<String> delete_list, final DeleteCallback callback){
        if (!Storage.getIsNet()) {
            (new NoInternetDialog()).show(context);
            return;
        }

        progress = new DarmanehProgressDialog(context);
        progress.show();

        AsyncHttpClient client = Template.configureClient();
        client.setTimeout(Variable.TIME_OUT);
        JSONObject json = new JSONObject();
        StringEntity entity;
        try {
            json.put("hidden", true);
            entity = new StringEntity(json.toString(), ContentType.APPLICATION_JSON);
        } catch (JSONException e) {
            return;
        }

        for (int i=0; i<delete_list.size(); i++){
            String deleteUrl = delete_list.get(i);
            client.put(context, deleteUrl, entity, "application/json", new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    super.onSuccess(statusCode, headers, response);
                    if (statusCode == 200) {
                        callback.onHttpResponse(true);
                    }else{
                        callback.onHttpResponse(false);
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    callback.onHttpResponse(false);
                }

                @Override
                public void onFinish() {
                    super.onFinish();
                    progress.dismiss();
                }
            });
        }
    }

    public interface DeleteCallback {
        void onHttpResponse(Boolean success);
    }

}
