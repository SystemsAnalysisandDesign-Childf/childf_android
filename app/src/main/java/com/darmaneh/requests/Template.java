package com.darmaneh.requests;

import com.darmaneh.utilities.Storage;
import com.loopj.android.http.AsyncHttpClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import cz.msebera.android.httpclient.entity.ContentType;
import cz.msebera.android.httpclient.entity.StringEntity;

/**
 * Created by pourya on 5/17/17.
 */

public class Template {
    static String failureMessage = "عدم برقراری ارتباط با سرور، لطفاً دوباره تلاش کنید ...";

    public static AsyncHttpClient configureClient() {
        AsyncHttpClient client = new AsyncHttpClient();
        client.setTimeout(Variable.TIME_OUT);
        client.addHeader("Accept", "application/json");
        client.addHeader("Content-Type", "application/json");
        if (Storage.getIsLogin()) {
            client.addHeader("Authorization", Storage.getToken());
        }
        return client;
    }

    static StringEntity putItInJson(List<String> keys, List<String> values) throws JSONException {
        JSONObject json = new JSONObject();
        StringEntity entity;
        for (int i=0; i < keys.size(); i++) {
            json.put(keys.get(i), values.get(i));
        }
        entity = new StringEntity(json.toString(), ContentType.APPLICATION_JSON);
        return entity;
    }
}
