package com.darmaneh.requests;

import android.content.Context;

import com.darmaneh.dialogs.DarmanehProgressDialog;
import com.darmaneh.dialogs.NoInternetDialog;
import com.darmaneh.models.Mail.MailModel;
import com.darmaneh.models.call.InstantInfoListModel;
import com.darmaneh.utilities.Storage;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import cz.msebera.android.httpclient.Header;

/**
 * Created by Sepehr Abdous on 7/9/2017.
 */

public class PatientInfoDetailList {
    private static DarmanehProgressDialog progress;

    public static void get_info_list(final Context context, final boolean showProgress,
                                     final GetInfoList callback) {
        if(showProgress) {
//            if (!Storage.getIsNet()) {
//                (new NoInternetDialog()).show(context);
//                return;
//            }

            progress = new DarmanehProgressDialog(context);
            progress.show();
        }

        AsyncHttpClient client = Template.configureClient();
        client.addHeader("Accept", "application/json");
        client.addHeader("Content-Type", "application/json");
        client.addHeader("Authorization", Storage.getID().toString());

        client.get(context,
                Variable.SERVER_ADDRESS_V1 + "message/inbox/",
                new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                        super.onSuccess(statusCode, headers, response);
                        callback.onHttpResponse(false, null);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                        callback.onHttpResponse(false, null);
                    }

                    // handle response that isn't json array

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        super.onSuccess(statusCode, headers, response);
                        JSONArray data = null;
                        try {
                            data = response.getJSONArray("data");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        List<MailModel> mailListModels =
                                (new Gson()).fromJson(data.toString(),
                                        new TypeToken<List<MailModel>>(){}.getType());
                        if (statusCode == 200)
                            callback.onHttpResponse(true, mailListModels);
                        else
                            callback.onHttpResponse(false, null);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                        callback.onHttpResponse(false, null);
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, String responseString) {
                        super.onSuccess(statusCode, headers, responseString);
                        callback.onHttpResponse(false, null);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        super.onFailure(statusCode, headers, responseString, throwable);
                        callback.onHttpResponse(false, null);
                    }

                    @Override
                    public void onFinish() {
                        super.onFinish();
                        if(showProgress) {
                            progress.dismiss();
                        }
                    }
                });
    }
    public interface GetInfoList {
        void onHttpResponse(Boolean success, List<MailModel> instantInfoListModel);
    }

}
