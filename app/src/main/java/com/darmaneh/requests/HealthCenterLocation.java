package com.darmaneh.requests;

import android.content.Context;
import android.location.Location;
import android.util.Log;

import com.darmaneh.dialogs.DarmanehProgressDialog;
import com.darmaneh.dialogs.NoInternetDialog;

import com.darmaneh.models.health_centers.NearestListModel;
import com.darmaneh.models.location.HealthCentersLocationModel;
import com.darmaneh.utilities.Storage;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

import cz.msebera.android.httpclient.Header;

/**
 * Created by alireza on 8/10/17.
 */

public class HealthCenterLocation {
    final static String TAG = HealthCenterLocation.class.getSimpleName();
    static DarmanehProgressDialog progress;
    public static void get_nearest (Context context, double lat, double lng, String type,
                                    final LocationGot callback) {

        if (!Storage.getIsNet()) {
            (new NoInternetDialog()).show(context);
            return;
        }
        progress = new DarmanehProgressDialog(context);
        progress.show();
        AsyncHttpClient client = Template.configureClient();
        client.setTimeout(Variable.TIME_OUT);
        RequestParams params = new RequestParams();
        params.put("lat", lat);
        params.put("lng", lng);
        params.put("type",type);
        client.get(context,
                Variable.SERVER_ADDRESS_V1 +"user/patient/health_centers/", params,
                new JsonHttpResponseHandler(){
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                        super.onSuccess(statusCode, headers, response);
                        List<NearestListModel> nearestListModel = (new Gson()).fromJson(response.toString(),
                                new TypeToken<List<NearestListModel>>() { }.getType());
                        if (statusCode == 200)
                            callback.onHttpResponse(true, nearestListModel);
                        else
                            callback.onHttpResponse(false, null);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                        callback.onHttpResponse(false, null);
                    }

                    // handle response that isn't json array

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        super.onSuccess(statusCode, headers, response);
                        callback.onHttpResponse(false, null);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                        callback.onHttpResponse(false, null);
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, String responseString) {
                        super.onSuccess(statusCode, headers, responseString);
                        callback.onHttpResponse(false, null);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        super.onFailure(statusCode, headers, responseString, throwable);
                        callback.onHttpResponse(false, null);
                    }

                    @Override
                    public void onFinish() {
                        super.onFinish();
                        progress.dismiss();
                    }
                });
    }

    public interface LocationGot {
        void onHttpResponse(Boolean success,List<NearestListModel> healthCenters);
    }
}
