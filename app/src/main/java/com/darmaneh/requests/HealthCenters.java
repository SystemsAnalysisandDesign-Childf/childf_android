package com.darmaneh.requests;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.darmaneh.ava.ConditionDetailActivity;
import com.darmaneh.ava.health_centers.HospitalDetailActivity;
import com.darmaneh.ava.health_centers.PharmacyDetailActivity;
import com.darmaneh.dialogs.DarmanehProgressDialog;
import com.darmaneh.dialogs.DarmanehToast;
import com.darmaneh.dialogs.NoInternetDialog;
import com.darmaneh.models.call.ProfessionListModel;
import com.darmaneh.models.health_centers.HospitalModel;
import com.darmaneh.models.health_centers.PharmacyDetailModel;
import com.darmaneh.models.health_centers.PharmacyModel;
import com.darmaneh.models.health_centers.PhysicianListModel;
import com.darmaneh.models.health_centers.ProfessionCategoryModel;
import com.darmaneh.models.location.Hospital;
import com.darmaneh.models.physician.PhysicianModel;
import com.darmaneh.utilities.Storage;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

import cz.msebera.android.httpclient.Header;

/**
 * Created by pourya on 5/22/17.
 */

public class HealthCenters {
    private final static String TAG = HealthCenters.class.getSimpleName();
    private static DarmanehProgressDialog progress;

    public static void get_pharmacy_list(final Context context,
                                         String province, String city,
                                         final HealthCenters.GetPharmacyList callback) {
        if (!Storage.getIsNet()) {
            (new NoInternetDialog()).show(context);
            return;
        }

        progress = new DarmanehProgressDialog(context);
        progress.show();

        AsyncHttpClient client = Template.configureClient();

        client.get(context,
                Variable.SERVER_ADDRESS_V1 + "health_centers/pharmacy/?province="
                        + province + "&city=" + city,
                new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                        super.onSuccess(statusCode, headers, response);
                        List<PharmacyModel> pharmacyList = (new Gson()).fromJson(response.toString(),
                                new TypeToken<List<PharmacyModel>>(){}.getType());
                        if (statusCode == 200)
                            callback.onHttpResponse(true, pharmacyList);
                        else
                            callback.onHttpResponse(false, null);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                        DarmanehToast.makeText(context, Template.failureMessage, Toast.LENGTH_LONG);
                        callback.onHttpResponse(false, null);
                    }

                    // handle response that isn't json array

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        super.onSuccess(statusCode, headers, response);
                        DarmanehToast.makeText(context, Template.failureMessage, Toast.LENGTH_LONG);
                        callback.onHttpResponse(false, null);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                        DarmanehToast.makeText(context, Template.failureMessage, Toast.LENGTH_LONG);
                        callback.onHttpResponse(false, null);
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, String responseString) {
                        super.onSuccess(statusCode, headers, responseString);
                        DarmanehToast.makeText(context, Template.failureMessage, Toast.LENGTH_LONG);
                        callback.onHttpResponse(false, null);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        super.onFailure(statusCode, headers, responseString, throwable);
                        DarmanehToast.makeText(context, Template.failureMessage, Toast.LENGTH_LONG);
                        callback.onHttpResponse(false, null);
                    }

                    @Override
                    public void onFinish() {
                        super.onFinish();
                        progress.dismiss();
                    }
                });
    }
    public interface GetPharmacyList {
        void onHttpResponse(Boolean success, List<PharmacyModel> pharmacyList);
    }

    public static void get_pharmacy_detail(final Context context, String url) {

        if (!Storage.getIsNet()) {
            (new NoInternetDialog()).show(context);
            return;
        }

        progress = new DarmanehProgressDialog(context);
        progress.show();

        AsyncHttpClient client = Template.configureClient();

        client.get(context, url,
                new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        super.onSuccess(statusCode, headers, response);
                        PharmacyDetailModel pharmacyDetailModel = (new Gson()).fromJson(response.toString(),
                                PharmacyDetailModel.class);
                        if (statusCode == 200) {
                            Intent intent = new Intent(context, PharmacyDetailActivity.class);
                            intent.putExtra("pharmacy_detail", pharmacyDetailModel.toString());
                            context.startActivity(intent);
                        } else {
                            DarmanehToast.makeText(context, Template.failureMessage, Toast.LENGTH_LONG);
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                        DarmanehToast.makeText(context, Template.failureMessage, Toast.LENGTH_LONG);
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, String responseString) {
                        super.onSuccess(statusCode, headers, responseString);
                        DarmanehToast.makeText(context, Template.failureMessage, Toast.LENGTH_LONG);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        super.onFailure(statusCode, headers, responseString, throwable);
                        DarmanehToast.makeText(context, Template.failureMessage, Toast.LENGTH_LONG);
                    }

                    @Override
                    public void onFinish() {
                        super.onFinish();
                        progress.dismiss();
                    }
                });
    }

    public static void get_hosptial_list(final Context context,
                                         String province, String city,
                                         final HealthCenters.GetHospitalList callback) {
        if (!Storage.getIsNet()) {
            (new NoInternetDialog()).show(context);
            return;
        }

        progress = new DarmanehProgressDialog(context);
        progress.show();

        AsyncHttpClient client = Template.configureClient();

        client.get(context,
                Variable.SERVER_ADDRESS_V1 + "health_centers/hospital/?province="
                        + province + "&city=" + city,
                new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                        super.onSuccess(statusCode, headers, response);
                        List<HospitalModel> hospitalList = (new Gson()).fromJson(response.toString(),
                                new TypeToken<List<HospitalModel>>(){}.getType());
                        if (statusCode == 200)
                            callback.onHttpResponse(true, hospitalList);
                        else
                            callback.onHttpResponse(false, null);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                        DarmanehToast.makeText(context, Template.failureMessage, Toast.LENGTH_LONG);
                        callback.onHttpResponse(false, null);
                    }

                    // handle response that isn't json array

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        super.onSuccess(statusCode, headers, response);
                        DarmanehToast.makeText(context, Template.failureMessage, Toast.LENGTH_LONG);
                        callback.onHttpResponse(false, null);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                        DarmanehToast.makeText(context, Template.failureMessage, Toast.LENGTH_LONG);
                        callback.onHttpResponse(false, null);
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, String responseString) {
                        super.onSuccess(statusCode, headers, responseString);
                        DarmanehToast.makeText(context, Template.failureMessage, Toast.LENGTH_LONG);
                        callback.onHttpResponse(false, null);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        super.onFailure(statusCode, headers, responseString, throwable);
                        DarmanehToast.makeText(context, Template.failureMessage, Toast.LENGTH_LONG);
                        callback.onHttpResponse(false, null);
                    }

                    @Override
                    public void onFinish() {
                        super.onFinish();
                        progress.dismiss();
                    }
                });
    }
    public interface GetHospitalList {
        void onHttpResponse(Boolean success, List<HospitalModel> hospitalList);
    }

    public static void get_hospital_detail(final Context context, String url) {

        if (!Storage.getIsNet()) {
            (new NoInternetDialog()).show(context);
            return;
        }

        progress = new DarmanehProgressDialog(context);
        progress.show();

        AsyncHttpClient client = Template.configureClient();

        client.get(context, url,
                new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        super.onSuccess(statusCode, headers, response);
                        Hospital hospitalDetailModel = (new Gson()).fromJson(response.toString(),
                                Hospital.class);
                        if (statusCode == 200) {
                            Intent intent = new Intent(context, HospitalDetailActivity.class);
                            intent.putExtra("hospital_detail", hospitalDetailModel.toString());
                            context.startActivity(intent);
                        } else {
                            DarmanehToast.makeText(context, Template.failureMessage, Toast.LENGTH_LONG);
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                        DarmanehToast.makeText(context, Template.failureMessage, Toast.LENGTH_LONG);
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, String responseString) {
                        super.onSuccess(statusCode, headers, responseString);
                        DarmanehToast.makeText(context, Template.failureMessage, Toast.LENGTH_LONG);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        super.onFailure(statusCode, headers, responseString, throwable);
                        DarmanehToast.makeText(context, Template.failureMessage, Toast.LENGTH_LONG);
                    }

                    @Override
                    public void onFinish() {
                        super.onFinish();
                        progress.dismiss();
                    }
                });
    }

    public static void get_profession_list(final Context context, final GetProfession callback ){
        if (!Storage.getIsNet()) {
            (new NoInternetDialog()).show(context);
            return;
        }

        progress = new DarmanehProgressDialog(context);
        progress.show();

        AsyncHttpClient client = Template.configureClient();
        client.get(context,
                Variable.SERVER_ADDRESS_V1 + "physician/profession_category/",
                new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        super.onSuccess(statusCode, headers, response);
                        callback.onHttpResponse(false,null);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                        super.onSuccess(statusCode, headers, response);
                        Log.e(TAG, response.toString());
                        List<ProfessionCategoryModel> professionListModels =
                                (new Gson()).fromJson(response.toString(),
                                        new TypeToken<List<ProfessionCategoryModel>>() {
                                        }.getType());
                        if (statusCode == 200) {
                            callback.onHttpResponse(true, professionListModels);
                        } else {
                            callback.onHttpResponse(false, null);
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                        callback.onHttpResponse(false, null);

                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, String responseString) {
                        super.onSuccess(statusCode, headers, responseString);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        super.onFailure(statusCode, headers, responseString, throwable);
                    }

                    @Override
                    public void onFinish() {
                        if (progress!=null){
                            progress.dismiss();
                        }
                        super.onFinish();
                    }
                });

    }

    public interface GetProfession {
        void onHttpResponse(Boolean success, List<ProfessionCategoryModel> professionListModels);
    }

    public static void get_physician_list(final Context context, String province, String city,
            String category, final GetPhysicianList callback){
        if (!Storage.getIsNet()) {
            (new NoInternetDialog()).show(context);
            return;
        }

        progress = new DarmanehProgressDialog(context);
        progress.show();

        RequestParams params = new RequestParams();
        params.put("province",province);
        params.put("city",city);
        params.put("category",category);
        AsyncHttpClient client = Template.configureClient();

        client.get(context, Variable.SERVER_ADDRESS_V1 + "health_centers/physician/", params,
                new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        super.onSuccess(statusCode, headers, response);
                        callback.onHttpResponse(false,null);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                        super.onSuccess(statusCode, headers, response);
                        Log.e(TAG, response.toString());
                        List<PhysicianListModel> physicianListModels =
                                (new Gson()).fromJson(response.toString(),
                                        new TypeToken<List<PhysicianListModel>>() {
                                        }.getType());
                        if (statusCode == 200) {
                            callback.onHttpResponse(true, physicianListModels);
                        } else {
                            callback.onHttpResponse(false, null);
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                        callback.onHttpResponse(false, null);

                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, String responseString) {
                        super.onSuccess(statusCode, headers, responseString);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        super.onFailure(statusCode, headers, responseString, throwable);
                    }

                    @Override
                    public void onFinish() {
                        if (progress!=null){
                            progress.dismiss();
                        }
                        super.onFinish();
                    }
                });
    }
    public interface GetPhysicianList{
        void onHttpResponse(Boolean success, List<PhysicianListModel> physicianListModels);
    }

    public static void get_doctor_detail(final Context context, String url, final GetDoctorDetail callback){
        if (!Storage.getIsNet()) {
            (new NoInternetDialog()).show(context);
            return;
        }

        progress = new DarmanehProgressDialog(context);
        progress.show();

        AsyncHttpClient client = Template.configureClient();
        client.get(context, url,
                new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        super.onSuccess(statusCode, headers, response);
                        Log.e(TAG, response.toString());
                        PhysicianModel physicianModel =
                                (new Gson()).fromJson(response.toString(),
                                        new TypeToken<PhysicianModel>() {
                                        }.getType());
                        if (statusCode == 200) {
                            callback.onHttpResponse(true, physicianModel);
                        } else {
                            callback.onHttpResponse(false, null);
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                        super.onSuccess(statusCode, headers, response);
                        callback.onHttpResponse(false,null);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                        callback.onHttpResponse(false, null);

                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, String responseString) {
                        super.onSuccess(statusCode, headers, responseString);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        super.onFailure(statusCode, headers, responseString, throwable);
                    }

                    @Override
                    public void onFinish() {
                        if (progress!=null){
                            progress.dismiss();
                        }
                        super.onFinish();
                    }
                });
    }
    public interface GetDoctorDetail{
        void onHttpResponse(Boolean success, PhysicianModel physicianModel);
    }
}
