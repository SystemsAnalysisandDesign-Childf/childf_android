package com.darmaneh.requests;

import android.content.Context;
import android.util.Log;

import com.darmaneh.dialogs.DarmanehProgressDialog;
import com.darmaneh.dialogs.NoInternetDialog;
import com.darmaneh.models.ProvinceCityModel;
import com.darmaneh.utilities.Storage;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;

/**
 * Created by pourya on 5/17/17.
 */

public class ProvinceCity {
    final static String TAG = ProvinceCity.class.getSimpleName();
    private static DarmanehProgressDialog progress;

    public static void get_province_city(final Context context,
                                         final ProvinceCity.GetProvinceCity callback) {
        if (!Storage.getIsNet()) {
            (new NoInternetDialog()).show(context);
            return;
        }

        progress = new DarmanehProgressDialog(context);
        progress.show();

        AsyncHttpClient client = Template.configureClient();

        client.get(context,
                Variable.SERVER_ADDRESS_V1 + "health_centers/province_city/",
                new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                        super.onSuccess(statusCode, headers, response);
                        List<ProvinceCityModel> provinceCityModels = (new Gson()).fromJson(response.toString(),
                                new TypeToken<List<ProvinceCityModel>>(){}.getType());
                        ProvinceCityModel blankModel = new ProvinceCityModel();
                        blankModel.setName("استان را انتخاب کنید");
                        List<String> blankCity = new ArrayList<>();
                        blankCity.add("شهر را انتخاب کنید");
                        blankModel.setCities(blankCity);
                        provinceCityModels.add(0,blankModel);
                        if (statusCode == 200)
                            callback.onHttpResponse(true, provinceCityModels);
                        else
                            callback.onHttpResponse(false, null);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                        callback.onHttpResponse(false, null);
                    }

                    // handle response that isn't json array

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        super.onSuccess(statusCode, headers, response);
                        callback.onHttpResponse(false, null);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                        callback.onHttpResponse(false, null);
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, String responseString) {
                        super.onSuccess(statusCode, headers, responseString);
                        callback.onHttpResponse(false, null);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        super.onFailure(statusCode, headers, responseString, throwable);
                        callback.onHttpResponse(false, null);
                    }

                    @Override
                    public void onFinish() {
                        super.onFinish();
                        progress.dismiss();
                    }
                });
    }
    public interface GetProvinceCity {
        void onHttpResponse(Boolean success, List<ProvinceCityModel> provinceCityModels);
    }

}
