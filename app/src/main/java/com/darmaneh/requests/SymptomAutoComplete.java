package com.darmaneh.requests;

import android.content.Context;

import com.darmaneh.ava.App;
import com.darmaneh.dialogs.DarmanehProgressDialog;
import com.darmaneh.dialogs.NoInternetDialog;
import com.darmaneh.models.bodypart_symptom.SymptomCommon;
import com.darmaneh.utilities.Storage;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

import cz.msebera.android.httpclient.Header;

/**
 * Created by alireza on 6/5/17.
 */

public class SymptomAutoComplete {
    private static DarmanehProgressDialog progress;

    public static void get_auto_complete_sympthom(final Context context,
                                         final GetSymptom callback) {
        if (!Storage.getIsNet()) {
            (new NoInternetDialog()).show(context);
            return;
        }

        progress = new DarmanehProgressDialog(context);
        progress.show();

        AsyncHttpClient client = Template.configureClient();

        client.get(context,
                Variable.SERVER_ADDRESS_V1 + "symptom_checker/symptoms/?sex="+ App.diagnosis_req.getSex() ,
                new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                        super.onSuccess(statusCode, headers, response);
                        List<SymptomCommon> symptomCommons = (new Gson()).fromJson(response.toString(),
                                new TypeToken<List<SymptomCommon>>(){}.getType());
                        if (statusCode == 200)
                            callback.onHttpResponse(true, symptomCommons);
                        else
                            callback.onHttpResponse(false, null);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                        callback.onHttpResponse(false, null);
                    }

                    // handle response that isn't json array

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        super.onSuccess(statusCode, headers, response);
                        callback.onHttpResponse(false, null);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                        callback.onHttpResponse(false, null);
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, String responseString) {
                        super.onSuccess(statusCode, headers, responseString);
                        callback.onHttpResponse(false, null);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        super.onFailure(statusCode, headers, responseString, throwable);
                        callback.onHttpResponse(false, null);
                    }

                    @Override
                    public void onFinish() {
                        super.onFinish();
                        progress.dismiss();
                    }
                });
    }
    public interface GetSymptom {
        void onHttpResponse(Boolean success, List<SymptomCommon> symptomCommons);
    }
}
