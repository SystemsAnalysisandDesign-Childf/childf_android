package com.darmaneh.requests;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.darmaneh.dialogs.DarmanehToast;
import com.darmaneh.utilities.Storage;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.ContentType;
import cz.msebera.android.httpclient.entity.StringEntity;

/**
 * Created by pourya on 5/18/17.
 */

public class UserFlowV3 {
    private final static String TAG = UserFlowV3.class.getSimpleName();

    private static int no_phone_number = 1,
                invalid_phone_number = 2,
                no_password = 3,
                password_mismatch = 4,
                no_verify_code = 5,
                invalid_verify_code = 6,
                verify_code_expired = 8,
                verify_code_mismatch = 9,
                patient_does_not_exist = 10,
                patient_does_not_verified = 11,
                patient_does_not_have_verify_code = 12;

    public static void patient_check_registered(final Context context,
                                                String phone_number,
                                                final UserFlowV3.PatientCheckRegistered callback) {
        AsyncHttpClient client = Template.configureClient();

//        StringEntity entity;
//        try {
//            entity = Template.putItInJson(
//                    Arrays.asList("phone_number", "code"),
//                    Arrays.asList(phone_number, code_number));
//        } catch (JSONException e) {
//            e.printStackTrace();
//            return;
//        }

        client.get(context,
                Variable.SERVER_ADDRESS_V3 + "auth/user/" + phone_number + '/',
                new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        super.onSuccess(statusCode, headers, response);
                        Log.e(TAG, response.toString());
                        String registered = response.optString("data", "unknown");
                        if (registered.equals("true"))
                            callback.onHttpResponse(true, true);
                        else
                            callback.onHttpResponse(true, false);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                        DarmanehToast.makeText(context, Template.failureMessage, Toast.LENGTH_LONG);
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                        super.onSuccess(statusCode, headers, response);
                        DarmanehToast.makeText(context, Template.failureMessage, Toast.LENGTH_LONG);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                        DarmanehToast.makeText(context, Template.failureMessage, Toast.LENGTH_LONG);
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, String responseString) {
                        super.onSuccess(statusCode, headers, responseString);
                        DarmanehToast.makeText(context, Template.failureMessage, Toast.LENGTH_LONG);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        super.onFailure(statusCode, headers, responseString, throwable);
                        DarmanehToast.makeText(context, Template.failureMessage, Toast.LENGTH_LONG);
                    }

                    @Override
                    public void onFinish() {
                        super.onFinish();
                        callback.onFinishResponse();
                    }
                });
    }
    public interface PatientCheckRegistered {
        void onHttpResponse(Boolean success, Boolean isRegistered);
        void onFinishResponse();
    }

    public static void logout(final Context context,
                                                final UserFlowV3.Logout callback) {
        AsyncHttpClient client = Template.configureClient();
        client.addHeader("Accept", "application/json");
        client.addHeader("Content-Type", "application/json");
        client.addHeader("Authorization", Storage.getID().toString());

//        StringEntity entity;
//        try {
//            entity = Template.putItInJson(
//                    Arrays.asList("phone_number", "code"),
//                    Arrays.asList(phone_number, code_number));
//        } catch (JSONException e) {
//            e.printStackTrace();
//            return;
//        }

        client.get(context,
                Variable.SERVER_ADDRESS_V3 + "auth/logout/",
                new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        super.onSuccess(statusCode, headers, response);
                        Log.e(TAG, response.toString());
                        String registered = response.optString("message", "unknown");
                        if (statusCode == 200 && registered.equals(""))
                            callback.onHttpResponse(true);
                        else
                            callback.onHttpResponse(true);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                        DarmanehToast.makeText(context, Template.failureMessage, Toast.LENGTH_LONG);
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                        super.onSuccess(statusCode, headers, response);
                        DarmanehToast.makeText(context, Template.failureMessage, Toast.LENGTH_LONG);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                        DarmanehToast.makeText(context, Template.failureMessage, Toast.LENGTH_LONG);
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, String responseString) {
                        super.onSuccess(statusCode, headers, responseString);
                        DarmanehToast.makeText(context, Template.failureMessage, Toast.LENGTH_LONG);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        super.onFailure(statusCode, headers, responseString, throwable);
                        DarmanehToast.makeText(context, Template.failureMessage, Toast.LENGTH_LONG);
                    }

                    @Override
                    public void onFinish() {
                        super.onFinish();
                    }
                });
    }
    public interface Logout {
        void onHttpResponse(Boolean success);
    }

    public static void patient_send_verification_again(final Context context,
                                                        String username,
                                                        final UserFlowV3.PatientSendVerificationAgain callback) {
        AsyncHttpClient client = Template.configureClient();
        StringEntity entity;
        JSONObject json = new JSONObject();
        try {
            json.put("username", username);
            entity = new StringEntity(json.toString(), ContentType.APPLICATION_JSON);
        } catch (JSONException e) {
            e.printStackTrace();
            return;
        }

        client.post(context,
                Variable.SERVER_ADDRESS_V3 + "auth/send_code/",
                entity, "application/json",
                new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        super.onSuccess(statusCode, headers, response);
                        Log.e("message", "salam");
                        String detail = response.optString("message", "failure");
                        if (statusCode == 200 && detail.equals("")) {
                            DarmanehToast.makeText(context,
                                    "کد فعال‌سازی مجددا برای شما ارسال شد.",
                                    Toast.LENGTH_SHORT);
                            callback.onHttpResponse(true);
                        } else
                            DarmanehToast.makeText(context, Template.failureMessage, Toast.LENGTH_LONG);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                        try {
                            String detail = errorResponse.optString("detail", "failure");
                            if (statusCode == 400)
                                DarmanehToast.makeText(context, detail, Toast.LENGTH_SHORT);
                            else
                                DarmanehToast.makeText(context, Template.failureMessage, Toast.LENGTH_LONG);
                        }catch (NullPointerException e){
                            DarmanehToast.makeText(context, Template.failureMessage, Toast.LENGTH_LONG);
                        }
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                        super.onSuccess(statusCode, headers, response);
                        DarmanehToast.makeText(context, Template.failureMessage, Toast.LENGTH_LONG);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                        DarmanehToast.makeText(context, Template.failureMessage, Toast.LENGTH_LONG);
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, String responseString) {
                        super.onSuccess(statusCode, headers, responseString);
                        DarmanehToast.makeText(context, Template.failureMessage, Toast.LENGTH_LONG);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        super.onFailure(statusCode, headers, responseString, throwable);
                        DarmanehToast.makeText(context, Template.failureMessage, Toast.LENGTH_LONG);
                    }

                    @Override
                    public void onFinish() {
                        super.onFinish();
                    }
                });
    }

    public interface PatientSendVerificationAgain {
        void onHttpResponse(Boolean success);
    }

    public static void patient_send_verification(final Context context,
                                                 String username,
                                                 String password,
                                                 String email,
                                                 String firstName,
                                                 String lastName,
                                                 String phoneNumber,
                                                 String fatherName,
                                                 final UserFlowV3.PatientSendVerification callback) {
        AsyncHttpClient client = Template.configureClient();
        StringEntity entity;
        JSONObject json = new JSONObject();
        try {
            json.put("username", username);
            json.put("password", password);
            json.put("email", email);
            json.put("first_name", firstName);
            json.put("last_name", lastName);
            json.put("phone", phoneNumber);
            json.put("father_name", fatherName);
            json.put("person_type", 2);
            entity = new StringEntity(json.toString(), ContentType.APPLICATION_JSON);
        } catch (JSONException e) {
            e.printStackTrace();
            return;
        }

        client.post(context,
                Variable.SERVER_ADDRESS_V3 + "auth/register/",
                entity, "application/json",
                new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        super.onSuccess(statusCode, headers, response);
                        Log.e("message", "salam");
                        String detail = response.optString("data", "failure");
                        if (statusCode == 200 && detail.equals("register successfully")) {
                            callback.onHttpResponse(true);
                        } else
                            DarmanehToast.makeText(context, Template.failureMessage, Toast.LENGTH_LONG);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                        try {
                            String detail = errorResponse.optString("detail", "failure");
                            if (statusCode == 400)
                                DarmanehToast.makeText(context, detail, Toast.LENGTH_SHORT);
                            else
                                DarmanehToast.makeText(context, Template.failureMessage, Toast.LENGTH_LONG);
                        }catch (NullPointerException e){
                            DarmanehToast.makeText(context, Template.failureMessage, Toast.LENGTH_LONG);
                        }
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                        super.onSuccess(statusCode, headers, response);
                        DarmanehToast.makeText(context, Template.failureMessage, Toast.LENGTH_LONG);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                        DarmanehToast.makeText(context, Template.failureMessage, Toast.LENGTH_LONG);
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, String responseString) {
                        super.onSuccess(statusCode, headers, responseString);
                        DarmanehToast.makeText(context, Template.failureMessage, Toast.LENGTH_LONG);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        super.onFailure(statusCode, headers, responseString, throwable);
                        DarmanehToast.makeText(context, Template.failureMessage, Toast.LENGTH_LONG);
                    }

                    @Override
                    public void onFinish() {
                        super.onFinish();
                        callback.onFinishResponse();
                    }
                });
    }
    public interface PatientSendVerification {
        void onHttpResponse(Boolean success);
        void onFinishResponse();
    }

    public static void patient_restart_password(final Context context,
                                                 String username) {
        AsyncHttpClient client = Template.configureClient();
        StringEntity entity;
        JSONObject json = new JSONObject();
        try {
            json.put("username", username);
            entity = new StringEntity(json.toString(), ContentType.APPLICATION_JSON);
        } catch (JSONException e) {
            e.printStackTrace();
            return;
        }

        client.post(context,
                Variable.SERVER_ADDRESS_V3 + "auth/forget_pass/",
                entity, "application/json",
                new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        super.onSuccess(statusCode, headers, response);
                        Log.e("message", "salam");
                        String detail = response.optString("message", "failure");
                        if (statusCode == 200 && detail.equals("")) {
                            DarmanehToast.makeText(context,
                                    "رمز عبور جدید شما به ایمیل شما ارسال شد.",
                                    Toast.LENGTH_SHORT);
                        } else
                            DarmanehToast.makeText(context, Template.failureMessage, Toast.LENGTH_LONG);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                        try {
                            String detail = errorResponse.optString("detail", "failure");
                            if (statusCode == 400)
                                DarmanehToast.makeText(context, detail, Toast.LENGTH_SHORT);
                            else
                                DarmanehToast.makeText(context, Template.failureMessage, Toast.LENGTH_LONG);
                        }catch (NullPointerException e){
                            DarmanehToast.makeText(context, Template.failureMessage, Toast.LENGTH_LONG);
                        }
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                        super.onSuccess(statusCode, headers, response);
                        DarmanehToast.makeText(context, Template.failureMessage, Toast.LENGTH_LONG);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                        DarmanehToast.makeText(context, Template.failureMessage, Toast.LENGTH_LONG);
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, String responseString) {
                        super.onSuccess(statusCode, headers, responseString);
                        DarmanehToast.makeText(context, Template.failureMessage, Toast.LENGTH_LONG);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        super.onFailure(statusCode, headers, responseString, throwable);
                        DarmanehToast.makeText(context, Template.failureMessage, Toast.LENGTH_LONG);
                    }

                    @Override
                    public void onFinish() {
                        super.onFinish();
                    }
                });
    }

    public static void patient_set_or_change_pass(final Context context,
                                                  String phone_number,
                                                  String verifyCode,
                                                  final UserFlowV3.PatientSetOrChangePass callback) {
        AsyncHttpClient client = Template.configureClient();

        StringEntity entity;
        JSONObject json = new JSONObject();
        try {
            json.put("username", phone_number);
            json.put("verification_code", verifyCode);
            entity = new StringEntity(json.toString(), ContentType.APPLICATION_JSON);
        } catch (JSONException e) {
            e.printStackTrace();
            return;
        }

        client.post(context,
                Variable.SERVER_ADDRESS_V3 + "auth/activate/",
                entity, "application/json",
                new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        super.onSuccess(statusCode, headers, response);
                        Log.e("message", response.toString());
                        String detail = response.optString("message", "failure");
                        if (statusCode == 200 && detail.equals("")) {
                            Storage.setToken(response.optString("token", ""));
                            Storage.setIsLogin(true);
                            DarmanehToast.makeText(context,
                                    "شما با موفقیت وارد حساب کاربری خود شدید.",
                                    Toast.LENGTH_SHORT);
                            callback.onHttpResponse(true);
                        } else
                            DarmanehToast.makeText(context, Template.failureMessage, Toast.LENGTH_LONG);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                        try {
                            int code = errorResponse.optInt("code", 0);
                            if (code == invalid_verify_code)
                                callback.onInvalidVerifyCode();
                            else if (code == verify_code_expired)
                                callback.onExpiredVerificationCode();
                            else if (code == verify_code_mismatch)
                                callback.onVerificationCodeMismatch();
                        }
                        catch (NullPointerException e){
                            DarmanehToast.makeText(context, Template.failureMessage, Toast.LENGTH_LONG);
                        }
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                        super.onSuccess(statusCode, headers, response);
                        DarmanehToast.makeText(context, Template.failureMessage, Toast.LENGTH_LONG);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                        DarmanehToast.makeText(context, Template.failureMessage, Toast.LENGTH_LONG);
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, String responseString) {
                        super.onSuccess(statusCode, headers, responseString);
                        DarmanehToast.makeText(context, Template.failureMessage, Toast.LENGTH_LONG);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        super.onFailure(statusCode, headers, responseString, throwable);
                        DarmanehToast.makeText(context, Template.failureMessage, Toast.LENGTH_LONG);
                    }

                    @Override
                    public void onFinish() {
                        super.onFinish();
                        callback.onFinishResponse();
                    }
                });
    }
    public interface PatientSetOrChangePass {
        void onHttpResponse(Boolean success);
        void onInvalidVerifyCode();
        void onExpiredVerificationCode();
        void onVerificationCodeMismatch();
        void onFinishResponse();
    }

    public static void patient_login(final Context context,
                                     String phone_number,
                                     String password,
                                     final UserFlowV3.PatientLogin callback) {

        AsyncHttpClient client = Template.configureClient();

        StringEntity entity;
        JSONObject json = new JSONObject();
        try {
            json.put("username", phone_number);
            json.put("password", password);
            entity = new StringEntity(json.toString(), ContentType.APPLICATION_JSON);
        } catch (JSONException e) {
            e.printStackTrace();
            return;
        }

        client.post(context,
                Variable.SERVER_ADDRESS_V3 + "auth/login/",
                entity, "application/json",
                new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        super.onSuccess(statusCode, headers, response);
                        Log.e("message", response.toString());
                        String detail = response.optString("message", "failure");
                        JSONObject data = null;
                        try {
                            data = new JSONObject(response.optString("data", null));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        int id = Integer.parseInt(data.optString("id", "0"));
                        if (statusCode == 200 && detail.equals("")) {
                            Storage.setToken(response.optString("token", ""));
                            Storage.setID(id);
                            Storage.setIsLogin(true);
                            DarmanehToast.makeText(context,
                                    "شما با موفقیت وارد حساب کاربری خود شدید.",
                                    Toast.LENGTH_SHORT);
                            callback.onHttpResponse(true);
                        } else
                            DarmanehToast.makeText(context, Template.failureMessage, Toast.LENGTH_LONG);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                        try {
                            int code = errorResponse.optInt("code", 0);
                            if (code == password_mismatch)
                                callback.onPasswordMismatch();
                            else
                                DarmanehToast.makeText(context, Template.failureMessage, Toast.LENGTH_LONG);
                        }catch (NullPointerException e) {
                            DarmanehToast.makeText(context, Template.failureMessage, Toast.LENGTH_LONG);
                        }
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                        super.onSuccess(statusCode, headers, response);
                        DarmanehToast.makeText(context, Template.failureMessage, Toast.LENGTH_LONG);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                        DarmanehToast.makeText(context, Template.failureMessage, Toast.LENGTH_LONG);
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, String responseString) {
                        super.onSuccess(statusCode, headers, responseString);
                        DarmanehToast.makeText(context, Template.failureMessage, Toast.LENGTH_LONG);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        super.onFailure(statusCode, headers, responseString, throwable);
                        DarmanehToast.makeText(context, Template.failureMessage, Toast.LENGTH_LONG);
                    }

                    @Override
                    public void onFinish() {
                        super.onFinish();
                        callback.onFinishResponse();
                    }
                });
    }
    public interface PatientLogin {
        void onHttpResponse(Boolean success);
        void onPasswordMismatch();
        void onFinishResponse();
    }
}
