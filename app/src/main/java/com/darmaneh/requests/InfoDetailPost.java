package com.darmaneh.requests;

import android.content.Context;
import android.util.Log;

import com.darmaneh.dialogs.DarmanehProgressDialog;
import com.darmaneh.dialogs.NoInternetDialog;
import com.darmaneh.models.call.ImageCategoryModel;
import com.darmaneh.models.call.ImageModel;
import com.darmaneh.models.call.InstantInfo;
import com.darmaneh.utilities.Storage;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.List;

import cz.msebera.android.httpclient.entity.StringEntity;
import cz.msebera.android.httpclient.Header;

/**
 * Created by Sepehr Abdous on 7/8/2017.
 */

public class InfoDetailPost {
    private final static String TAG = InfoDetailPost.class.getSimpleName();
    private static DarmanehProgressDialog progress;

    public static void post_info_detail(Context context,
                                   final List<Integer> ids ,
                                   final String key,
                                   final String url,
                                   final InfoDetailUploaded callback) {

        if (!Storage.getIsNet()) {
            (new NoInternetDialog()).show(context);
            return;
        }

        progress = new DarmanehProgressDialog(context);
        progress.show();

        AsyncHttpClient client = Template.configureClient();

        JSONObject json = new JSONObject();
        JSONArray jsonIds = new JSONArray(ids);
        StringEntity entity;
        try {
            json.put(key, jsonIds);
            entity = new StringEntity(json.toString());
        } catch (JSONException | UnsupportedEncodingException e) {
            return;
        }

        client.post(context,
                Variable.SERVER_ADDRESS_V1 + url,
                entity, "application/json",
                new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        super.onSuccess(statusCode, headers, response);
                        progress.dismiss();
                        Log.e(TAG, response.toString());
                        String detail = response.optString("detail", " ");
                        if (statusCode == 201)
                            callback.onHttpResponse(true);
                        else
                            callback.onHttpResponse(false);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                        progress.dismiss();
                        callback.onHttpResponse(false);
                    }
                });
    }

    public interface InfoDetailUploaded {
        void onHttpResponse(Boolean success);
    }

    public static void getImageCategories(Context context, final ImageCategories callback){
        if (!Storage.getIsNet()) {
            (new NoInternetDialog()).show(context);
            return;
        }

        progress = new DarmanehProgressDialog(context);
        progress.show();

        AsyncHttpClient client = Template.configureClient();
        client.get(context,
                Variable.SERVER_ADDRESS_V1+ "content_pool/image_categories/",
                new JsonHttpResponseHandler(){
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                        super.onSuccess(statusCode, headers, response);
                        List<ImageCategoryModel> imageCategoryModels = (new Gson()).fromJson(response.toString(),
                                new TypeToken<List<ImageCategoryModel>>(){}.getType());
                        if (statusCode == 200)
                            callback.onHttpResponse(true, imageCategoryModels);
                        else
                            callback.onHttpResponse(false, null);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                        callback.onHttpResponse(false, null);
                    }

                    // handle response that isn't json array

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        super.onSuccess(statusCode, headers, response);
                        callback.onHttpResponse(false, null);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                        callback.onHttpResponse(false, null);
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, String responseString) {
                        super.onSuccess(statusCode, headers, responseString);
                        callback.onHttpResponse(false, null);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        super.onFailure(statusCode, headers, responseString, throwable);
                        callback.onHttpResponse(false, null);
                    }

                    @Override
                    public void onFinish() {
                        super.onFinish();
                        progress.dismiss();
                    }
                });

    }
    public interface ImageCategories {
        void onHttpResponse(Boolean success, List<ImageCategoryModel> models);
    }
}
