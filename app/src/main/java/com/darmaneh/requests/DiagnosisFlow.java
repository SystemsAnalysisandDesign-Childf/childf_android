package com.darmaneh.requests;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.darmaneh.ava.App;
import com.darmaneh.ava.BuildConfig;
import com.darmaneh.ava.diagnosis.DiagnosisResults;
import com.darmaneh.ava.diagnosis.EnoughQuestion;
import com.darmaneh.ava.diagnosis.GroupMultipleQuestion;
import com.darmaneh.ava.diagnosis.GroupSingleQuestion;
import com.darmaneh.ava.diagnosis.SingleQuestion;
import com.darmaneh.dialogs.DarmanehProgressDialog;
import com.darmaneh.dialogs.DarmanehToast;
import com.darmaneh.dialogs.NoInternetDialog;
import com.darmaneh.models.diagnosis.Response;
import com.darmaneh.utilities.Analytics;
import com.darmaneh.utilities.Storage;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;

/**
 * Created by pourya on 3/19/17.
 */

public class DiagnosisFlow {
    private final static String TAG = DiagnosisFlow.class.getSimpleName();
    private static DarmanehProgressDialog progress;
    private static int ENOUGH_QUESTION_STEP_NUMBER = 13;

    public static void patient_diagnosis(final Context context, final boolean closeLauncher) {
        if (!Storage.getIsNet()) {
            (new NoInternetDialog()).show(context);
            return;
        }

        progress = new DarmanehProgressDialog(context);
        progress.show();

        AsyncHttpClient client = Template.configureClient();
        // send interview id for Pawel :)
        client.addHeader("interview-id", App.diagnosis_req.getInterviewId());
        client.addHeader("app-version", Integer.toString(BuildConfig.VERSION_CODE));

        StringEntity entity;
        try {
            entity = new StringEntity((new Gson()).toJson(App.diagnosis_req));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return;
        }

        final String failureMessage = "عدم برقراری ارتباط با سرور، لطفاً دوباره تلاش کنید ...";

        client.post(context,
                Variable.SERVER_ADDRESS_V1 + "symptom_checker/diagnosis/",
                entity, "application/json",
                new JsonHttpResponseHandler() {

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        super.onSuccess(statusCode, headers, response);
                        Log.e(TAG, response.toString());
                        if (statusCode == 200) {
                            // add list of evidences into history
                            Response diagnosis = (new Gson()).fromJson(response.toString(), Response.class);

                            // save histories for back functionality
                            App.histories.addHistoryResponse(App.diagnosis_req.getReqNum(), diagnosis);
                            App.histories.addHistoryRequest(App.diagnosis_req.getReqNum(), App.diagnosis_req);

                            String type = null;
                            try {
                                type = diagnosis.getQuestion().getType();
                            } catch (NullPointerException e) {
                                Analytics.sendScreenName("Pawel :)");
                            }

                            Intent intent = new Intent();
                            if (type == null) {
                                intent = new Intent(context, DiagnosisResults.class);
                                intent.putExtra("diagnosis", diagnosis.toString());
                            }
                            else if (App.diagnosis_req.getReqNum() == ENOUGH_QUESTION_STEP_NUMBER) {
                                intent = new Intent(context, EnoughQuestion.class);
                                intent.putExtra("diagnosis", diagnosis.toString());
                                intent.putExtra("direction", 1);
                            }
                            else {
                                switch (type) {
                                    case "single":
                                        intent = new Intent(context, SingleQuestion.class);
                                        break;
                                    case "group_single":
                                        intent = new Intent(context, GroupSingleQuestion.class);
                                        break;
                                    case "group_multiple":
                                        intent = new Intent(context, GroupMultipleQuestion.class);
                                        break;
                                }
                                intent.putExtra("question", diagnosis.getQuestion().toString());

                                // forward direction
                                intent.putExtra("direction", 1);
                            }

                            // prepare for next question
                            App.diagnosis_req.increaseReqNum();

                            context.startActivity(intent);
                            if (closeLauncher)
                                ((AppCompatActivity) context).finish();
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                        DarmanehToast.makeText(context, failureMessage, Toast.LENGTH_LONG);
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                        super.onSuccess(statusCode, headers, response);
                        DarmanehToast.makeText(context, failureMessage, Toast.LENGTH_LONG);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                        DarmanehToast.makeText(context, failureMessage, Toast.LENGTH_LONG);
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, String responseString) {
                        super.onSuccess(statusCode, headers, responseString);
                        DarmanehToast.makeText(context, failureMessage, Toast.LENGTH_LONG);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        super.onFailure(statusCode, headers, responseString, throwable);
                        DarmanehToast.makeText(context, failureMessage, Toast.LENGTH_LONG);
                    }

                    @Override
                    public void onFinish() {
                        super.onFinish();
                        progress.dismiss();
                    }
                });
    }

    public static void patient_diagnosis_backward(final Context context) {
        // pop data from stacks
        int index = App.diagnosis_req.getReqNum() - 2;

        if (index < 1) {
            App.diagnosis_req.decreaseReqNum();
            return;
        }

        App.diagnosis_req = App.histories.getHistoryRequest(index);
        Response diagnosis = App.histories.getHistoryResponse(index);

        String type = diagnosis.getQuestion().getType();

        Intent intent = new Intent();
        if (App.diagnosis_req.getReqNum() == ENOUGH_QUESTION_STEP_NUMBER) {
            intent = new Intent(context, EnoughQuestion.class);
            intent.putExtra("diagnosis", diagnosis.toString());
        }
        else {
            switch (type) {
                case "single":
                    intent = new Intent(context, SingleQuestion.class);
                    break;
                case "group_single":
                    intent = new Intent(context, GroupSingleQuestion.class);
                    break;
                case "group_multiple":
                    intent = new Intent(context, GroupMultipleQuestion.class);
                    break;
            }
            intent.putExtra("question", diagnosis.getQuestion().toString());
        }

        // backward
        intent.putExtra("direction", -1);

        // prepare for next question
        App.diagnosis_req.increaseReqNum();

        context.startActivity(intent);
    }
}
