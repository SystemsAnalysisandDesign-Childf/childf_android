package com.darmaneh.requests;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.darmaneh.ava.ConditionDetailActivity;
import com.darmaneh.dialogs.DarmanehProgressDialog;
import com.darmaneh.dialogs.DarmanehToast;
import com.darmaneh.dialogs.NoInternetDialog;
import com.darmaneh.models.ConditionDetailListModel;
import com.darmaneh.models.ConditionDetailModel;
import com.darmaneh.models.emergency.EmergencyModel;
import com.darmaneh.models.unadopted.UnadoptedListModel;
import com.darmaneh.utilities.Storage;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.UnsupportedEncodingException;
import java.util.List;
import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;

/**
 * Created by pourya on 3/28/17.
 */

public class ConditionDetail {
    private final static String TAG = ConditionDetail.class.getSimpleName();
    private static DarmanehProgressDialog progress;

    public static void get_condition_detail_list(Context context,
                                                 final ConditionDetail.GetConditionDetailList callback){
//        if (!Storage.getIsNet()) {
//            (new NoInternetDialog()).show(context);
//            return;
//        }

        progress = new DarmanehProgressDialog(context);
        progress.show();

        AsyncHttpClient client = Template.configureClient();


        client.addHeader("Accept", "application/json");
        client.addHeader("Content-Type", "application/json");
        client.addHeader("Authorization", Storage.getID().toString());

        client.get(context,
                Variable.SERVER_ADDRESS_V1 + "adoption/unadopteds/all/",
                new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                        super.onSuccess(statusCode, headers, response);
                        callback.onHttpResponse(false, null);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                        callback.onHttpResponse(false, null);
                    }

                    // handle response that isn't json array

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        super.onSuccess(statusCode, headers, response);
                        JSONArray data = null;
                        try {
                            data = response.getJSONArray("data");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        List<UnadoptedListModel> unadoptedListModels =
                                (new Gson()).fromJson(data.toString(),
                                        new TypeToken<List<UnadoptedListModel>>(){}.getType());
                        if (statusCode == 200) {
                            callback.onHttpResponse(true, unadoptedListModels);
                        } else {
                            callback.onHttpResponse(false, null);
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                        callback.onHttpResponse(false, null);
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, String responseString) {
                        super.onSuccess(statusCode, headers, responseString);
                        callback.onHttpResponse(false, null);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        super.onFailure(statusCode, headers, responseString, throwable);
                        callback.onHttpResponse(false, null);
                    }

                    @Override
                    public void onFinish() {
                        super.onFinish();
                        progress.dismiss();
                    }
        });
    }
    public interface GetConditionDetailList {
        void onHttpResponse(Boolean success, List<UnadoptedListModel> models);
    }

    public static void get_condition_detail(final Context context,
                                            Integer id) {


//        if (!Storage.getIsNet()) {
//            (new NoInternetDialog()).show(context);
//            return;
//        }

        progress = new DarmanehProgressDialog(context);
        progress.show();

        AsyncHttpClient client = Template.configureClient();
        client.addHeader("Accept", "application/json");
        client.addHeader("Content-Type", "application/json");
        client.addHeader("Authorization", Storage.getID().toString());
        client.get(context,
                Variable.SERVER_ADDRESS_V1 + "adoption/adopt/" + id.toString() + "/",
                new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        super.onSuccess(statusCode, headers, response);
                        progress.dismiss();
//                        ConditionDetailModel conditionDetailModel = (new Gson()).fromJson(response.toString(),
//                                ConditionDetailModel.class);
                        String detail = response.optString("message", "failure");
                        if (statusCode == 200 && detail.equals("")) {
//                            Intent intent = new Intent(context, ConditionDetailActivity.class);
//                            intent.putExtra("condition_detail", conditionDetailModel.toString());
//                            context.startActivity(intent);
                            DarmanehToast.makeText(context,
                                    "مددجو تحت کفالت شما در آمد.",
                                    Toast.LENGTH_SHORT);
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                        progress.dismiss();
                    }
                });
    }

    public static void bookmark_condition_detail(final Context context, int id,
                                                 final BookmarkConditionDetail callback) {

        if (!Storage.getIsNet()) {
            (new NoInternetDialog()).show(context);
            return;
        }

        progress = new DarmanehProgressDialog(context);
        progress.show();

        AsyncHttpClient client = Template.configureClient();

        JSONObject json = new JSONObject();
        StringEntity entity;
        try {
            json.put("func", "add");
            json.put("id", id);
            entity = new StringEntity(json.toString());
        } catch (JSONException | UnsupportedEncodingException e) {
            e.printStackTrace();
            return;
        }

        client.post(context,
                Variable.SERVER_ADDRESS_V1 + "user/patient/condition/",
                entity, "application/json",
                new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        super.onSuccess(statusCode, headers, response);
                        Log.e(TAG, response.toString());
                        if (statusCode == 200) {
                            callback.onHttpResponse(true);
                            DarmanehToast.makeText(context, "به مطالب منتخب افزوده شد.", Toast.LENGTH_SHORT);
                        } else {
                            callback.onHttpResponse(false);
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                        Log.e(TAG, errorResponse.toString());
                        callback.onHttpResponse(false);
                    }

                    @Override
                    public void onFinish() {
                        super.onFinish();
                        progress.dismiss();
                    }
                });
    }
    public interface BookmarkConditionDetail {
        void onHttpResponse(Boolean success);
    }

    public static void unbookmark_condition_detail(final Context context, int id,
                                                   final UnbookmarkConditionDetail callback) {
        if (!Storage.getIsNet()) {
            (new NoInternetDialog()).show(context);
            return;
        }

        progress = new DarmanehProgressDialog(context);
        progress.show();

        AsyncHttpClient client = new AsyncHttpClient();
        client.setTimeout(Variable.TIME_OUT);
        client.addHeader("Accept", "application/json");
        client.addHeader("Content-Type", "application/json");
        client.addHeader("Authorization", Storage.getToken());

        JSONObject json = new JSONObject();
        StringEntity entity;
        try {
            json.put("func", "remove");
            json.put("id", id);
            entity = new StringEntity(json.toString());
        } catch (JSONException | UnsupportedEncodingException e) {
            e.printStackTrace();
            return;
        }

        client.post(context,
                Variable.SERVER_ADDRESS_V1 + "user/patient/condition/",
                entity, "application/json",
                new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        super.onSuccess(statusCode, headers, response);
                        progress.dismiss();
                        Log.e(TAG, response.toString());
                        if (statusCode == 200) {
                            DarmanehToast.makeText(context, "از مطالب منتخب حذف شد.", Toast.LENGTH_SHORT);
                            callback.onHttpResponse(true);
                        } else {
                            callback.onHttpResponse(false);
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                        progress.dismiss();
                        callback.onHttpResponse(false);
                    }
                });
    }
    public interface UnbookmarkConditionDetail {
        void onHttpResponse(Boolean success);
    }

    public static void get_emergency_list(Context context,
                                                 final ConditionDetail.GetEmergencyList callback){
//        if (!Storage.getIsNet()) {
//            (new NoInternetDialog()).show(context);
//            return;
//        }

        progress = new DarmanehProgressDialog(context);
        progress.show();

        AsyncHttpClient client = Template.configureClient();


        client.addHeader("Accept", "application/json");
        client.addHeader("Content-Type", "application/json");
        client.addHeader("Authorization", Storage.getID().toString());

        client.get(context,
                Variable.SERVER_ADDRESS_V1 + "requirement/all_emergency/",
                new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                        super.onSuccess(statusCode, headers, response);
                        callback.onHttpResponse(false, null);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                        callback.onHttpResponse(false, null);
                    }

                    // handle response that isn't json array

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        super.onSuccess(statusCode, headers, response);
                        JSONArray data = null;
                        try {
                            data = response.getJSONArray("data");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        List<EmergencyModel> emergencyModels =
                                (new Gson()).fromJson(data.toString(),
                                        new TypeToken<List<EmergencyModel>>(){}.getType());
                        if (statusCode == 200) {
                            callback.onHttpResponse(true, emergencyModels);
                        } else {
                            callback.onHttpResponse(false, null);
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                        callback.onHttpResponse(false, null);
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, String responseString) {
                        super.onSuccess(statusCode, headers, responseString);
                        callback.onHttpResponse(false, null);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        super.onFailure(statusCode, headers, responseString, throwable);
                        callback.onHttpResponse(false, null);
                    }

                    @Override
                    public void onFinish() {
                        super.onFinish();
                        progress.dismiss();
                    }
                });
    }
    public interface GetEmergencyList {
        void onHttpResponse(Boolean success, List<EmergencyModel> models);
    }

    public static void pay(final Context context,
                           int senderId,
                           int receiverId,
                           int requirementId,
                           int value,
                           final ConditionDetail.GetPayment callback){

//        if (!Storage.getIsNet()) {
//            (new NoInternetDialog()).show(context);
//            return;
//        }

        progress = new DarmanehProgressDialog(context);
        progress.show();

        AsyncHttpClient client = Template.configureClient();
        client.addHeader("Accept", "application/json");
        client.addHeader("Content-Type", "application/json");
        client.addHeader("Authorization", Storage.getToken());

        JSONObject json = new JSONObject();
        StringEntity entity;
        try {
            json.put("sender_id", senderId);
            json.put("receiver_id", receiverId);
            json.put("value", value);
            json.put("requirement_id", requirementId);
            entity = new StringEntity(json.toString());
        } catch (JSONException | UnsupportedEncodingException e) {
            e.printStackTrace();
            return;
        }

        client.post(context,
                Variable.SERVER_ADDRESS_V1 + "transaction/do/",
                entity, "application/json",
                new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        super.onSuccess(statusCode, headers, response);
                        Log.e(TAG, response.toString());
//                        if (statusCode == 200) {
                        callback.onHttpResponse(true);
                        DarmanehToast.makeText(context, "پرداخت صورت گرفت.", Toast.LENGTH_SHORT);
//                        } else {
//                            callback.onHttpResponse(false);
//                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                        Log.e(TAG, errorResponse.toString());
                        callback.onHttpResponse(false);
                    }

                    @Override
                    public void onFinish() {
                        super.onFinish();
                        progress.dismiss();
                    }
                });
    }
    public interface GetPayment {
        void onHttpResponse(Boolean success);
    }
}
