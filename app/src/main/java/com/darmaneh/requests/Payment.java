package com.darmaneh.requests;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.darmaneh.dialogs.DarmanehProgressDialog;
import com.darmaneh.dialogs.DarmanehToast;
import com.darmaneh.dialogs.NoInternetDialog;
import com.darmaneh.models.call.CouponModel;
import com.darmaneh.utilities.Storage;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.ContentType;
import cz.msebera.android.httpclient.entity.StringEntity;

/**
 * Created by pourya on 7/17/17.
 */

public class Payment {
    private final static String TAG = Payment.class.getSimpleName();
    static DarmanehProgressDialog progress;

    public static void set_dr_and_authority(final Context context,
                                            int id, final String authority,
                                            String code, int value,
                                            final Payment.SetDrAndAuthority callback) {

        AsyncHttpClient client = Template.configureClient();

        StringEntity entity;
        try {
            JSONObject json = new JSONObject();
            json.put("physician", id);
            json.put("authority", authority);
            json.put("code", code);
            json.put("pay", value);
            entity = new StringEntity(json.toString(), ContentType.APPLICATION_JSON);
        } catch (JSONException e) {
            e.printStackTrace();
            return;
        }

        client.put(context,
                Variable.SERVER_ADDRESS_V1 + "user/patient/set_dr_visit/",
                entity, "application/json",
                new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        super.onSuccess(statusCode, headers, response);
                        if (statusCode == 200) {
                                callback.onHttpResponse(true);
                        } else
                            DarmanehToast.makeText(context, Template.failureMessage, Toast.LENGTH_LONG);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                        DarmanehToast.makeText(context, Template.failureMessage, Toast.LENGTH_LONG);
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                        super.onSuccess(statusCode, headers, response);
                        DarmanehToast.makeText(context, Template.failureMessage, Toast.LENGTH_LONG);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                        DarmanehToast.makeText(context, Template.failureMessage, Toast.LENGTH_LONG);
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, String responseString) {
                        super.onSuccess(statusCode, headers, responseString);
                        DarmanehToast.makeText(context, Template.failureMessage, Toast.LENGTH_LONG);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        super.onFailure(statusCode, headers, responseString, throwable);
                        DarmanehToast.makeText(context, Template.failureMessage, Toast.LENGTH_LONG);
                    }

                    @Override
                    public void onFinish() {
                        super.onFinish();
                    }
                });
    }
    public interface SetDrAndAuthority {
        void onHttpResponse(Boolean success);
    }

    public static void check_coupon_validity(final Context context,
                                            final String code,
                                            final Payment.CheckCouponValidity callback) {

        if (!Storage.getIsNet()) {
            (new NoInternetDialog()).show(context);
            return;
        }

        progress = new DarmanehProgressDialog(context);
        progress.show();

        AsyncHttpClient client = Template.configureClient();

        StringEntity entity;
        try {
            JSONObject json = new JSONObject();
            json.put("code", code);
            entity = new StringEntity(json.toString(), ContentType.APPLICATION_JSON);
        } catch (JSONException e) {
            e.printStackTrace();
            return;
        }

        client.post(context,
                Variable.SERVER_ADDRESS_V1 + "coupon/check_validity/",
                entity, "application/json",
                new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        super.onSuccess(statusCode, headers, response);
                        CouponModel couponInfo =
                                (new Gson()).fromJson(response.toString(),
                                        new TypeToken<CouponModel>() {
                                        }.getType());
                        if (statusCode == 200) {
                            callback.onHttpResponse(true ,couponInfo);
                        } else
                            callback.onHttpResponse(false ,null);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                        CouponModel couponInfo =
                                (new Gson()).fromJson(errorResponse.toString(),
                                        new TypeToken<CouponModel>() {
                                        }.getType());
                        switch (statusCode){
                            case 401:
                                callback.on401(couponInfo);
                                break;
                            case 403:
                                callback.on403(couponInfo);
                                break;
                            case 404:
                                callback.on404(couponInfo);
                                break;
                            default:
                                callback.onHttpResponse(false, null);
                        }
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                        super.onSuccess(statusCode, headers, response);
                        callback.onHttpResponse(false ,null);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                        callback.onHttpResponse(false ,null);
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, String responseString) {
                        super.onSuccess(statusCode, headers, responseString);
                        callback.onHttpResponse(false ,null);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        super.onFailure(statusCode, headers, responseString, throwable);
                        callback.onHttpResponse(false ,null);
                    }

                    @Override
                    public void onFinish() {
                        super.onFinish();
                        progress.dismiss();
                    }
                });
    }
    public interface CheckCouponValidity {
        void onHttpResponse(Boolean success, CouponModel couponInfo);
        void on404(CouponModel couponInfo);
        void on403(CouponModel couponInfo);
        void on401(CouponModel couponInfo);
    }
}
