package com.darmaneh.requests;

import com.darmaneh.ava.BuildConfig;

/**
 * Created by pourya on 3/19/17.
 */

public class Variable {

    private static String MAIN_HOST = "http://46.101.212.235:8007/";

    private static String BASE = MAIN_HOST;

    final public static String SERVER_ADDRESS_V1 = BASE;
    final public static String SERVER_ADDRESS_V2 = BASE;
    final public static String SERVER_ADDRESS_V3 = BASE;
    public static final String CONDITION_BASE_URL = MAIN_HOST + "content_pool/condition_details/";

    public final static Integer TIME_OUT = 5000;

    public final static String PACKAGE_NAME = "com.darmaneh.ava";

    public static String getDownloadLink() {
        return BuildConfig.SHARE_URL;
    }

    public final static int MAX_REQ_NO = 31;
}
