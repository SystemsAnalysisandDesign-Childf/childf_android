package com.darmaneh.requests;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.darmaneh.ava.SymptomCheckerHistoryActivity;
import com.darmaneh.dialogs.DarmanehProgressDialog;
import com.darmaneh.dialogs.DarmanehToast;
import com.darmaneh.dialogs.NoInternetDialog;
import com.darmaneh.models.sc_history.SCHistoryDetailModel;
import com.darmaneh.utilities.SignInFunction;
import com.darmaneh.utilities.Storage;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.ContentType;
import cz.msebera.android.httpclient.entity.StringEntity;

/**
 * Created by pourya on 3/29/17.
 */

public class SymptomCheckerHistory {
    private final static String TAG = SymptomCheckerHistory.class.getSimpleName();
    private static DarmanehProgressDialog progress;

    public static void get_sc_history_detail(final Context context,
                                             String url) {

        if (!Storage.getIsNet()) {
            (new NoInternetDialog()).show(context);
            return;
        }

        progress = new DarmanehProgressDialog(context);
        progress.show();

        AsyncHttpClient client = new AsyncHttpClient();
        client.setTimeout(Variable.TIME_OUT);
        client.addHeader("Accept", "application/json");
        client.addHeader("Content-Type", "application/json");
        client.addHeader("Authorization", Storage.getToken());

        client.get(context, url,
                new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        super.onSuccess(statusCode, headers, response);
                        progress.dismiss();
                        Log.e("test", response.toString());
                        SCHistoryDetailModel scHistoryDetailModel = (new Gson()).fromJson(response.toString(),
                                SCHistoryDetailModel.class);
                        if (statusCode == 200) {
                            Intent intent = new Intent(context, SymptomCheckerHistoryActivity.class);
                            intent.putExtra("sc_history_detail", scHistoryDetailModel.toString());
                            context.startActivity(intent);
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                        progress.dismiss();
                    }
                });
    }

    public static void patient_add_sc_history(final Context context, String data) {
        if (!Storage.getIsLogin()) {
            SignInFunction.signInDialog(context,"برای اضافه کردن به پرونده پزشکی، ابتدا وارد شوید.");
            return;
        }

        if (!Storage.getIsNet()) {
            (new NoInternetDialog()).show(context);
            return;
        }

        progress = new DarmanehProgressDialog(context);
        progress.show();

        AsyncHttpClient client = Template.configureClient();

        JSONObject json = new JSONObject();
        StringEntity entity;
        try {
            json.put("func", "add");
            json.put("data", data);
            entity = new StringEntity(json.toString(), ContentType.APPLICATION_JSON);
        } catch (JSONException e) {
            e.printStackTrace();
            return;
        }

        client.post(context,
                Variable.SERVER_ADDRESS_V1 + "user/patient/sc_history/",
                entity, "application/json",
                new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        super.onSuccess(statusCode, headers, response);
                        progress.dismiss();
                        Log.e(TAG, response.toString());
                        if (statusCode == 200) {
                            DarmanehToast.makeText(context, "به پرونده‌ پزشکی افزوده شد.", Toast.LENGTH_SHORT);
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                        progress.dismiss();
                    }
                });
    }

    public static void patient_remove_sc_history(final Context context,
                                                 int id,
                                                 final PatientRemoveSymptomCheckerHistory callback) {

        if (!Storage.getIsNet()) {
            (new NoInternetDialog()).show(context);
            return;
        }

        progress = new DarmanehProgressDialog(context);
        progress.show();

        AsyncHttpClient client = new AsyncHttpClient();
        client.setTimeout(Variable.TIME_OUT);
        client.addHeader("Accept", "application/json");
        client.addHeader("Content-Type", "application/json");
        client.addHeader("Authorization", Storage.getToken());

        JSONObject json = new JSONObject();
        StringEntity entity;
        try {
            json.put("func", "remove");
            json.put("id", id);
            entity = new StringEntity(json.toString());
        } catch (JSONException | UnsupportedEncodingException e) {
            e.printStackTrace();
            return;
        }

        client.post(context,
                Variable.SERVER_ADDRESS_V1 + "user/patient/sc_history/",
                entity, "application/json",
                new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        super.onSuccess(statusCode, headers, response);
                        progress.dismiss();
                        Log.e(TAG, response.toString());
                        if (statusCode == 200) {
                            callback.onHttpResponse(true);
                        } else {
                            callback.onHttpResponse(false);
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                        progress.dismiss();
                        callback.onHttpResponse(false);
                    }
                });
    }
    public interface PatientRemoveSymptomCheckerHistory {
        void onHttpResponse(Boolean success);
    }
}
