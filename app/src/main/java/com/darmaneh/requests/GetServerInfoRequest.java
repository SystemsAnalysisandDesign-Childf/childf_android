package com.darmaneh.requests;

/**
 * Created by Sepehr Abdous on 7/22/2017.
 */

import android.content.Context;
import android.util.Log;

import com.darmaneh.dialogs.DarmanehProgressDialog;
import com.darmaneh.dialogs.NoInternetDialog;
import com.darmaneh.models.call.ServerInfoModel;
import com.darmaneh.utilities.Storage;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

import cz.msebera.android.httpclient.Header;
public class GetServerInfoRequest {

    private final static String TAG = GetServerInfoRequest.class.getSimpleName();
    private static DarmanehProgressDialog progress;

    public static void get_server_info(Context context,
                                       final GetServerInfoRequest.GetServerInfo callback,
                                       boolean showProgress) {

        if (!Storage.getIsNet()) {
            (new NoInternetDialog()).show(context);
            return;
        }

        if (showProgress) {
            progress = new DarmanehProgressDialog(context);
            progress.show();
        }

        AsyncHttpClient client = Template.configureClient();
        client.get(context,
                Variable.SERVER_ADDRESS_V1 + "utility/ice_servers/",
                new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        super.onSuccess(statusCode, headers, response);
                        callback.onHttpResponse(false,null);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                        super.onSuccess(statusCode, headers, response);
                        Log.e(TAG, response.toString());
                        List<ServerInfoModel> serverInfoModels =
                                (new Gson()).fromJson(response.toString(),
                                        new TypeToken<List<ServerInfoModel>>() {
                                        }.getType());
                        if (statusCode == 200) {
                            callback.onHttpResponse(true, serverInfoModels);
                        } else {
                            callback.onHttpResponse(false, null);
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                        Log.e("error", errorResponse.toString());
                        callback.onHttpResponse(false, null);

                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, String responseString) {
                        super.onSuccess(statusCode, headers, responseString);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        super.onFailure(statusCode, headers, responseString, throwable);
                    }

                    @Override
                    public void onFinish() {
                        Log.d("token", Storage.getToken());
                        if (progress!=null){
                            progress.dismiss();
                        }
                        super.onFinish();
                    }
                });
    }

    public interface GetServerInfo {
        void onHttpResponse(Boolean success, List<ServerInfoModel> serverInfoModels);
    }
}
