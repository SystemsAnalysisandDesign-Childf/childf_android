package com.darmaneh.requests;

import android.content.Context;

import com.darmaneh.dialogs.DarmanehProgressDialog;
import com.darmaneh.dialogs.NoInternetDialog;
import com.darmaneh.models.call.CategoryListDescText;
import com.darmaneh.models.call.ProfessionListModel;
import com.darmaneh.utilities.Storage;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

import cz.msebera.android.httpclient.Header;


public class GetProfessionRequest {
    private final static String TAG = GetProfessionRequest.class.getSimpleName();

    public static void get_profession_list(Context context,
                                                 final GetProfessionRequest.GetProfessionList callback) {
        if (!Storage.getIsNet()) {
            (new NoInternetDialog()).show(context);
            return;
        }

        AsyncHttpClient client = Template.configureClient();

        client.get(context,
                Variable.SERVER_ADDRESS_V1 + "physician/get_profession/",
                new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                        super.onSuccess(statusCode, headers, response);
                        List<ProfessionListModel> professionListModels =
                                (new Gson()).fromJson(response.toString(),
                                        new TypeToken<List<ProfessionListModel>>() {
                                        }.getType());
                        if (statusCode == 200) {
                            callback.onHttpResponse(true, professionListModels);
                        } else {
                            callback.onHttpResponse(false, null);
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                        callback.onHttpResponse(false, null);
                    }

                    // handle response that isn't json array

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        super.onSuccess(statusCode, headers, response);
                        callback.onHttpResponse(false, null);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                        callback.onHttpResponse(false, null);
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, String responseString) {
                        super.onSuccess(statusCode, headers, responseString);
                        callback.onHttpResponse(false, null);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        super.onFailure(statusCode, headers, responseString, throwable);
                        callback.onHttpResponse(false, null);
                    }

                    @Override
                    public void onFinish() {
                        super.onFinish();
                        callback.stopProgress();
                    }
                });
    }

    public interface GetProfessionList {
        void onHttpResponse(Boolean success, List<ProfessionListModel> models);
        void stopProgress();
    }

    public static void get_desc_text(Context context,
                                     final GetProfessionRequest.GetDescText callback) {

        AsyncHttpClient client = Template.configureClient();

        client.get(context,
                Variable.SERVER_ADDRESS_V1 + "utility/android/visit_instruction/",
                new JsonHttpResponseHandler() {

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        super.onSuccess(statusCode, headers, response);
                        CategoryListDescText text =
                                (new Gson()).fromJson(response.toString(),
                                        CategoryListDescText.class);
                        if (statusCode == 200) {
                            callback.onHttpResponse(true, text);
                        } else {
                            callback.onHttpResponse(false, null);
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                        callback.onHttpResponse(false, null);
                    }

                    // handle response that isn't json array
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                        super.onSuccess(statusCode, headers, response);
                        callback.onHttpResponse(false, null);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                        callback.onHttpResponse(false, null);
                    }




                    @Override
                    public void onSuccess(int statusCode, Header[] headers, String responseString) {
                        super.onSuccess(statusCode, headers, responseString);
                        callback.onHttpResponse(false, null);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        super.onFailure(statusCode, headers, responseString, throwable);
                        callback.onHttpResponse(false, null);
                    }

                    @Override
                    public void onFinish() {
                        super.onFinish();
                    }
                });
    }

    public interface GetDescText {
        void onHttpResponse(Boolean success, CategoryListDescText categoryListDescText);
    }
}
