package com.darmaneh.requests;

import android.content.Context;
import android.util.Log;

import com.darmaneh.dialogs.DarmanehProgressDialog;
import com.darmaneh.dialogs.NoInternetDialog;
import com.darmaneh.models.patient_record.PatientRecordModel;
import com.darmaneh.models.patient_record.PatientRecordRequest;
import com.darmaneh.utilities.Storage;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.ContentType;
import cz.msebera.android.httpclient.entity.StringEntity;

/**
 * Created by pourya on 3/29/17.
 */

public class PatientRecord {
    final static String TAG = PatientRecord.class.getSimpleName();
    private static DarmanehProgressDialog progress;

    public static void get_patient_record(Context context,
                                          final PatientRecord.GetPatientRecord callback) {
        // user must be login
        if (!Storage.getIsLogin())
            return;

        if (!Storage.getIsNet()) {
            (new NoInternetDialog()).show(context);
            return;
        }

        progress = new DarmanehProgressDialog(context);
        progress.show();

        AsyncHttpClient client = new AsyncHttpClient();
        client.setTimeout(Variable.TIME_OUT);
        client.addHeader("Accept", "application/json");
        client.addHeader("Content-Type", "application/json");
        client.addHeader("Authorization", Storage.getToken());

        client.get(context,
                Variable.SERVER_ADDRESS_V1 + "user/patient/info/",
                new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        super.onSuccess(statusCode, headers, response);
                        PatientRecordModel patientRecordModel = (new Gson()).fromJson(response.toString(),
                                PatientRecordModel.class);
                        if (statusCode == 200)
                            callback.onHttpResponse(true, patientRecordModel);
                        else
                            callback.onHttpResponse(false, null);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                        callback.onHttpResponse(false, null);
                    }

                    // handle response that isn't json array

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                        super.onSuccess(statusCode, headers, response);
                        callback.onHttpResponse(false, null);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                        callback.onHttpResponse(false, null);
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, String responseString) {
                        super.onSuccess(statusCode, headers, responseString);
                        callback.onHttpResponse(false, null);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        super.onFailure(statusCode, headers, responseString, throwable);
                        callback.onHttpResponse(false, null);
                    }

                    @Override
                    public void onFinish() {
                        super.onFinish();
                        progress.dismiss();
                    }
                });
    }
    public interface GetPatientRecord {
        void onHttpResponse(Boolean success, PatientRecordModel patientRecordModel);
    }

    public static void edit_patient_record(Context context,
                                           PatientRecordRequest patientRecordRequest,
                                           final PatientRecord.EditPatientRecord callback) {

        if (!Storage.getIsNet()) {
            (new NoInternetDialog()).show(context);
            return;
        }

        progress = new DarmanehProgressDialog(context);
        progress.show();

        AsyncHttpClient client = new AsyncHttpClient();
        client.setTimeout(Variable.TIME_OUT);
        client.addHeader("Accept", "application/json");
        client.addHeader("Content-Type", "application/json");
        client.addHeader("Authorization", Storage.getToken());

        StringEntity entity;
        entity = new StringEntity(patientRecordRequest.toString(), ContentType.APPLICATION_JSON);

        client.put(context,
                Variable.SERVER_ADDRESS_V1 + "user/patient/info/",
                entity, "application/json",
                new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        super.onSuccess(statusCode, headers, response);
                        Log.e("test", response.toString());
                        progress.dismiss();
                        PatientRecordModel patientRecordModel = (new Gson()).fromJson(response.toString(),
                                PatientRecordModel.class);
                        if (statusCode == 200)
                            callback.onHttpResponse(true, patientRecordModel);
                        else
                            callback.onHttpResponse(false, null);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                        progress.dismiss();
                        callback.onHttpResponse(false, null);
                    }
                });
    }
    public interface EditPatientRecord {
        void onHttpResponse(Boolean success, PatientRecordModel patientRecordModel);
    }
}
