package com.darmaneh.requests;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.darmaneh.ava.call.AudioVideoCheck;
import com.darmaneh.ava.call.DoctorDetail;
import com.darmaneh.ava.call.PaymentPage;
import com.darmaneh.dialogs.DarmanehProgressDialog;
import com.darmaneh.dialogs.NoInternetDialog;
import com.darmaneh.models.call.DoctorInfoDetailModel;
import com.darmaneh.utilities.Storage;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;


public class DoctorDetailRequest {
    final static String TAG = DoctorDetailRequest.class.getSimpleName();
    private static DarmanehProgressDialog progress;

    public static void get_doctor_detail(final Context context,
                                         final String url,
                                         final int activityChooser) {

        if (!Storage.getIsNet()) {
            (new NoInternetDialog()).show(context);
            return;
        }

        progress = new DarmanehProgressDialog(context);
        progress.show();

        AsyncHttpClient client = Template.configureClient();

        client.get(context,
                url,
                new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                        super.onSuccess(statusCode, headers, response);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        super.onSuccess(statusCode, headers, response);

                        DoctorInfoDetailModel doctorDetail = (new Gson()).fromJson(response.toString(),
                                new TypeToken<DoctorInfoDetailModel>(){}.getType());
                        if (statusCode == 200) {
                            Intent intent = null;
                            switch (activityChooser){
                                case 0:
                                    intent = new Intent(context, DoctorDetail.class);
                                    break;
                                case 1://// TODO: 8/10/17 change to tester page
                                    intent = new Intent(context, PaymentPage.class);
                                    break;
                            }
                            String stringDetail = doctorDetail.toString();
                            intent.putExtra("personalInfo", stringDetail);
                            context.startActivity(intent);
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, String responseString) {
                        super.onSuccess(statusCode, headers, responseString);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        super.onFailure(statusCode, headers, responseString, throwable);
                    }

                    @Override
                    public void onFinish() {
                        super.onFinish();
                        progress.dismiss();
                    }
                });
    }
}
