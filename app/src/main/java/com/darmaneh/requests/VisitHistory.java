package com.darmaneh.requests;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.darmaneh.ava.SymptomCheckerHistoryActivity;
import com.darmaneh.ava.VisitHistoryActivity;
import com.darmaneh.dialogs.DarmanehProgressDialog;
import com.darmaneh.dialogs.NoInternetDialog;
import com.darmaneh.models.sc_history.SCHistoryDetailModel;
import com.darmaneh.models.sc_history.VisitHistoryModel;
import com.darmaneh.utilities.Storage;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

/**
 * Created by alireza on 7/20/17.
 */

public class VisitHistory {
    private static DarmanehProgressDialog progress;

    public static void get_visit_history_detail(final Context context, String url){
        if (!Storage.getIsNet()) {
            (new NoInternetDialog()).show(context);
            return;
        }

        progress = new DarmanehProgressDialog(context);
        progress.show();

        AsyncHttpClient client = new AsyncHttpClient();
        client.setTimeout(Variable.TIME_OUT);
        client.addHeader("Accept", "application/json");
        client.addHeader("Content-Type", "application/json");
        client.addHeader("Authorization", Storage.getToken());
        client.get(context,url,
                new JsonHttpResponseHandler(){
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        super.onSuccess(statusCode, headers, response);
                        progress.dismiss();
                        Log.e("test", response.toString());
                        VisitHistoryModel visitHistoryModel = (new Gson()).fromJson(response.toString(),
                                VisitHistoryModel.class);
                        if (statusCode == 200) {
                            Intent intent = new Intent(context, VisitHistoryActivity.class);
                            intent.putExtra("visit_history_detail", visitHistoryModel.toString());
                            context.startActivity(intent);
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                        progress.dismiss();
                    }
                });
    }
}
