package com.darmaneh.requests;

import android.content.Context;

import com.darmaneh.dialogs.DarmanehProgressDialog;
import com.darmaneh.dialogs.NoInternetDialog;
import com.darmaneh.utilities.Storage;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;


/**
 * Created by root on 7/31/17.
 */

public class CallInternetCutRequest {
    private final static String TAG = CallInternetCutRequest.class.getSimpleName();
    private static DarmanehProgressDialog progress;

    public static void get_call_internet_cut_info(Context context,
                                       final CallInternetCutRequest.GetInternetCutInfo callback) {

        if (!Storage.getIsNet()) {
            (new NoInternetDialog()).show(context);
            return;
        }

        progress = new DarmanehProgressDialog(context);
        progress.show();

        AsyncHttpClient client = Template.configureClient();
        client.get(context,
                Variable.SERVER_ADDRESS_V1 + "utility/android/force_update/",
                new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        super.onSuccess(statusCode, headers, response);
                        if (statusCode == 200)
                            callback.onHttpResponse(true);
                        else
                            callback.onHttpResponse(false);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                        super.onSuccess(statusCode, headers, response);
                        if (statusCode == 200)
                            callback.onHttpResponse(true);
                        else
                            callback.onHttpResponse(false);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                        callback.onHttpResponse(false);

                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, String responseString) {
                        super.onSuccess(statusCode, headers, responseString);
                        if (statusCode == 200)
                            callback.onHttpResponse(true);
                        else
                            callback.onHttpResponse(false);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        super.onFailure(statusCode, headers, responseString, throwable);
                        callback.onHttpResponse(false);
                    }

                    @Override
                    public void onFinish() {
                        progress.dismiss();
                        super.onFinish();
                    }
                });
    }

    public interface GetInternetCutInfo {
        void onHttpResponse(Boolean success);
    }
}
