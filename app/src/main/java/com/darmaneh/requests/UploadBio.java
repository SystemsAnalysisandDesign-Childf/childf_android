package com.darmaneh.requests;

import android.content.Context;
import android.util.Log;

import com.darmaneh.dialogs.DarmanehProgressDialog;
import com.darmaneh.dialogs.NoInternetDialog;
import com.darmaneh.utilities.Storage;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.entity.ContentType;
import cz.msebera.android.httpclient.entity.StringEntity;
import cz.msebera.android.httpclient.Header;

/**
 * Created by Sepehr Abdous on 7/8/2017.
 */

public class UploadBio {
    private final static String TAG = UploadBio.class.getSimpleName();
    private static DarmanehProgressDialog progress;

    public static void post_bio_text(Context context,
                                   final String biography ,
                                   final BiographyUploaded callback) {

        if (!Storage.getIsNet()) {
            (new NoInternetDialog()).show(context);
            return;
        }

        progress = new DarmanehProgressDialog(context);
        progress.show();

        AsyncHttpClient client = Template.configureClient();

        JSONObject json = new JSONObject();
        StringEntity entity;
        try {
            json.put("biography", biography);
            entity = new StringEntity(json.toString(), ContentType.APPLICATION_JSON);
        } catch (JSONException e) {
            return;
        }

        client.post(context,
                Variable.SERVER_ADDRESS_V1 + "user/patient/setup_visit/",
                entity, "application/json",
                new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        super.onSuccess(statusCode, headers, response);
                        progress.dismiss();
                        Log.e(TAG, response.toString());
                        String detail = response.optString("detail", " ");
                        if (statusCode == 201)
                            callback.onHttpResponse(true);
                        else
                            callback.onHttpResponse(false);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                        progress.dismiss();
                        callback.onHttpResponse(false);
                    }
                });
    }
    public interface BiographyUploaded {
        void onHttpResponse(Boolean success);
    }
}
