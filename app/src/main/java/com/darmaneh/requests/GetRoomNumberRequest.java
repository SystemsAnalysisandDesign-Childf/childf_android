package com.darmaneh.requests;

import android.content.Context;
import android.util.Log;

import com.darmaneh.dialogs.DarmanehProgressDialog;
import com.darmaneh.dialogs.NoInternetDialog;
import com.darmaneh.models.call.ProfessionListModel;
import com.darmaneh.utilities.Storage;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

import cz.msebera.android.httpclient.Header;
public class GetRoomNumberRequest {

    private final static String TAG = GetRoomNumberRequest.class.getSimpleName();
    private static DarmanehProgressDialog progress;

    public static void get_room_number(Context context,
                                           final GetRoomNumberRequest.GetRoomNumber callback,
                                            final boolean showProgress,
                                       final boolean checkNoInternet) {


        if (checkNoInternet && !Storage.getIsNet()) {
            (new NoInternetDialog()).show(context);
            return;
        }

        if (showProgress) {
            progress = new DarmanehProgressDialog(context);
            progress.show();
        }

        AsyncHttpClient client = Template.configureClient();
        client.get(context,
                Variable.SERVER_ADDRESS_V1 + "user/patient/get_room_number/",
                new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        super.onSuccess(statusCode, headers, response);
                        Log.e(TAG, response.toString());
                        if (progress!=null)
                            progress.dismiss();
                        if (statusCode == 200) {
                            callback.onHttpResponse(true, response);
                        } else {
                            callback.onHttpResponse(false, response);
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                        callback.onHttpResponse(false, null);
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                        super.onSuccess(statusCode, headers, response);
                        callback.onHttpResponse(false, null);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                        if (errorResponse!=null)
                            Log.e("error", errorResponse.toString());
                        callback.onHttpResponse(false, errorResponse);

                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, String responseString) {
                        super.onSuccess(statusCode, headers, responseString);
                        callback.onHttpResponse(false, null);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        super.onFailure(statusCode, headers, responseString, throwable);
                        callback.onHttpResponse(false, null);
                    }

                    @Override
                    public void onFinish() {
                        Log.d("token", Storage.getToken());
                        super.onFinish();
                        callback.stopProgress(progress);
                    }
                });
    }

    public interface GetRoomNumber {
        void onHttpResponse(Boolean success, JSONObject response);
        void stopProgress(DarmanehProgressDialog progress);
    }
}

