package com.darmaneh.requests;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

import com.darmaneh.ava.App;
import com.darmaneh.ava.BuildConfig;
import com.darmaneh.ava.R;
import com.darmaneh.dialogs.DarmanehProgressDialog;
import com.darmaneh.dialogs.NoInternetDialog;
import com.darmaneh.dialogs.SweetAlertDialog;
import com.darmaneh.utilities.Storage;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.ContentType;
import cz.msebera.android.httpclient.entity.StringEntity;

/**
 * Created by pourya on 3/29/17.
 */

public class Utility {
    final static String TAG = Utility.class.getSimpleName();
    private static DarmanehProgressDialog progress;

    public static void send_email(Context context,
                                  String origin, String subject, String content,
                                  final Utility.SendEmail callback) {

        if (!Storage.getIsNet()) {
            (new NoInternetDialog()).show(context);
            return;
        }

        progress = new DarmanehProgressDialog(context);
        progress.show();

        AsyncHttpClient client = Template.configureClient();

        JSONObject json = new JSONObject();
        try {
            json.put("origin", origin);
            json.put("subject", subject);
            json.put("content", content);
        } catch (JSONException  e) {
            e.printStackTrace();
            return;
        }

        StringEntity entity = new StringEntity(json.toString(), ContentType.APPLICATION_JSON);

        client.post(context,
                Variable.SERVER_ADDRESS_V1 + "utility/email/",
                entity, "application/json",
                new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        super.onSuccess(statusCode, headers, response);
                        progress.dismiss();
                        Log.e("test", response.toString());
                        if (statusCode == 201)
                            callback.onHttpResponse(true);
                        else
                            callback.onHttpResponse(false);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                        progress.dismiss();
                        callback.onHttpResponse(false);
                    }
                });
    }
    public interface SendEmail {
        void onHttpResponse(Boolean success);
    }

    public static void check_forceupdate(final Context context , final ForceUpdateCallback callback) {


        int versionCode = BuildConfig.VERSION_CODE;

        AsyncHttpClient client = Template.configureClient();
        client.setTimeout(Variable.TIME_OUT);

        client.get(context,
                Variable.SERVER_ADDRESS_V1 + "utility/android/force_update/?version=" + String.valueOf(versionCode),
                new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        super.onSuccess(statusCode, headers, response);
                        Log.e("test", response.toString());
                        if (statusCode == 200) {
                            if (response.optBoolean("force_update", false)) {
                                callback.onHttpResponse(true);
                            }else{
                                callback.onHttpResponse(false);
                            }
                        }else{
                            callback.onHttpResponse(false);
                        }
                    }
                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                        callback.onHttpResponse(false);
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                        super.onSuccess(statusCode, headers, response);
                        callback.onHttpResponse(false);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                        callback.onHttpResponse(false);
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, String responseString) {
                        super.onSuccess(statusCode, headers, responseString);
                        callback.onHttpResponse(false);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        super.onFailure(statusCode, headers, responseString, throwable);
                        callback.onHttpResponse(false);
                    }
                });
    }

    public interface ForceUpdateCallback{
        void onHttpResponse(Boolean force_update );
    }

    public static void forceUpdateDialog(final Context context) {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                switch (i){
                    case DialogInterface.BUTTON_POSITIVE:
                        try {
                            Intent intent = new Intent(Intent.ACTION_VIEW);
                            intent.setData(Uri.parse("bazaar://details?id=" + Variable.PACKAGE_NAME));
                            intent.setPackage("com.farsitel.bazaar");
                            context.startActivity(intent);
                        } catch (ActivityNotFoundException e) {
                            // TODO: redirect to our site
                            Intent intent = new Intent(Intent.ACTION_VIEW);
                            intent.setData(Uri.parse(Variable.getDownloadLink()));
                            context.startActivity(intent);
                        }
                        break;
                    case DialogInterface.BUTTON_NEGATIVE:
                        ((AppCompatActivity) context).finish();
                        break;
                }
            }
        };
        AlertDialog dialog = new AlertDialog.Builder(context)
                .setMessage("نسخه برنامه شما قدیمی است. \nبرای استفاده از درمانه لطفاً برنامه را بروزرسانی کنید.")
                .setPositiveButton("دریافت" , dialogClickListener)
                .setNegativeButton("خروج", dialogClickListener)
                .setCancelable(false)
                .show();
        try {
            ((TextView) dialog.findViewById(android.R.id.message)).setTypeface(App.getFont(3));
            TextView textPositive = ((TextView) dialog.findViewById(android.R.id.button1));
            textPositive.setTypeface(App.getFont(3));
            textPositive.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
            TextView textNegative = (TextView) dialog.findViewById(android.R.id.button2);
            textNegative.setTypeface(App.getFont(3));
            textNegative.setTextColor(ContextCompat.getColor(context ,R.color.colorPrimary));
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    public static void get_about_us(final Context context) {

        if (!Storage.getIsNet()) {
            (new NoInternetDialog()).show(context);
            return;
        }

        progress = new DarmanehProgressDialog(context);
        progress.show();

        AsyncHttpClient client = Template.configureClient();

        client.get(context,
                Variable.SERVER_ADDRESS_V1 + "utility/android/about_us/",
                new JsonHttpResponseHandler() {

                    @Override
                    public void onFinish() {
                        super.onFinish();
                        progress.dismiss();
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        super.onSuccess(statusCode, headers, response);
                        if (statusCode == 200) {
                            String message = response.optString("text");
                            aboutUsDialog(context, message);
                        }
                    }
                });
    }

    private static void aboutUsDialog(final Context context, String message) {
        SweetAlertDialog sd = new SweetAlertDialog(context);
        sd.setContentText(message);
        sd.setTitleText("درمانه");
        sd.setCancelable(true);
        sd.setCanceledOnTouchOutside(true);
        sd.setConfirmText("خُب");
        sd.setConfirmClickListener(null);
        sd.show();
    }

    public static void check_internet_connection(final Context context) {
        AsyncHttpClient client = new AsyncHttpClient();
        client.setTimeout(4500);

        client.get(context,
                Variable.SERVER_ADDRESS_V1 + "utility/android/force_update/",
                new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
//                        Log.e("internet response", responseBody.toString());
                        if (statusCode == 200) {
                            Storage.setIsNet(true);
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                        Storage.setIsNet(false);
                        Log.e("checkNet", "fail");
                    }
                });
    }


}
