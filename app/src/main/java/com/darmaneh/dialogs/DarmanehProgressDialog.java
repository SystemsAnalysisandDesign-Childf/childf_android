package com.darmaneh.dialogs;

import android.content.Context;
import android.support.v7.app.AppCompatDialog;
import android.widget.TextView;

import com.darmaneh.ava.App;
import com.darmaneh.ava.R;
import com.victor.loading.rotate.RotateLoading;

/**
 * Created by pourya on 4/5/17.
 */

public class DarmanehProgressDialog extends AppCompatDialog {

    private RotateLoading rotateLoading;
    private TextView textLoading;

    public DarmanehProgressDialog(Context context) {
        super(context);
        setContentView(R.layout.progress_dialog);

        rotateLoading = (RotateLoading) this.findViewById(R.id.rotate_loading);
        textLoading = (TextView) findViewById(R.id.text_loading);
        textLoading.setTypeface(App.getFont(3));
    }

    @Override
    public void show() {
        super.show();
        rotateLoading.start();
    }

    @Override
    public void dismiss() {
        super.dismiss();
        rotateLoading.stop();
    }

    public void changeText(String insteadText){
        textLoading.setText(insteadText);
    }
}
