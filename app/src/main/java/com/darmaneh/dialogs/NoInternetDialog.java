package com.darmaneh.dialogs;

import android.content.Context;
import android.os.Handler;
import android.util.Log;

import com.darmaneh.requests.Utility;
import com.darmaneh.utilities.Storage;

/**
 * Created by pourya on 4/6/17.
 */

public class NoInternetDialog {

    private SweetAlertDialog sweetAlertDialog;
    private Context context;
    // Create the Handler object (on the main thread by default)
    private Handler handler = new Handler();
    // Define the code block to be executed
    private Runnable runnableCode = new Runnable() {
        @Override
        public void run() {
            Utility.check_internet_connection(context);
            // Repeat this the same runnable code block again another 2 seconds
            if (!Storage.getIsNet()) {
                handler.postDelayed(runnableCode, 5000);
            }
            else {
                sweetAlertDialog.dismiss();
            }
        }
    };

    public void show (Context context) {
        this.context = context;
        sweetAlertDialog = new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE);
        sweetAlertDialog.setTitleText("عدم اتصال به اینترنت ...");
        sweetAlertDialog.setContentText("لطفاً اتصال خود را با اینترنت بررسی کنید.");
        sweetAlertDialog.setConfirmText("خُب");
        sweetAlertDialog.setConfirmClickListener(null);
        sweetAlertDialog.setCancelable(false);
        sweetAlertDialog.show();

        // Start the initial runnable task by posting through the handler
        handler.post(runnableCode);
    }
}
