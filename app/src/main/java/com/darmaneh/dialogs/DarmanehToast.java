package com.darmaneh.dialogs;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.darmaneh.ava.App;
import com.darmaneh.ava.R;

/**
 * Created by pourya on 4/6/17.
 */

public class DarmanehToast {
    public static void makeText(Context context, String content, int LENGTH) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View customToastRoot = inflater.inflate(R.layout.dialog_toast, null);
        TextView text = (TextView) customToastRoot.findViewById(R.id.text);
        text.setTypeface(App.getFont(3));
        text.setText(content);

        Toast customToast = new Toast(context);
        customToast.setView(customToastRoot);
        customToast.setGravity(Gravity.CENTER, 0, 0);
        customToast.setDuration(LENGTH);
        customToast.show();
    }
}
