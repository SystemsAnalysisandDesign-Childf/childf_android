package com.darmaneh.service;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;

import com.darmaneh.ava.App;
import com.darmaneh.ava.MainActivity;
import com.darmaneh.ava.R;
import com.darmaneh.ava.call.ChooseCallType;
import com.darmaneh.ava.call.DoctorCalling;
import com.darmaneh.dialogs.DarmanehProgressDialog;
import com.darmaneh.requests.GetRoomNumberRequest;
import com.darmaneh.requests.Template;
import com.darmaneh.requests.Variable;
import com.darmaneh.utilities.Storage;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

public class DoctorOnlineService extends android.app.Service {
    Context context;
    boolean done = false;
    private static String TAG = DoctorOnlineService.class.getSimpleName();

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    //must be checked
    @Override
    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
        Log.d(TAG, "FirstService started");
        context = this;
        customHandler.postDelayed(updateTimerThread, 10000);
    }
    int timeCounter=0;
    Handler customHandler = new Handler();
    public Runnable updateTimerThread = new Runnable()
    {
        public void run()
        {
            timeCounter++;
            GetRoomNumberRequest.get_room_number(context, getRoomNumber, false, false);
            if (timeCounter==6 * Storage.getMaxWaitingTime() * 6)
                stopSelf();
            customHandler.postDelayed(this, 10000);
        }
    };
    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "FirstService destroyed");
    }

    public void func(String roomNumber, String doctorName, String doctorImageUrl){
        Intent intent = new Intent(context, DoctorCalling.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("roomNumber", roomNumber);
        intent.putExtra("doctorName", doctorName);
        intent.putExtra("doctorImageUrl", doctorImageUrl);
        startActivity(intent);
//        Log.e(TAG, "func called");
//        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(DoctorOnlineService.this);
//        mBuilder.setSmallIcon(R.mipmap.ic_launcher);
//        mBuilder.setAutoCancel(true);
//        mBuilder.setContentTitle("پزشک آماده پاسخ گویی است.");
//        mBuilder.setContentText("برای انتقال به صفحه تماس، این اعلان را لمس کنید.");
//        Intent resultIntent = new Intent(this, ChooseCallType.class);
//        resultIntent.putExtra("roomNumber", roomNumber);
//        resultIntent.putExtra("doctorName", doctorName);
//        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
//        stackBuilder.addParentStack(ChooseCallType.class);
//
//        stackBuilder.addNextIntent(resultIntent);
//        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0,PendingIntent.FLAG_UPDATE_CURRENT);
//        mBuilder.setContentIntent(resultPendingIntent);
//
//        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//
//        mNotificationManager.notify(0, mBuilder.build());
    }

    GetRoomNumberRequest.GetRoomNumber getRoomNumber = new GetRoomNumberRequest.GetRoomNumber() {
        @Override
        public void onHttpResponse(Boolean success, JSONObject response) {
            if(success){
                Log.e(TAG, response.toString());
                String roomNumber = response.optString("room_number", null);
                if (roomNumber != null) {
                    String doctorName = response.optString("ph_first_name", "") + " " + response.optString("ph_last_name", "");
                    String doctorImageUrl = response.optString("ph_image");
                    if (!done) {
                        done = true;
                        Log.e("myservice", "service is ran");
                        func(roomNumber, doctorName, doctorImageUrl);
                    }
                    customHandler.removeCallbacks(updateTimerThread);
                    ((DoctorOnlineService) context).stopSelf();
                }
            }else {
                if (response != null) {
                    Log.e("error", response.toString());
                    String detail = response.optString("detail", null);
                    if (detail != null && detail.equals("Not found.")) {
                        stopSelf();
//                        Intent intent = new Intent(context, MainActivity.class);
//                        startActivity(intent);
                    }
                }
            }
        }

        @Override
        public void stopProgress(DarmanehProgressDialog progress) {
            if (progress!=null)
                progress.dismiss();
        }
    };

}