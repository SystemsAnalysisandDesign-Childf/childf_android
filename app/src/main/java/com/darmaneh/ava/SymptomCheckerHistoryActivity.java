package com.darmaneh.ava;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.darmaneh.adapters.SCHistoryDetailAdapter;
import com.darmaneh.models.sc_history.SCHistoryDetailModel;
import com.darmaneh.requests.Utility;
import com.darmaneh.utilities.Analytics;
import com.darmaneh.utilities.Functions;
import com.google.gson.Gson;

public class SymptomCheckerHistoryActivity extends AppCompatActivity {

    SCHistoryDetailModel scHistoryDetailModel;
    RecyclerView scHistoryItems;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_symptom_checher_history);
        initialize();
        Analytics.sendScreenName("EMR/sc_record/detail");
    }

    private void initialize() {

        scHistoryDetailModel = (new Gson()).fromJson(getIntent().getExtras().
                        getString("sc_history_detail"), SCHistoryDetailModel.class);

        TextView reportDate = (TextView) findViewById(R.id.report_date);
        reportDate.setTypeface(App.getFont(4));
        reportDate.setText(Functions.toPersian(scHistoryDetailModel.getCreatedAt()));

        scHistoryItems = (RecyclerView) findViewById(R.id.sc_history_list);
        scHistoryItems.setHasFixedSize(true);

        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        scHistoryItems.setLayoutManager(llm);

        SCHistoryDetailAdapter scHistoryDetailAdapter= new SCHistoryDetailAdapter(scHistoryDetailModel.getData());
        scHistoryItems.setAdapter(scHistoryDetailAdapter);
    }
}
