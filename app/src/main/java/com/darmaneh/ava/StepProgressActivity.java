package com.darmaneh.ava;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.darmaneh.fragments.user_progress.CodeVerifyFragment;
import com.darmaneh.fragments.user_progress.ForgottenPassFragment;
import com.darmaneh.fragments.user_progress.NewPassFragment;
import com.darmaneh.fragments.user_progress.PassVerifyFragment;
import com.darmaneh.fragments.user_progress.UserAgeFragment;
import com.darmaneh.fragments.user_progress.UserHeightFragment;
import com.darmaneh.fragments.user_progress.UserNameFragment;
import com.darmaneh.fragments.user_progress.UserPhoneFragment;
import com.darmaneh.fragments.user_progress.UserSexFragment;
import com.darmaneh.fragments.user_progress.UserWeightFragment;
import com.darmaneh.models.patient_record.PatientRecordModel;
import com.darmaneh.models.patient_record.PatientRecordRequest;
import com.darmaneh.requests.PatientRecord;
import com.darmaneh.requests.Utility;
import com.victor.loading.rotate.RotateLoading;

public class StepProgressActivity extends AppCompatActivity implements UserNameFragment.OnFragmentInteractionListener ,
        UserHeightFragment.OnFragmentInteractionListener , UserWeightFragment.OnFragmentInteractionListener ,
        UserSexFragment.OnFragmentInteractionListener , UserPhoneFragment.OnFragmentInteractionListener ,
        CodeVerifyFragment.OnFragmentInteractionListener, UserAgeFragment.OnFragmentInteractionListener,
        PassVerifyFragment.OnFragmentInteractionListener, ForgottenPassFragment.OnFragmentInteractionListener,
        NewPassFragment.OnFragmentInteractionListener{

    private static final String TAG = StepProgressActivity.class.getSimpleName();
    FrameLayout mainContainer;
    LinearLayout loadingContainer;
    RotateLoading rotateLoading;

    private enum state {
        PHONE , CODE_VERIFY, NAME , WEIGHT , HEIGHT, AGE, SEX, WAIT_CODE, WAIT_INFO, PASS_VERIFY
        , FORGOTTEN_PASS, NEW_PASS
    }
    private state mState = state.PHONE;
    private String userName="";
    private String userLastName="";
    private Integer userHeight = null;
    private Integer userWeight= null;
    private Integer userAge= null;
    private String userSex="";
    private String userCode="";
    private String userPass="";
    Context thisCntx;

    ImageView stepDot;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_step_progress);
        thisCntx = this;
        stepDot = (ImageView) findViewById(R.id.step_dot);
        (findViewById(R.id.back_button)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        mainContainer = (FrameLayout) findViewById(R.id.main_container);
        loadingContainer = (LinearLayout) findViewById(R.id.loading_container);
        ((TextView)findViewById(R.id.wait_text)).setTypeface(App.getFont(3));
        rotateLoading = (RotateLoading) findViewById(R.id.rotate_loading);
        checkUserIdentity();
    }

    private void gotoMainActivity() {
        Intent returnIntent = new Intent();
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
//        Utility.check_forceupdate(this);
    }

    private void checkUserIdentity() {
        mainContainer.setVisibility(View.VISIBLE);
        loadingContainer.setVisibility(View.GONE);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        UserPhoneFragment userPhoneFragment =UserPhoneFragment.newInstance();
        ft.setCustomAnimations(R.anim.slide_in , R.anim.slide_out);
        ft.replace(R.id.main_fragment, userPhoneFragment ).commit();
        mState = state.PHONE;
        changeStepDot();
    }

    @Override
    public void onNameCallback(String name, String lastName) {
        userName = name;
        userLastName = lastName;
        UserHeightFragment userHeightFragment = UserHeightFragment.newInstance(userHeight);
        changeFragment(userHeightFragment , R.anim.slide_in , R.anim.slide_out , state.HEIGHT);
    }

    @Override
    public void onAgeCallback(Integer age) {
        userAge = age;
        UserSexFragment userSexFragment = UserSexFragment.newInstance();
        changeFragment(userSexFragment , R.anim.slide_in , R.anim.slide_out , state.SEX);
    }

    @Override
    public void onHeightCallback(Integer height) {
        userHeight = height;
        UserWeightFragment userWeightFragment = UserWeightFragment.newInstance(userWeight);
        changeFragment(userWeightFragment , R.anim.slide_in , R.anim.slide_out , state.WEIGHT) ;
    }
    @Override
    public void onWeightCallback(Integer weight) {
        userWeight = weight;
        UserAgeFragment userAgeFragment = UserAgeFragment.newInstance(userAge);
        changeFragment(userAgeFragment, R.anim.slide_in, R.anim.slide_out, state.AGE);
    }

    @Override
    public void onSexCallback(String sex) {
        userSex = sex;
        Log.d(TAG , "sex : "+sex);
//        mainContainer.setVisibility(View.GONE);
//        loadingContainer.setVisibility(View.VISIBLE);
//        rotateLoading.start();
        PatientRecordRequest prr = new PatientRecordRequest();
        prr.setFirstName(userName);
        prr.setLastName(userLastName);
        prr.setHeight(userHeight);
        prr.setWeight(userWeight);
        prr.setSex(userSex);
        prr.setBirthYear(userAge);
        prr.setEmail("");
        prr.setCity("");
        prr.setProvince("");
        PatientRecord.edit_patient_record(thisCntx, prr, new PatientRecord.EditPatientRecord() {
            @Override
            public void onHttpResponse(Boolean success, PatientRecordModel patientRecordModel) {
                gotoMainActivity();
            }
        });
    }

    @Override
    public void onPhoneCallback(String phoneNum, Boolean isRegistered) {
        if(isRegistered){
            PassVerifyFragment passVerifyFragment = PassVerifyFragment.newInstance(userPass);
            changeFragment(passVerifyFragment, R.anim.slide_in, R.anim.slide_out, state.PASS_VERIFY);
        }else{
            NewPassFragment newPassFragment = NewPassFragment.newInstance();
            changeFragment(newPassFragment, R.anim.slide_in, R.anim.slide_out, state.NEW_PASS);
        }
    }

    @Override
    public void onCodeCallback(String code) {
//        userCode = code;
//        UserNameFragment userNameFragment = UserNameFragment.newInstance(userName, userLastName);
//        changeFragment(userNameFragment, R.anim.slide_in, R.anim.slide_out, state.NAME);
        gotoMainActivity();
    }
    @Override
    public void onPassVerifyCallback(String pass, boolean forget) {
        if(forget){
            ForgottenPassFragment forgottenPassFragment = ForgottenPassFragment.newInstance();
            changeFragment(forgottenPassFragment, R.anim.slide_in, R.anim.slide_out, state.FORGOTTEN_PASS);
        }else{
            gotoMainActivity();
        }
    }

    @Override
    public void onNewPassCallback(String password) {
        userPass = password;
        CodeVerifyFragment codeVerifyFragment = CodeVerifyFragment.newInstance(password);
        changeFragment(codeVerifyFragment, R.anim.slide_in, R.anim.slide_out, state.CODE_VERIFY);
    }

    @Override
    public void onForgottenPassCallback() {
        gotoMainActivity();
    }

    private void changeFragment(Fragment frag , int enter , int exit , state newState ){
        mState = newState;
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(enter , exit);
        ft.replace(R.id.main_fragment, frag).commit();
        changeStepDot();
    }

    private void changeStepDot() {
        switch (mState){
            case NAME:
                stepDot.setImageResource(R.drawable.page_01);
                break;
            case HEIGHT:
                stepDot.setImageResource(R.drawable.page_02);
                break;
            case WEIGHT:
                stepDot.setImageResource(R.drawable.page_03);
                break;
            case AGE:
                stepDot.setImageResource(R.drawable.page_04);
                break;
            case SEX:
                stepDot.setImageResource(R.drawable.page_05);
                break;
            default:
                stepDot.setImageResource(android.R.color.transparent);
                break;
        }
    }

    @Override
    public void onBackPressed() {
        switch (mState){
            case PHONE:
                Intent returnIntent = new Intent();
                setResult(Activity.RESULT_CANCELED, returnIntent);
                finish();
                break;
            case CODE_VERIFY:
                NewPassFragment newPassFragment = NewPassFragment.newInstance();
                changeFragment(newPassFragment,R.anim.slide_in_back , R.anim.slide_out_back , state.NEW_PASS);
                break;
            case PASS_VERIFY:
                UserPhoneFragment userPhoneFragment = UserPhoneFragment.newInstance();
                changeFragment(userPhoneFragment,R.anim.slide_in_back , R.anim.slide_out_back , state.PHONE);
                break;
            case FORGOTTEN_PASS:
                PassVerifyFragment passVerifyFragment = PassVerifyFragment.newInstance(userPass);
                changeFragment(passVerifyFragment, R.anim.slide_in_back, R.anim.slide_out_back, state.PASS_VERIFY);
                break;
            case NEW_PASS:
                UserPhoneFragment userPhoneFragment2 = UserPhoneFragment.newInstance();
                changeFragment(userPhoneFragment2,R.anim.slide_in_back , R.anim.slide_out_back , state.PHONE);
                break;
            case NAME:
                CodeVerifyFragment codeVerifyFragment = CodeVerifyFragment.newInstance(userCode);
                changeFragment(codeVerifyFragment,R.anim.slide_in_back, R.anim.slide_out_back, state.CODE_VERIFY);
                break;
            case HEIGHT:
                UserNameFragment userNameFragment = UserNameFragment.newInstance(userName , userLastName);
                changeFragment(userNameFragment, R.anim.slide_in_back, R.anim.slide_out_back, state.NAME);
                break;
            case WEIGHT:
                UserHeightFragment userHeightFragment = UserHeightFragment.newInstance(userHeight);
                changeFragment(userHeightFragment, R.anim.slide_in_back, R.anim.slide_out_back, state.HEIGHT);
                break;
            case AGE:
                UserWeightFragment userWeightFragment = UserWeightFragment.newInstance(userWeight);
                changeFragment(userWeightFragment, R.anim.slide_in_back, R.anim.slide_out_back, state.WEIGHT);
                break;
            case SEX:
                UserAgeFragment userAgeFragment = UserAgeFragment.newInstance(userAge);
                changeFragment(userAgeFragment, R.anim.slide_in_back, R.anim.slide_out_back, state.AGE);
                break;
            case WAIT_CODE:
                break;
            case WAIT_INFO:
                break;

            default:
                super.onBackPressed();
                break;
        }
    }
}
