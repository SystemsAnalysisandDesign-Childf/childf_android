package com.darmaneh.ava;

import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.darmaneh.requests.Utility;
import com.darmaneh.utilities.Storage;

public class SplashActivity extends AppCompatActivity {
    private static final int STEP_REQUEST_CODE = 1;
    private RelativeLayout splashLayout;
    private Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        splashLayout = (RelativeLayout) findViewById(R.id.splash_layout);

        if (!Storage.getIsLogin() && App.firstUsage) {
            Intent intent = new Intent(SplashActivity.this, StepProgressActivity.class);
            startActivityForResult(intent,STEP_REQUEST_CODE);
        } else {
            check_visit_access();
        }
    }

    private void check_visit_access() {
        splashLayout.setVisibility(View.VISIBLE);
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        }, 1200);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == STEP_REQUEST_CODE){
            if(resultCode == Activity.RESULT_CANCELED){
                finish();
            }
            else if (resultCode == Activity.RESULT_OK){
                App.firstUsage = false;
                check_visit_access();
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
