package com.darmaneh.ava.health_centers;

import android.content.Intent;
import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.darmaneh.adapters.DoctorProfileAdapter;
import com.darmaneh.ava.App;
import com.darmaneh.ava.R;
import com.darmaneh.fragments.ContactUsDialog;
import com.darmaneh.fragments.RequestFailureDialog;
import com.darmaneh.models.call.DoctorInfoDetailModel;
import com.darmaneh.models.physician.Address;
import com.darmaneh.models.physician.PhysicianModel;
import com.darmaneh.requests.HealthCenters;
import com.darmaneh.requests.Utility;
import com.darmaneh.requests.Variable;
import com.darmaneh.utilities.Analytics;
import com.darmaneh.utilities.Functions;
import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class DoctorProfileActivity extends AppCompatActivity {
    PhysicianModel model;
    String url;
    TextView profession, name;
    LinearLayout shareIcon;

    CircleImageView pic;
    RecyclerView rv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctor_profile);
        initialize();
        init_list();
    }

    private void init_list() {
        HealthCenters.get_doctor_detail(DoctorProfileActivity.this, url, getDoctorDetail);
    }

    private void initialize() {
        Bundle extras = getIntent().getExtras();
        shareIcon = (LinearLayout) findViewById(R.id.share);
        shareIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String shareString = getShareString();
                Log.d("shareString", shareString);
                //Analytics.sendScreenName("share/doctorInfo/" + model.get);
                Intent msg = new Intent(Intent.ACTION_SEND);
                msg.putExtra(Intent.EXTRA_TEXT, shareString);
                msg.setType("text/plain");
                startActivity(Intent.createChooser(msg, "اطلاعات دکتر منتخب را به اشتراک گذارید:"));
            }
        });
        url = extras.getString("url");
        profession = (TextView) findViewById(R.id.doctor_profession);
        pic = (CircleImageView) findViewById(R.id.doctor_pic);
        name = (TextView) findViewById(R.id.doctor_name);
        rv = (RecyclerView) findViewById(R.id.rv);

        profession.setTypeface(App.getFont(3));
        name.setTypeface(App.getFont(4));
        ((TextView) findViewById(R.id.profession_title)).setTypeface(App.getFont(3));
        ((TextView) findViewById(R.id.share_text)).setTypeface(App.getFont(3));
        findViewById(R.id.fab).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String title = "";
                if(model != null)
                title = Functions.toPersian(model.getPk())
                        + ". بازخورد در مورد " + model.getName();
                ContactUsDialog contactUsDialog = ContactUsDialog.newInstance(title);
                contactUsDialog.show(getSupportFragmentManager() , "contact_us");
            }
        });
    }

    private void setContent() {
        name.setText(model.getName());
        String professionStr = "";
        for (String profession : model.getProfession()) {
            professionStr += profession + '\n';
        }
        profession.setText(professionStr);
        if (model.getImage() != null) {
            Picasso.with(DoctorProfileActivity.this).load(model.getImage()).placeholder(R.drawable.doctor_sample_profile)
                    .into(pic);
        } else {
            pic.setImageResource(R.drawable.doctor_sample_profile);
        }

        rv.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(DoctorProfileActivity.this, LinearLayoutManager.VERTICAL, false);
        rv.setLayoutManager(llm);
        DoctorProfileAdapter profileAdapter = new DoctorProfileAdapter(DoctorProfileActivity.this,
                model.getAddresses(), model.getName());
        rv.setAdapter(profileAdapter);
    }

    HealthCenters.GetDoctorDetail getDoctorDetail = new HealthCenters.GetDoctorDetail() {
        @Override
        public void onHttpResponse(Boolean success, PhysicianModel physicianModel) {
            if (success) {
                model = physicianModel;
                setContent();
            } else {
                RequestFailureDialog dialogFragment = new RequestFailureDialog();
                dialogFragment.setRetryFunction(new RequestFailureDialog.RetryOnClickInterface() {
                    @Override
                    public void onClick() {
                        init_list();
                    }
                });
                dialogFragment.show(getSupportFragmentManager(), "RequestFailureDialog");
            }
        }
    };

    public String getNotEmptyString(String base, String info) {
        String infoForSend;
        if (info == null) {
            infoForSend = "";
        } else {
            infoForSend = info;
        }
        return (infoForSend.equals("") ? "" : (base.equals("") ? infoForSend + "\n"  : base + ":\n" + infoForSend + "\n\n"));
    }

    public String getShareString() {



        String msg = getNotEmptyString("", model.getName());
        int numProf = model.getProfession().size();

        for ( int i=0 ; i<numProf;i++){
            msg = msg + getNotEmptyString("" , model.getProfession().get(i));
        }

        int numAdr = model.getAddresses().size();
        for (int i = 0 ; i<numAdr ; i++) {

            msg = msg + getNotEmptyString("آدرس " + Functions.toPersian(i+1),model.getAddresses().get(i).getAddress() )+ " شماره تماس : " +"\n";
            for (int j = 0; j < model.getAddresses().get(i).getContacts().size(); j++) {

                msg= msg + getNotEmptyString( "",  model.getAddresses().get(i).getContacts().get(j));

            }
        }
        msg = msg + "لینک دانلود برنامه:" + "\n" +
                Variable.getDownloadLink();
        return msg;
    }

}
