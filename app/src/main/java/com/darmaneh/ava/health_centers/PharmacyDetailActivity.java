package com.darmaneh.ava.health_centers;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.darmaneh.adapters.PharmacyDetailAdapter;
import com.darmaneh.ava.App;
import com.darmaneh.ava.R;
import com.darmaneh.fragments.ContactUsDialog;
import com.darmaneh.models.health_centers.PharmacyDetailModel;
import com.darmaneh.requests.Utility;
import com.darmaneh.requests.Variable;
import com.darmaneh.utilities.Analytics;
import com.darmaneh.utilities.Functions;
import com.google.gson.Gson;

public class PharmacyDetailActivity extends AppCompatActivity {

    PharmacyDetailModel pharmacyDetailModel;
    String province, city;
    TextView name;
    FloatingActionButton fab;
    RecyclerView recyclerView;
    PharmacyDetailAdapter pharmacyDetailAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pharmacy_detail);
        initialize();
        initViews();
        setViewsFont();
        setContent();
        setOnClick();
        init_rv();
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        Analytics.sendScreenName("locations/" + province + "/" + city + "/pharmacy/" + pharmacyDetailModel.getSpecialName());
    }

    private void setOnClick() {
        findViewById(R.id.back_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String title = Functions.toPersian(pharmacyDetailModel.getPk())
                        + ". بازخورد در مورد داروخانه " + pharmacyDetailModel.getSpecialName();
                ContactUsDialog contactUsDialog = ContactUsDialog.newInstance(title);
                contactUsDialog.show(getSupportFragmentManager() , "contact_us");
            }
        });
        findViewById(R.id.share_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Analytics.sendScreenName("share/pharmacy/" + pharmacyDetailModel.getPk());
                Intent msg = new Intent(Intent.ACTION_SEND);
                msg.putExtra(Intent.EXTRA_TEXT, getShareString());
                msg.setType("text/plain");
                startActivity(Intent.createChooser(msg, "اطلاعات این داروخانه را به اشتراک گذارید:"));
            }
        });
    }

    private void init_rv() {
        recyclerView = (RecyclerView) findViewById(R.id.rv);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(llm);
        pharmacyDetailAdapter = new PharmacyDetailAdapter(this, pharmacyDetailModel);
        recyclerView.setAdapter(pharmacyDetailAdapter);
    }

    private void setContent() {
        name.setText(pharmacyDetailModel.getSpecialName());
    }

    public String getNotEmptyString(String base, String info){
        if(info == null)
            return "";
        return  (info.equals("") ? "" : base + ":\n" + info + "\n\n");
    }

    public String getShareString(){
        String nationalNumber;
        if(pharmacyDetailModel.getNationalNumber()==null)
        {
            nationalNumber = "";
        }
        else{
            nationalNumber=pharmacyDetailModel.getNationalNumber().toString();
        }

        String msg = "اطلاعات داروخانه " +
                pharmacyDetailModel.getSpecialName() + "\n\n" +
                getNotEmptyString("نام عام" , pharmacyDetailModel.getPublicName()) +
                getNotEmptyString( "آدرس" , pharmacyDetailModel.getAddress()) +
                getNotEmptyString("کد ملی" , nationalNumber) +
                getNotEmptyString("نام موسس" , pharmacyDetailModel.getFounder()) +
                getNotEmptyString("دانشگاه" , pharmacyDetailModel.getUniversity());

        msg += "لینک دانلود برنامه:" + "\n" +
                Variable.getDownloadLink();

        return msg;
    }

    private void setViewsFont() {
        name.setTypeface(App.getFont(4));
    }

    private void initViews() {
        name = (TextView) findViewById(R.id.pharmacy_name);
        fab = (FloatingActionButton) findViewById(R.id.fab);
    }

    private void initialize() {
        pharmacyDetailModel = (new Gson()).fromJson(getIntent().getExtras().
                        getString("pharmacy_detail"), PharmacyDetailModel.class);
        province = pharmacyDetailModel.getProvince();
        city = pharmacyDetailModel.getCity();
    }
}
