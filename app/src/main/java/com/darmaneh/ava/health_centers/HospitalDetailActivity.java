package com.darmaneh.ava.health_centers;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.darmaneh.adapters.HospitalDetailAdapter;
import com.darmaneh.ava.App;
import com.darmaneh.ava.R;
import com.darmaneh.fragments.ContactUsDialog;
import com.darmaneh.models.location.Hospital;
import com.darmaneh.requests.Utility;
import com.darmaneh.requests.Variable;
import com.darmaneh.utilities.Analytics;
import com.darmaneh.utilities.Functions;
import com.google.gson.Gson;

public class HospitalDetailActivity extends AppCompatActivity {

    Hospital hospitalDetailModel;
    String province, city;
    TextView name;
    RecyclerView recyclerView;
    HospitalDetailAdapter hospitalDetailAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hospital_detail);
        initialize();
        initViews();
        setViewsFont();
        setContent();
        setOnClick();
        init_rv();
    }

    private void init_rv() {
        recyclerView = (RecyclerView) findViewById(R.id.rv);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(llm);
        hospitalDetailAdapter = new HospitalDetailAdapter(hospitalDetailModel, this);
        recyclerView.setAdapter(hospitalDetailAdapter);
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        Analytics.sendScreenName("locations/" + province + "/" + city + "/hospital/" + hospitalDetailModel.getName());
    }


    private void setOnClick() {
        findViewById(R.id.back_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        findViewById(R.id.fab).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String title = Functions.toPersian(hospitalDetailModel.getPk())
                        + ". بازخورد در مورد بیمارستان " + hospitalDetailModel.getName();
                ContactUsDialog contactUsDialog = ContactUsDialog.newInstance(title);
                contactUsDialog.show(getSupportFragmentManager() , "contact_us");
            }
        });
        findViewById(R.id.share_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String shareString = getShareString();
                Analytics.sendScreenName("share/hospital/" + hospitalDetailModel.getPk());
                Intent msg = new Intent(Intent.ACTION_SEND);
                msg.putExtra(Intent.EXTRA_TEXT, shareString);
                msg.setType("text/plain");
                startActivity(Intent.createChooser(msg, "اطلاعات این بیمارستان را به اشتراک گذارید:"));
            }
        });
    }

    private void setContent() {
        name.setText(hospitalDetailModel.getName());
    }

    public String getNotEmptyString(String base, String info){
        if(info == null)
            return "";
        return  (info.equals("") ? "" : base + ":\n" + info + "\n\n");
    }

    public String getShareString(){
        String msg = "اطلاعات بیمارستان " +
                hospitalDetailModel.getName() + "\n\n" +
                getNotEmptyString("تلفن تماس" , hospitalDetailModel.getTell()) +
                getNotEmptyString( "ایمیل" , hospitalDetailModel.getEmail()) +
                getNotEmptyString("آدرس سایت" , hospitalDetailModel.getSite()) +
                getNotEmptyString("سازمان متبوع" , hospitalDetailModel.getOrganization()) +
                getNotEmptyString("نوع فعالیت" , hospitalDetailModel.getActivity().get(0)) +
                getNotEmptyString("تعداد تخت فعال" , hospitalDetailModel.getBedCount().toString()) +
                getNotEmptyString("نوع تخصص" , hospitalDetailModel.getExpertise()) +
                getNotEmptyString("بخش های بستری" , hospitalDetailModel.getHospitalization()) +
                getNotEmptyString("بخش های ستاره دار" ,hospitalDetailModel.getWithStar()) +
                getNotEmptyString("بخش های پاراکلینیک" , hospitalDetailModel.getParaClinic()) +
                getNotEmptyString("بخش های درمانگاهی" ,hospitalDetailModel.getClinical());
        msg += "لینک دانلود برنامه:" + "\n" +
                Variable.getDownloadLink();

        return msg;
    }


    private void setViewsFont() {
        name.setTypeface(App.getFont(4));
    }

    private void initViews() {
        name = (TextView) findViewById(R.id.hospital_name);
    }

    private void initialize() {
        hospitalDetailModel = (new Gson()).fromJson(getIntent().getExtras().
                getString("hospital_detail"), Hospital.class);
        province = hospitalDetailModel.getProvince();
        city = hospitalDetailModel.getCity();
    }
}
