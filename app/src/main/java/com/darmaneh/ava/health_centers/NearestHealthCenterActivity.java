package com.darmaneh.ava.health_centers;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.darmaneh.adapters.NearestCentersAdapter;
import com.darmaneh.ava.App;
import com.darmaneh.ava.R;
import com.darmaneh.fragments.RequestFailureDialog;
import com.darmaneh.models.health_centers.NearestListModel;
import com.darmaneh.models.location.HealthCentersLocationModel;
import com.darmaneh.requests.HealthCenterLocation;
import com.darmaneh.requests.Utility;
import com.darmaneh.utilities.Analytics;
import com.darmaneh.utilities.Functions;

import java.util.ArrayList;
import java.util.List;

public class NearestHealthCenterActivity extends AppCompatActivity {
    public enum healthCenterType {
        HOSPITAL, PHARMACY,
    }

    RecyclerView recyclerView;
    List<NearestListModel> nearestListModels = new ArrayList<>();
    NearestCentersAdapter adapter;
    String typeStr;
    double lat, lng;
    healthCenterType type;
    TextView  centerName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nearest_health_center);
        initialize();
        init_centers_list();
        setOnClick();
    }

    private void setOnClick() {
        findViewById(R.id.back_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private void init_centers_list(){
        HealthCenterLocation.get_nearest(this, lat, lng, typeStr, lg);
    }


    private void initialize() {
        centerName = (TextView) findViewById(R.id.center_name);
        centerName.setTypeface(App.getFont(4));
        Bundle extras = getIntent().getExtras();
        type = (healthCenterType) extras.get("type");
        lat = extras.getDouble("lat");
        lng = extras.getDouble("lng");
        switch (type){
            case HOSPITAL:
                typeStr="hospital";
                centerName.setText("نزدیکترین بیمارستان ها");
                break;
            case PHARMACY:
                typeStr="pharmacy";
                centerName.setText("نزدیکترین داروخانه ها");
                break;
        }
    }

    private void rvInit(List<NearestListModel> nearestListModels) {
        recyclerView = (RecyclerView) findViewById(R.id.rv);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(llm);
        adapter = new NearestCentersAdapter(this, nearestListModels, type );
        recyclerView.setAdapter(adapter);
    }

    HealthCenterLocation.LocationGot lg = new HealthCenterLocation.LocationGot() {
        @Override
        public void onHttpResponse(Boolean success, List<NearestListModel> healthCenters) {
            if(success){
                nearestListModels = healthCenters;
                rvInit(healthCenters);
            }else{
                RequestFailureDialog dialogFragment = new RequestFailureDialog();
                dialogFragment.setRetryFunction(new RequestFailureDialog.RetryOnClickInterface() {
                    @Override
                    public void onClick() {
                        init_centers_list();
                    }
                });
                dialogFragment.show(getSupportFragmentManager(), "RequestFailureDialog");
            }
        }
    };
}
