package com.darmaneh.ava.health_centers;

import android.animation.LayoutTransition;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.darmaneh.adapters.PhysicianListAdapter;
import com.darmaneh.ava.App;
import com.darmaneh.ava.R;
import com.darmaneh.fragments.RequestFailureDialog;
import com.darmaneh.models.health_centers.PhysicianListModel;
import com.darmaneh.models.physician.PhysicianModel;
import com.darmaneh.requests.HealthCenters;
import com.darmaneh.requests.Utility;
import com.darmaneh.utilities.Analytics;
import com.darmaneh.utilities.Functions;

import java.util.ArrayList;
import java.util.List;

public class DoctorListActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {

    String city,province,category;
    TextView provinceCity, titleText;
    List<PhysicianListModel> physicianList, basePhysicianList;
    RecyclerView recyclerView;
    TextView noResult;
    SearchView searchDoctor;
    PhysicianListAdapter physicianListAdapter;
    RelativeLayout.LayoutParams lp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hospital_list);
        initialize();
        init_list();
        init_doctor_search();
        setOnClick();
    }

    private void init_doctor_search() {
        searchDoctor = (SearchView) findViewById(R.id.search_hospital);
        noResult     = (TextView)  findViewById(R.id.no_result_found);
        noResult.setTypeface(App.getFont(4));
        searchDoctor.setIconifiedByDefault(true);
        lp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT);
        searchDoctor.setLayoutParams(lp);
    }

    private void init_list() {
        HealthCenters.get_physician_list(this,province,city,category,getPhysicianList);
    }

    @Override
    public void onBackPressed()
    {
        if (!searchDoctor.isIconified()){
            searchDoctor.setIconified(true);
        }else{
            super.onBackPressed();
        }
    }

    private void setOnClick() {
        findViewById(R.id.back_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        searchDoctor.setOnSearchClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,
                        RelativeLayout.LayoutParams.WRAP_CONTENT);
                searchDoctor.setLayoutParams(lp);
                searchDoctor.setBackgroundResource(R.drawable.search_bg_gray);
            }
        });

        searchDoctor.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                lp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
                        RelativeLayout.LayoutParams.WRAP_CONTENT);
                searchDoctor.setLayoutParams(lp);
                noResult.setVisibility(View.INVISIBLE);
                searchDoctor.setBackgroundResource(R.drawable.search_bg_white);
                return false;
            }
        });
    }

    private void initialize() {
        Bundle extras = getIntent().getExtras();
        province = extras.getString("province");
        city = extras.getString("city");
        category = extras.getString("category");
        provinceCity = (TextView) findViewById(R.id.province_city);
        titleText = (TextView) findViewById(R.id.hospital_name);
        titleText.setTypeface(App.getFont(4));
        titleText.setText("متخصص "+ category);
        String provinceCityStr = province + "-" + city;
        provinceCity.setText(provinceCityStr);
        provinceCity.setTypeface(App.getFont(3));

        physicianList = new ArrayList<>();
        basePhysicianList = new ArrayList<>();
    }

    private void init_rv(){
        recyclerView = (RecyclerView) findViewById(R.id.rv);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(DoctorListActivity.this,LinearLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(llm);
        physicianListAdapter = new PhysicianListAdapter(DoctorListActivity.this,
                physicianList);
        recyclerView.setAdapter(physicianListAdapter);
    }

    public void SetupDoctorSearch(){
        searchDoctor.setOnQueryTextListener(this);
    }

    HealthCenters.GetPhysicianList getPhysicianList = new HealthCenters.GetPhysicianList() {
        @Override
        public void onHttpResponse(Boolean success, List<PhysicianListModel> physicianListModels) {
            if(success){
                SetupDoctorSearch();
                physicianList = physicianListModels;
                backup();
                init_rv();
            }else{
                RequestFailureDialog dialogFragment = new RequestFailureDialog();
                dialogFragment.setRetryFunction(new RequestFailureDialog.RetryOnClickInterface() {
                    @Override
                    public void onClick() {
                        init_list();
                    }
                });
                dialogFragment.show(getSupportFragmentManager(), "RequestFailureDialog");
            }
        }
    };

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String query) {
        search(query);
        return false;
    }

    public void search(String query){
        physicianList.clear();
        noResult.setVisibility(View.INVISIBLE);
        for (int i = 0; i < basePhysicianList.size(); i++) {
            physicianList.add(basePhysicianList.get(i));
        }
        if (!query.equals("")){
            if (physicianList.size()!=0) {
                final List<PhysicianListModel> filteredHospitalList = filter(physicianList, query);
                if (filteredHospitalList.size()!=0) {
                    applyChangeInSearchText(filteredHospitalList);
                }
                else {
                    noResult.setVisibility(View.VISIBLE);
                    physicianList.clear();
                    physicianListAdapter.notifyDataSetChanged();
                }
            }
        }else {
            physicianListAdapter.notifyDataSetChanged();
        }
        physicianListAdapter.notifyDataSetChanged();
    }


    private void backup(){
        for (int i = 0; i < physicianList.size(); i++) {
            basePhysicianList.add(physicianList.get(i));
        }
    }

    private List<PhysicianListModel> filter(List<PhysicianListModel> models, String query){
        query = query.toLowerCase();
        final List<PhysicianListModel> filteredSymptomModel = new ArrayList<>();
        for (PhysicianListModel model: models){
            final String text = model.getName().toLowerCase();
//            final String profession = model.getProfession().toLowerCase();
            if (text.contains(query)) {
                filteredSymptomModel.add(model);
            }
        }
        return filteredSymptomModel;
    }

    private void applyChangeInSearchText(List<PhysicianListModel> models) {
        for (int i = 0; i < physicianList.size(); i++) {
            final PhysicianListModel model = physicianList.get(i);
            if (!models.contains(model)) {
                physicianList.remove(i);
                physicianListAdapter.notifyDataSetChanged();
                i--;/*when you remove an item in the iteration you should go back so you can check all of the elements,
                        if not, one element will be missed each time you remove*/
            }
        }
    }
}
