package com.darmaneh.ava.health_centers;

import android.animation.LayoutTransition;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.darmaneh.adapters.PharmacyAdapter;
import com.darmaneh.ava.App;
import com.darmaneh.ava.R;
import com.darmaneh.fragments.RequestFailureDialog;
import com.darmaneh.models.health_centers.PharmacyModel;
import com.darmaneh.requests.HealthCenters;
import com.darmaneh.requests.Utility;
import com.darmaneh.utilities.Analytics;
import com.darmaneh.utilities.Functions;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import android.support.v7.widget.SearchView;

public class PharmacyListActivity extends AppCompatActivity implements SearchView.OnQueryTextListener{
    String province, city;
    TextView provinceCity;
    RecyclerView recyclerView;
    TextView    noResult;
    SearchView searchPharmacy;
    List<PharmacyModel> pharmacyListModels, basePharmacyListModels;
    PharmacyAdapter pharmacyAdapter;
    RelativeLayout.LayoutParams lp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pharmacy_list);
        initialize();
        init_pharmacy_list();
        init_pharmacy_serarch();
        setOnClick();
    }

    private void init_pharmacy_serarch(){
        searchPharmacy = (SearchView) findViewById(R.id.search_pharmacy);
        noResult       = (TextView)  findViewById(R.id.no_result_found_ph);
        noResult.setTypeface(App.getFont(4));
        searchPharmacy.setIconifiedByDefault(true);
        lp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT);
        searchPharmacy.setLayoutParams(lp);
    }

    private void init_pharmacy_list() {
        HealthCenters.get_pharmacy_list(this,province,city,getPharmacyList);
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        Analytics.sendScreenName("locations/" + province + "/" + city + "/pharmacies");
    }

    private void setOnClick() {
        findViewById(R.id.back_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        searchPharmacy.setOnSearchClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,
                        RelativeLayout.LayoutParams.WRAP_CONTENT);
                searchPharmacy.setLayoutParams(lp);
                searchPharmacy.setBackgroundResource(R.drawable.search_bg_gray);
            }
        });

        searchPharmacy.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                lp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
                        RelativeLayout.LayoutParams.WRAP_CONTENT);
                noResult.setVisibility(View.INVISIBLE);
                searchPharmacy.setLayoutParams(lp);
                searchPharmacy.setBackgroundResource(R.drawable.search_bg_white);
                return false;
            }
        });
    }

    private void initialize() {
        Bundle extras = getIntent().getExtras();
        province = extras.getString("province");
        city = extras.getString("city");
        provinceCity = (TextView) findViewById(R.id.province_city);
        ((TextView) findViewById(R.id.pharmacy_name)).setTypeface(App.getFont(4));
        String provinceCityStr = province + "-" + city;
        provinceCity.setText(provinceCityStr);
        provinceCity.setTypeface(App.getFont(3));

        pharmacyListModels = new ArrayList<>();
        basePharmacyListModels = new ArrayList<>();
    }

    private void rvInit(List<PharmacyModel> pharmacyModelList) {
        recyclerView = (RecyclerView) findViewById(R.id.rv);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(llm);
        pharmacyAdapter = new PharmacyAdapter(pharmacyModelList);
        recyclerView.setAdapter(pharmacyAdapter);
    }

    public void SetupPharmacySearch(){
        searchPharmacy.setOnQueryTextListener(this);

    }

    HealthCenters.GetPharmacyList getPharmacyList = new HealthCenters.GetPharmacyList() {
        @Override
        public void onHttpResponse(Boolean success, List<PharmacyModel> pharmacyList) {
            if(success){
                SetupPharmacySearch();
                pharmacyListModels = pharmacyList;
                backup();
                rvInit(pharmacyList);
            }else{
                RequestFailureDialog dialogFragment = new RequestFailureDialog();
                dialogFragment.setRetryFunction(new RequestFailureDialog.RetryOnClickInterface() {
                    @Override
                    public void onClick() {
                        init_pharmacy_list();
                    }
                });
                dialogFragment.show(getSupportFragmentManager(), "RequestFailureDialog");
            }
        }
    };

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String query) {
        search(query);
        return false;
    }

    @Override
    public void onBackPressed()
    {
        if (!searchPharmacy.isIconified()){
            searchPharmacy.setIconified(true);
        }else{
            super.onBackPressed();
        }
    }

    public void search(String query){
        pharmacyListModels.clear();
        noResult.setVisibility(View.INVISIBLE);
        for (int i = 0; i < basePharmacyListModels.size(); i++) {
            pharmacyListModels.add(basePharmacyListModels.get(i));
        }
        if (!query.equals("")){
            if (pharmacyListModels.size()!=0) {
                final List<PharmacyModel> filteredPharmacyList = filter(pharmacyListModels, query);
                if (filteredPharmacyList.size()!=0) {
                    applyChangeInSearchText(filteredPharmacyList);
                }
                else {
                    noResult.setVisibility(View.VISIBLE);
                    pharmacyListModels.clear();
                    pharmacyAdapter.notifyDataSetChanged();
                }
            }
        }else {
            pharmacyAdapter.notifyDataSetChanged();
        }
        pharmacyAdapter.notifyDataSetChanged();
    }

    public void backup(){
        for (int i = 0; i < pharmacyListModels.size(); i++) {
            basePharmacyListModels.add(pharmacyListModels.get(i));
        }
    }

    private List<PharmacyModel> filter(List<PharmacyModel> models, String query){
        query = query.toLowerCase();
        final List<PharmacyModel> filteredSymptomModel = new ArrayList<>();
        for (PharmacyModel model: models){
            final String text = model.getSpecialName().toLowerCase();
            final String addrText = model.getAddress().toLowerCase();
            if (text.contains(query) || addrText.contains(query)) {
                filteredSymptomModel.add(model);
            }
        }
        return filteredSymptomModel;
    }

    private void applyChangeInSearchText(List<PharmacyModel> models) {
        for (int i = 0; i < pharmacyListModels.size(); i++) {
            final PharmacyModel model = pharmacyListModels.get(i);
            if (!models.contains(model)) {
                pharmacyListModels.remove(i);
                pharmacyAdapter.notifyDataSetChanged();
                i--; /*when you remove an item in the iteration you should go back so you can check all of the elements,
                        if not, one element will be missed each time you remove*/
            }
        }
    }
}
