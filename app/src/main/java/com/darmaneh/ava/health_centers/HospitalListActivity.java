package com.darmaneh.ava.health_centers;

import android.animation.LayoutTransition;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.darmaneh.adapters.HospitalAdapter;
import com.darmaneh.ava.App;
import com.darmaneh.ava.R;
import com.darmaneh.fragments.RequestFailureDialog;
import com.darmaneh.models.health_centers.HospitalModel;
import com.darmaneh.requests.HealthCenters;
import com.darmaneh.requests.Utility;
import com.darmaneh.utilities.Analytics;
import com.darmaneh.utilities.Functions;

import java.util.ArrayList;
import java.util.List;

import android.support.v7.widget.SearchView;
import android.widget.Toast;

public class HospitalListActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {
    String province, city;
    TextView provinceCity;
    RecyclerView recyclerView;
    SearchView searchHospital;
    TextView noResult;
    List<HospitalModel> hospitalListModels, baseHospitalListModels;
    HospitalAdapter hospitalAdapter;
    RelativeLayout.LayoutParams lp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hospital_list);
        initialize();
        init_hospital_list();
        init_hospital_serarch();
        setOnClick();
    }

    private void init_hospital_serarch(){
        searchHospital = (SearchView)findViewById(R.id.search_hospital);
        noResult       = (TextView)  findViewById(R.id.no_result_found);
        noResult.setTypeface(App.getFont(4));
        searchHospital.setIconifiedByDefault(true);
        lp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT);
        searchHospital.setLayoutParams(lp);
    }

    private void init_hospital_list() {
        HealthCenters.get_hosptial_list(this,province,city,getHospitalList);
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        Analytics.sendScreenName("locations/" + province + "/" + city + "/hospitals");
    }

    @Override
    public void onBackPressed()
    {
        if (!searchHospital.isIconified()){
            searchHospital.setIconified(true);
        }else{
            super.onBackPressed();
        }
    }

    private void setOnClick() {
        findViewById(R.id.back_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        searchHospital.setOnSearchClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                lp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,
                        RelativeLayout.LayoutParams.WRAP_CONTENT);
                searchHospital.setLayoutParams(lp);

                searchHospital.setBackgroundResource(R.drawable.search_bg_gray);

            }
        });

        searchHospital.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                lp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
                        RelativeLayout.LayoutParams.WRAP_CONTENT);
                searchHospital.setLayoutParams(lp);
                noResult.setVisibility(View.INVISIBLE);
                searchHospital.setBackgroundResource(R.drawable.search_bg_white);
                return false;
            }
        });
    }

    private void initialize() {
        Bundle extras = getIntent().getExtras();
        province = extras.getString("province");
        city = extras.getString("city");
        provinceCity = (TextView) findViewById(R.id.province_city);
        ((TextView) findViewById(R.id.hospital_name)).setTypeface(App.getFont(4));
        String provinceCityStr = province + "-" + city;
        provinceCity.setText(provinceCityStr);
        provinceCity.setTypeface(App.getFont(3));

        hospitalListModels = new ArrayList<>();
        baseHospitalListModels = new ArrayList<>();
    }

    private void rvInit(List<HospitalModel> hospitalList) {
        recyclerView = (RecyclerView) findViewById(R.id.rv);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(llm);
        hospitalAdapter = new HospitalAdapter(hospitalList);
        recyclerView.setAdapter(hospitalAdapter);
    }

    public void SetupHospitalSearch(){

        searchHospital.setOnQueryTextListener(this) ;

    }

    HealthCenters.GetHospitalList getHospitalList = new HealthCenters.GetHospitalList() {
        @Override
        public void onHttpResponse(Boolean success, List<HospitalModel> hospitalList) {
            if(success){
                SetupHospitalSearch();
                hospitalListModels = hospitalList;
                backup();
                rvInit(hospitalList);
            }else{
                RequestFailureDialog dialogFragment = new RequestFailureDialog();
                dialogFragment.setRetryFunction(new RequestFailureDialog.RetryOnClickInterface() {
                    @Override
                    public void onClick() {
                        init_hospital_list();
                    }
                });
                dialogFragment.show(getSupportFragmentManager(), "RequestFailureDialog");
            }
        }
    };

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String query) {
        search(query);
        return false;
    }

    public void search(String query){
        hospitalListModels.clear();
        noResult.setVisibility(View.INVISIBLE);
        for (int i = 0; i < baseHospitalListModels.size(); i++) {
            hospitalListModels.add(baseHospitalListModels.get(i));
        }
        if (!query.equals("")){
            if (hospitalListModels.size()!=0) {
                final List<HospitalModel> filteredHospitalList = filter(hospitalListModels, query);
                if (filteredHospitalList.size()!=0) {
                    applyChangeInSearchText(filteredHospitalList);
                }
                else {
                    noResult.setVisibility(View.VISIBLE);
                    hospitalListModels.clear();
                    hospitalAdapter.notifyDataSetChanged();
                }
            }
        }else {
            hospitalAdapter.notifyDataSetChanged();
        }
        hospitalAdapter.notifyDataSetChanged();
    }

    public void backup(){
        for (int i = 0; i < hospitalListModels.size(); i++) {
            baseHospitalListModels.add(hospitalListModels.get(i));
        }
    }

    private List<HospitalModel> filter(List<HospitalModel> models, String query){
        query = query.toLowerCase();
        final List<HospitalModel> filteredSymptomModel = new ArrayList<>();
        for (HospitalModel model: models){
            final String text = model.getName().toLowerCase();
            final String addrText = model.getAddress().toLowerCase();
            if (text.contains(query) || addrText.contains(query)) {
                filteredSymptomModel.add(model);
            }
        }
        return filteredSymptomModel;
    }

    private void applyChangeInSearchText(List<HospitalModel> models) {
        for (int i = 0; i < hospitalListModels.size(); i++) {
            final HospitalModel model = hospitalListModels.get(i);
            if (!models.contains(model)) {
                hospitalListModels.remove(i);
                hospitalAdapter.notifyDataSetChanged();
                i--;/*when you remove an item in the iteration you should go back so you can check all of the elements,
                        if not, one element will be missed each time you remove*/
            }
        }
    }
}
