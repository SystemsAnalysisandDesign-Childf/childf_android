package com.darmaneh.ava.health_centers;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.darmaneh.adapters.ProfessionCategoryAdapter;
import com.darmaneh.ava.App;
import com.darmaneh.ava.R;
import com.darmaneh.fragments.RequestFailureDialog;
import com.darmaneh.models.call.ProfessionListModel;
import com.darmaneh.models.health_centers.ProfessionCategoryModel;
import com.darmaneh.requests.HealthCenters;
import com.darmaneh.requests.Utility;
import com.darmaneh.utilities.Analytics;
import com.darmaneh.utilities.Functions;

import java.util.List;

public class ProfessionActivity extends AppCompatActivity {

    String province, city;
    TextView titleText;
    List<ProfessionCategoryModel> professionList;
    RecyclerView recyclerView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hospital_list);
        initialize();
        init_list();
        setOnClicks();
    }

    private void setOnClicks() {
        findViewById(R.id.back_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private void init_list() {
        HealthCenters.get_profession_list(this,getProfession);
    }


    private void initialize() {
        Bundle extras = getIntent().getExtras();
        province = extras.getString("province");
        city = extras.getString("city");
        titleText = (TextView) findViewById(R.id.hospital_name);
        titleText.setTypeface(App.getFont(4));
        titleText.setText("تخصص ها");
        findViewById(R.id.province_city).setVisibility(View.GONE);
        findViewById(R.id.search_hospital).setVisibility(View.GONE);
    }

    private void rvInit(List<ProfessionCategoryModel> professionList) {
        recyclerView = (RecyclerView) findViewById(R.id.rv);
        recyclerView.setHasFixedSize(true);
        GridLayoutManager glm = new GridLayoutManager(this,2);
        recyclerView.setLayoutManager(glm);
        ProfessionCategoryAdapter professionCategoryAdapter = new ProfessionCategoryAdapter(ProfessionActivity.this,
                professionList, province, city);
        recyclerView.setAdapter(professionCategoryAdapter);
    }

    HealthCenters.GetProfession getProfession = new HealthCenters.GetProfession() {
        @Override
        public void onHttpResponse(Boolean success, List<ProfessionCategoryModel> professionListModels) {
            if(success){
                professionList = professionListModels;
                rvInit(professionListModels);
            }else{
                RequestFailureDialog dialogFragment = new RequestFailureDialog();
                dialogFragment.setRetryFunction(new RequestFailureDialog.RetryOnClickInterface() {
                    @Override
                    public void onClick() {
                        init_list();
                    }
                });
                dialogFragment.show(getSupportFragmentManager(), "RequestFailureDialog");
            }
        }
    };
}
