package com.darmaneh.ava.health_centers;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.darmaneh.ava.App;
import com.darmaneh.ava.R;
import com.darmaneh.requests.HealthCenters;
import com.darmaneh.requests.Utility;
import com.darmaneh.utilities.Analytics;
import com.darmaneh.utilities.Functions;


public class LocationMainActivity extends AppCompatActivity {
    String province, city;
    double lat,lng;
    boolean isNearest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_main);

        isNearest = getIntent().getExtras().getBoolean("nearest");
        getIntentData();
        initialize();
        if(isNearest) {
            setOnClickNearest();
            findViewById(R.id.clinic_layout).setVisibility(View.GONE);
        }
        else {
            setOnClickProvinceCity();
        }

    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        Analytics.sendScreenName("locations/" + province + "/" + city);
    }

    private void setOnClickProvinceCity() {
        findViewById(R.id.hospital_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), HospitalListActivity.class);
                intent.putExtra("province",province);
                intent.putExtra("city",city);
                startActivity(intent);
            }
        });
        findViewById(R.id.pharmacy_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), PharmacyListActivity.class);
                intent.putExtra("province",province);
                intent.putExtra("city",city);
                startActivity(intent);
            }
        });

        findViewById(R.id.clinic_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), ProfessionActivity.class);
                intent.putExtra("province",province);
                intent.putExtra("city",city);
                startActivity(intent);
            }
        });

        findViewById(R.id.back_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private void setOnClickNearest(){
        findViewById(R.id.hospital_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), NearestHealthCenterActivity.class);
                intent.putExtra("lat",lat);
                intent.putExtra("lng",lng);
                intent.putExtra("type",NearestHealthCenterActivity.healthCenterType.HOSPITAL);
                startActivity(intent);
            }
        });
        findViewById(R.id.pharmacy_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), NearestHealthCenterActivity.class);
                intent.putExtra("lat",lat);
                intent.putExtra("lng",lng);
                intent.putExtra("type",NearestHealthCenterActivity.healthCenterType.PHARMACY);
                startActivity(intent);
            }
        });
        findViewById(R.id.back_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private void initialize() {
        ((TextView)findViewById(R.id.hospital_text)).setTypeface(App.getFont(4));
        ((TextView)findViewById(R.id.pharmacy_text)).setTypeface(App.getFont(4));
        ((TextView)findViewById(R.id.clinic_text)).setTypeface(App.getFont(4));

        ((TextView)findViewById(R.id.city_name)).setTypeface(App.getFont(4));
        ((TextView)findViewById(R.id.province_name)).setTypeface(App.getFont(4));

        ((TextView)findViewById(R.id.city_name)).setText(city);
        ((TextView)findViewById(R.id.province_name)).setText(province);
    }

    public void getIntentData() {
        if(isNearest){
            lat = getIntent().getExtras().getDouble("lat");
            lng = getIntent().getExtras().getDouble("lng");
        }else {
            province = getIntent().getExtras().getString("province");
            city = getIntent().getExtras().getString("city");
        }
    }
}
