package com.darmaneh.ava.health_centers;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.TextView;

import com.darmaneh.ava.App;
import com.darmaneh.ava.R;
import com.darmaneh.models.location.HealthCentersLocationModel;
import com.darmaneh.models.location.Hospital;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private LatLng currentLocation ;
    private final String TAG = MapsActivity.class.getSimpleName();
    HealthCentersLocationModel healthCentersLocation;
    TextView title, address, phone, moreInfo;
    CardView cardView;
    Hospital hospital_in_card;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        initView();
        setOnClick();
        double lat = getIntent().getDoubleExtra("lat",100.0);
        double lng = getIntent().getDoubleExtra("lng",100.0);
        String healthCentersStr = getIntent().getStringExtra("model");
        currentLocation = new LatLng(lat,lng);
        healthCentersLocation = (new Gson()).fromJson(healthCentersStr,
                new TypeToken<HealthCentersLocationModel>(){}.getType());

        mapFragment.getMapAsync(this);
    }

    private void setOnClick() {
        moreInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(hospital_in_card != null){
                    Intent intent = new Intent(MapsActivity.this, HospitalDetailActivity.class);
                    intent.putExtra("hospital_detail",hospital_in_card.toString());
                    startActivity(intent);
                }
            }
        });
    }

    private void initView() {
        title = (TextView) findViewById(R.id.hospital_title);
        address = (TextView) findViewById(R.id.hospital_address);
        phone = (TextView) findViewById(R.id.hospital_phone);
        moreInfo = (TextView) findViewById(R.id.more_info);
        cardView = (CardView) findViewById(R.id.card_view);

        title.setTypeface(App.getFont(4));
        address.setTypeface(App.getFont(3));
        phone.setTypeface(App.getFont(3));
        moreInfo.setTypeface(App.getFont(3));
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();

    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        if(currentLocation != null) {
//            mMap.moveCamera(CameraUpdateFactory.newLatLng(currentLocation));
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(currentLocation, 13);
            mMap.animateCamera(cameraUpdate);
        }
        for(Hospital hospital: healthCentersLocation.getHospitals()){
            mMap.addMarker(new MarkerOptions()
                    .position(new LatLng(hospital.getLocation().getLatitude(), hospital.getLocation().getLongitude()))
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE))
                    .title(hospital.getName()))
                    .setTag(hospital);
        }
        mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
            @Override
            public View getInfoWindow(Marker marker) {
                return null;
            }

            @Override
            public View getInfoContents(Marker marker) {
                View v = getLayoutInflater().inflate(R.layout.marker_info_title,null);
                TextView infoTitle = (TextView) v.findViewById(R.id.info_title);
                infoTitle.setTypeface(App.getFont(3));
                infoTitle.setText(marker.getTitle());
                return v;
            }
        });

        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                cardView.setVisibility(View.VISIBLE);
                hospital_in_card = (Hospital) marker.getTag();
                title.setText(marker.getTitle());
                address.setText(hospital_in_card.getAddress());
                phone.setText(hospital_in_card.getTell());
                return false;
            }
        });

        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                cardView.setVisibility(View.GONE);
            }
        });
    }


}
