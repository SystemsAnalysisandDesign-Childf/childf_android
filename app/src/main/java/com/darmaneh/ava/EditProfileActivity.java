package com.darmaneh.ava;

import android.content.Context;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.darmaneh.adapters.CustomSpinnerAdapter;
import com.darmaneh.dialogs.DarmanehToast;
import com.darmaneh.models.ProvinceCityModel;
import com.darmaneh.models.patient_record.PatientRecordModel;
import com.darmaneh.models.patient_record.PatientRecordRequest;
import com.darmaneh.requests.PatientRecord;
import com.darmaneh.requests.ProvinceCity;
import com.darmaneh.requests.Utility;
import com.darmaneh.utilities.Analytics;
import com.darmaneh.utilities.Functions;
import com.darmaneh.utilities.JalaliCalendar;
import com.darmaneh.utilities.Storage;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EditProfileActivity extends AppCompatActivity {
    private static final String TAG = EditProfileActivity.class.getSimpleName();
    EditText inputFirstName, inputLastName ,inputEmail ,  inputBirth , inputHeight , inputWeight ;
    RadioGroup radioSex;
    RadioButton radioMale , radioFemale;
    TextView phoneTxt;
    Spinner editProvince , editCity;
    ImageView profPic;
    PatientRecordModel prm;
    String[] sexTypesFa = {"مرد" , "زن"};
    String[] sexTypesEn = {"male" , "female"};
    FrameLayout saveLayout , discardLayout;
    int currentYear;
    private List<ProvinceCityModel> pcm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        String modelStr = getIntent().getExtras().getString("model");
        prm = (new Gson()).fromJson(modelStr,PatientRecordModel.class);
        Calendar c = Calendar.getInstance();
        currentYear = (new JalaliCalendar(c.get(Calendar.YEAR),
                c.get(Calendar.MONTH) + 1,
                c.get(Calendar.DATE))).getYear();
        init_view();
        setOnClick();
        getDataFromServer();
        change_font();
    }

    private void setOnClick() {

        editProvince.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            int count = 0;
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                if (pcm != null && count > 0) {
                    CustomSpinnerAdapter adaptercity = new CustomSpinnerAdapter(view.getContext(), (ArrayList<String>) pcm.get(position).getCities());
                    editCity.setAdapter(adaptercity);
                }
                count ++;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void init_view() {
        inputFirstName = (TextInputEditText) findViewById(R.id.input_first_name);
        inputLastName = (TextInputEditText) findViewById(R.id.input_last_name);
        inputEmail = (TextInputEditText) findViewById(R.id.input_email);
        inputBirth = (TextInputEditText) findViewById(R.id.input_birth);
        inputHeight = (TextInputEditText) findViewById(R.id.input_height);
        inputWeight = (TextInputEditText) findViewById(R.id.input_weight);

        editProvince = (Spinner) findViewById(R.id.edit_province);
        editCity = (Spinner) findViewById(R.id.edit_city);

        radioSex = (RadioGroup) findViewById(R.id.radio_sex);
        radioMale = (RadioButton) findViewById(R.id.radio_male);
        radioFemale = (RadioButton) findViewById(R.id.radio_female);
        profPic = (ImageView) findViewById(R.id.prof_pic);
        phoneTxt = (TextView) findViewById(R.id.input_phone);

        findViewById(R.id.btn_save_card).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveRequest();
            }
        });

        findViewById(R.id.btn_discard_card).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        radioMale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                profPic.setImageResource(R.drawable.male_pro_pic);
            }
        });

        radioFemale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                profPic.setImageResource(R.drawable.female_pro_pic);
            }
        });

    }

    private void change_font() {

        inputFirstName.setTypeface(App.getFont(3));
        inputLastName.setTypeface(App.getFont(3));
        inputEmail.setTypeface(App.getFont(3));
        inputBirth.setTypeface(App.getFont(3));
        inputHeight.setTypeface(App.getFont(3));
        inputWeight.setTypeface(App.getFont(3));
        radioMale.setTypeface(App.getFont(3));
        radioFemale.setTypeface(App.getFont(3));
        ((TextView)findViewById(R.id.personal_info_txt)).setTypeface(App.getFont(4));
    }

    public void setComponentText(){
        if(prm != null){
            inputFirstName.setText(prm.getFirstName());
            inputLastName.setText(prm.getLastName());
            inputEmail.setText(prm.getEmail());
            inputBirth.setText(getValueOf(prm.getBirthYear()));
            inputHeight.setText(getValueOf(prm.getHeight()));
            inputWeight.setText(getValueOf(prm.getWeight()));
            String phoneStr = Storage.getCompletePhoneNum("fa");
            phoneTxt.setText(Functions.toPersian(phoneStr));
            setSexCity();
        }
    }


    private boolean validateAge() {
        if (!inputBirth.getText().toString().trim().isEmpty()) {
            if (inputBirth.getText().toString().length() != 4) {
                inputBirth.setError("سال تولد باید ۴ رقم باشد.(به عنوان مثال ۱۳۷۵)");
                requestFocus(inputBirth);
                return false;
            } else if (currentYear - Integer.parseInt(inputBirth.getText().toString()) <= 0 || currentYear - Integer.parseInt(inputBirth.getText().toString()) > 150) {
                inputBirth.setError("سال تولد وارد شده صحیح نمی باشد.");
                requestFocus(inputBirth);
                return false;
            } else {
                inputBirth.setError(null);
            }
        } else {
            inputBirth.setError(null);
        }

        return true;
    }

    private boolean validateWeight() {
        if (!inputWeight.getText().toString().trim().isEmpty()) {
            if (Integer.parseInt(inputWeight.getText().toString()) < 1 || Integer.parseInt(inputWeight.getText().toString()) > 400) {
                inputWeight.setError("وزن وارد شده صحیح نمی باشد.");
                requestFocus(inputWeight);
                return false;
            } else {
                inputWeight.setError(null);
            }
        } else {
            inputWeight.setError(null);
        }

        return true;
    }

    private boolean validateHeight() {
        if (!inputHeight.getText().toString().trim().isEmpty()) {
            if (Integer.parseInt(inputHeight.getText().toString())<20 || Integer.parseInt(inputHeight.getText().toString()) > 270){
                inputHeight.setError("قد وارد شده صحیح نمی باشد.");
                requestFocus(inputHeight);
                return false;
            } else {
                inputHeight.setError(null);
            }
        } else {
            inputHeight.setError(null);
        }

        return true;
    }

    public boolean validateEmail() {
        if (!inputEmail.getText().toString().trim().isEmpty()) {

            String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
            CharSequence inputStr = inputEmail.getText().toString();

            Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
            Matcher matcher = pattern.matcher(inputStr);
            if (!matcher.matches()) {
                inputEmail.setError("پست الکترونیکی وارد شده صحیح نمی باشد.");
                requestFocus(inputEmail);
                return false;
            } else {
                inputEmail.setError(null);
            }
        } else {
            inputEmail.setError(null);
        }

        return true;
    }

    private boolean saveRequest() {

        boolean ageIsValid, heightIsValid, weightIsValid, emailIsValid;

        ageIsValid = validateAge();
        heightIsValid = validateHeight();
        weightIsValid = validateWeight();
        emailIsValid = validateEmail();


        if (!ageIsValid || !heightIsValid || !weightIsValid || !emailIsValid)
            return false;

        PatientRecordRequest prr = new PatientRecordRequest();
        prr.setFirstName(inputFirstName.getText().toString());
        prr.setLastName(inputLastName.getText().toString());
        prr.setEmail(inputEmail.getText().toString());
        prr.setBirthYear(getValueOf(inputBirth.getText().toString()));
        prr.setHeight(getValueOf(inputHeight.getText().toString()));
        prr.setWeight(getValueOf(inputWeight.getText().toString()));
        String selectedSex = radioMale.isChecked() ? "male" : "female" ;
        prr.setSex(selectedSex);
        prr.setProvince(editProvince.getSelectedItem().toString());
        prr.setCity(editCity.getSelectedItem().toString());
        PatientRecord.edit_patient_record(this, prr, new PatientRecord.EditPatientRecord() {
            @Override
            public void onHttpResponse(Boolean success , PatientRecordModel patientRecordModel) {
                if(success){
                    Analytics.sendScreenName("EMR/bi/edited");
                    finish();
                }else{
                    DarmanehToast.makeText(EditProfileActivity.this,"بارگذاری با مشکل مواجه شد. لطفا مجددا تلاش کنید."
                            , Toast.LENGTH_SHORT);
                }
            }
        });
        return true;
    }

    private void setSexCity() {
        if(prm.getSex().equals(sexTypesEn[0])) {
            radioMale.setChecked(true);
            profPic.setImageResource(R.drawable.male_pro_pic);

        }
        else {
            radioFemale.setChecked(true);
            profPic.setImageResource(R.drawable.female_pro_pic);
        }


        if (prm.getProvince() != null) {
            int province_id = getostanid(prm.getProvince());
            if (province_id != -1) {
                editProvince.setSelection(province_id);
                if (prm.getCity() != null) {
                    int city_id = getcityid(province_id, prm.getCity());
                    if (city_id != -1) {
                        CustomSpinnerAdapter adacity = new CustomSpinnerAdapter(EditProfileActivity.this,
                                (ArrayList<String>) pcm.get(province_id).getCities());
                        editCity.setAdapter(adacity);
                        editCity.setSelection(city_id);
                    }
                }

            }
        }
    }

    private String getValueOf(Integer param){
        if(param == null)
            return "";
        else
            return String.valueOf(param);
    }

    private Integer getValueOf(String param){
        if(param.equals(""))
            return null;
        else
            return Integer.valueOf(param);
    }

    private void requestFocus(View view) {//// TODO: 4/8/2017 check if it's not causing any problem
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    public void getDataFromServer() {
        ProvinceCity.get_province_city(this, new ProvinceCity.GetProvinceCity() {
            @Override
            public void onHttpResponse(Boolean success, List<ProvinceCityModel> provinceCityModels) {
                if (success) {
                    pcm = provinceCityModels;
                    initSpinner(EditProfileActivity.this, provinceCityModels);
                    setComponentText();
                }
            }
        });
    }

    private void initSpinner(Context context, List<ProvinceCityModel> provinceCityModels) {
        ArrayList<String> provinces = new ArrayList<>();
        for (int i = 0; i < provinceCityModels.size(); i++) {
            provinces.add(provinceCityModels.get(i).getName());
        }
        CustomSpinnerAdapter adapter = new CustomSpinnerAdapter(context, provinces);
        editProvince.setAdapter(adapter);
    }

    private int getostanid(String provinceStr){
        for (int index=0; index< pcm.size() ; index++){
            if(pcm.get(index).getName().equals(provinceStr)){
                   return index;
            }
        }
        return -1;
    }
    private int getcityid(int province_id, String cityStr){
        List<String> cityList = pcm.get(province_id).getCities();
        for (int index=0; index< cityList.size() ; index++){
            if(cityList.get(index).equals(cityStr)){
                return index;
            }
        }
        return -1;
    }
}
