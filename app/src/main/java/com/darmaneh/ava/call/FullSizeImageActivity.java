package com.darmaneh.ava.call;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.darmaneh.ava.App;
import com.darmaneh.ava.R;
import com.darmaneh.dialogs.DarmanehProgressDialog;
import com.darmaneh.models.call.GalleryModel;
import com.darmaneh.utilities.Storage;
import com.darmaneh.utilities.TouchImageView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import com.squareup.picasso.Callback;
import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;

import java.io.IOException;

public class FullSizeImageActivity extends AppCompatActivity {
    private TouchImageView imageView;
    private RelativeLayout infoLayout;
    TextView desc, category;
    ImageButton editBtn, backBtn;
    GalleryModel model;
    Boolean infoShown = true;

    private String TAG = FullSizeImageActivity.class.getSimpleName();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_size_image);
        model = (new Gson()).fromJson(getIntent().getExtras().getString("pic_model"),
                new TypeToken<GalleryModel>(){}.getType());
        initView();
        setFonts();
        setContents();
        setOnClicks();
    }

    private void setOnClicks() {
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(infoShown){
                    infoLayout.setVisibility(View.GONE);
                    infoShown = false;
                }else{
                    infoLayout.setVisibility(View.VISIBLE);
                    infoShown = true;
                }
            }
        });

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        editBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    private void setContents() {
        final DarmanehProgressDialog dialog = new DarmanehProgressDialog(this);
        dialog.show();

        category.setText(model.getCategory());
        desc.setText(model.getDescription());

        OkHttpClient client = new OkHttpClient();
        client.interceptors().add(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request request = chain.request().newBuilder()
                        .addHeader("Authorization", Storage.getToken())
                        .build();
                return chain.proceed(request);
            }
        });
        Picasso picasso = new Picasso.Builder(this)
                .downloader(new OkHttpDownloader(client)).build();
        picasso.load(model.getImage())
                .into(imageView, new Callback() {
                    @Override
                    public void onSuccess() {
                        dialog.dismiss();
                        Log.d(TAG,"success");
                    }

                    @Override
                    public void onError() {
                        dialog.dismiss();
                        Log.d(TAG,"fail");
                    }
                });

    }

    private void setFonts() {
        category.setTypeface(App.getFont(3));
        desc.setTypeface(App.getFont(3));
        ((TextView)findViewById(R.id.category_text)).setTypeface(App.getFont(4));
        ((TextView)findViewById(R.id.desc_text)).setTypeface(App.getFont(4));
    }

    private void initView() {
        imageView = (TouchImageView) findViewById(R.id.fullsize_pic);
        infoLayout = (RelativeLayout) findViewById(R.id.info_layout);
        category = (TextView) findViewById(R.id.category_name);
        desc = (TextView) findViewById(R.id.desc);
        editBtn = (ImageButton) findViewById(R.id.edit_button);
        backBtn = (ImageButton) findViewById(R.id.back_button);
        if(infoShown){
            infoLayout.setVisibility(View.VISIBLE);
        }else{
            infoLayout.setVisibility(View.GONE);
        }
    }
}
