package com.darmaneh.ava.call;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.darmaneh.ava.App;
import com.darmaneh.ava.MainActivity;
import com.darmaneh.ava.R;
import com.darmaneh.dialogs.DarmanehProgressDialog;
import com.darmaneh.requests.CallInternetCutRequest;
import com.darmaneh.requests.GetRoomNumberRequest;
import com.darmaneh.requests.Utility;
import com.darmaneh.utilities.Analytics;
import com.darmaneh.utilities.Functions;

import org.json.JSONObject;

public class CallIntrupted extends AppCompatActivity {

    Context context;
    private static String TAG = CallIntrupted.class.getSimpleName();
    TextView retryText, callInterruptedText;

    @Override
    public void onBackPressed() {}

    public void changeFonts(){
        ((TextView) findViewById(R.id.return_text)).setTypeface(App.getFont(3));
        retryText.setTypeface(App.getFont(3));
        callInterruptedText.setTypeface(App.getFont(3));
    }

    public void initialize(){
        retryText = (TextView) findViewById(R.id.retry_text);
        callInterruptedText = (TextView) findViewById(R.id.call_interrupted_text);
    }

    public void addListeners(){
        findViewById(R.id.return_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, MainActivity.class);
                startActivity(intent);
            }
        });

        findViewById(R.id.retry_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                GetRoomNumberRequest.get_room_number(context, getRoomNumber, true, true);
            }
        });
    }

    GetRoomNumberRequest.GetRoomNumber getRoomNumber = new GetRoomNumberRequest.GetRoomNumber() {
        @Override
        public void onHttpResponse(Boolean success, JSONObject response) {
            if(success){
                Log.e(TAG, response.toString());
                String roomNumber = response.optString("room_number", null);
                if (roomNumber != null) {
                    String doctorName = response.optString("ph_first_name", "") + " " + response.optString("ph_last_name", "");
                    Intent resultIntent = new Intent(context, ChooseCallType.class);
                    resultIntent.putExtra("roomNumber", roomNumber);
                    resultIntent.putExtra("doctorName", doctorName);
                    startActivity(resultIntent);
                }
            }else{
                if (response!=null) {
                    Log.e("error", response.toString());
                    String detail = response.optString("detail", null);
                    if (detail != null && detail.equals("Not found.")) {
                        finish();
                        Intent intent = new Intent(context, MainActivity.class);
                        startActivity(intent);
                    }
                }
            }
        }

        @Override
        public void stopProgress(DarmanehProgressDialog progress) {
            if (progress!=null)
                progress.dismiss();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call_intrupted);
        context = this;
        initialize();
        changeFonts();
        addListeners();
        CallInternetCutRequest.get_call_internet_cut_info(context, getInternetCutInfo);
    }

    @Override
    public void onResume(){
        super.onResume();
        Analytics.sendScreenName("Telemedicine/call_interrupted");
    }

    CallInternetCutRequest.GetInternetCutInfo getInternetCutInfo = new CallInternetCutRequest.GetInternetCutInfo(){
        @Override
        public void onHttpResponse(Boolean success) {
            if(success){
                retryText.setText("بازگشت به صفحه تماس");
                callInterruptedText.setText("متاسفانه اینترنت پزشک با خطا مواجه شده است. با استفاده از دکمه بازگشت به صفحه تماس می توانید تماس را مجددا آغاز کنید. همچنین شما می توانید با استفاده از دکمه بازگشت به صفحه اصلی از طریق دکمه تماس در قسمت پایین صفحه اصلی تماس خود را مجددا آغاز کنید.");
            }else{
                retryText.setText("تلاش مجدد");
                callInterruptedText.setText("متاسفانه اینترنت شما با خطا مواجه شده است. پس از رفع مشکل اینترنت خود با استفاده از دکمه تلاش مجدد می توانید تماس را مجددا آغاز کنید. همچنین شما می توانید با استفاده از دکمه بازگشت به صفحه اصلی از طریق دکمه تماس در قسمت پایین صفحه اصلی تماس خود را مجددا آغاز کنید.");
            }
        }
    };
}
