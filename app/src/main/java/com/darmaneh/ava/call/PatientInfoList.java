package com.darmaneh.ava.call;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.TextInputEditText;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.darmaneh.ava.App;
import com.darmaneh.ava.BuildConfig;
import com.darmaneh.ava.R;
import com.darmaneh.fragments.RequestFailureDialog;
import com.darmaneh.models.Mail.MailModel;
import com.darmaneh.models.call.InstantInfo;
import com.darmaneh.models.call.InstantInfoListModel;
import com.darmaneh.requests.PatientInfoDetailList;
import com.darmaneh.requests.UploadBio;
import com.darmaneh.requests.Utility;
import com.darmaneh.utilities.Analytics;
import com.darmaneh.utilities.Functions;

import java.util.ArrayList;
import java.util.List;

public class PatientInfoList extends AppCompatActivity {

    TextView sign, illnessHistory, illnessHistoryList,
        drugs, drugsList, allergy, allergyList, surgery, surgeryList,
        uploadPic, picNumText;

    TextInputEditText illnessSignsText;

    CardView pic;

    Context context;

    List<InstantInfo> conditions, medications, allergies, surgeries;
    int image_num;

    RelativeLayout next;

    boolean checkNextOn = false;

    static final int REQUEST_CODE = 1;

    public void initializeTextViews(){
        sign = (TextView) findViewById(R.id.illness_sign_text);
        illnessHistory = (TextView) findViewById(R.id.past_medical_history_text);
        drugs = (TextView) findViewById(R.id.drugs_taken_text);
        allergy = (TextView) findViewById(R.id.allergy_text);
        surgery = (TextView) findViewById(R.id.past_surgery_text);
        pic = (CardView) findViewById(R.id.upload_photo_button);
        uploadPic = (TextView) findViewById(R.id.upload_photo_text);
        picNumText = (TextView) findViewById(R.id.image_num_uploaded);
        next = (RelativeLayout) findViewById(R.id.next_button);
        illnessSignsText = (TextInputEditText) findViewById(R.id.input_illness_sign);
        illnessHistoryList = (TextView) findViewById(R.id.past_medical_history_list);
        allergyList = (TextView) findViewById(R.id.allergy_list);
        drugsList = (TextView) findViewById(R.id.drugs_taken_list);
        surgeryList = (TextView) findViewById(R.id.past_surgery_list);
        checkNextOn = false;
    }

    public void changeFonts(){
        sign.setTypeface(App.getFont(3));
        illnessHistory.setTypeface(App.getFont(3));
        drugs.setTypeface(App.getFont(3));
        allergy.setTypeface(App.getFont(3));
        surgery.setTypeface(App.getFont(3));
        uploadPic.setTypeface(App.getFont(3));
        picNumText.setTypeface(App.getFont(3));
        illnessHistoryList.setTypeface(App.getFont(3));
        drugsList.setTypeface(App.getFont(3));
        allergyList.setTypeface(App.getFont(3));
        surgeryList.setTypeface(App.getFont(3));
        ((TextView) findViewById(R.id.accept_text)).setTypeface(App.getFont(3));
    }

    public void checkNextStatus(){
        if (checkNextOn){
            next.setClickable(true);
            next.setBackgroundColor(ContextCompat.getColor(context, R.color.color_button_green));
        }else{
            next.setClickable(false);
            next.setBackgroundColor(ContextCompat.getColor(context, R.color.gray_text));
        }
    }

    public ArrayList<String> listToString(ArrayList<InstantInfo> instantInfos){
        ArrayList<String> infos = new ArrayList<>();
        for (InstantInfo instantInfo: instantInfos){
            infos.add(instantInfo.toString());
        }
        return infos;
    }

    public void uploadBioText(){
        String bio = (BuildConfig.FLAVOR.equals("bazaar") ? "tele: " : "") + illnessHistory.getText().toString();
        UploadBio.post_bio_text(context, bio, new UploadBio.BiographyUploaded() {
            @Override
            public void onHttpResponse(Boolean success) {
                if(success){
                    Intent intent = new Intent(context, DoctorList.class);
                    startActivity(intent);
                } else {
                    RequestFailureDialog dialogFragment = new RequestFailureDialog();
                    dialogFragment.setRetryFunction(new RequestFailureDialog.RetryOnClickInterface() {
                        @Override
                        public void onClick() {
                            uploadBioText();
                        }
                    });
                    dialogFragment.show(getSupportFragmentManager(), "RequestFailureDialog");
                }
            }
        });
    }

    public void addListeners(){

        illnessSignsText.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                checkNextOn = !illnessSignsText.getText().toString().equals("");
                checkNextStatus();
            }
        });

        findViewById(R.id.upload_photo_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, GalleryActivity.class);
                startActivityForResult(intent, 1);
            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                uploadBioText();
            }
        });

        findViewById(R.id.past_medical_history_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, PatientInfoDetail.class);
                intent.putExtra("type", 0);
                intent.putExtra("parent", "PatientInfoList");
                intent.putExtra("model", listToString((ArrayList<InstantInfo>) conditions));
                startActivity(intent);
            }
        });

        findViewById(R.id.drugs_taken_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, PatientInfoDetail.class);
                intent.putExtra("type", 1);
                intent.putExtra("parent", "PatientInfoList");
                intent.putExtra("model", listToString((ArrayList<InstantInfo>) medications));
                startActivity(intent);
            }
        });

        findViewById(R.id.allergy_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, PatientInfoDetail.class);
                intent.putExtra("type", 2);
                intent.putExtra("parent", "PatientInfoList");
                intent.putExtra("model", listToString((ArrayList<InstantInfo>) allergies));
                startActivity(intent);
            }
        });

        findViewById(R.id.past_surgery_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, PatientInfoDetail.class);
                intent.putExtra("type", 3);
                intent.putExtra("parent", "PatientInfoList");
                intent.putExtra("model", listToString((ArrayList<InstantInfo>) surgeries));
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            super.onActivityResult(requestCode, resultCode, data);
            if (requestCode == REQUEST_CODE  && resultCode  == RESULT_OK) {
                String requiredValue = data.getStringExtra("imageNum");
                Integer imageNum = Integer.parseInt(requiredValue);
                if(imageNum>0){
                    picNumText.setVisibility(View.VISIBLE);
                    picNumText.setText(Functions.toPersian(imageNum).toString() + " عکس بارگذاری شده است.");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void initialize(){
        conditions = new ArrayList<>();
        medications = new ArrayList<>();
        allergies = new ArrayList<>();
        surgeries = new ArrayList<>();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient_info_list);
        context = this;
        initialize();
        initializeTextViews();
        addListeners();
        changeFonts();
        checkNextStatus();
    }

    public void init_info_list(){
        PatientInfoDetailList.get_info_list(PatientInfoList.this, true, getInfoList);
    }



    @Override
    public void onResume(){
        super.onResume();
        init_info_list();
    }

    private void list_init(InstantInfoListModel instantInfoListModel) {
        conditions = instantInfoListModel.getPatientConditions();
        medications = instantInfoListModel.getPatientMedications();
        allergies = instantInfoListModel.getPatientAllergies();
        surgeries = instantInfoListModel.getPatientSurgeries();
        image_num = instantInfoListModel.getPatinet_images_no();
    }

    public void showLists(){
        illnessHistoryList.setText(conditions.size() == 0? "هیچ سابقه بیماری ثبت نشده است": "");
        drugsList.setText(medications.size() == 0? "هیچ دارویی ثبت نشده است": "");
        allergyList.setText(allergies.size() == 0? "هیچ حساسیتی ثبت نشده است": "");
        surgeryList.setText(surgeries.size() == 0? "هیچ سابقه ای ثبت نشده است": "");
        String imageNumStr = Functions.toPersian(image_num)+ " عکس بارگذاری شده است.";
        picNumText.setText(image_num == 0? "هیچ عکسی ثبت نشده است" : imageNumStr );

        for (int i = 0; i < conditions.size(); i++) {
            if (i == 0)
                illnessHistoryList.setText(conditions.get(i).getNameFa());
            else
                illnessHistoryList.setText(illnessHistoryList.getText() + ", " + conditions.get(i).getNameFa());
        }

        for (int i = 0; i < medications.size(); i++) {
            if (i == 0)
                drugsList.setText(medications.get(i).getNameFa());
            else
                drugsList.setText(drugsList.getText() + ", " + medications.get(i).getNameFa());
        }

        for (int i = 0; i < allergies.size(); i++) {
            if (i == 0)
                allergyList.setText(allergies.get(i).getNameFa());
            else
                allergyList.setText(allergyList.getText() + ", " + allergies.get(i).getNameFa());
        }

        for (int i = 0; i < surgeries.size(); i++) {
            if (i == 0)
                surgeryList.setText(surgeries.get(i).getNameFa());
            else
                surgeryList.setText(surgeryList.getText() + ", " + surgeries.get(i).getNameFa());
        }
    }

    PatientInfoDetailList.GetInfoList getInfoList = new PatientInfoDetailList.GetInfoList() {
        @Override
        public void onHttpResponse(Boolean success, List<MailModel> mailModelList) {
            if(success){
//                list_init(instantInfoListModel);
//                showLists();
            }else{
                RequestFailureDialog dialogFragment = new RequestFailureDialog();
                dialogFragment.setRetryFunction(new RequestFailureDialog.RetryOnClickInterface() {
                    @Override
                    public void onClick() {
                        init_info_list();
                    }
                });
            }
        }
    };
}
