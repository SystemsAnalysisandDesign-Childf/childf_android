package com.darmaneh.ava.call;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.darmaneh.ava.App;
import com.darmaneh.ava.BuildConfig;
import com.darmaneh.ava.MainActivity;
import com.darmaneh.ava.R;
import com.darmaneh.requests.Utility;
import com.darmaneh.requests.Variable;
import com.darmaneh.service.DoctorOnlineService;
import com.darmaneh.utilities.Analytics;
import com.darmaneh.utilities.Functions;

public class WaitForConnection extends AppCompatActivity {

    Context context;

    private boolean isServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public void changeFonts(){
        ((TextView) findViewById(R.id.return_text)).setTypeface(App.getFont(3));
        ((TextView) findViewById(R.id.waiting_title)).setTypeface(App.getFont(3));
    }

    public void addListeners(){
        findViewById(R.id.return_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, MainActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onResume(){
        super.onResume();
        Analytics.sendScreenName("telemedicine/waiting");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wait_for_connection);
        context = this;
        changeFonts();
        addListeners();
        if (!isServiceRunning(DoctorOnlineService.class) && !BuildConfig.FLAVOR.equals("bazaar"))
            startService(new Intent(this, DoctorOnlineService.class));
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(context, MainActivity.class);
        startActivity(intent);
        finish();
    }
}
