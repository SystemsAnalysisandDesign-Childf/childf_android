package com.darmaneh.ava.call;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.IntegerRes;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.darmaneh.ava.App;
import com.darmaneh.ava.R;
import com.darmaneh.fragments.RequestFailureDialog;
import com.darmaneh.models.call.DetailInfoModel;
import com.darmaneh.models.call.DoctorInfoDetailModel;
import com.darmaneh.models.call.ProfessionModel;
import com.darmaneh.models.emergency.EmergencyModel;
import com.darmaneh.requests.ConditionDetail;
import com.darmaneh.requests.DoctorDetailRequest;
import com.darmaneh.requests.UserFlowV3;
import com.darmaneh.requests.Utility;
import com.darmaneh.utilities.Analytics;
import com.darmaneh.utilities.Functions;
import com.darmaneh.utilities.Storage;
import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class DoctorDetail extends AppCompatActivity {

    DoctorInfoDetailModel doctorDetailInfo;
    DoctorInfoDetailModel doctorDetail;
    TextView name, cost, hamyarTitle, hamyarText
            ,hamyarPhoneTitle, hamyarPhoneText, email;
    CircleImageView image;
    Context context;
    FrameLayout acceptLayout;
    String hamyarPhoneS, costS, hamyarS, nameS, emailS;
    TextInputEditText costEditText;
    TextInputLayout costLayout;
    int requirementId, recieverID;

    public void list_init(){
        name.setText(nameS);
        cost.setText("هزینه مورد نیاز: " + Functions.toPersian(costS));
        hamyarPhoneText.setText(Functions.toPersian(hamyarPhoneS));
        hamyarText.setText(hamyarS);
        email.setText("ایمیل: " + emailS);
        Functions.putInPicasso(context, null, image, R.drawable.doctor_detail);
    }

    public void initializeVars(){
        name = (TextView) findViewById(R.id.name_detail);
        cost = (TextView) findViewById(R.id.cost_detail);
        hamyarTitle = (TextView) findViewById(R.id.hamyar_title);
        hamyarText = (TextView) findViewById(R.id.hamyar);
        hamyarPhoneTitle = (TextView) findViewById(R.id.hamyar_phone_title);
        hamyarPhoneText = (TextView) findViewById(R.id.hamyar_phone);
        email = (TextView) findViewById(R.id.email_detail);
        costEditText = (TextInputEditText) findViewById(R.id.cost_edit_text);
        costLayout = (TextInputLayout) findViewById(R.id.cost_layout);
        image = (CircleImageView) findViewById(R.id.doctor_pic_detail);

        acceptLayout = (FrameLayout) findViewById(R.id.accept_layout);
    }

    public void changeFonts(){
        name.setTypeface(App.getFont(3));
        cost.setTypeface(App.getFont(3));
        hamyarTitle.setTypeface(App.getFont(3));
        hamyarText.setTypeface(App.getFont(3));
        hamyarPhoneTitle.setTypeface(App.getFont(3));
        hamyarPhoneText.setTypeface(App.getFont(3));
    }

    public boolean validateCost(String cost) {
        Integer mCost = Integer.parseInt(cost);
        if (mCost > Integer.parseInt(costS) || mCost<0) {
            costLayout.setError(Functions.setFont("قیمت پرداختی ناصحیح می‌باشد", 3));
            return false;
        }
        return true;
    }

    public void addListeners(){
        acceptLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validateCost(costEditText.getText().toString())) {
                    ConditionDetail.pay(view.getContext(),
                            Storage.getID(),
                            recieverID,
                            requirementId,
                            Integer.parseInt(costEditText.getText().toString()),
                            new ConditionDetail.GetPayment() {
                                @Override
                                public void onHttpResponse(Boolean success) {
                                    if (success) {
                                        finish();
                                    }
                                }
                            });
                }
//                finish();
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
        setContentView(R.layout.activity_doctor_detail);
        Intent intent = getIntent();
        nameS = intent.getExtras().getString("name");
        hamyarS = intent.getExtras().getString("hamyar_name");
        hamyarPhoneS = intent.getExtras().getString("hamyar_phone");
        costS = intent.getExtras().getString("cost");
        emailS = intent.getExtras().getString("email");
        recieverID = Integer.parseInt(intent.getExtras().getString("receiverID"));
        requirementId = Integer.parseInt(intent.getExtras().getString("requirementID"));
        initializeVars();
        list_init();
        changeFonts();
        addListeners();
    }
}
