package com.darmaneh.ava.call;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.darmaneh.ava.App;
import com.darmaneh.ava.MainActivity;
import com.darmaneh.ava.R;
import com.darmaneh.dialogs.DarmanehProgressDialog;
import com.darmaneh.fragments.RequestFailureDialog;
import com.darmaneh.models.call.ServerInfoModel;
import com.darmaneh.requests.GetRoomNumberRequest;
import com.darmaneh.requests.GetServerInfoRequest;
import com.darmaneh.requests.Utility;
import com.darmaneh.service.DoctorOnlineService;
import com.darmaneh.utilities.Analytics;
import com.darmaneh.utilities.Functions;
import com.darmaneh.utilities.Storage;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ChooseCallType extends AppCompatActivity {

    String TAG = ChooseCallType.class.getSimpleName();

    Context context;
    String roomNumber, doctorName;
    Intent intent;

    public void changeFonts(){
        ((TextView)findViewById(R.id.call_form_text)).setTypeface(App.getFont(3));
        ((TextView)findViewById(R.id.voice_call_text)).setTypeface(App.getFont(3));
        ((TextView)findViewById(R.id.video_call_text)).setTypeface(App.getFont(3));
    }

    public void getServersInfo(){
        GetServerInfoRequest.get_server_info(ChooseCallType.this, getServerInfo, true);
    }

    GetServerInfoRequest.GetServerInfo getServerInfo = new GetServerInfoRequest.GetServerInfo() {
        @Override
        public void onHttpResponse(Boolean success, List<ServerInfoModel> serverInfoModels) {
            if(success){
                List<String> stringServerModels = new ArrayList<>();
                for(ServerInfoModel serverInfoModel: serverInfoModels){
                    stringServerModels.add(serverInfoModel.toString());
                }
                Log.e(TAG, stringServerModels.toString());
                intent.putStringArrayListExtra("server_list", (ArrayList<String>) stringServerModels);
                context.startActivity(intent);
            }else{
                RequestFailureDialog dialogFragment = new RequestFailureDialog();
                dialogFragment.setRetryFunction(new RequestFailureDialog.RetryOnClickInterface() {
                    @Override
                    public void onClick() {
                        GetServerInfoRequest.get_server_info(ChooseCallType.this, getServerInfo,true);
                    }
                });
                dialogFragment.show(getSupportFragmentManager(), "RequestFailureDialog");
            }
        }
    };

    public void addListener(){
        findViewById(R.id.video_call_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Analytics.sendScreenName("telemedicine/call_type/video");
//                intent = new Intent(context, RtcActivity.class);
//                intent.putExtra("roomNumber", roomNumber);
//                intent.putExtra("doctorName", doctorName);
//                intent.putExtra("type", "video");
//                getServersInfo();
            }
        });

        findViewById(R.id.voice_call_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Analytics.sendScreenName("telemedicine/call_type/voice");
//                intent = new Intent(context, RtcActivity.class);
//                intent.putExtra("roomNumber", roomNumber);
//                intent.putExtra("doctorName", doctorName);
//                intent.putExtra("type", "audio");
//                getServersInfo();
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_call_type);
        context = this;
        Intent intent = getIntent();
        roomNumber = intent.getStringExtra("roomNumber");
        doctorName = intent.getStringExtra("doctorName");
        if (roomNumber!=null && doctorName!=null) {
            changeFonts();
            addListener();
        }
    }

    @Override
    public void onResume(){
        super.onResume();
        Analytics.sendScreenName("telemedicine/call_type");
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(context, MainActivity.class);
        startActivity(intent);
        finish();
    }

}
