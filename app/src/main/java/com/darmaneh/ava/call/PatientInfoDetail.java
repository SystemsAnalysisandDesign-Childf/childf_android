package com.darmaneh.ava.call;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.darmaneh.adapters.AddInfoDetailCheckBoxAdapter;
import com.darmaneh.adapters.AddSymptomCheckBoxAdapter;
import com.darmaneh.adapters.InfoAutoCompleteAdapter;
import com.darmaneh.ava.App;
import com.darmaneh.ava.R;
import com.darmaneh.fragments.RequestFailureDialog;
import com.darmaneh.models.call.InstantInfo;
import com.darmaneh.requests.InfoAutoComplete;
import com.darmaneh.requests.InfoDetailPost;
import com.darmaneh.requests.Utility;
import com.darmaneh.utilities.Analytics;
import com.darmaneh.utilities.Functions;

import java.util.ArrayList;
import java.util.List;

public class PatientInfoDetail extends AppCompatActivity implements AddInfoDetailCheckBoxAdapter.OnDeleteRecyclerViewListener {

    private static final String TAG = PatientInfoDetail.class.getSimpleName();
    AutoCompleteTextView actv;
    TextView noDataText ,title;
    RecyclerView infoDetailRecyclerView;
    AddInfoDetailCheckBoxAdapter addInfoDetailCheckBoxAdapter;
    List<InstantInfo> modelList = new ArrayList<>(), recyclerViewModelList = new ArrayList<>();
    Integer requestType = 0;
    ArrayList<String> requestModel = new ArrayList<>();
    String parent;
    PatientDetailConnectionInfo patientDetailConnectionInfo;

    public void fillModel(){
        for (String string: requestModel){
            recyclerViewModelList.add(InstantInfo.toJson(string));
        }
        addAdapter();
    }

    private void init_info_list() {
        InfoAutoComplete.get_auto_complete_info(this, getInfo, patientDetailConnectionInfo.getGetUrl());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient_info_detail);
        Bundle extras = getIntent().getExtras();
        if(extras == null) {
            requestType = null;
            requestModel = null;
            parent = null;
        } else {
            requestType = extras.getInt("type");
            parent = extras.getString("parent");
            requestModel = extras.getStringArrayList("model");
            if (requestModel.size()!=0){
                findViewById(R.id.info_detail_no_data_txt).setVisibility(View.GONE);
            }
        }
        patientDetailConnectionInfo = new PatientDetailConnectionInfo(requestType);
        initialize();
    }

    public void sendAnalaytics(){
        switch (parent){
            case "PatientInfoList":
                Analytics.sendScreenName("telemedicine/" + patientDetailConnectionInfo.getAnalytics());
                break;
            case "BasicInfoFragment":
                Analytics.sendScreenName("profile/" + patientDetailConnectionInfo.getAnalytics());
                break;
        }
    }

    @Override
    public void onResume(){
        super.onResume();
        sendAnalaytics();
    }

    private void list_init(List<InstantInfo> autoCompleteModelList) {
        Log.d(TAG, String.valueOf(autoCompleteModelList.size()));
        modelList = autoCompleteModelList;
        InfoAutoCompleteAdapter adapter = new InfoAutoCompleteAdapter(this,modelList);
        actv.setThreshold(1);
        actv.setAdapter(adapter);
    }

    InfoAutoComplete.GetInfo getInfo = new InfoAutoComplete.GetInfo() {
        @Override
        public void onHttpResponse(Boolean success, List<InstantInfo> autoCompleteModels) {
            if(success){
                list_init(autoCompleteModels);
            }else{
                RequestFailureDialog dialogFragment = new RequestFailureDialog();
                dialogFragment.setRetryFunction(new RequestFailureDialog.RetryOnClickInterface() {
                    @Override
                    public void onClick() {
                        init_info_list();
                    }
                });
                dialogFragment.show(getSupportFragmentManager(), "RequestFailureDialog");
            }
        }
    };

    public void addAdapter(){
        AddInfoDetailCheckBoxAdapter adapter = new AddInfoDetailCheckBoxAdapter(this, (ArrayList<InstantInfo>) recyclerViewModelList);
        infoDetailRecyclerView.setAdapter(adapter);
    }

    private void addInfo(InstantInfo info) {
        if (!recyclerViewModelList.contains(info)) {
            recyclerViewModelList.add(info);
        }
        if (recyclerViewModelList.size() != 0){
            findViewById(R.id.info_detail_no_data_txt).setVisibility(View.GONE);
        }
        addAdapter();
//        infoDetailRecyclerView.notifyAll();
    }

    public boolean listChanged(){
        if (recyclerViewModelList.size() == requestModel.size()){
            for (int i = 0; i < recyclerViewModelList.size(); i++) {
                if (!recyclerViewModelList.get(i).toString().equals(requestModel.get(i))){
                    return true;
                }
            }
            return false;
        }else {
            return true;
        }
    }

    public void uploadInfo(){
        if (listChanged()) {
            List<Integer> ids = new ArrayList<>();
            for (int i = 0; i < recyclerViewModelList.size(); i++) {
                ids.add(recyclerViewModelList.get(i).getId());
            }
            InfoDetailPost.post_info_detail(PatientInfoDetail.this, ids, patientDetailConnectionInfo.getKey(), patientDetailConnectionInfo.getPostUrl(), new InfoDetailPost.InfoDetailUploaded() {
                @Override
                public void onHttpResponse(Boolean success) {
                    if (success) {
                        Toast.makeText(PatientInfoDetail.this, "اطلاعات شما ذخیره شد.",
                                Toast.LENGTH_SHORT).show();
                        finish();
                    } else {
                        RequestFailureDialog dialogFragment = new RequestFailureDialog();
                        dialogFragment.setRetryFunction(new RequestFailureDialog.RetryOnClickInterface() {
                            @Override
                            public void onClick() {
                                uploadInfo();
                            }
                        });
                        dialogFragment.show(getSupportFragmentManager(), "RequestFailureDialog");
                    }
                }
            });
        } else {
            finish();
        }
    }

    public void initialize(){

        init_info_list();

        ((TextView)findViewById(R.id.accept_text)).setTypeface(App.getFont(3));

        findViewById(R.id.accept_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                uploadInfo();
            }
        });

        findViewById(R.id.back_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        actv = (AutoCompleteTextView) findViewById(R.id.info_detail_auto_complete_text);
        actv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                InstantInfo model = (InstantInfo) adapterView.getItemAtPosition(position);
                actv.setText("");
                addInfo(model);
            }
        });

        actv.setTypeface(App.getFont(3));
        noDataText = (TextView) findViewById(R.id.info_detail_no_data_txt);
        noDataText.setTypeface(App.getFont(4));

        title = (TextView) findViewById(R.id.patient_info_detail_title);
        title.setText(patientDetailConnectionInfo.getTitle());
        title.setTypeface(App.getFont(3));
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        init_rv();
        fillModel();
    }

    private void init_rv() {
        infoDetailRecyclerView = (RecyclerView) findViewById(R.id.info_detail_list);
        infoDetailRecyclerView.setHasFixedSize(true);

        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        infoDetailRecyclerView.setLayoutManager(llm);

        addInfoDetailCheckBoxAdapter = new AddInfoDetailCheckBoxAdapter(this, (ArrayList<InstantInfo>) recyclerViewModelList);
        infoDetailRecyclerView.setAdapter(addInfoDetailCheckBoxAdapter);
        infoDetailRecyclerView.setRecyclerListener(new RecyclerView.RecyclerListener() {
            @Override
            public void onViewRecycled(RecyclerView.ViewHolder holder) {
            }
        });
    }

    @Override
    public void deleteInfo(final ArrayList<InstantInfo> instantInfos) {
        this.recyclerViewModelList = (List<InstantInfo>) instantInfos;
        if (recyclerViewModelList.size() == 0){
            findViewById(R.id.info_detail_no_data_txt).setVisibility(View.VISIBLE);
        }
        addAdapter();
//        infoDetailRecyclerView.notifyAll();
    }
}
