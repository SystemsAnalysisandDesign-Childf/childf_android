package com.darmaneh.ava.call;

import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.content.PermissionChecker;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.darmaneh.adapters.ImageRecyclerViewAdapter;
import com.darmaneh.adapters.PhotoDescriptionAdapter;
import com.darmaneh.ava.App;
import com.darmaneh.ava.Manifest;
import com.darmaneh.ava.R;
import com.darmaneh.dialogs.DarmanehProgressDialog;
import com.darmaneh.dialogs.ProgressHelper;
import com.darmaneh.fragments.PhotoDescriptionFragment;
import com.darmaneh.fragments.RequestFailureDialog;
import com.darmaneh.models.call.GalleryModel;
import com.darmaneh.models.call.ImageCategoryModel;
import com.darmaneh.models.call.ImageModel;
import com.darmaneh.requests.ImageList;
import com.darmaneh.requests.InfoDetailPost;
import com.darmaneh.requests.Template;
import com.darmaneh.requests.Utility;
import com.darmaneh.requests.Variable;
import com.darmaneh.utilities.Analytics;
import com.darmaneh.utilities.Files;
import com.darmaneh.utilities.Functions;
import com.darmaneh.utilities.Storage;
import com.loopj.android.http.AsyncHttpClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.entity.StringEntity;

public class PickImage extends AppCompatActivity implements PhotoDescriptionFragment.OnFragmentInteractionListener {

    private static final String TAG = PickImage.class.getSimpleName();
    private static final int CAMERA_PERMISSION = 4;
    private static final int STORAGE_PERMISSION = 5;
    RecyclerView recyclerView;
    Context context;
    ArrayList<ImageModel> imageModels;
    static Integer imageNum;
    FrameLayout acceptLayout;
    boolean isAcceptOn = false;
    private static final int TAKEPICTURECAMERA = 1, TAKEPICTUREGALLERY = 2;
    private Uri mImageUri;
    DarmanehProgressDialog progressDialog;
    PhotoDescriptionAdapter adapter;
    public String currentPicturePath;
    private ArrayList<String> imageCategories = new ArrayList<>();

    public void initVars() {
        imageModels = new ArrayList<>();
        ((TextView) findViewById(R.id.upload_photo_text)).setTypeface(App.getFont(3));
        ((TextView) findViewById(R.id.upload_title)).setTypeface(App.getFont(3));
        acceptLayout = (FrameLayout) findViewById(R.id.accept_layout);
        imageNum = 0;
    }

    public void initRecyclerView() {
        recyclerView = (RecyclerView) findViewById(R.id.image_recycler_view);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(llm);
    }

    public void addAdapter() {
        adapter = new PhotoDescriptionAdapter(imageModels, this);
        recyclerView.setAdapter(adapter);
    }

    public void addListeners() {
        findViewById(R.id.upload_photo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final CharSequence[] options = {"دریافت عکس از دوربین", "دریافت عکس از گالری", "لغو"};
                AlertDialog.Builder builder = new AlertDialog.Builder(PickImage.this);
                builder.setTitle("افزودن عکس");
                builder.setItems(options, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int item) {
                        if (options[item].equals("دریافت عکس از دوربین")) {
                            try {
                                if (Build.VERSION.SDK_INT > 23 &&
                                        (checkSelfPermission(android.Manifest.permission.CAMERA)) == PermissionChecker.PERMISSION_GRANTED) {
                                    dialog.dismiss();
                                }
                                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                File image = Files.generatePicturePath();
                                if (Build.VERSION.SDK_INT > 23) {
//                                File path = new File(Environment.getExternalStorageDirectory(),"images");
//                                File f = new File(path, "image.jpg");
                                    mImageUri = FileProvider.getUriForFile(PickImage.this,
                                            "com.darmaneh.provider", image);
                                    intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageUri);
                                    intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                                    intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                                } else {
                                    File f = new File(Environment.getExternalStorageDirectory(), "image.jpg");
                                    mImageUri = Uri.fromFile(f);
                                    intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageUri);
                                }
                                currentPicturePath = image.getAbsolutePath();
                                startActivityForResult(intent, TAKEPICTURECAMERA);
                            } catch (Exception e) {
                                Log.e(TAG, e.toString());
                            }
                        } else if (options[item].equals("دریافت عکس از گالری")) {
                            Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            startActivityForResult(intent, TAKEPICTUREGALLERY);
                        } else if (options[item].equals("لغو")) {
                            dialog.dismiss();
                        }
                    }

                });

                builder.show();
            }
        });

        acceptLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isAcceptOn) {
                    Intent intent = getIntent();
                    intent.putExtra("imageNum", imageNum.toString());
                    setResult(RESULT_OK, intent);
                    finish();
                }
            }
        });

        findViewById(R.id.back_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onResume(){
        super.onResume();
        Analytics.sendScreenName("telemedicine/image");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == TAKEPICTURECAMERA && resultCode == RESULT_OK) {
            try {
                if (Build.VERSION.SDK_INT > 23) {
                    Bitmap takenImage = BitmapFactory.decodeFile(currentPicturePath);
                    progressDialog = new DarmanehProgressDialog(this);
                    progressDialog.show();
                    AsyncCompress compress = new AsyncCompress();
                    compress.execute(takenImage);
                } else {
                    getContentResolver().notifyChange(mImageUri, null);
                    ContentResolver cr = getContentResolver();
                    Bitmap bitmap = null;

                    bitmap = MediaStore.Images.Media.getBitmap(cr, mImageUri);
                    progressDialog = new DarmanehProgressDialog(this);
                    progressDialog.show();
                    AsyncCompress compress = new AsyncCompress();
                    compress.execute(bitmap);

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (requestCode == TAKEPICTUREGALLERY && resultCode == RESULT_OK && data != null && data.getData() != null) {
            Uri filePath = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                progressDialog = new DarmanehProgressDialog(this);
                progressDialog.show();
                AsyncCompress compress = new AsyncCompress();
                compress.execute(bitmap);
            } catch (OutOfMemoryError | Exception e) {
                e.printStackTrace();
            }
        }

        if (requestCode == CAMERA_PERMISSION && resultCode == RESULT_OK) {
            Log.d(TAG, "camera permission OK");
        }
        if (requestCode == STORAGE_PERMISSION && resultCode == RESULT_OK) {
            Log.d(TAG, "storage permission OK");
        }
    }

//    public void changeFonts(){
//        ((TextView) findViewById(R.id.upload_photo_title)).setTypeface(App.getFont(3));
//    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
        setContentView(R.layout.activity_pick_image);
        initVars();
        addPermissions();
        getDataFromServer();
        initRecyclerView();
        addAdapter();
        addListeners();
    }

    private void addPermissions() {
        int cameraPermissionInt = PermissionChecker.checkSelfPermission(context, android.Manifest.permission.CAMERA);
        if (cameraPermissionInt == PermissionChecker.PERMISSION_GRANTED) {
            Log.d(TAG, "Camera permission is OK");
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.CAMERA},
                    CAMERA_PERMISSION);
        }
        int storagePermissionInt = PermissionChecker.checkSelfPermission(context, android.Manifest.permission.READ_EXTERNAL_STORAGE);
        if (storagePermissionInt == PermissionChecker.PERMISSION_GRANTED) {
            Log.d(TAG, "STORAGE permission is OK");
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE},
                    STORAGE_PERMISSION);
        }
    }

    @Override
    public void onPhotoCallback(String desc, String category, Bitmap bitmap) {
        imageModels.add(new ImageModel(bitmap, category, "", desc, ""));
        adapter.notifyDataSetChanged();
        Log.d(TAG, "image model updated");
        acceptLayout.setBackgroundColor(getResources().getColor(R.color.color_button_green));
        isAcceptOn = true;
        imageNum++;
    }

    public void getDataFromServer() {
        InfoDetailPost.getImageCategories(this, new InfoDetailPost.ImageCategories() {
            @Override
            public void onHttpResponse(Boolean success, List<ImageCategoryModel> models) {
                if(success){
                    imageCategories.clear();
                    imageCategories.add("دسته را انتخاب کنید");
                    for(ImageCategoryModel model: models){
                        imageCategories.add(model.getName());
                    }
                }else{
                    RequestFailureDialog dialogFragment = new RequestFailureDialog();
                    dialogFragment.setRetryFunction(new RequestFailureDialog.RetryOnClickInterface() {
                        @Override
                        public void onClick() {
                            getDataFromServer();
                        }
                    });
                    dialogFragment.show(getSupportFragmentManager(), "RequestFailureDialog");
                }
            }
        });
    }

    private class AsyncCompress extends AsyncTask<Bitmap, String, ImageTypes> {

        @Override
        protected ImageTypes doInBackground(Bitmap... params) {
            Bitmap bitmap = params[0];
            Bitmap.CompressFormat format;
            String base;
            if (bitmap.getByteCount() > 55000000) {
                Log.d(TAG, String.valueOf(bitmap.getByteCount()));
                return null;
            }
            format = Bitmap.CompressFormat.JPEG;
            base = "data:image/jpg;base64,";
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            bitmap.compress(format, 75, byteArrayOutputStream);
            byte[] byteArray = byteArrayOutputStream.toByteArray();
            Log.d(TAG, String.valueOf(byteArray.length));
            String encoded = base + Base64.encodeToString(byteArray, Base64.DEFAULT);
            return new ImageTypes(encoded, byteArray);
        }

        @Override
        protected void onPostExecute(ImageTypes imageTypes) {
            if (imageTypes != null) {
                String encoded = imageTypes.getEncoded();
                Log.d("base64", String.valueOf(encoded.length()));
                PhotoDescriptionFragment fragment = PhotoDescriptionFragment
                        .newInstance(imageTypes.getByteArray(), imageTypes.getEncoded(), imageCategories);
                fragment.show(getSupportFragmentManager(), "photo_description");
            } else {
                Toast.makeText(context, "حجم عکس شما زیاد است، دقت کنید که حجم همه عکس ها باید کمتر از 8 مگابایت باشد",
                        Toast.LENGTH_LONG).show();
            }
            progressDialog.dismiss();
        }
    }

    private class ImageTypes {
        private String encoded;
        private byte[] byteArray;

        public ImageTypes(String encoded, byte[] byteArray) {
            this.encoded = encoded;
            this.byteArray = byteArray;
        }

        public String getEncoded() {
            return encoded;
        }

        public byte[] getByteArray() {
            return byteArray;
        }
    }
}
