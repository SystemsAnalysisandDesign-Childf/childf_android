package com.darmaneh.ava.call;

/**
 * Created by root on 8/7/17.
 */

public class PatientDetailConnectionInfo {

    String key;
    String getUrl;
    String postUrl;
    String title;

    public String getAnalytics() {
        return analytics;
    }

    String analytics;

    public String getKey() {
        return key;
    }

    public String getGetUrl() {
        return getUrl;
    }

    public String getPostUrl() {
        return postUrl;
    }

    public String getTitle() {
        return title;
    }

    public PatientDetailConnectionInfo(int type){
        switch (type){
            case 0:
                this.title = "بیماری های گذشته";
                this.key = "conditions";
                this.getUrl = "content_pool/conditions/";
                this.postUrl = "user/patient/add/condition/";
                this.analytics = "conditions";
                break;
            case 1:
                this.title = "داروهای مصرف شده";
                this.key = "medications";
                this.getUrl = "content_pool/medications/";
                this.postUrl = "user/patient/add/medication/";
                this.analytics = "medications";
                break;
            case 2:
                this.title = "حساسیت ها";
                this.key = "allergies";
                this.getUrl = "content_pool/allergies/";
                this.postUrl = "user/patient/add/allergy/";
                this.analytics = "surgeries";
                break;
            case 3:
                this.title = "عمل های پزشکی گذشته";
                this.key = "surgeries";
                this.getUrl = "content_pool/surgeries/";
                this.postUrl = "user/patient/add/surgery/";
                this.analytics = "allergies";
                break;
        }
    }

}
