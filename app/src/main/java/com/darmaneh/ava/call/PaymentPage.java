package com.darmaneh.ava.call;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.darmaneh.ava.App;
import com.darmaneh.ava.R;
import com.darmaneh.dialogs.DarmanehProgressDialog;
import com.darmaneh.dialogs.DarmanehToast;
import com.darmaneh.dialogs.NoInternetDialog;
import com.darmaneh.fragments.RequestFailureDialog;
import com.darmaneh.models.call.CouponModel;
import com.darmaneh.models.call.DoctorInfoDetailModel;
import com.darmaneh.models.call.ProfessionModel;
import com.darmaneh.requests.Payment;
import com.darmaneh.requests.Utility;
import com.darmaneh.requests.Variable;
import com.darmaneh.utilities.Analytics;
import com.darmaneh.utilities.Functions;
import com.darmaneh.utilities.Storage;
import com.zarinpal.ewallets.purchase.OnCallbackRequestPaymentListener;
import com.zarinpal.ewallets.purchase.PaymentRequest;
import com.zarinpal.ewallets.purchase.ZarinPal;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class PaymentPage extends AppCompatActivity {

    Context context;
    DoctorInfoDetailModel doctorDetailInfo;
    TextView doctorName, doctorExperties, paymentFee,  validationResultText;
    EditText couponCodeText;
    CircleImageView doctorImage;
    FrameLayout payLayout;
    Integer mainPrice, payedPrice;
    String acceptedCode = "";

    private static DarmanehProgressDialog progress;

    public void changeFonts(){
        ((TextView)findViewById(R.id.last_review_text)).setTypeface(App.getFont(3));
        ((TextView)findViewById(R.id.validate_coupon_text)).setTypeface(App.getFont(3));
        ((TextView)findViewById(R.id.payment_fee_text)).setTypeface(App.getFont(3));
        couponCodeText.setTypeface(App.getFont(3));
        doctorName.setTypeface(App.getFont(3));
        doctorExperties.setTypeface(App.getFont(3));
        validationResultText.setTypeface(App.getFont(3));
//        ((TextView)findViewById(R.id.call_time)).setTypeface(App.getFont(3));
//        ((TextView)findViewById(R.id.payment_text)).setTypeface(App.getFont(3));
        paymentFee.setTypeface(App.getFont(3));
        ((TextView)findViewById(R.id.pay_text)).setTypeface(App.getFont(3));
    }

    public void calculatePayedPrice(String type, Integer value){
        if (value == 0){
            payedPrice = mainPrice;
            paymentFee.setText(Functions.toPersian(Integer.parseInt(payedPrice.toString())));
        } else{
            if (type.equals("monetary")){
                Log.e("type", "monetary");
                payedPrice = mainPrice - value;
                Log.e("payedPrice", String.valueOf(payedPrice));
                if (payedPrice<100)
                    payedPrice = 0;
            } else if(type.equals("percentage")){
                Log.e("type", "percentage");
                if (value >= 100)
                    payedPrice = 0;
                else{
                    payedPrice = Math.round(mainPrice * (100 - value) / 100);
                    if (payedPrice<100)
                        payedPrice = 0;
                }
            }
            paymentFee.setText(Functions.toPersian(Integer.parseInt(payedPrice.toString())));
        }
    }

    public void list_init(DoctorInfoDetailModel doctorInfoDetailModel){
        doctorName.setText(doctorInfoDetailModel.getFirstName() + " " +doctorInfoDetailModel.getLastName());
        List<ProfessionModel> professions = doctorInfoDetailModel.getProfession();
        for (int i = 0; i < professions.size(); i++) {
            if (i==0){
                doctorExperties.setText(professions.get(i).getName());
            } else {
                doctorExperties.setText(doctorExperties.getText() + "/" + professions.get(i).getName());
            }
        }
        Functions.putInPicasso(context, doctorInfoDetailModel.getImage(), doctorImage, R.drawable.payment);
        mainPrice = doctorInfoDetailModel.getPrice();
        calculatePayedPrice("", 0);
    }

    public void validateCouponCode(){
        Payment.check_coupon_validity(context, couponCodeText.getText().toString(), new Payment.CheckCouponValidity() {
            @Override
            public void onHttpResponse(Boolean success, CouponModel couponInfo) {
                if (success) {
                    validationResultText.setTextColor(ContextCompat.getColor(context, R.color.color_button_green));
                    validationResultText.setText("کد تخفیف صحیح است.");
                    acceptedCode = couponCodeText.getText().toString();
                    calculatePayedPrice(couponInfo.getType(), couponInfo.getValue());

                }else{
                    RequestFailureDialog dialogFragment = new RequestFailureDialog();
                    dialogFragment.setRetryFunction(new RequestFailureDialog.RetryOnClickInterface() {
                        @Override
                        public void onClick() {
                            validateCouponCode();
                        }
                    });
                    dialogFragment.show(getSupportFragmentManager(), "RequestFailureDialog");
                }
            }

            @Override
            public void on404(CouponModel couponInfo) {
                validationResultText.setTextColor(ContextCompat.getColor(context, R.color.color_dark_red));
                validationResultText.setText("کد تخفیف وجود ندارد.");
                couponCodeText.setText("");
            }

            @Override
            public void on403(CouponModel couponInfo) {
                validationResultText.setTextColor(ContextCompat.getColor(context, R.color.color_dark_red));
                validationResultText.setText("این کد تخفیف متعلق به شما نیست.");
                couponCodeText.setText("");
            }

            @Override
            public void on401(CouponModel couponInfo) {
                validationResultText.setTextColor(ContextCompat.getColor(context, R.color.color_dark_red));
                switch (couponInfo.getDetail()){
                    case "coupon has been expired":
                        validationResultText.setText("مهلت ارسال کد تخفیف به پایان رسیده است.");
                        break;
                    case "coupon has been used":
                        validationResultText.setText("کد تخفیف استفاده شده است.");
                        break;
                }
                couponCodeText.setText("");

            }
        });
    }

    public void setListeners(){

        findViewById(R.id.validate_coupon_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validateCouponCode();

            }
        });

        findViewById(R.id.pay_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Analytics.sendScreenName("telemedicine/payment");
                requestPayment(payedPrice, "هزینه ویزیت دکتر " + doctorName.getText().toString(), null);
            }
        });
    }

    private void requestPayment(final int amount, final String desc, final String email) {

        if (!Storage.getIsNet()) {
            (new NoInternetDialog()).show(context);
            return;
        }

        progress = new DarmanehProgressDialog(context);
        progress.show();

        if (payedPrice == 0) {

            String authority;
            authority = "free";
            Payment.set_dr_and_authority(context, doctorDetailInfo.getId(), authority, acceptedCode, payedPrice, new Payment.SetDrAndAuthority() {
                @Override
                public void onHttpResponse(Boolean success) {
                    if (success) {
                        Intent intent = new Intent(context, WaitForConnection.class);
                        startActivity(intent);
                        ((AppCompatActivity) context).finishAffinity();
                        progress.dismiss();
                    }else{
                        progress.dismiss();
                        RequestFailureDialog dialogFragment = new RequestFailureDialog();
                        dialogFragment.setRetryFunction(new RequestFailureDialog.RetryOnClickInterface() {
                            @Override
                            public void onClick() {
                                requestPayment(amount, desc, email);
                            }
                        });
                        dialogFragment.show(getSupportFragmentManager(), "RequestFailureDialog");
                    }
                }
            });

        }
        else {
            ZarinPal purchase = ZarinPal.getPurchase(this);
            PaymentRequest payment = ZarinPal.getPaymentRequest();

            payment.setMerchantID("a7baf0be-5e1a-11e7-b70a-005056a205be");
            payment.setAmount(amount);
            Log.e("amount", String.valueOf(amount));
            payment.setDescription(desc);
            payment.setCallbackURL(Variable.SERVER_ADDRESS_V1 + "user/patient/verify_visit_pay/");

            if (!Storage.getPhoneNum().equals("")) {
                payment.setMobile(Storage.getCompletePhoneNum("en"));
            }
            if (email != null) {
                payment.setEmail(email);
            }

            purchase.startPayment(payment, new OnCallbackRequestPaymentListener() {
                @Override
                public void onCallbackResultPaymentRequest(int status,
                                                           String authority,
                                                           Uri paymentGatewayUri,
                                                           Intent intent) {
                    if (status == 100) {
                        final Intent zarinpalIntent = intent;
                        Payment.set_dr_and_authority(context, doctorDetailInfo.getId(), authority, acceptedCode, payedPrice, new Payment.SetDrAndAuthority() {
                            @Override
                            public void onHttpResponse(Boolean success) {
                                if (success) {
                                    startActivity(zarinpalIntent);
                                    ((AppCompatActivity) context).finishAffinity();
                                    progress.dismiss();
                                } else {
                                    progress.dismiss();
                                    RequestFailureDialog dialogFragment = new RequestFailureDialog();
                                    dialogFragment.setRetryFunction(new RequestFailureDialog.RetryOnClickInterface() {
                                        @Override
                                        public void onClick() {
                                            requestPayment(amount, desc, email);
                                        }
                                    });
                                    dialogFragment.show(getSupportFragmentManager(), "RequestFailureDialog");
                                }
                            }
                        });
                    } else {
                        DarmanehToast.makeText(context, "پرداخت ناموفق!", Toast.LENGTH_LONG);
                        progress.dismiss();
                    }
                }
            });
        }

    }

    public void initializeVars(){
        doctorName = (TextView) findViewById(R.id.doctor_name);
        doctorExperties = (TextView) findViewById(R.id.doctor_experties);
        paymentFee = (TextView) findViewById(R.id.payment_fee);
        couponCodeText = (EditText) findViewById(R.id.coupon_code_text);
        validationResultText = (TextView) findViewById(R.id.validation_result_text);

        doctorImage = (CircleImageView) findViewById(R.id.profile_image);

        payLayout = (FrameLayout) findViewById(R.id.pay_button);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        context = this;
        String doctorDetailInfoString = getIntent().getStringExtra("personalInfo");
        initializeVars();
        if (doctorDetailInfoString!=null) {
            doctorDetailInfo = DoctorInfoDetailModel.toJson(doctorDetailInfoString);
            list_init(doctorDetailInfo);
        }
        changeFonts();
        setListeners();
    }
}
