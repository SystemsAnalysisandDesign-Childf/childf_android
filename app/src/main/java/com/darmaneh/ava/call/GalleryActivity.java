package com.darmaneh.ava.call;

import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.content.PermissionChecker;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.darmaneh.adapters.GalleryAdapter;
import com.darmaneh.ava.App;
import com.darmaneh.ava.R;
import com.darmaneh.dialogs.DarmanehProgressDialog;
import com.darmaneh.dialogs.DarmanehToast;
import com.darmaneh.fragments.PhotoDescriptionFragment;
import com.darmaneh.fragments.RequestFailureDialog;
import com.darmaneh.models.call.GalleryModel;
import com.darmaneh.models.call.ImageCategoryModel;
import com.darmaneh.requests.ImageList;
import com.darmaneh.requests.InfoDetailPost;
import com.darmaneh.utilities.Files;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class GalleryActivity extends AppCompatActivity implements PhotoDescriptionFragment.OnFragmentInteractionListener {

    RecyclerView galleryRV;
    List<GalleryModel> galleryModelList = new ArrayList<>();
    private static final int CAMERA_PERMISSION = 4;
    private static final int STORAGE_PERMISSION = 5;
    private static final int TAKEPICTURECAMERA = 1, TAKEPICTUREGALLERY = 2;
    private Uri mImageUri;
    public String currentPicturePath;
    DarmanehProgressDialog progressDialog;
    private ArrayList<String> imageCategories = new ArrayList<>();
    ImageButton deletePics, addBtn, backBtn;
    GalleryAdapter adapter;
    TextView deleteBtn, dismissBtn;
    LinearLayout deleteModeLayout;
    boolean deleteMode = false;
    int numberFailure, numberSuccess = 0;
    final String TAG = GalleryActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);
        init_views();
        setOnClicks();
        getDataFromServer();
        init_gallery_list();
    }

    private void setOnClicks() {
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        deletePics.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!deleteMode) {
                    adapter.deleteMode = true;
                    deleteMode = true;
                    adapter.notifyDataSetChanged();
                    deleteModeLayout.setVisibility(View.VISIBLE);
                }
            }
        });

        deleteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                numberFailure = 0;
                numberSuccess = 0;
                List<String> list = adapter.getTobeHiddenList();
                final int listCount = list.size();
                ImageList.delete_image(GalleryActivity.this, list, new ImageList.DeleteCallback() {
                    @Override
                    public void onHttpResponse(Boolean success) {
                        Log.d("gallery", numberFailure + " , "+ numberSuccess);
                        if(success){
                            numberSuccess++;
                        }else{
                            numberFailure++;
                        }
                        if(numberFailure + numberSuccess == listCount ) {
                            if (numberFailure == listCount) {
                                DarmanehToast.makeText(GalleryActivity.this, "متاسفانه هیچ عکسی پاک نشد", Toast.LENGTH_SHORT);
                            } else if (numberFailure == 0) {
                                DarmanehToast.makeText(GalleryActivity.this, "عکس ها با موفقیت حذف شدند", Toast.LENGTH_SHORT);
                                init_gallery_list();
                            } else {
                                DarmanehToast.makeText(GalleryActivity.this,
                                        numberFailure + " عکس حذف نشد", Toast.LENGTH_SHORT);
                                init_gallery_list();
                            }
                        }
                    }
                });
                dismissBtn.callOnClick();
            }
        });

        dismissBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteMode = false;
                adapter.deleteMode = false;
                adapter.notifyDataSetChanged();
                deleteModeLayout.setVisibility(View.GONE);
            }
        });

        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addPermissions();
                final CharSequence[] options = {"دریافت عکس از دوربین", "دریافت عکس از گالری", "لغو"};
                AlertDialog.Builder builder = new AlertDialog.Builder(GalleryActivity.this);
                builder.setTitle("افزودن عکس");
                builder.setItems(options, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int item) {
                        if (options[item].equals("دریافت عکس از دوربین")) {
                            try {
                                if (Build.VERSION.SDK_INT > 23 &&
                                        (checkSelfPermission(android.Manifest.permission.CAMERA)) == PermissionChecker.PERMISSION_GRANTED) {
                                    dialog.dismiss();
                                }
                                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                File image = Files.generatePicturePath();
                                if (Build.VERSION.SDK_INT > 23) {
//                                File path = new File(Environment.getExternalStorageDirectory(),"images");
//                                File f = new File(path, "image.jpg");
                                    mImageUri = FileProvider.getUriForFile(GalleryActivity.this,
                                            "com.darmaneh.provider", image);
                                    intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageUri);
                                    intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                                    intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                                } else {
                                    File f = new File(Environment.getExternalStorageDirectory(), "image.jpg");
                                    mImageUri = Uri.fromFile(f);
                                    intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageUri);
                                }
                                currentPicturePath = image.getAbsolutePath();
                                startActivityForResult(intent, TAKEPICTURECAMERA);
                            } catch (Exception e) {
                                Log.e(TAG, e.toString());
                            }
                        } else if (options[item].equals("دریافت عکس از گالری")) {
                            Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            startActivityForResult(intent, TAKEPICTUREGALLERY);
                        } else if (options[item].equals("لغو")) {
                            dialog.dismiss();
                        }
                    }

                });

                builder.show();

            }
        });
    }

    private void init_views() {
        deletePics = (ImageButton) findViewById(R.id.delete_pic);
        addBtn = (ImageButton) findViewById(R.id.add_pic);
        ((TextView) findViewById(R.id.toolbar_title)).setTypeface(App.getFont(4));
        backBtn = (ImageButton) findViewById(R.id.back_button);
        deleteBtn = (TextView) findViewById(R.id.delete_btn);
        dismissBtn = (TextView) findViewById(R.id.dismiss_btn);
        deleteModeLayout = (LinearLayout) findViewById(R.id.delete_mode_layout);
        deleteBtn.setTypeface(App.getFont(3));
        dismissBtn.setTypeface(App.getFont(3));
    }

    private void init_rv() {
        galleryRV = (RecyclerView) findViewById(R.id.gallery_rv);
        GridLayoutManager glm = new GridLayoutManager(this,2);
        adapter = new GalleryAdapter(this,galleryModelList);
        galleryRV.setAdapter(adapter);
        galleryRV.setLayoutManager(glm);
    }

    @Override
    public void onBackPressed() {
        if(deleteMode)
            dismissBtn.callOnClick();
        else {
            Intent intent = getIntent();
            intent.putExtra("imageNum", String.valueOf(galleryModelList.size()));
            setResult(RESULT_OK, intent);
            finish();
        }
    }

    ImageList.GalleryGot getGalleryList = new ImageList.GalleryGot() {
        @Override
        public void onHttpResponse(Boolean success, List<GalleryModel> modelList) {
            if(success){
                galleryModelList = modelList;
                Log.d("gallery" , String.valueOf(galleryModelList.size()));
                init_rv();
            }else{
                RequestFailureDialog dialogFragment = new RequestFailureDialog();
                dialogFragment.setRetryFunction(new RequestFailureDialog.RetryOnClickInterface() {
                    @Override
                    public void onClick() {
                        init_gallery_list();
                    }
                });
                dialogFragment.show(getSupportFragmentManager(), "RequestFailureDialog");
            }
        }
    };

    private void init_gallery_list() {
        ImageList.get_gallery(this, getGalleryList);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == TAKEPICTURECAMERA && resultCode == RESULT_OK) {
            try {
                if (Build.VERSION.SDK_INT > 23) {
                    Bitmap takenImage = BitmapFactory.decodeFile(currentPicturePath);
                    progressDialog = new DarmanehProgressDialog(this);
                    progressDialog.show();
                    AsyncCompress compress = new AsyncCompress();
                    compress.execute(takenImage);
                } else {
                    getContentResolver().notifyChange(mImageUri, null);
                    ContentResolver cr = getContentResolver();
                    Bitmap bitmap = null;

                    bitmap = MediaStore.Images.Media.getBitmap(cr, mImageUri);
                    progressDialog = new DarmanehProgressDialog(this);
                    progressDialog.show();
                    AsyncCompress compress = new AsyncCompress();
                    compress.execute(bitmap);

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (requestCode == TAKEPICTUREGALLERY && resultCode == RESULT_OK && data != null && data.getData() != null) {
            Uri filePath = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                progressDialog = new DarmanehProgressDialog(this);
                progressDialog.show();
                AsyncCompress compress = new AsyncCompress();
                compress.execute(bitmap);
            } catch (OutOfMemoryError | Exception e) {
                e.printStackTrace();
            }
        }

        if (requestCode == CAMERA_PERMISSION && resultCode == RESULT_OK) {
            Log.d(TAG, "camera permission OK");
        }
        if (requestCode == STORAGE_PERMISSION && resultCode == RESULT_OK) {
            Log.d(TAG, "storage permission OK");
        }
    }

    private void addPermissions() {
        int cameraPermissionInt = PermissionChecker.checkSelfPermission(this, android.Manifest.permission.CAMERA);
        if (cameraPermissionInt == PermissionChecker.PERMISSION_GRANTED) {
            Log.d(TAG, "Camera permission is OK");
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.CAMERA},
                    CAMERA_PERMISSION);
        }
        int storagePermissionInt = PermissionChecker.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE);
        if (storagePermissionInt == PermissionChecker.PERMISSION_GRANTED) {
            Log.d(TAG, "STORAGE permission is OK");
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE},
                    STORAGE_PERMISSION);
        }
    }


    public void getDataFromServer() {
        InfoDetailPost.getImageCategories(this, new InfoDetailPost.ImageCategories() {
            @Override
            public void onHttpResponse(Boolean success, List<ImageCategoryModel> models) {
                if(success){
                    imageCategories.clear();
                    imageCategories.add("دسته را انتخاب کنید");
                    for(ImageCategoryModel model: models){
                        imageCategories.add(model.getName());
                    }
                }else{
                    RequestFailureDialog dialogFragment = new RequestFailureDialog();
                    dialogFragment.setRetryFunction(new RequestFailureDialog.RetryOnClickInterface() {
                        @Override
                        public void onClick() {
                            getDataFromServer();
                        }
                    });
                    dialogFragment.show(getSupportFragmentManager(), "RequestFailureDialog");
                }
            }
        });
    }

    @Override
    public void onPhotoCallback(String desc, String category, Bitmap bm) {
        init_gallery_list();
    }


    private class AsyncCompress extends AsyncTask<Bitmap, String, ImageTypes> {

        @Override
        protected ImageTypes doInBackground(Bitmap... params) {
            Bitmap bitmap = params[0];
            Bitmap.CompressFormat format;
            String base;
                if (bitmap.getByteCount() > 55000000) {
                    Log.d(TAG, String.valueOf(bitmap.getByteCount()));
                    return null;
                }
                format = Bitmap.CompressFormat.JPEG;
                base = "data:image/jpg;base64,";
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            bitmap.compress(format, 75, byteArrayOutputStream);
            byte[] byteArray = byteArrayOutputStream.toByteArray();
            Log.d(TAG, String.valueOf(byteArray.length));
            String encoded = base + Base64.encodeToString(byteArray, Base64.DEFAULT);
            return new ImageTypes(encoded, byteArray);
        }

        @Override
        protected void onPostExecute(ImageTypes imageTypes) {
            if (imageTypes != null) {
                String encoded = imageTypes.getEncoded();
                Log.d("base64", String.valueOf(encoded.length()));
                PhotoDescriptionFragment fragment = PhotoDescriptionFragment
                        .newInstance(imageTypes.getByteArray(), imageTypes.getEncoded(), imageCategories);
                fragment.show(getSupportFragmentManager(), "photo_description");
            } else {
                Toast.makeText(GalleryActivity.this, "حجم عکس شما زیاد است، دقت کنید که حجم همه عکس ها باید کمتر از 8 مگابایت باشد",
                        Toast.LENGTH_LONG).show();
            }
            progressDialog.dismiss();
        }
    }

    private class ImageTypes {
        private String encoded;
        private byte[] byteArray;

        public ImageTypes(String encoded, byte[] byteArray) {
            this.encoded = encoded;
            this.byteArray = byteArray;
        }

        public String getEncoded() {
            return encoded;
        }

        public byte[] getByteArray() {
            return byteArray;
        }
    }
}
