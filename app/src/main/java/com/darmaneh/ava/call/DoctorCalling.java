package com.darmaneh.ava.call;

import android.content.Context;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.darmaneh.ava.App;
import com.darmaneh.ava.MainActivity;
import com.darmaneh.ava.R;
import com.darmaneh.utilities.Analytics;
import com.darmaneh.utilities.Functions;
import com.darmaneh.utilities.ImageTouchSlider;
import com.squareup.picasso.Picasso;

public class DoctorCalling extends AppCompatActivity {

    Context context;
    Ringtone ringtone;
    Intent intent;
    String roomNumber, doctorName, doctorImageUrl;
    TextView doctorInfoText;
    ImageView doctorImageView;

    public void playRingtone(){
        try {
            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
            ringtone = RingtoneManager.getRingtone(getApplicationContext(), notification);
            ringtone.play();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void initialize(){
        doctorInfoText = (TextView) findViewById(R.id.doctor_calling_info);
        doctorImageView = (ImageView) findViewById(R.id.doctor_calling_image);

        doctorInfoText.setText(doctorName);
    }

    public void changeFonts(){
        ((TextView) findViewById(R.id.calling_title)).setTypeface(App.getFont(3));
        doctorInfoText.setTypeface(App.getFont(3));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctor_calling);
        context = this;
        intent = getIntent();
        roomNumber = intent.getStringExtra("roomNumber");
        doctorName = intent.getStringExtra("doctorName");
        doctorImageUrl = intent.getStringExtra("doctorImageUrl");
        initialize();
        changeFonts();
        playRingtone();
        Functions.putInPicasso(context, doctorImageUrl, doctorImageView, R.drawable.doctor_calling);
        ImageTouchSlider slider = (ImageTouchSlider) findViewById(R.id.slider);
        slider.setOnImageSliderChangedListener(new ImageTouchSlider.OnImageSliderChangedListener() {
            @Override
            public void onChanged() {
                ringtone.stop();
                Intent intent = new Intent(context, ChooseCallType.class);
                intent.putExtra("roomNumber", roomNumber);
                intent.putExtra("doctorName", doctorName);
                startActivity(intent);
            }

        });
    }

    @Override
    public void onResume(){
        super.onResume();
        Analytics.sendScreenName("telemedicine/doctor_calling/" + doctorName);
    }

    @Override
    public void onBackPressed() {}
}
