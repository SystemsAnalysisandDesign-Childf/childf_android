package com.darmaneh.ava;
import android.app.Application;
import android.content.Context;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.preference.PreferenceManager;

import com.darmaneh.models.diagnosis.History;
import com.darmaneh.models.diagnosis.Request;
import com.darmaneh.recievers.NetworkChangeListener;
import com.darmaneh.utilities.Analytics;
import com.darmaneh.utilities.Storage;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.onesignal.OneSignal;

/**
 * Created by pourya on 3/16/17.
 */

public class App extends Application {

    private final String TAG = App.class.getSimpleName();
    public static Request diagnosis_req;
    public static History histories;
    public static SharedPreferences SP;
    public static Context applicationContext;

    private static Typeface typeUltraLight, typeLight, typeRegular, typeMedium, typeBold;

    private static GoogleAnalytics sAnalytics;
    private static Tracker sTracker;
    public static boolean firstUsage = true;
    @Override
    public void onCreate() {
        super.onCreate();
        SP = PreferenceManager.getDefaultSharedPreferences(this);
        sAnalytics = GoogleAnalytics.getInstance(this);
        OneSignal.startInit(this)
                .unsubscribeWhenNotificationsAreDisabled(true)
                .init();
        initial_fonts();
        applicationContext = getApplicationContext();
        // register receiver for connection change
        this.registerReceiver(new NetworkChangeListener(),
                new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));

//        Storage.countLaunched();
//        if (Storage.getLaunched() == 1) {
//            Analytics.sendScreenName("ads/shik_group");
//        }
    }

    private void initial_fonts() {
        // initialize font type face
        typeUltraLight = Typeface.createFromAsset(this.getAssets(), "IRANSansMobile_UltraLight.ttf");
        typeLight = Typeface.createFromAsset(this.getAssets(), "IRANSansMobile_Light.ttf");
        typeRegular = Typeface.createFromAsset(this.getAssets(), "IRANSansMobile.ttf");
        typeMedium = Typeface.createFromAsset(this.getAssets(), "IRANSansMobile_Medium.ttf");
        typeBold = Typeface.createFromAsset(this.getAssets(), "IRANSansMobile_Bold.ttf");
    }

    public static Typeface getFont(int n) {
        switch (n) {
            case 1:
                return typeUltraLight;
            case 2:
                return typeLight;
            case 3:
                return typeRegular;
            case 4:
                return typeMedium;
            case 5:
                return typeBold;
            default:
                return typeRegular;
        }
    }

    public static Tracker getDefaultTracker() {
        if (sTracker == null) {
            sTracker = sAnalytics.newTracker("UA-107595642-1");
            sTracker.enableAutoActivityTracking(false);
            sTracker.enableExceptionReporting(true);
        }
        return sTracker;
    }
}
