package com.darmaneh.ava;

import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.darmaneh.adapters.ConditionDetailAdapter;
import com.darmaneh.models.ConditionDetailModel;
import com.darmaneh.requests.ConditionDetail;
import com.darmaneh.requests.Utility;
import com.darmaneh.requests.Variable;
import com.darmaneh.utilities.Analytics;
import com.darmaneh.utilities.Functions;
import com.darmaneh.utilities.SignInFunction;
import com.darmaneh.utilities.Storage;
import com.google.gson.Gson;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class ConditionDetailActivity extends AppCompatActivity {

    ConditionDetailModel conditionDetailModel;
    RecyclerView conditionDetailItems;
    ConditionDetailAdapter conditionDetailAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_condition_detail);
        initialize();
    }

    private void initialize() {
        conditionDetailModel = (new Gson()).fromJson(getIntent().getExtras().
                        getString("condition_detail"),
                        ConditionDetailModel.class);

        Analytics.sendScreenName("condition_detail/" + conditionDetailModel.getName());

        conditionDetailItems = (RecyclerView) findViewById(R.id.condition_detail_items);
        conditionDetailItems.setHasFixedSize(true);

        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        conditionDetailItems.setLayoutManager(llm);

        conditionDetailAdapter = new ConditionDetailAdapter(conditionDetailModel);
        conditionDetailItems.setAdapter(conditionDetailAdapter);

        findViewById(R.id.share_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent msg = new Intent(Intent.ACTION_SEND);
                msg.putExtra(Intent.EXTRA_TEXT, getShareString() );
                msg.setType("text/plain");
                startActivity(Intent.createChooser(msg, "اطلاعات این بیماری را به اشتراک گذارید:"));
            }
        });
    }

    public String getShareString() {
        String msg = "از طریق لینک زیر می توانید به اطلاعات پزشکی بیماری «" +
                conditionDetailModel.getName() +
                "» دسترسی داشته باشید:" + "\n\n" + Variable.CONDITION_BASE_URL +
                conditionDetailModel.getId()+ "/" +
                Uri.encode(conditionDetailModel.getName(), "@#&=*+-_.,:!?()/~'%")+"/" + "\n\n" +
                "لینک دانلود اپلیکیشن \"درمانه\":" + "\n" +
                Variable.getDownloadLink();
        Log.d("condition",msg);
        return msg;
    }
}
