package com.darmaneh.ava.diagnosis;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.darmaneh.ava.App;
import com.darmaneh.ava.R;
import com.darmaneh.models.diagnosis.Condition;
import com.darmaneh.models.diagnosis.Response;
import com.darmaneh.requests.DiagnosisFlow;
import com.darmaneh.requests.Utility;
import com.darmaneh.utilities.Analytics;
import com.darmaneh.utilities.Functions;
import com.google.gson.Gson;

import java.util.List;

public class EnoughQuestion extends AppCompatActivity {

    Response diagnosis;
    List<Condition> conditions;
    Context context = this;
    String message;
    int direction = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        direction = getIntent().getExtras().getInt("direction", 0);

        if (direction==1){
            getWindow().getAttributes().windowAnimations = R.style.SlideForward;
        }else if(direction==-1){
            getWindow().getAttributes().windowAnimations = R.style.SlideBackward;
        }
        setContentView(R.layout.activity_enough_question);
        initialize();
        Analytics.sendScreenName("sc/question/enough13");
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        DiagnosisFlow.patient_diagnosis_backward(this);
    }

    private void initialize() {
        findViewById(R.id.back_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        diagnosis = (new Gson()).fromJson(getIntent().getExtras().getString("diagnosis"),
                Response.class);
        conditions = diagnosis.getFilteredConditions();

        if (conditions.size() == 0) {
            message = "ما هنوز در حال تلاش برای تشخیص بیماری شما هستیم." +
                      " شما می‌توانید با پاسخ دادن به سوالات بیشتر، در تشخیص بیماری کمک کنید، آیا مایل به ادامه هستید؟";
        } else {
            String prob = conditions.get(0).getProbability();
            String name = conditions.get(0).getName();
            message = "تا این لحظه بیماری «" +
                    name + "» با احتمال " +
                    prob + " درصد تشخیص داده شده‌است." +
                    " با پاسخ دادن به سوالات بیشتر می‌توانید به نتایج دقیق‌تری برسید." +
                    " آیا مایل به ادامه هستید؟";
        }

        TextView questionText = (TextView) findViewById(R.id.question_text);
        questionText.setTypeface(App.getFont(4));
        questionText.setText(message);

        ((TextView) findViewById(R.id.btn_present_text)).setTypeface(App.getFont(4));
        ((TextView) findViewById(R.id.btn_absent_text)).setTypeface(App.getFont(3));

        findViewById(R.id.btn_present).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Analytics.sendScreenName("sc/question/continue13");
                DiagnosisFlow.patient_diagnosis(context, true);
            }
        });
        findViewById(R.id.btn_absent).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Analytics.sendScreenName("sc/question/stop13");
                Intent intent = new Intent(context, DiagnosisResults.class);
                intent.putExtra("diagnosis", diagnosis.toString());
                // prepare for next question
                App.diagnosis_req.increaseReqNum();
                context.startActivity(intent);
                ((AppCompatActivity) context).finish();
            }
        });
    }
}
