package com.darmaneh.ava.diagnosis;

import android.content.Intent;
import android.graphics.Color;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.darmaneh.ava.App;
import com.darmaneh.ava.R;
import com.darmaneh.dialogs.DarmanehToast;
import com.darmaneh.models.diagnosis.History;
import com.darmaneh.models.diagnosis.Request;
import com.darmaneh.models.patient_record.PatientRecordModel;
import com.darmaneh.requests.PatientRecord;
import com.darmaneh.requests.Utility;
import com.darmaneh.utilities.Analytics;
import com.darmaneh.utilities.Functions;


public class GetInformation extends AppCompatActivity {

    private TextInputLayout inputLayoutAge, inputLayoutHeight, inputLayoutWeight;
    private EditText inputAge, inputHeight, inputWeight;
    private enum sexType {non , male , female}
    sexType selectedSex = sexType.non;
    TextView radioMale, radioFemale, ageTV, heightTV, weightTV;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_information);
        getWindow().getAttributes().windowAnimations = R.style.FadeOutExit;
        initialize();
        Analytics.sendScreenName("sc/bi");
    }

    private void initialize() {

        inputLayoutAge = (TextInputLayout) findViewById(R.id.input_layout_age);
        inputLayoutAge.setTypeface(App.getFont(3));
        inputLayoutHeight = (TextInputLayout) findViewById(R.id.input_layout_height);
        inputLayoutHeight.setTypeface(App.getFont(3));
        inputLayoutWeight = (TextInputLayout) findViewById(R.id.input_layout_weight);
        inputLayoutWeight.setTypeface(App.getFont(3));

        inputAge = (EditText) findViewById(R.id.input_age);
        inputHeight = (EditText) findViewById(R.id.input_height);
        inputWeight = (EditText) findViewById(R.id.input_weight);

        ageTV = (TextView) findViewById(R.id.age_text);
        ageTV.setTypeface(App.getFont(3));

        heightTV = (TextView) findViewById(R.id.height_text);
        heightTV.setTypeface(App.getFont(3));

        weightTV = (TextView) findViewById(R.id.weight_text);
        weightTV.setTypeface(App.getFont(3));

        TextView textSex = (TextView) findViewById(R.id.sex_text);
        textSex.setTypeface(App.getFont(3));

        ((TextView) findViewById(R.id.info_text)).setTypeface(App.getFont(5));
        radioMale = (TextView) findViewById(R.id.radio_male);
        radioFemale = (TextView) findViewById(R.id.radio_female);
        (radioMale).setTypeface(App.getFont(3));
        (radioFemale).setTypeface(App.getFont(3));
        radioMale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setChecked(sexType.male);
            }
        });
        radioFemale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setChecked(sexType.female);
            }
        });
        findViewById(R.id.btn_next).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submitForm();
            }
        });
        ((TextView) findViewById(R.id.btn_next_text)).setTypeface(App.getFont(3));

        PatientRecord.get_patient_record(this, new PatientRecord.GetPatientRecord() {
            @Override
            public void onHttpResponse(Boolean success, PatientRecordModel patientRecordModel) {
                if (success) {
                    Integer patientAge = patientRecordModel.getAge();
                    inputAge.setText(patientAge == null ? "" : String.valueOf(patientAge));

                    Integer patientHeight = patientRecordModel.getHeight();
                    inputHeight.setText(patientHeight == null ? "" : String.valueOf(patientHeight));

                    Integer patientWeight = patientRecordModel.getWeight();
                    inputWeight.setText(patientWeight == null ? "" : String.valueOf(patientWeight));

                    String patientSex = patientRecordModel.getSex();
                    if (patientSex.equals("male")) {
                        setChecked(sexType.male);
                    } else if (patientSex.equals("female")) {
                        setChecked(sexType.female);
                    }
                }
            }
        });
    }

    private void handleErrorColor(final EditText editText, final TextView textView, boolean error) {
        if (error) {
            editText.setTextColor(Color.RED);
            textView.setTextColor(Color.RED);
        } else {
            editText.setTextColor(Color.BLACK);
            textView.setTextColor(Color.parseColor("#676767"));
        }
    }

    private void submitForm() {
        App.diagnosis_req = new Request();
        App.histories = new History();

        boolean ageIsValid, heightIsValid, weightIsValid, sexIsValid;

        ageIsValid = validateAge();
        heightIsValid = validateHeight();
        weightIsValid = validateWeight();
        sexIsValid = validateSex();

        if (!ageIsValid || !heightIsValid || !weightIsValid || !sexIsValid)
            return;

        App.diagnosis_req.setAge(Integer.valueOf(inputAge.getText().toString()));
        App.diagnosis_req.setHeight(Integer.valueOf(inputHeight.getText().toString()));
        App.diagnosis_req.setWeight(Integer.valueOf(inputWeight.getText().toString()));

        switch (selectedSex){
            case male:
                App.diagnosis_req.setSex("male");
                break;
            case female:
                App.diagnosis_req.setSex("female");
                break;
        }

        App.diagnosis_req.setReqNum(1);

        Intent intent = new Intent(this, SymptomSearchList.class);
        startActivity(intent);
    }

    private boolean validateAge() {
        if (inputAge.getText().toString().trim().isEmpty()) {
            inputLayoutAge.setError("لطفا سن را وارد کنید.");
            handleErrorColor(inputAge, ageTV, true);
            requestFocus(inputAge);
            return false;
        } else if (Integer.parseInt(inputAge.getText().toString()) <= 0){
            inputLayoutAge.setError("لطفا سن را درست وارد نمائید.");
            handleErrorColor(inputAge, ageTV, true);
            requestFocus(inputAge);
            return false;
        } else if (Integer.parseInt(inputAge.getText().toString()) > 150){
            inputLayoutAge.setError("لطفا سن را درست وارد نمائید.");
            handleErrorColor(inputAge, ageTV, true);
            requestFocus(inputAge);
            return false;
        }else {
            inputLayoutAge.setErrorEnabled(false);
            handleErrorColor(inputAge, ageTV, false);
        }
        return true;
    }

    private boolean validateSex(){
        switch (selectedSex){
            case male:
                return true;
            case female:
                return true;
            default:
                DarmanehToast.makeText(this,"لطفا جنسیت را انتخاب نمایید", Toast.LENGTH_LONG);
                return false;
        }
    }

    private boolean validateWeight() {
        if (inputWeight.getText().toString().trim().isEmpty()) {
            inputLayoutWeight.setError("لطفا وزن را وارد کنید.");
            handleErrorColor(inputWeight, weightTV, true);
            requestFocus(inputWeight);
            return false;
        } else if (Integer.parseInt(inputWeight.getText().toString()) < 1){
            inputLayoutWeight.setError("لطفاً وزن را درست وارد نمائید.");
            handleErrorColor(inputWeight, weightTV, true);
            requestFocus(inputWeight);
            return false;
        } else if (Integer.parseInt(inputWeight.getText().toString()) > 400){
            inputLayoutWeight.setError("لطفاً وزن را درست وارد نمائید.");
            handleErrorColor(inputWeight, weightTV, true);
            requestFocus(inputWeight);
            return false;
        } else {
            handleErrorColor(inputWeight, weightTV, false);
            inputLayoutWeight.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateHeight() {
        if (inputHeight.getText().toString().trim().isEmpty()) {
            inputLayoutHeight.setError("لطفا قد را وارد کنید.");
            handleErrorColor(inputHeight, heightTV, true);
            requestFocus(inputHeight);
            return false;
        } else if (Integer.parseInt(inputHeight.getText().toString())<30){
            inputLayoutHeight.setError("لطفاً قد را درست وارد نمائید.");
            handleErrorColor(inputHeight, heightTV, true);
            requestFocus(inputHeight);
            return false;
        } else if (Integer.parseInt(inputHeight.getText().toString())>270){
            inputLayoutHeight.setError("لطفاً قد را درست وارد نمائید.");
            handleErrorColor(inputHeight, heightTV, true);
            requestFocus(inputHeight);
            return false;
        } else {
            handleErrorColor(inputHeight, heightTV, false);
            inputLayoutHeight.setErrorEnabled(false);
        }

        return true;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private void setChecked(sexType sex){
        if(sex == sexType.male){
            radioMale.setTextColor(getResources().getColor(R.color.colorPrimary));
            radioFemale.setTextColor(getResources().getColor(R.color.gray_text));
            radioMale.setBackground(getResources().getDrawable(R.drawable.btn_bg_blue));
            radioFemale.setBackground(getResources().getDrawable(R.drawable.btn_bg_gray));
            selectedSex = sexType.male;
        }else if(sex == sexType.female){
            radioMale.setTextColor(getResources().getColor(R.color.gray_text));
            radioFemale.setTextColor(getResources().getColor(R.color.colorPrimary));
            radioMale.setBackground(getResources().getDrawable(R.drawable.btn_bg_gray));
            radioFemale.setBackground(getResources().getDrawable(R.drawable.btn_bg_blue));
            selectedSex = sexType.female;
        }
    }
}
