package com.darmaneh.ava.diagnosis;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.darmaneh.adapters.BodyPartAdapter;
import com.darmaneh.ava.App;
import com.darmaneh.ava.R;
import com.darmaneh.fragments.RequestFailureDialog;
import com.darmaneh.models.bodypart_symptom.BodyPart;
import com.darmaneh.requests.BodyParts;
import com.darmaneh.requests.Utility;
import com.darmaneh.utilities.Analytics;
import com.darmaneh.utilities.Functions;
import com.darmaneh.utilities.Statics;
import java.util.List;

public class BodyPartList extends AppCompatActivity {

    RecyclerView bodyPartList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_body_part_list);
        initialize();
        Analytics.sendScreenName("sc/bodypart_list");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Statics.REQUEST_EXIT) {
            if (resultCode == Statics.RESULT_OK) {
                this.finish();
            }
        }
    }

    private void initialize() {
        findViewById(R.id.back_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        init_body_part_list();
    }

    private void init_body_part_list() {
        ((TextView) findViewById(R.id.question_text)).setTypeface(App.getFont(4));

        bodyPartList = (RecyclerView) findViewById(R.id.body_part_list);
        BodyParts.get_bodyparts_symptoms(this, gbps);
    }

    BodyParts.GetBodyPartsSymptoms gbps = new BodyParts.GetBodyPartsSymptoms() {
        @Override
        public void onHttpResponse(Boolean success, List<BodyPart> bodyparts) {
            if (success) {
                bodyPartList.setHasFixedSize(true);
                LinearLayoutManager llm = new LinearLayoutManager(BodyPartList.this);
                bodyPartList.setLayoutManager(llm);
                BodyPartAdapter bodyPartAdapter = new BodyPartAdapter(bodyparts);
                bodyPartList.setAdapter(bodyPartAdapter);
            }
            else {
                RequestFailureDialog dialogFragment = new RequestFailureDialog();
                dialogFragment.setRetryFunction(new RequestFailureDialog.RetryOnClickInterface() {
                    @Override
                    public void onClick() {
                        init_body_part_list();
                    }
                });
                dialogFragment.show(getSupportFragmentManager(), "RequestFailureDialog");
            }
        }
    };
}
