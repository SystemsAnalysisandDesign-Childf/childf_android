package com.darmaneh.ava.diagnosis;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.darmaneh.adapters.ConditionResultAdapter;
import com.darmaneh.ava.App;
import com.darmaneh.ava.MainActivity;
import com.darmaneh.ava.R;
import com.darmaneh.ava.call.SingleTelemedicine;
import com.darmaneh.fragments.ContactUsDialog;
import com.darmaneh.models.diagnosis.Condition;
import com.darmaneh.models.diagnosis.Response;
import com.darmaneh.requests.SymptomCheckerHistory;
import com.darmaneh.requests.Utility;
import com.darmaneh.utilities.Analytics;
import com.darmaneh.utilities.Functions;
import com.darmaneh.utilities.Storage;
import com.google.gson.Gson;

import java.util.List;

public class DiagnosisResults extends AppCompatActivity {

    Response diagnosis;
    List<Condition> conditions;
    RecyclerView conditionList;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_diagnosis_results);
        context = this;
        initialize();
        Storage.countSCUsage();
        Analytics.sendScreenName("sc/diagnosis");
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        // open main activity when user clicks on back button
        startActivity(new Intent(this, MainActivity.class));
        // will clear all the activities on top of MainActivity
        finishAffinity();
    }

    private void initialize() {

        diagnosis = (new Gson()).fromJson(getIntent().getExtras().getString("diagnosis"),
                Response.class);

        conditions = diagnosis.getFilteredConditions();


        TextView goToMain = (TextView) findViewById(R.id.go_to_main);
        goToMain.setTypeface(App.getFont(4));
        goToMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Analytics.sendScreenName("sc/diagnosis/goto_firstpage");
                Context context = view.getContext();
                context.startActivity(new Intent(context, MainActivity.class));
                // will clear all the activities on top of MainActivity
                DiagnosisResults.this.finishAffinity();
            }
        });

        TextView comment = (TextView) findViewById(R.id.comment);
        comment.setTypeface(App.getFont(4));
        comment.setText("تماس با پزشک");
        comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, SingleTelemedicine.class);
                startActivity(intent);
            }
        });

        TextView addToHistory = (TextView) findViewById(R.id.add_to_history);
        addToHistory.setTypeface(App.getFont(4));
        addToHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Analytics.sendScreenName("sc/diagnosis/add_record");
                SymptomCheckerHistory.patient_add_sc_history(view.getContext(), diagnosis.toString());
            }
        });

        ((TextView)findViewById(R.id.result_text)).setTypeface(App.getFont(5));

        conditionList = (RecyclerView) findViewById(R.id.condition_list);
        conditionList.setHasFixedSize(true);

        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        conditionList.setLayoutManager(llm);

        ConditionResultAdapter conditionResultAdapter = new ConditionResultAdapter(conditions);
        conditionList.setAdapter(conditionResultAdapter);

        // disable add to sc history when no condition has been detected
        if (conditions.size() == 0) {
            findViewById(R.id.no_condition_card).setVisibility(View.VISIBLE);
            ((TextView) findViewById(R.id.no_condition_text)).setTypeface(App.getFont(3));

            addToHistory.setVisibility(View.GONE);
            findViewById(R.id.menu_separator).setVisibility(View.GONE);
            conditionList.setVisibility(View.GONE);
        }
    }
}
