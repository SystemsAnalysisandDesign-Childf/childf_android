package com.darmaneh.ava.diagnosis;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.darmaneh.adapters.AddEvidenceRadioAdapter;
import com.darmaneh.ava.App;
import com.darmaneh.ava.R;
import com.darmaneh.dialogs.DarmanehToast;
import com.darmaneh.models.diagnosis.Evidence;
import com.darmaneh.models.diagnosis.Item;
import com.darmaneh.models.diagnosis.Question;
import com.darmaneh.requests.DiagnosisFlow;
import com.darmaneh.requests.Utility;
import com.darmaneh.utilities.Analytics;
import com.darmaneh.utilities.Functions;
import com.google.gson.Gson;
import java.util.List;

public class GroupSingleQuestion extends AppCompatActivity {

    public Item checkedItem = null;
    Question question;
    int direction = 0;
    RecyclerView evidenceList;
    AddEvidenceRadioAdapter addEvidenceRadioAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        direction = getIntent().getExtras().getInt("direction", 0);

        if (direction==1){
            getWindow().getAttributes().windowAnimations = R.style.SlideForward;
        }else if(direction==-1){
            getWindow().getAttributes().windowAnimations = R.style.SlideBackward;
        }
        setContentView(R.layout.activity_group_single_question);
        initialize();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        DiagnosisFlow.patient_diagnosis_backward(this);
    }

    private void initialize() {

        question = (new Gson()).fromJson(getIntent().getExtras().getString("question"),
                Question.class);

        TextView questionText = (TextView) findViewById(R.id.question_text);
        questionText.setTypeface(App.getFont(4));
        questionText.setText(question.getText());

        evidenceList = (RecyclerView) findViewById(R.id.evidence_list);
        evidenceList.setHasFixedSize(true);

        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        evidenceList.setLayoutManager(llm);

        addEvidenceRadioAdapter = new AddEvidenceRadioAdapter(question.getItems());
        evidenceList.setAdapter(addEvidenceRadioAdapter);

        ((TextView) findViewById(R.id.btn_submit_text)).setTypeface(App.getFont(5));
        findViewById(R.id.btn_submit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkedItem == null) {
                    DarmanehToast.makeText(GroupSingleQuestion.this, "لطفا یکی از گزینه ها را انتخاب کنید.", Toast.LENGTH_SHORT);
                } else {
                    List<Evidence> temp = App.diagnosis_req.getEvidence();
                    Evidence e = new Evidence();
                    e.setId(checkedItem.getId());
                    e.setName(checkedItem.getName());
                    e.setChoiceId("present");
                    temp.add(e);
                    App.diagnosis_req.setEvidence(temp);

                    DiagnosisFlow.patient_diagnosis(view.getContext(), true);
                }
            }
        });
        findViewById(R.id.back_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }
}
