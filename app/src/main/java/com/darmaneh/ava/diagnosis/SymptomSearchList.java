package com.darmaneh.ava.diagnosis;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.darmaneh.adapters.AddSymptomCheckBoxAdapter;
import com.darmaneh.adapters.SymptomAutoCompleteAdapter;
import com.darmaneh.ava.App;
import com.darmaneh.ava.R;
import com.darmaneh.fragments.RequestFailureDialog;
import com.darmaneh.models.AutoCompleteModel;
import com.darmaneh.models.bodypart_symptom.SymptomCommon;
import com.darmaneh.models.diagnosis.Evidence;
import com.darmaneh.requests.SymptomAutoComplete;
import com.darmaneh.requests.DiagnosisFlow;
import com.darmaneh.requests.Utility;
import com.darmaneh.utilities.Analytics;
import com.darmaneh.utilities.Functions;

import java.util.ArrayList;
import java.util.List;

public class SymptomSearchList extends AppCompatActivity {

    private static final String TAG = SymptomSearchList.class.getSimpleName();
    AutoCompleteTextView actv;
    TextView btnNext, noDataText;
    RecyclerView symptomRecyclerView;
    AddSymptomCheckBoxAdapter addSymptomCheckBoxAdapter;

    List<String> symptomStrList = new ArrayList<>();
    List<AutoCompleteModel> modelList = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_symptom_search_list);
        getWindow().getAttributes().windowAnimations = R.style.FadeOutExit;
        initialize();
        Analytics.sendScreenName("sc/symptom_list");
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        show_next_button();
        init_rv();
    }

    private void init_symptom_list() {
        SymptomAutoComplete.get_auto_complete_sympthom(this, getSymptom);
    }

    SymptomAutoComplete.GetSymptom getSymptom = new SymptomAutoComplete.GetSymptom() {
        @Override
        public void onHttpResponse(Boolean success, List<SymptomCommon> symptomCommonList) {
            if(success){
                list_init(symptomCommonList);
            }else{
                RequestFailureDialog dialogFragment = new RequestFailureDialog();
                dialogFragment.setRetryFunction(new RequestFailureDialog.RetryOnClickInterface() {
                    @Override
                    public void onClick() {
                        init_symptom_list();
                    }
                });
                dialogFragment.show(getSupportFragmentManager(), "RequestFailureDialog");
            }
        }
    };

    private void initialize() {
        init_symptom_list();
        findViewById(R.id.btn_add_symptom).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Context context = view.getContext();
                Intent intent = new Intent(context, BodyPartList.class);
                context.startActivity(intent);
            }
        });
        ((TextView)findViewById(R.id.btn_add_symptom)).setTypeface(App.getFont(3));

        btnNext = (TextView) findViewById(R.id.btn_next);
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DiagnosisFlow.patient_diagnosis(view.getContext(), false);
            }
        });
        btnNext.setTypeface(App.getFont(3));
        actv = (AutoCompleteTextView) findViewById(R.id.auto_complete_text);
        actv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                AutoCompleteModel model = (AutoCompleteModel) adapterView.getItemAtPosition(position);
                actv.setText("");
                addSymptom(model);
                show_next_button();
            }
        });
        actv.setTypeface(App.getFont(3));
        noDataText = (TextView) findViewById(R.id.no_data_txt);
        noDataText.setTypeface(App.getFont(4));
    }

    private void addSymptom(AutoCompleteModel symptom) {
        List<Evidence> temp = App.diagnosis_req.getEvidence();
        Evidence e = new Evidence();
        e.setId(symptom.getModelId());
        e.setName(symptom.getNameFa());
        e.setChoiceId("present");
        if (!temp.contains(e)) {
            temp.add(e);
        }
        App.diagnosis_req.setEvidence(temp);
        addSymptomCheckBoxAdapter.notifyDataSetChanged();
    }

    private void list_init(List<SymptomCommon> symptomCommonList) {
        Log.d(TAG, String.valueOf(symptomCommonList.size()));
        List<AutoCompleteModel> autoCompleteModelList = new ArrayList<>();
        for (SymptomCommon symptom : symptomCommonList){
            autoCompleteModelList.add(new AutoCompleteModel(symptom.getModelId(),symptom.getNameFa()));
            for (String common_name : symptom.getCommonNames()){
                autoCompleteModelList.add(new AutoCompleteModel(symptom.getModelId(),common_name));
            }
        }
        modelList = autoCompleteModelList;
        SymptomAutoCompleteAdapter adapter = new SymptomAutoCompleteAdapter(this,modelList);
        actv.setThreshold(1);
        actv.setAdapter(adapter);
    }
    private void init_rv() {
        symptomRecyclerView = (RecyclerView) findViewById(R.id.symptom_list);
        symptomRecyclerView.setHasFixedSize(true);

        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        symptomRecyclerView.setLayoutManager(llm);

        addSymptomCheckBoxAdapter = new AddSymptomCheckBoxAdapter();
        symptomRecyclerView.setAdapter(addSymptomCheckBoxAdapter);
        symptomRecyclerView.setRecyclerListener(new RecyclerView.RecyclerListener() {
            @Override
            public void onViewRecycled(RecyclerView.ViewHolder holder) {
                show_next_button();
            }
        });
    }

    private void show_next_button() {
        if (App.diagnosis_req.getEvidence().size() > 0) {
            noDataText.setVisibility(View.GONE);
            btnNext.setBackgroundResource(R.color.colorPrimary);
            btnNext.setClickable(true);
        }
        else {
            noDataText.setVisibility(View.VISIBLE);
            btnNext.setBackgroundResource(R.color.gray_text);
            btnNext.setClickable(false);
        }
    }
}
