package com.darmaneh.ava.diagnosis;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.darmaneh.ava.App;
import com.darmaneh.ava.R;
import com.darmaneh.models.diagnosis.Evidence;
import com.darmaneh.models.diagnosis.Question;
import com.darmaneh.requests.DiagnosisFlow;
import com.darmaneh.requests.Utility;
import com.darmaneh.utilities.Analytics;
import com.darmaneh.utilities.Functions;
import com.google.gson.Gson;
import java.util.List;

public class SingleQuestion extends AppCompatActivity {

    Question question;
    int direction = 0;
    final static int PRESENT = 1;
    final static int ABSENT = -1;
    final static int UNKNOWN = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        direction = getIntent().getExtras().getInt("direction", 0);

        if (direction==1){
            getWindow().getAttributes().windowAnimations = R.style.SlideForward;
        }else if(direction==-1){
            getWindow().getAttributes().windowAnimations = R.style.SlideBackward;
        }
        setContentView(R.layout.activity_single_question);
        initialize();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        DiagnosisFlow.patient_diagnosis_backward(this);
    }

    private void initialize() {
        findViewById(R.id.back_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        question = (new Gson()).fromJson(getIntent().getExtras().getString("question"),
                Question.class);

        TextView questionText = (TextView) findViewById(R.id.question_text);
        questionText.setTypeface(App.getFont(4));
        questionText.setText(question.getText());

        ((TextView) findViewById(R.id.btn_present_text)).setTypeface(App.getFont(4));
        ((TextView) findViewById(R.id.btn_absent_text)).setTypeface(App.getFont(4));
        ((TextView) findViewById(R.id.btn_unknown_text)).setTypeface(App.getFont(3));

        findViewById(R.id.btn_present).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submit(PRESENT);
            }
        });
        findViewById(R.id.btn_unknown).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submit(UNKNOWN);
            }
        });
        findViewById(R.id.btn_absent).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submit(ABSENT);
            }
        });
    }

    private void submit(int choice) {
        List<Evidence> temp = App.diagnosis_req.getEvidence();
        Evidence e = new Evidence();
        e.setId(question.getItems().get(0).getId());
        e.setName(question.getItems().get(0).getName());

        switch (choice) {
            case PRESENT:
                e.setChoiceId("present");
                break;
            case UNKNOWN:
                e.setChoiceId("unknown");
                break;
            case ABSENT:
                e.setChoiceId("absent");
                break;
        }

        if (!Evidence.isInList(temp, e)) {
            temp.add(e);
            App.diagnosis_req.setEvidence(temp);
        }

        DiagnosisFlow.patient_diagnosis(this, true);
    }
}
