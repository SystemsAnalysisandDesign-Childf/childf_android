package com.darmaneh.ava.diagnosis;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.darmaneh.adapters.AddBodyPartSymptomAdapter;
import com.darmaneh.ava.App;
import com.darmaneh.ava.R;
import com.darmaneh.models.bodypart_symptom.Symptom;
import com.darmaneh.models.diagnosis.Evidence;
import com.darmaneh.requests.Utility;
import com.darmaneh.utilities.Analytics;
import com.darmaneh.utilities.Functions;
import com.darmaneh.utilities.Statics;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.util.ArrayList;
import java.util.List;

public class BodyPartSymptomList extends AppCompatActivity {

    public List<Symptom> checkedSymptoms;
    List<Symptom> symptoms;
    RecyclerView symptomList;
    AddBodyPartSymptomAdapter addBodyPartSymptomAdapter;
    TextView bodyPart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_body_part_symptom_list);
        initialize();
    }

    private void initialize() {

        symptoms = (new Gson()).fromJson(getIntent().getExtras().getString("symptoms"),
                new TypeToken<List<Symptom>>(){}.getType());
        String bodypartName = getIntent().getExtras().getString("bodypart");

        Analytics.sendScreenName("sc/bodypart/" + bodypartName);

        checkedSymptoms = new ArrayList<>();

        symptomList = (RecyclerView) findViewById(R.id.symptom_list);
        symptomList.setHasFixedSize(true);

        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        symptomList.setLayoutManager(llm);

        addBodyPartSymptomAdapter = new AddBodyPartSymptomAdapter(symptoms);
        symptomList.setAdapter(addBodyPartSymptomAdapter);

        ((TextView) findViewById(R.id.btn_submit_text)).setTypeface(App.getFont(5));
        ((TextView) findViewById(R.id.symptom_question)).setTypeface(App.getFont(3));
        bodyPart = (TextView) findViewById(R.id.body_part);
        bodyPart.setText(bodypartName);
        bodyPart.setTypeface(App.getFont(5));
        findViewById(R.id.btn_submit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                List<Evidence> temp = App.diagnosis_req.getEvidence();
                for (Symptom symptom : checkedSymptoms) {
                    Evidence e = new Evidence();
                    e.setId(symptom.getModelId());
                    e.setName(symptom.getNameFa());
                    e.setChoiceId("present");
                    if (!temp.contains(e)) {
                        temp.add(e);
                    }
                }
                App.diagnosis_req.setEvidence(temp);

                // close parent activity
                setResult(Statics.RESULT_OK, null);

                finish();
            }
        });
        findViewById(R.id.back_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }
}
