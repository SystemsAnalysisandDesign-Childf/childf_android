package com.darmaneh.ava;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.darmaneh.models.diagnosis.Condition;
import com.tapstream.sdk.*;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem;
import com.darmaneh.fragments.ShareDialog;
import com.darmaneh.fragments.bottom_navigation.ConditionFragment;
import com.darmaneh.fragments.bottom_navigation.LocationFragment;
import com.darmaneh.fragments.bottom_navigation.RecordFragment;
import com.darmaneh.fragments.bottom_navigation.TeleMedicineFragment;
import com.darmaneh.fragments.bottom_navigation.VirtualFragment;
import com.darmaneh.utilities.Analytics;
import com.darmaneh.utilities.BackStackTracer;
import com.darmaneh.utilities.Functions;
import com.darmaneh.utilities.SignInFunction;
import com.darmaneh.utilities.Storage;
import com.onesignal.OneSignal;


public class MainActivity extends AppCompatActivity{
    private static final int STEP_REQUEST_CODE = 1;
    public static int currentItem = 2;
    BackStackTracer backStackTracer;
    AHBottomNavigation bottomNavigation;
    private final static String TAG = MainActivity.class.getSimpleName();
    Fragment frag;

    private void initiate_icons(){
        bottomNavigation = (AHBottomNavigation) findViewById(R.id.bottomTabs);

        //// TODO: 7/6/2017 make item5 change drawable to filled while clicked
        // Create items
        AHBottomNavigationItem item1 = new AHBottomNavigationItem("شعبه‌های فعال", R.drawable.location_black);
//        AHBottomNavigationItem item2 = new AHBottomNavigationItem("فرم همکاری", R.drawable.list_black);
        AHBottomNavigationItem item3 = new AHBottomNavigationItem("حساب کاربری", R.drawable.files_black);
        AHBottomNavigationItem item4 = new AHBottomNavigationItem("نیاز اضطراری", R.drawable.visit_black);
        AHBottomNavigationItem item5 = new AHBottomNavigationItem("لیست مددجویان", R.drawable.list_black);

        // Add items
        bottomNavigation.addItem(item1);
//        bottomNavigation.addItem(item2);
        bottomNavigation.addItem(item3);
        bottomNavigation.addItem(item4);
        bottomNavigation.addItem(item5);

        // Set background color
        bottomNavigation.setDefaultBackgroundColor(Color.WHITE);

        // Change colors
        bottomNavigation.setAccentColor(Color.BLACK);
        bottomNavigation.setInactiveColor(Color.parseColor("#0d92c8"));

        // Manage titles
        bottomNavigation.setTitleTypeface(App.getFont(3));
        bottomNavigation.setTitleState(AHBottomNavigation.TitleState.ALWAYS_SHOW);

        bottomNavigation.setOnTabSelectedListener(new AHBottomNavigation.OnTabSelectedListener() {
            @Override
            public boolean onTabSelected(int position, boolean wasSelected) {
                return selectFragment(position);
            }
        });

        // Set current item programmatically
        bottomNavigation.setCurrentItem(3);
    }

    public void init_backStack(){
        backStackTracer = new BackStackTracer();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init_backStack();
        initiate_icons();
        changePhone();
        sendTag();
        initTapStream();
    }

    private void changePhone() {
        String newPhone = Storage.getPhoneNum();
        newPhone = newPhone.replaceFirst("^0+(?!$)", "");
        Storage.setPhoneNum(newPhone);
    }

    private void sendTag() {
        String phone = Storage.getCompletePhoneNum("en");
        if (!phone.equals("")) {
            OneSignal.sendTag("phone", phone);
        }
    }

    private void initTapStream() {
        Config config = new Config("darmaneh", "tEIsm4xjSKa1E4sOi9JxCA");
        Tapstream.create(getApplication(), config);
    }

    private void rateAndShareDarmaneh() {
//        if (BuildConfig.FLAVOR.equals("bazaar")) {
//            final long ONE_WEEK = 604800000;
//            long currentTime = (new Date(System.currentTimeMillis())).getTime();
//            long lastTime = Storage.getTimeRate();
//
//            if (!Storage.getIsRated() &&
//                    Storage.getSCUsage() > 0 &&
//                    currentTime - lastTime > ONE_WEEK) {
//                RateDialog rateDialog = new RateDialog();
//                rateDialog.show(getSupportFragmentManager(), "Rate_Dialog");
//            }
//        } else {
            if (!Storage.getIsShared() && Storage.getSCUsage() == 1) {
                ShareDialog shareDialog = new ShareDialog();
                shareDialog.show(getSupportFragmentManager(), "Share_Dialog");
            }
//        }
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        rateAndShareDarmaneh();

        // TODO: remove
        if (!Storage.getSentPhone() && Storage.getIsLogin()) {
            Analytics.sendScreenName("testTele/v25/" + Storage.getCompletePhoneNum("en"));
            Storage.setSentPhone(true);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == STEP_REQUEST_CODE){
            if(resultCode == Activity.RESULT_CANCELED){
                finish();
            }
            else if (resultCode == Activity.RESULT_OK){
                App.firstUsage = false;
                Log.d(TAG,"result code");
            }
        }
    }

    private Boolean selectFragment(int position) {
        frag = null;
        // init corresponding fragment
        switch (position) {
            case 3:
                if (!Storage.getIsLogin()){
                    SignInFunction.signInDialog(this, "برای مشاهده لیست مددجویان ابتدا وارد شوید.");
                    return false;
                }
                else{
                    frag = new ConditionFragment();
                    backStackTracer.trace(frag, "3", "call");
                }
                break;
            case 2:
                if (!Storage.getIsLogin()){
                    SignInFunction.signInDialog(this, "برای مشاهده نیازهای اضطراری ابتدا وارد شوید.");
                    return false;
                }
                else {
                    frag = VirtualFragment.newInstance();
                    backStackTracer.trace(frag, "2", "virtual");
                }
                break;

            case 1:
                if (!Storage.getIsLogin()){
                    SignInFunction.signInDialog(this, "برای مشاهده حساب کاربری، ابتدا وارد شوید.");
                    return false;
                }
                else{
                    frag = RecordFragment.newInstance();
                    backStackTracer.trace(frag, "1", "record");
                }
                break;
            case 0:
                if (!Storage.getIsLogin()){
                    SignInFunction.signInDialog(this, "برای مشاهده شعبه‌های فعال، ابتدا وارد شوید.");
                    return false;
                }
                else {
                    frag = new LocationFragment();
                    backStackTracer.trace(frag, "0", "location");
                }
                break;
        }

        if (frag != null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.main_fragment, frag).commit();
        }

        return true;
    }

    @Override
    public void onBackPressed() {
        App.firstUsage = true;
        if (backStackTracer.isTheLastSlide()){
            super.onBackPressed();
            Log.d(TAG,"last back");
        } else{
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            backStackTracer.getBackTrackStack().remove(backStackTracer.getBackTrackStack().size()-1);
            backStackTracer.getBackTrackPos().remove(backStackTracer.getBackTrackPos().size()-1);
            backStackTracer.getBackTrackTAG().remove(backStackTracer.getBackTrackTAG().size()-1);
            bottomNavigation.setCurrentItem(Integer.parseInt(backStackTracer.getBackTrackPos().get(backStackTracer.getBackTrackPos().size()-1)));
            ft.replace(R.id.main_fragment, backStackTracer.getBackTrackStack().get(backStackTracer.getBackTrackStack().size()-1)).commit();
        }
    }
}
