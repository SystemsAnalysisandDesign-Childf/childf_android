package com.darmaneh.ava.BasicInfoPages;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.darmaneh.adapters.DoctorListRecyclerViewAdapter;
import com.darmaneh.ava.App;
import com.darmaneh.ava.R;
import com.darmaneh.fragments.RequestFailureDialog;
import com.darmaneh.models.adopted.AdoptedModel;
import com.darmaneh.models.call.DoctorInfoListModel;
import com.darmaneh.requests.DoctorListRequest;
import com.darmaneh.utilities.Analytics;
import com.darmaneh.utilities.Storage;

import java.util.ArrayList;
import java.util.List;

public class AdoptedChildren extends AppCompatActivity {

    RecyclerView doctorListRecyclerView;
    ArrayList<AdoptedModel> adoptedModels;

    private void init_doctor_list() {
        DoctorListRequest.get_doctor_list(AdoptedChildren.this, getDoctorList);
    }

    DoctorListRequest.GetDoctorList getDoctorList = new DoctorListRequest.GetDoctorList() {
        @Override
        public void onHttpResponse(Boolean success, List<AdoptedModel> adoptedModels) {
            if(success){
                list_init(adoptedModels);
            }else{
                RequestFailureDialog dialogFragment = new RequestFailureDialog();
                dialogFragment.setRetryFunction(new RequestFailureDialog.RetryOnClickInterface() {
                    @Override
                    public void onClick() {
                        init_doctor_list();
                    }
                });
                dialogFragment.show(getSupportFragmentManager(), "RequestFailureDialog");
            }
        }
    };

    public void addAdapter(){
        DoctorListRecyclerViewAdapter adapter = new DoctorListRecyclerViewAdapter(AdoptedChildren.this,adoptedModels);
        doctorListRecyclerView.setAdapter(adapter);
    }

    public void initRecyclerView(){
        doctorListRecyclerView = (RecyclerView) findViewById(R.id.doctor_list_recycler_view);
        doctorListRecyclerView.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        doctorListRecyclerView.setLayoutManager(llm);
    }

    public void addListeners(){
        findViewById(R.id.back_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onResume(){
        super.onResume();
        Analytics.sendScreenName("telemedicine/physician_list");
    }

    public void changeFonts(){
        ((TextView) findViewById(R.id.doctor_category_name)).setTypeface(App.getFont(3));
    }


    public void list_init(List<AdoptedModel> adoptedModels){
        this.adoptedModels = (ArrayList<AdoptedModel>) adoptedModels;
        if (this.adoptedModels != null) {
            addAdapter();
        }
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adopted_children);
        init_doctor_list();
        initRecyclerView();
        addListeners();
        changeFonts();
    }
}
