package com.darmaneh.ava;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.darmaneh.models.sc_history.VisitHistoryModel;
import com.darmaneh.requests.Utility;
import com.darmaneh.utilities.Analytics;
import com.darmaneh.utilities.Functions;
import com.google.gson.Gson;

public class VisitHistoryActivity extends AppCompatActivity {

    VisitHistoryModel visitHistoryModel;
    TextView reportDate , adviseTxt , biographyTxt, prescriptionTxt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visit_history);
        visitHistoryModel = (new Gson()).fromJson(getIntent().getExtras().
                getString("visit_history_detail"), VisitHistoryModel.class);
        init_view();
        changeFont();
        setContent();
    }

    private void init_view() {
        reportDate = (TextView) findViewById(R.id.report_date);
        adviseTxt = (TextView) findViewById(R.id.body_advice);
        biographyTxt = (TextView) findViewById(R.id.body_biography);
        prescriptionTxt = (TextView) findViewById(R.id.body_prescription);
        findViewById(R.id.back_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
    private void changeFont() {
        reportDate.setTypeface(App.getFont(4));
        adviseTxt.setTypeface(App.getFont(3));
        biographyTxt.setTypeface(App.getFont(3));
        prescriptionTxt.setTypeface(App.getFont(3));
        ((TextView) findViewById(R.id.header_advise)).setTypeface(App.getFont(4));
        ((TextView) findViewById(R.id.header_biography)).setTypeface(App.getFont(4));
        ((TextView) findViewById(R.id.header_prescription)).setTypeface(App.getFont(4));
    }


    private void setContent() {
        reportDate.setText(Functions.toPersian(visitHistoryModel.getJalaliDate()));
        adviseTxt.setText(visitHistoryModel.getAdvise());
        biographyTxt.setText(visitHistoryModel.getBiography());
        prescriptionTxt.setText(visitHistoryModel.getPrescription());
    }
}
