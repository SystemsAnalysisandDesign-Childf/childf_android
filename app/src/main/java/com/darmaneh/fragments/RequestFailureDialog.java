package com.darmaneh.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.darmaneh.ava.App;
import com.darmaneh.ava.R;
import com.darmaneh.utilities.Analytics;

/**
 * Created by pourya on 5/4/17.
 */

public class RequestFailureDialog extends DialogFragment {

    public RequestFailureDialog() {
        // Required empty public constructor
    }

    RetryOnClickInterface retryOnClickInterface;

    Context myCntx;
    TextView failureText;

    Button retryButton;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Analytics.sendScreenName("request/failure");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_reqeust_failure_dialog, container, false);
        WindowManager.LayoutParams wlp = getDialog().getWindow().getAttributes();
        wlp.windowAnimations = R.style.DialogAnimation;
        getDialog().getWindow().setAttributes(wlp);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().setCanceledOnTouchOutside(false);
        myCntx = getContext();

        failureText = (TextView) rootView.findViewById(R.id.failure_text);
        failureText .setTypeface(App.getFont(4));

        retryButton = (Button) rootView.findViewById(R.id.retry_button1);
        retryButton.setTypeface(App.getFont(4));
        retryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                retryOnClickInterface.onClick();
            }
        });

        return rootView;
    }

    public interface RetryOnClickInterface {
        void onClick();
    }

    public void setRetryFunction(RetryOnClickInterface retryOnClickInterface) {
        this.retryOnClickInterface = retryOnClickInterface;
    }

}
