package com.darmaneh.fragments;

/**
 * Created by pourya on 5/27/17.
 */

public interface FragmentLifecycle {
    public void onResumeFragment();
}
