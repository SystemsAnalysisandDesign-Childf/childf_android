package com.darmaneh.fragments;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.AppCompatSpinner;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.darmaneh.adapters.CustomSpinnerAdapter;
import com.darmaneh.ava.App;
import com.darmaneh.ava.R;
import com.darmaneh.dialogs.DarmanehToast;
import com.darmaneh.models.call.ImageModel;
import com.darmaneh.requests.ImageList;

import java.util.ArrayList;
import java.util.List;

public class PhotoDescriptionFragment extends DialogFragment {
    private static final String BYTE_ARRAY = "imageByteArray";
    private static final String ENCODED = "imageEncoded";
    private static final String CATEGORY = "categoryList";
    private byte[] imageByteArray;
    private String encodedImage;
    ImageView image;
    TextInputEditText description;
    AppCompatSpinner categorySpinner;
    TextView doneBtn;
    Bitmap bm;
    private ArrayList<String> imageCategoryModelList;

    private OnFragmentInteractionListener mListener;

    public PhotoDescriptionFragment() {
        // Required empty public constructor
    }

    public static PhotoDescriptionFragment newInstance(byte[] imageByteArray, String encoded,
                                                       ArrayList<String> imageCategories) {
        PhotoDescriptionFragment fragment = new PhotoDescriptionFragment();
        Bundle args = new Bundle();
        args.putByteArray(BYTE_ARRAY, imageByteArray);
        args.putString(ENCODED, encoded);
        args.putStringArrayList(CATEGORY,imageCategories);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            imageByteArray = getArguments().getByteArray(BYTE_ARRAY);
            encodedImage = getArguments().getString(ENCODED);
            imageCategoryModelList = getArguments().getStringArrayList(CATEGORY);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        View rootView = inflater.inflate(R.layout.fragment_photo_description, container, false);
        image = (ImageView) rootView.findViewById(R.id.image);
        categorySpinner = (AppCompatSpinner) rootView.findViewById(R.id.category_spinner);
        description = (TextInputEditText) rootView.findViewById(R.id.desc_text);
        doneBtn = (TextView) rootView.findViewById(R.id.done_btn);

        description.setTypeface(App.getFont(3));
        doneBtn.setTypeface(App.getFont(4));

        BitmapFactory.Options config = new BitmapFactory.Options();
        config.inSampleSize = 4;
        bm = BitmapFactory.decodeByteArray(imageByteArray, 0, imageByteArray.length,config);
//        Picasso.with(rootView.getContext()).load().into(image);
        image.setImageBitmap(bm);

        init_spinner();
        setOnClicks();

        return rootView;
    }

    private void init_spinner() {
        CustomSpinnerAdapter adapter = new CustomSpinnerAdapter(getContext(), imageCategoryModelList);
        categorySpinner.setAdapter(adapter);
    }

    private void setOnClicks() {
        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(categorySpinner.getSelectedItemPosition() == 0 || categorySpinner.getSelectedItem()==null){
                    DarmanehToast.makeText(getContext(), "دسته را انتخاب کنید", Toast.LENGTH_SHORT);
                }else {
                    final String descStr = description.getText().toString();
                    final String catStr = String.valueOf(categorySpinner.getSelectedItem());
                    ImageModel model = new ImageModel(null, catStr, "", descStr, encodedImage);
                    ImageList.post_images(getContext(), model, new ImageList.ImageUploaded() {
                        @Override
                        public void onHttpResponse(Boolean success) {
                            if (success) {
                                dismiss();
                                onButtonPressed(descStr, catStr, bm);
                            }
                        }
                    });
                }
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof OnFragmentInteractionListener)
            mListener = (OnFragmentInteractionListener) context;
        else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void onButtonPressed(String desc, String category, Bitmap bm) {
        if (mListener != null) {
            mListener.onPhotoCallback(desc, category, bm);
        }
    }


    public interface OnFragmentInteractionListener {
        void onPhotoCallback(String desc, String category, Bitmap bm);
    }


}
