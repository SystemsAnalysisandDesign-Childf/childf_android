package com.darmaneh.fragments;


import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.darmaneh.adapters.CustomSpinnerAdapter;
import com.darmaneh.ava.App;
import com.darmaneh.ava.BasicInfoPages.AdoptedChildren;
import com.darmaneh.ava.BasicInfoPages.PaymentInfo;
import com.darmaneh.ava.EditProfileActivity;
import com.darmaneh.ava.MainActivity;
import com.darmaneh.ava.R;
import com.darmaneh.ava.call.GalleryActivity;
import com.darmaneh.ava.call.PatientInfoDetail;
import com.darmaneh.dialogs.DarmanehProgressDialog;
import com.darmaneh.models.call.InstantInfo;
import com.darmaneh.models.call.InstantInfoListModel;
import com.darmaneh.models.patient_record.PatientRecordModel;
import com.darmaneh.models.patient_record.PatientRecordRequest;
import com.darmaneh.requests.PatientRecord;
import com.darmaneh.requests.UserFlowV3;
import com.darmaneh.utilities.Analytics;
import com.darmaneh.utilities.Functions;
import com.darmaneh.utilities.JalaliCalendar;
import com.darmaneh.utilities.Storage;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.Inflater;

public class BasicInfoFragment extends Fragment implements FragmentLifecycle{
    TextView userTel,userEmail, sexTxt, userSex, birthTxt, userBirth, heightTxt , userHeight,
            weightTxt, userWeight, userLocation, userNameToolbar, userName;
    ImageView profPic;
    LinearLayout infoToolbar,mTitleContainer;
    TextView mTitle;
    RelativeLayout heartDiseaseHistoryFullLayout, drugsTakenFullLayout;
    ImageButton editHeartDiseaseHistory, editDrugs;
    Context context;

    static int currentYear;
    private CollapsingToolbarLayout collapsingToolbarLayout = null;
    private AppBarLayout mAppBarLayout;
    private InstantInfoListModel instantInfoListModel ;

    Context thisCntx;
    boolean mainShowed = true;

    PatientRecordModel prm ;
    String[] sexTypesFa = {"مرد" , "زن"};
    String[] sexTypesEn = {"male" , "female"};



    public BasicInfoFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Calendar c = Calendar.getInstance();
        currentYear = (new JalaliCalendar(c.get(Calendar.YEAR),
                c.get(Calendar.MONTH) + 1,
                c.get(Calendar.DATE))).getYear();
    }


    private void init_view (final View rootView){
        heartDiseaseHistoryFullLayout = (RelativeLayout) rootView.findViewById(R.id.heart_disease_history_full_layout);
        drugsTakenFullLayout = (RelativeLayout) rootView.findViewById(R.id.drugs_taken_full_layout);

        editHeartDiseaseHistory = (ImageButton) rootView.findViewById(R.id.edit_illness_history);
        editDrugs = (ImageButton) rootView.findViewById(R.id.edit_medicine);

        userName = (TextView) rootView.findViewById(R.id.user_name);
        userTel = (TextView) rootView.findViewById(R.id.user_tel);
        userEmail = (TextView) rootView.findViewById(R.id.user_email);
        userLocation = (TextView) rootView.findViewById(R.id.user_location);

        profPic = (ImageView) rootView.findViewById(R.id.prof_pic);
//        mTitleContainer = (LinearLayout) rootView.findViewById(R.id.title_container);
//        mTitle = (TextView) rootView.findViewById(R.id.name_title);

        thisCntx = rootView.getContext();
        mAppBarLayout   = (AppBarLayout) rootView.findViewById(R.id.app_bar);

        editHeartDiseaseHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), AdoptedChildren.class);
//                intent.putExtra("type", 0);
//                intent.putExtra("parent", "BasicInfoFragment");
//                intent.putExtra("model", listToString((ArrayList<InstantInfo>) instantInfoListModel.getPatientConditions()));
                startActivity(intent);
            }
        });

        editDrugs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), PaymentInfo.class);
//                intent.putExtra("type", 1);
//                intent.putExtra("parent", "BasicInfoFragment");
//                intent.putExtra("model", listToString((ArrayList<InstantInfo>) instantInfoListModel.getPatientMedications()));
                startActivity(intent);
            }
        });

        heartDiseaseHistoryFullLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editHeartDiseaseHistory.callOnClick();
            }
        });

        drugsTakenFullLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editDrugs.callOnClick();
            }
        });

        rootView.findViewById(R.id.edit_prof_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(thisCntx, EditProfileActivity.class);
                intent.putExtra("model",prm.toString());
                startActivity(intent);
                mainShowed = false;
            }
        });

        rootView.findViewById(R.id.logout_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                android.support.v7.app.AlertDialog dialog = new android.support.v7.app.AlertDialog.Builder(context)
                        .setMessage(" آیا از خروج از حساب کاربری خود اطمینان دارید؟")
                        .setPositiveButton("بله" , dialogClickListener)
                        .setNegativeButton("خیر", dialogClickListener)
                        .show();
                TextView textMessage = (TextView) dialog.findViewById(android.R.id.message);
                textMessage.setTypeface(App.getFont(3));
                TextView textPositive = (TextView) dialog.findViewById(android.R.id.button1);
                textPositive.setTypeface(App.getFont(3));
                textPositive.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
                TextView textNegative = (TextView) dialog.findViewById(android.R.id.button2);
                textNegative.setTypeface(App.getFont(3));
                textNegative.setTextColor(ContextCompat.getColor(context ,R.color.colorPrimary));
            }
        });
    }

    DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialogInterface, int i) {
            switch (i){
                case DialogInterface.BUTTON_POSITIVE:
                    logout();
                    break;
                case DialogInterface.BUTTON_NEGATIVE:
                    break;
            }
        }
    };

    public ArrayList<String> listToString(ArrayList<InstantInfo> instantInfos){
        ArrayList<String> infos = new ArrayList<>();
        for (InstantInfo instantInfo: instantInfos){
            infos.add(instantInfo.toString());
        }
        return infos;
    }

    private void logout() {
        final DarmanehProgressDialog progress = new DarmanehProgressDialog(getContext());
        progress.show();

        UserFlowV3.logout(context,
                new UserFlowV3.Logout() {
                    @Override
                    public void onHttpResponse(Boolean success) {
                        if (success) {
                            Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    // Do something after 2s = 2000ms
                                    // recreate main activity to clear token
                                    Storage.setIsLogin(false);
                                    Storage.setToken("");
                                    progress.dismiss();
                                    App.firstUsage = false;
                                    Context context = getContext();
                                    ((AppCompatActivity) context).finish();
                                    Intent intent = new Intent(context, MainActivity.class);
                                    context.startActivity(intent);
                                }
                            }, 1000);
                        }
                    }
                });
    }

    private void change_font(View rootView) {
        userName.setTypeface(App.getFont(3));
        userTel.setTypeface(App.getFont(3));
        userEmail.setTypeface(App.getFont(3));
        userSex.setTypeface(App.getFont(3));
        sexTxt.setTypeface(App.getFont(3));
        userBirth.setTypeface(App.getFont(3));
        birthTxt.setTypeface(App.getFont(3));
        heightTxt.setTypeface(App.getFont(3));
        userHeight.setTypeface(App.getFont(3));
        weightTxt.setTypeface(App.getFont(3));
        userWeight.setTypeface(App.getFont(3));
        userLocation.setTypeface(App.getFont(3));
        userNameToolbar.setTypeface(App.getFont(4));

        ((TextView) rootView.findViewById(R.id.personal_info_header) ).setTypeface(App.getFont(3));
        ((TextView) rootView.findViewById(R.id.history_header) ).setTypeface(App.getFont(3));
        ((TextView) rootView.findViewById(R.id.illness_history_header) ).setTypeface(App.getFont(3));
        ((TextView) rootView.findViewById(R.id.medicine_header) ).setTypeface(App.getFont(3));
        ((TextView) rootView.findViewById(R.id.logout_text)).setTypeface(App.getFont(3));
        ((TextView) rootView.findViewById(R.id.edit_prof_text)).setTypeface(App.getFont(3));


    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView =  inflater.inflate(R.layout.fragment_basic_info, container, false);
        context = rootView.getContext();
        init_view(rootView);
        init_toolbar(rootView);
        change_font(rootView);
        setComponentText();
        return rootView;
    }

    private void init_toolbar(View rootView) {
        collapsingToolbarLayout = (CollapsingToolbarLayout) rootView.findViewById(R.id.collapsing_toolbar);
        collapsingToolbarLayout.setTitle(" ");
        collapsingToolbarLayout.setCollapsedTitleGravity(Gravity.CENTER_HORIZONTAL);
        sexTxt = (TextView) rootView.findViewById(R.id.user_sex_text);
        userSex = (TextView) rootView.findViewById(R.id.user_sex);
        birthTxt = (TextView) rootView.findViewById(R.id.age_text);
        userBirth = (TextView) rootView.findViewById(R.id.user_age);
        heightTxt = (TextView) rootView.findViewById(R.id.height_txt);
        userHeight = (TextView) rootView.findViewById(R.id.user_height);
        weightTxt = (TextView) rootView.findViewById(R.id.weight_text);
        userWeight = (TextView) rootView.findViewById(R.id.user_weight);
        userNameToolbar = (TextView) rootView.findViewById(R.id.user_name_toolbar);
        infoToolbar = (LinearLayout) rootView.findViewById(R.id.info_toolbar);
        Toolbar mToolbar = (Toolbar) rootView.findViewById(R.id.toolbar);
        ((AppCompatActivity)getActivity()).setSupportActionBar(mToolbar);
    }



    public void setPatientRecordModel(PatientRecordModel prm) {
        this.prm = prm;
        if(isAdded()){
            setComponentText();
        }
    }

    public void setIllnessHistoryResults(InstantInfoListModel model){
        this.instantInfoListModel = model;
    }
    public void setComponentText(){
        if(prm != null){
            if (prm.getFirstName().equals("") && prm.getLastName().equals("")){
                userName.setText("نام و نام خانوادگی");
                userName.setTextColor(ContextCompat.getColor(context, R.color.gray_text));
            }else {
                userName.setText(prm.getFirstName() + " " + prm.getLastName());
                userName.setTextColor(ContextCompat.getColor(context, R.color.colorBlack));
            }
            userTel.setText(Functions.toPersian(prm.getPhoneNumber()));
            if (prm.getEmail().equals("")){
                userEmail.setText("پست الکترونیکی");
                userEmail.setTextColor(ContextCompat.getColor(context, R.color.gray_text));
            }else {
                userEmail.setText(prm.getEmail());
                userEmail.setTextColor(ContextCompat.getColor(context, R.color.colorBlack));
            }
            userSex.setText(prm.getSex().equals(sexTypesEn[0])? sexTypesFa[0] : sexTypesFa[1]);
            if(prm.getBirthYear() == null)
                userBirth.setText("");
            else
                userBirth.setText(Functions.toPersian(getValueOf(currentYear - prm.getBirthYear())));
            userHeight.setText(Functions.toPersian(getValueOf(prm.getHeight())));
            userWeight.setText(Functions.toPersian(getValueOf(prm.getWeight())));
            String location="";
            if (prm.getProvince().isEmpty() && prm.getCity().isEmpty()) {
                location = "استان / شهر";
                userLocation.setTextColor(ContextCompat.getColor(context, R.color.gray_text));
            } else {
                userLocation.setTextColor(ContextCompat.getColor(context, R.color.colorBlack));
                if (prm.getProvince().isEmpty())
                    location = prm.getCity();
                else if (prm.getCity().isEmpty())
                    location = prm.getProvince();
                else
                    location = prm.getProvince() + "/" + prm.getCity();
            }
            userLocation.setText(location);
            userNameToolbar.setText(prm.getFirstName()+" "+ prm.getLastName());
            if(prm.getSex().equals(sexTypesEn[0]))
                profPic.setImageResource(R.drawable.male_pro_pic);
            else
                profPic.setImageResource(R.drawable.female_pro_pic);
        }
        else{
            userName.setText("");
            userTel.setText("");
            userEmail.setText("");
            userSex.setText("");
            userBirth.setText("");
            userHeight.setText("");
            userWeight.setText("");
            userLocation.setText("");
            userNameToolbar.setText("");
        }
    }

    private String getValueOf(Integer param){
        if(param == null)
            return "";
        else
            return String.valueOf(param);
    }

    private Integer getValueOf(String param){
        if(param.equals(""))
            return null;
        else
            return Integer.valueOf(param);
    }

    private void requestFocus(View view) {//// TODO: 4/8/2017 check if it's not causing any problem 
        if (view.requestFocus()) {
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

//    public interface checkBeingInTheEditMode {
//        void passTheSituation(Boolean isOnEdit, CardView cardView);
//    }

    @Override
    public void onResumeFragment() {
        Analytics.sendScreenName("EMR/bi");
    }

}