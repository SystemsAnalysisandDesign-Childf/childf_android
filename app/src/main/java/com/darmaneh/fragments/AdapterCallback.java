package com.darmaneh.fragments;

/**
 * Created by alireza on 5/31/17.
 * This is a callback for check whether a list (bookmark list and history list) is empty
 */

public interface AdapterCallback {
    public void checkEmptyCallback();
}
