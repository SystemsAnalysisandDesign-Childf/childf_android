package com.darmaneh.fragments.bottom_navigation;


import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.darmaneh.ava.App;
import com.darmaneh.ava.MainActivity;
import com.darmaneh.ava.R;
import com.darmaneh.fragments.BasicInfoFragment;
import com.darmaneh.fragments.BookmarkFragment;
import com.darmaneh.fragments.FragmentLifecycle;
import com.darmaneh.fragments.HistoryFragment;
import com.darmaneh.fragments.RequestFailureDialog;
import com.darmaneh.models.Mail.MailModel;
import com.darmaneh.models.call.InstantInfoListModel;
import com.darmaneh.models.patient_record.PatientRecordModel;
import com.darmaneh.requests.PatientInfoDetailList;
import com.darmaneh.requests.PatientRecord;

import java.util.ArrayList;
import java.util.List;

public class RecordFragment extends Fragment {
    ViewPager viewPager;
    TabLayout tabLayout;
    Context thisCntx;
    ViewPagerAdapter adapter;
    BasicInfoFragment basicInfo;
    HistoryFragment history;
    BookmarkFragment bookmark;
    String TAG = RecordFragment.class.getSimpleName();

    public RecordFragment() {
        // Required empty public constructor
    }
    public static RecordFragment newInstance() {
        RecordFragment fragment = new RecordFragment();
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_record, container, false);
        viewPager = (ViewPager) rootView.findViewById(R.id.view_pager);
        thisCntx = rootView.getContext();
        tabLayout = (TabLayout) rootView.findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        setupAdapter();
        return rootView;
    }

    @Override
    public void onStart() {
//        getDataFromServer();
//        getIllnessHistoryDataFromServer();
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
        MainActivity.currentItem = viewPager.getCurrentItem();
    }

    private void changeTabsFont() {
        ViewGroup vg = (ViewGroup) tabLayout.getChildAt(0);
        int tabsCount = vg.getChildCount();
        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
            int tabChildsCount = vgTab.getChildCount();
            for (int i = 0; i < tabChildsCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    ((TextView) tabViewChild).setTypeface(App.getFont(3));
                    ((TextView) tabViewChild).setTextColor(Color.parseColor("#000000"));
                }
            }
        }
    }

    private void setupAdapter() {
        adapter = new ViewPagerAdapter(getChildFragmentManager());
        basicInfo = new BasicInfoFragment();
        history = new HistoryFragment();
        bookmark = new BookmarkFragment();

//        adapter.addFragment(bookmark,"افراد تحت کفالت");
        adapter.addFragment(history, "ایمیل‌ها");
        adapter.addFragment(basicInfo,"اطلاعات اولیه");
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}

            @Override
            public void onPageSelected(int position) {
                FragmentLifecycle fragmentToShow = (FragmentLifecycle) adapter.getItem(position);
                fragmentToShow.onResumeFragment();
            }

            @Override
            public void onPageScrollStateChanged(int state) {}
        });
        viewPager.setCurrentItem(MainActivity.currentItem);
        changeTabsFont();
    }

    private void fillAdapterContent(PatientRecordModel prm){
        basicInfo.setPatientRecordModel(prm);
        history.setSymptomCheckerResults(prm.getSymptomCheckerResults(), prm.getVisitResults());
        bookmark.setConditionDetails(prm.getConditionDetails());
    }

    public void getDataFromServer() {
        PatientRecord.get_patient_record(thisCntx, gpr);
    }

    public void getIllnessHistoryDataFromServer(){
        PatientInfoDetailList.get_info_list(thisCntx, false, getInfoList);
    }

    PatientRecord.GetPatientRecord gpr = new PatientRecord.GetPatientRecord() {
        @Override
        public void onHttpResponse(Boolean success, PatientRecordModel patientRecordModel) {
            if (success) {
                fillAdapterContent(patientRecordModel);
            } else {
                RequestFailureDialog dialogFragment = new RequestFailureDialog();
                dialogFragment.setRetryFunction(new RequestFailureDialog.RetryOnClickInterface() {
                    @Override
                    public void onClick() {
                        getDataFromServer();
                    }
                });
                dialogFragment.show(getFragmentManager(), "RequestFailureDialog");
            }
        }
    };

    PatientInfoDetailList.GetInfoList getInfoList = new PatientInfoDetailList.GetInfoList() {
        @Override
        public void onHttpResponse(Boolean success, List<MailModel> mailModelList) {
            if(success){
//                basicInfo.setIllnessHistoryResults(instantInfoListModel);
            }else{
                RequestFailureDialog dialogFragment = new RequestFailureDialog();
                dialogFragment.setRetryFunction(new RequestFailureDialog.RetryOnClickInterface() {
                    @Override
                    public void onClick() {
                        getIllnessHistoryDataFromServer();
                    }
                });
            }
        }
    };

    private class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

}
