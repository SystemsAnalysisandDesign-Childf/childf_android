package com.darmaneh.fragments.bottom_navigation;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.darmaneh.adapters.ProfessionListAdapter;
import com.darmaneh.ava.App;
import com.darmaneh.ava.BuildConfig;
import com.darmaneh.ava.R;
import com.darmaneh.ava.call.ChooseCallType;
import com.darmaneh.ava.call.WaitForConnection;
import com.darmaneh.dialogs.DarmanehProgressDialog;
import com.darmaneh.fragments.RequestFailureDialog;
import com.darmaneh.models.call.CategoryListDescText;
import com.darmaneh.models.call.ProfessionListModel;
import com.darmaneh.requests.GetProfessionRequest;
import com.darmaneh.requests.GetRoomNumberRequest;
import com.darmaneh.requests.Variable;
import com.darmaneh.utilities.Analytics;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Sepehr Abdous on 6/12/2017.
 */

public class TeleMedicineFragment extends Fragment {

    TextView choose_specialist;
    List<ProfessionListModel> doctorProfessionListModels;
    RecyclerView rv;
    LinearLayoutManager llm;
    Context context;
    ProfessionListAdapter adapter;
    DarmanehProgressDialog mProgress;
    TextView desc;

    public TeleMedicineFragment() {
        // Required empty public constructor
    }

    public static TeleMedicineFragment newInstance() {
        return new TeleMedicineFragment();
    }

    public void initialize(View view){
        choose_specialist = (TextView) view.findViewById(R.id.choose_specialist_text);
        desc = (TextView) view.findViewById(R.id.category_text_desc);

        choose_specialist.setTypeface(App.getFont(2));
        desc.setTypeface(App.getFont(3));

        doctorProfessionListModels =new ArrayList<>();
        rv = (RecyclerView)view.findViewById(R.id.rv);
        llm = new LinearLayoutManager(view.getContext());
        rv.setHasFixedSize(true);
        rv.setLayoutManager(llm);
        context = view.getContext();
    }

    public void getCategories() {
        GetProfessionRequest.get_profession_list(context, getProfessions);
    }

    public void getDescText() {
        GetProfessionRequest.get_desc_text(context, getDescText);
    }

    GetProfessionRequest.GetProfessionList getProfessions = new GetProfessionRequest.GetProfessionList() {
        @Override
        public void onHttpResponse(Boolean success, List<ProfessionListModel> models) {
            if (success) {
                doctorProfessionListModels = models;
                adapter = new ProfessionListAdapter(doctorProfessionListModels, context);
                rv.setAdapter(adapter);
            } else {
                RequestFailureDialog dialogFragment = new RequestFailureDialog();
                dialogFragment.setRetryFunction(new RequestFailureDialog.RetryOnClickInterface() {
                    @Override
                    public void onClick() {
                        init_room_num();
                    }
                });
                dialogFragment.show(getFragmentManager(), "RequestFailureDialog");
            }
        }

        @Override
        public void stopProgress() {
            if (mProgress!=null)
                mProgress.dismiss();
        }
    };

    GetProfessionRequest.GetDescText getDescText = new GetProfessionRequest.GetDescText() {
        @Override
        public void onHttpResponse(Boolean success, CategoryListDescText categoryListDescText) {
            if (success) {
                if (BuildConfig.FLAVOR.equals("bazaar")) {
                    desc.setText(categoryListDescText.getTextBazaar());
                } else {
                    desc.setText(categoryListDescText.getText());
                }
            }
        }
    };

    public void init_room_num(){
        GetRoomNumberRequest.get_room_number(context, getRoomNumber, true, true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.telemedicine_fragment, container, false);
        initialize(rootView);
        return rootView;
    }

    @Override
    public void onResume(){
        super.onResume();
        //OnResume Fragment
        init_room_num();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Analytics.sendScreenName("telemedicine");

    }

    GetRoomNumberRequest.GetRoomNumber getRoomNumber = new GetRoomNumberRequest.GetRoomNumber() {
        @Override
        public void onHttpResponse(Boolean success, JSONObject response) {
            if(success){
                String roomNumber = response.optString("room_number", null);
                if (roomNumber == null) {
                    Intent intent = new Intent(context, WaitForConnection.class);
                    startActivity(intent);
                } else if (!BuildConfig.FLAVOR.equals("bazaar")){
                    String doctorName = response.optString("ph_first_name", "") + " " + response.optString("ph_last_name", "");
                    Intent intent = new Intent(context, ChooseCallType.class);
                    intent.putExtra("roomNumber", roomNumber);
                    intent.putExtra("doctorName", doctorName);
                    startActivity(intent);
                }
            }else{
                if (response!=null) {
                    String detail = response.optString("detail", null);
                    if (detail != null && detail.equals("Not found.")) {
                        getCategories();
                        getDescText();
                    }
                } else {
                    RequestFailureDialog dialogFragment = new RequestFailureDialog();
                    dialogFragment.setRetryFunction(new RequestFailureDialog.RetryOnClickInterface() {
                        @Override
                        public void onClick() {
                            init_room_num();
                        }
                    });
                    dialogFragment.show(getFragmentManager(), "RequestFailureDialog");
                }
            }
        }

        @Override
        public void stopProgress(DarmanehProgressDialog progress) {
            mProgress = progress;
        }
    };
}
