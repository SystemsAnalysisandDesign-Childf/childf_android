package com.darmaneh.fragments.bottom_navigation;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.darmaneh.adapters.ConditionDetailListAdapter;
import com.darmaneh.ava.R;
import com.darmaneh.fragments.RequestFailureDialog;
import com.darmaneh.models.ConditionDetailListModel;
import com.darmaneh.models.unadopted.UnadoptedListModel;
import com.darmaneh.requests.ConditionDetail;
import com.darmaneh.utilities.Analytics;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class ConditionFragment extends Fragment  implements SearchView.OnQueryTextListener{
    List<UnadoptedListModel> unadoptedListModels, baseUnadoptedlListModels;
    ConditionDetailListAdapter adapter;
    RecyclerView rv;
    LinearLayoutManager llm;
    Context thisCntx;
    SearchView searchCondition;


    public ConditionFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Analytics.sendScreenName("condition_detail/list");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView =  inflater.inflate(R.layout.fragment_condition, container, false);
        initialize(rootView);
        getDataFromServer();
        return rootView;
    }

    public void initialize(View view){
        unadoptedListModels =new ArrayList<>();
        rv = (RecyclerView)view.findViewById(R.id.rv);
        llm = new LinearLayoutManager(view.getContext());
        rv.setHasFixedSize(true);
        rv.setLayoutManager(llm);
        thisCntx = view.getContext();
        searchCondition = (SearchView) view.findViewById(R.id.search);
        searchCondition.setIconifiedByDefault(false);
        baseUnadoptedlListModels= new ArrayList<>();
    }


    public void getDataFromServer() {
        ConditionDetail.get_condition_detail_list(thisCntx, gcdl);
    }

    ConditionDetail.GetConditionDetailList gcdl = new ConditionDetail.GetConditionDetailList() {
        @Override
        public void onHttpResponse(Boolean success, List<UnadoptedListModel> models) {
            if (success) {
                unadoptedListModels = models;
//                sortConditionModels();
                backup();
                adapter = new ConditionDetailListAdapter(unadoptedListModels, thisCntx);
                SetupConditionSearch();
                rv.setAdapter(adapter);
            } else {
                RequestFailureDialog dialogFragment = new RequestFailureDialog();
                dialogFragment.setRetryFunction(new RequestFailureDialog.RetryOnClickInterface() {
                    @Override
                    public void onClick() {
                        getDataFromServer();
                    }
                });
                dialogFragment.show(getFragmentManager(), "RequestFailureDialog");
            }
        }
    };

    public void SetupConditionSearch(){
        TextView TV_SearchLocations = (TextView) searchCondition.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        int searchImgId = android.support.v7.appcompat.R.id.search_button;
        ImageView IV_SearchIcon = (ImageView) searchCondition.findViewById(searchImgId);
        searchCondition.setOnQueryTextListener(this);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }


    @Override
    public boolean onQueryTextChange(String query) {
        search(query);
        return true;
    }

    public void backup(){
        for (int i = 0; i < unadoptedListModels.size(); i++) {
            baseUnadoptedlListModels.add(unadoptedListModels.get(i));
        }
    }

    public void search(String query){
        unadoptedListModels.clear();
        for (int i = 0; i < baseUnadoptedlListModels.size(); i++) {
            unadoptedListModels.add(baseUnadoptedlListModels.get(i));
        }
        if (!query.equals("")){
            if (unadoptedListModels.size()!=0) {
                final List<UnadoptedListModel> filteredConditionList = filter(unadoptedListModels, query);
                if (filteredConditionList.size()!=0) {
                    applyChangeInSearchText(filteredConditionList);
                }
                else {
                    unadoptedListModels.clear();
                    adapter.notifyDataSetChanged();
                }
            }
        }else {
            adapter.notifyDataSetChanged();
        }
        adapter.notifyDataSetChanged();
    }

    private void applyChangeInSearchText(List<UnadoptedListModel> models) {
        for (int i = 0; i < unadoptedListModels.size(); i++) {
            final UnadoptedListModel model = unadoptedListModels.get(i);
            if (!models.contains(model)) {
                unadoptedListModels.remove(i);
                adapter.notifyDataSetChanged();
                i--;/*when you remove an item in the iteration you should go back so you can check all of the elements,
                        if not, one element will be missed each time you remove*/
            }
        }
    }

    private List<UnadoptedListModel> filter(List<UnadoptedListModel> models, String query){
        query = query.toLowerCase();
        final List<UnadoptedListModel> filteredSymptomModel = new ArrayList<>();
        for (UnadoptedListModel model: models){
            final String text = model.getUser().getLastName().toLowerCase();
            final String text2 = model.getUser().getFirstName().toLowerCase();
            if ((text2 + " " + text).contains(query)) {
                filteredSymptomModel.add(model);
            }
        }
        return filteredSymptomModel;
    }
}