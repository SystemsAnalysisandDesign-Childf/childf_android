package com.darmaneh.fragments.bottom_navigation;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatSpinner;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;
import android.widget.Toast;

import com.darmaneh.adapters.CustomSpinnerAdapter;
import com.darmaneh.ava.App;
import com.darmaneh.ava.R;
import com.darmaneh.ava.health_centers.LocationMainActivity;
import com.darmaneh.ava.health_centers.MapsActivity;
import com.darmaneh.dialogs.DarmanehProgressDialog;
import com.darmaneh.dialogs.DarmanehToast;
import com.darmaneh.dialogs.SweetAlertDialog;
import com.darmaneh.models.ProvinceCityModel;
import com.darmaneh.models.location.HealthCentersLocationModel;
import com.darmaneh.requests.HealthCenterLocation;
import com.darmaneh.requests.HealthCenters;
import com.darmaneh.requests.ProvinceCity;
import com.darmaneh.utilities.Analytics;
import com.darmaneh.utilities.SignInFunction;
import com.darmaneh.utilities.Storage;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;

import java.util.ArrayList;
import java.util.List;

public class LocationFragment extends Fragment {
    private static final int LOCATION_PERMISSION = 4;
    TextView okBtn, findBtn;
    AppCompatSpinner provinceSpinner, citySpinner;
    private static final String TAG = LocationFragment.class.getSimpleName();
    private List<ProvinceCityModel> pcm;
    private FusedLocationProviderClient mFusedLocationClient;
    private DarmanehProgressDialog progressDialog;
    private boolean locationOn;

    Handler locationHandler = new Handler();
    Runnable locationRunnalbe = new Runnable() {
        @Override
        public void run() {
            getLocation();
        }
    };

    public LocationFragment() {
        // Required empty public constructor
    }

    public static LocationFragment newInstance() {
        return new LocationFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Analytics.sendScreenName("locations");
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(context);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_location, container, false);
        okBtn = (TextView) rootView.findViewById(R.id.ok_btn);
        citySpinner = (AppCompatSpinner) rootView.findViewById(R.id.location_city);
        provinceSpinner = (AppCompatSpinner) rootView.findViewById(R.id.location_province);
        findBtn = (TextView) rootView.findViewById(R.id.find_btn);
        setViewsFont(rootView);
        requestForDB(getContext());
        setOnClick(getContext());
        checkPermission();
        return rootView;
    }

    private void checkPermission() {
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    LOCATION_PERMISSION);
        }
    }

    private void requestForDB(final Context context) {
        ProvinceCity.get_province_city(context, new ProvinceCity.GetProvinceCity() {
            @Override
            public void onHttpResponse(Boolean success, List<ProvinceCityModel> provinceCityModels) {
                if (success) {
                    pcm = provinceCityModels;
                    initSpinner(context, provinceCityModels);
                }
            }
        });
    }

    private void initSpinner(Context context, List<ProvinceCityModel> provinceCityModels) {
        ArrayList<String> provinces = new ArrayList<>();
        for (int i = 0; i < provinceCityModels.size(); i++) {
            provinces.add(provinceCityModels.get(i).getName());
        }
        CustomSpinnerAdapter adapter = new CustomSpinnerAdapter(context, provinces);
        provinceSpinner.setAdapter(adapter);

        CustomSpinnerAdapter adaptercity = new CustomSpinnerAdapter(context, (ArrayList<String>) provinceCityModels.get(0).getCities());
        citySpinner.setAdapter(adaptercity);
    }

    private void setOnClick(final Context context) {
        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (provinceSpinner.getSelectedItemPosition() == 0 || provinceSpinner.getSelectedItem() == null) {
                    DarmanehToast.makeText(context, "استان را انتخاب کنید", Toast.LENGTH_SHORT);
                } else {
                    String province = String.valueOf(provinceSpinner.getSelectedItem());
                    String city = String.valueOf(citySpinner.getSelectedItem());
                    Log.d(TAG, "p : " + province + " c : " + city);
                    Intent intent = new Intent(context, LocationMainActivity.class);
                    intent.putExtra("province", province);
                    intent.putExtra("city", city);
                    intent.putExtra("nearest",false);
                    context.startActivity(intent);
                }
            }
        });
        provinceSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                if (pcm != null) {
                    CustomSpinnerAdapter adaptercity = new CustomSpinnerAdapter(view.getContext(), (ArrayList<String>) pcm.get(position).getCities());
                    citySpinner.setAdapter(adaptercity);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        findBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!Storage.getIsLogin()){
                    SignInFunction.signInDialog(getContext(), "برای مشاهده مراکز درمانی، ابتدا وارد شوید.");
                }else {
                    progressDialog = new DarmanehProgressDialog(getContext());
                    progressDialog.changeText("در حال دریافت موقعیت مکانی شما");
                    progressDialog.show();
                    getLocation();
                }
            }
        });

    }

    private void setViewsFont(View view) {
        ((TextView) view.findViewById(R.id.location_text)).setTypeface(App.getFont(5));
        ((TextView) view.findViewById(R.id.or_text)).setTypeface(App.getFont(4));
        ((TextView) view.findViewById(R.id.nearest_txt)).setTypeface(App.getFont(5));
        ((TextView) view.findViewById(R.id.nearest_detail_txt)).setTypeface(App.getFont(3));
        okBtn.setTypeface(App.getFont(4));
        findBtn.setTypeface(App.getFont(4));

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == LOCATION_PERMISSION && resultCode == Activity.RESULT_OK) {
            getLocation();
        }
    }

    @SuppressWarnings({"ResourceType"})
    public void getLocation() {
        mFusedLocationClient.getLastLocation()
                .addOnSuccessListener(getActivity(), new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(final Location location) {
                        // Got last known location. In some rare situations this can be null.
                        if (location != null) {
                            if(progressDialog.isShowing())
                                progressDialog.dismiss();
                            Intent intent = new Intent(getContext(), LocationMainActivity.class);
                            intent.putExtra("lat", location.getLatitude());
                            intent.putExtra("lng", location.getLongitude());
                            intent.putExtra("nearest", true);
                            startActivity(intent);
                        } else if (! isLocationOn()) {
                            if(progressDialog.isShowing())
                                progressDialog.dismiss();
                            AlertDialog dialog = new AlertDialog.Builder(getContext())
                                    .setMessage("موقعیت مکانی شما غیرفعال است. آیا مایلید آن را فعال کنید؟")
                                    .setCancelable(false)
                                    .setPositiveButton("بله", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            Intent locationIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                            startActivityForResult(locationIntent, 10);
                                        }
                                    })
                                    .setNegativeButton("خیر", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {

                                        }
                                    })
                                    .show();
                            TextView textMessage = (TextView) dialog.findViewById(android.R.id.message);
                            textMessage.setTypeface(App.getFont(3));
                            TextView textPositive = (TextView) dialog.findViewById(android.R.id.button1);
                            textPositive.setTypeface(App.getFont(3));
                            textPositive.setTextColor(ContextCompat.getColor(getContext(), R.color.colorPrimary));
                            TextView textNegative = (TextView) dialog.findViewById(android.R.id.button2);
                            textNegative.setTypeface(App.getFont(3));
                            textNegative.setTextColor(ContextCompat.getColor(getContext(), R.color.colorPrimary));
                        } else {
                            if(!progressDialog.isShowing())
                                progressDialog.show();
                            locationHandler.postDelayed(locationRunnalbe,1000);
                            progressDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                @Override
                                public void onDismiss(DialogInterface dialog) {
                                    locationHandler.removeCallbacks(locationRunnalbe);
                                }
                            });
                        }

                    }
                });
    }

    public boolean isLocationOn() {
        LocationManager lm = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled = false;
        boolean network_enabled = false;
        gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        return gps_enabled & network_enabled;
    }
}
