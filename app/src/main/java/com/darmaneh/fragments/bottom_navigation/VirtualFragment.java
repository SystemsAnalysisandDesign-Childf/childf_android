package com.darmaneh.fragments.bottom_navigation;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.darmaneh.adapters.EmergencyAdapter;
import com.darmaneh.ava.App;
import com.darmaneh.ava.R;
import com.darmaneh.ava.diagnosis.GetInformation;
import com.darmaneh.fragments.RequestFailureDialog;
import com.darmaneh.models.diagnosis.Condition;
import com.darmaneh.models.emergency.EmergencyModel;
import com.darmaneh.models.unadopted.UnadoptedListModel;
import com.darmaneh.requests.ConditionDetail;
import com.darmaneh.utilities.Analytics;
import com.darmaneh.utilities.SignInFunction;
import com.darmaneh.utilities.Storage;

import java.util.ArrayList;
import java.util.List;


public class VirtualFragment extends Fragment {

    Context context;
    List<EmergencyModel> emergencyModels;
    EmergencyAdapter adapter;
    RecyclerView rv;
    LinearLayoutManager llm;

    public VirtualFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Analytics.sendScreenName("sc/main");
    }

    public static VirtualFragment newInstance() {
        return new VirtualFragment();
    }

    public void getDataFromServer() {
        ConditionDetail.get_emergency_list(context, gcdl);
    }

    ConditionDetail.GetEmergencyList gcdl = new ConditionDetail.GetEmergencyList() {
        @Override
        public void onHttpResponse(Boolean success, List<EmergencyModel> models) {
            if (success) {
                emergencyModels = models;
//                sortConditionModels();
                adapter = new EmergencyAdapter(emergencyModels, context);
                rv.setAdapter(adapter);
            } else {
                RequestFailureDialog dialogFragment = new RequestFailureDialog();
                dialogFragment.setRetryFunction(new RequestFailureDialog.RetryOnClickInterface() {
                    @Override
                    public void onClick() {
                        getDataFromServer();
                    }
                });
                dialogFragment.show(getFragmentManager(), "RequestFailureDialog");
//            }
            }
        }
    };

    public void initialize(View view) {
        emergencyModels =new ArrayList<>();
        rv = (RecyclerView)view.findViewById(R.id.rv);
        llm = new LinearLayoutManager(view.getContext());
        rv.setHasFixedSize(true);
        rv.setLayoutManager(llm);
        context = view.getContext();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_virtual, container, false);
        initialize(view);
        getDataFromServer();
        return view;
    }
}
