package com.darmaneh.fragments.user_progress;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.darmaneh.ava.App;
import com.darmaneh.ava.R;
import com.darmaneh.dialogs.DarmanehToast;
import com.darmaneh.utilities.Analytics;
import com.darmaneh.utilities.Functions;
import com.darmaneh.utilities.JalaliCalendar;

import java.util.Calendar;

public class UserAgeFragment extends Fragment {
    private static final String ARG_AGE = "age";
    private static final String EMPTY_ERROR = "پر کردن این بخش اجباری است";
    TextInputEditText userAgeInput;
    TextInputLayout userAgeLayout;
    Button okBtn;
    TextView userAgeTitle;
    int currentYear;

    private Integer ageParam;

    private OnFragmentInteractionListener mListener;

    public UserAgeFragment() {
        // Required empty public constructor
    }

    public static UserAgeFragment newInstance(Integer age) {
        UserAgeFragment fragment = new UserAgeFragment();
        if (age != null) {
            Bundle args = new Bundle();
            args.putInt(ARG_AGE, age);
            fragment.setArguments(args);
        }
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            ageParam = getArguments().getInt(ARG_AGE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_user_age, container, false);
        userAgeInput = (TextInputEditText) rootView.findViewById(R.id.user_age_input);
        userAgeLayout = (TextInputLayout) rootView.findViewById(R.id.user_age_layout);
        userAgeTitle = (TextView) rootView.findViewById(R.id.user_age_title);
        okBtn = (Button) rootView.findViewById(R.id.ok_btn);
        if(ageParam != null)
            userAgeInput.setText(String.valueOf(ageParam));
        setViewsFont();
        setOnclick();
        return rootView;
    }

    private void setOnclick() {
        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String inputAge = userAgeInput.getText().toString();
                if(validateAge(view.getContext(), inputAge))
                    onButtonPressed(getValueOf(inputAge),view);
            }
        });
    }

    private void setViewsFont() {
        userAgeInput.setTypeface(App.getFont(3));
        userAgeTitle.setTypeface(App.getFont(5));
        okBtn.setTypeface(App.getFont(4));
    }
    public void onButtonPressed(Integer age, View view) {
        if (mListener != null) {
            mListener.onAgeCallback (age);
        }
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken() , 0);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Analytics.sendScreenName("user/set_age");
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
        Calendar c = Calendar.getInstance();
        currentYear = (new JalaliCalendar(c.get(Calendar.YEAR),
                c.get(Calendar.MONTH) + 1,
                c.get(Calendar.DATE))).getYear();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onAgeCallback(Integer age);
    }
    private Integer getValueOf(String param){
        if(param.equals(""))
            return null;
        else
            return Integer.valueOf(param);
    }
    private boolean validateAge(Context context, String inputAge){
        if (!inputAge.trim().isEmpty()) {
            if(inputAge.length() != 4){
                userAgeLayout.setError(Functions.setFont("سال تولد باید ۴ رقم باشد.(به عنوان مثال ۱۳۷۵)", 3));
                return false;
            }else if(currentYear - Integer.parseInt(inputAge) < 0 || currentYear - Integer.parseInt(inputAge) > 150 ){
                userAgeLayout.setError(Functions.setFont("سال تولد وارد شده صحیح نمی باشد.", 3));
                return false;
            }else
                return true;
        }else {
            userAgeLayout.setError(Functions.setFont(EMPTY_ERROR, 3));
            return false;
        }
    }
}
