package com.darmaneh.fragments.user_progress;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.darmaneh.ava.App;
import com.darmaneh.ava.R;
import com.darmaneh.dialogs.DarmanehToast;
import com.darmaneh.requests.UserFlowV3;
import com.darmaneh.utilities.Analytics;
import com.darmaneh.utilities.Functions;
import com.darmaneh.utilities.Storage;
import com.victor.loading.rotate.RotateLoading;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link PassVerifyFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link PassVerifyFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PassVerifyFragment extends Fragment {
    private static final String ARG_PASS = "pass";
    TextView forgetBtn ;
    TextInputLayout passLayout;
    TextInputEditText passInput;
    Button okBtn;
    LinearLayout mainContainer, loadingContainer;
    RotateLoading rotateLoading;

    private String passParam;
    private static final String TAG = PassVerifyFragment.class.getSimpleName();

    private OnFragmentInteractionListener mListener;

    public PassVerifyFragment() {
        // Required empty public constructor
    }

    public static PassVerifyFragment newInstance(String pass) {
        PassVerifyFragment fragment = new PassVerifyFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PASS, pass);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            passParam = getArguments().getString(ARG_PASS);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView =  inflater.inflate(R.layout.fragment_pass_verify, container, false);
        forgetBtn = (TextView) rootView.findViewById(R.id.forget_btn);
        passLayout = (TextInputLayout) rootView.findViewById(R.id.pass_verify_input_layout);
        passInput = (TextInputEditText) rootView.findViewById(R.id.pass_verify_edit_text);
        okBtn = (Button) rootView.findViewById(R.id.ok_btn);
        loadingContainer = (LinearLayout) rootView.findViewById(R.id.loading_container);
        mainContainer = (LinearLayout) rootView.findViewById(R.id.main_container);
        rotateLoading = (RotateLoading) rootView.findViewById(R.id.rotate_loading);
        showMainContainer();
        passInput.setText(passParam);
        setViewsFont(rootView);
        setOnClicks();
        return rootView;
    }


    private void setOnClicks() {
        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String inputPass = passInput.getText().toString();
                if(validatePass(inputPass)){
                    requestPassVerify(view.getContext(), inputPass);
                    showLoadingContainer();
                }
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken() , 0);
            }
        });
        forgetBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                showLoadingContainer();
                requestCodeVerify(view.getContext());
//                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
//                imm.hideSoftInputFromWindow(view.getWindowToken() , 0);
            }
        });
    }

    private void requestCodeVerify(final Context context) {
        Log.d(TAG, "phone num "+ Storage.getPhoneNum());
        UserFlowV3.patient_restart_password(context, Storage.getPhoneNum());
    }

    private void setViewsFont(View rootView) {
        ((TextView) rootView.findViewById(R.id.pass_verify_text)).setTypeface(App.getFont(5));
        forgetBtn.setTypeface(App.getFont(3));
        passInput.setTypeface(App.getFont(3));
        okBtn.setTypeface(App.getFont(4));
        ((TextView) rootView.findViewById(R.id.wait_text)).setTypeface(App.getFont(4));
    }

    public void onButtonPressed(String pass, boolean forget) {
        if (mListener != null) {
            mListener.onPassVerifyCallback(pass, forget);
        }
    }

    @Override
    public void onAttach(Context context) {
        Analytics.sendScreenName("user/verify_pass");
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onPassVerifyCallback(String pass , boolean forget);
    }

    private void requestPassVerify(Context context, final String pass){
        UserFlowV3.patient_login(context, Storage.getPhoneNum(), pass,
                new UserFlowV3.PatientLogin() {
            @Override
            public void onHttpResponse(Boolean success) {
                if(success){
                    onButtonPressed(pass, false);
                }
            }

            @Override
            public void onPasswordMismatch() {
                passLayout.setError(Functions.setFont("رمز عبور اشتباه است", 3));
                requestFocus(passInput);
            }

            @Override
            public void onFinishResponse() {
                showMainContainer();
            }
        });
    }

    private boolean validatePass(String pass){
        if(pass.length() < 6){
            passLayout.setError(Functions.setFont("رمز عبور باید حداقل ۶ کاراکتر باشد", 3));
            requestFocus(passInput);
            return false;
        }else {
            passLayout.setErrorEnabled(false);
            return true;
        }
    }
    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private void showMainContainer() {
        mainContainer.setVisibility(View.VISIBLE);
        loadingContainer.setVisibility(View.GONE);
        rotateLoading.stop();
    }
    private void showLoadingContainer(){
        mainContainer.setVisibility(View.GONE);
        loadingContainer.setVisibility(View.VISIBLE);
        rotateLoading.start();
    }

}
