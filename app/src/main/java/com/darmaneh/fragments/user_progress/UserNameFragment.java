package com.darmaneh.fragments.user_progress;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.darmaneh.ava.App;
import com.darmaneh.ava.R;
import com.darmaneh.utilities.Analytics;
import com.darmaneh.utilities.Functions;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link UserNameFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link UserNameFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class UserNameFragment extends Fragment {
    private static final String ARG_NAME = "name";
    private static final String ARG_LAST_NAME = "last_name";

    private static final String TAG = UserNameFragment.class.getSimpleName();
    private String mName;
    private String mLastName;
    TextView userNameTitle, userLastNameTitle;
    Button okBtn;
    TextInputEditText userNameInput , userLastNameInput;
    TextInputLayout userNameLayout, userLastNameLayout;

    private OnFragmentInteractionListener mListener;

    public UserNameFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param firstName Parameter 1.
     * @return A new instance of fragment UserNameFragment.
     */
    public static UserNameFragment newInstance(String firstName, String lastName) {
        UserNameFragment fragment = new UserNameFragment();
        Bundle args = new Bundle();
        args.putString(ARG_NAME, firstName);
        args.putString(ARG_LAST_NAME,lastName);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mName = getArguments().getString(ARG_NAME);
            mLastName = getArguments().getString(ARG_LAST_NAME);
        }
    }

    private void setOnClick() {
        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String inputName = userNameInput.getText().toString();
                String inputLastName = userLastNameInput.getText().toString();
                if(!inputName.equals("") && !inputLastName.equals("")) {
                    onButtonPressed(inputName, inputLastName, view);
                    userLastNameLayout.setErrorEnabled(false);
                    userNameLayout.setErrorEnabled(false);
                }
                else if(inputName.equals("")){
                    userNameLayout.setError(Functions.setFont("پر کردن این بخش اجباری است", 3));
                    userLastNameLayout.setErrorEnabled(false);
                }else if(inputLastName.equals("")){
                    userLastNameLayout.setError(Functions.setFont("پر کردن این بخش اجباری است", 3));
                    userNameLayout.setErrorEnabled(false);
                }
            }
        });
    }

    private void setViewsFont() {
        userNameTitle.setTypeface(App.getFont(5));
        userLastNameTitle.setTypeface(App.getFont(5));
        userNameInput.setTypeface(App.getFont(3));
        userLastNameInput.setTypeface(App.getFont(3));
        okBtn.setTypeface(App.getFont(4));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView =  inflater.inflate(R.layout.fragment_user_name, container, false);
        userNameTitle = (TextView) rootView.findViewById(R.id.user_name_title);
        userLastNameTitle = (TextView) rootView.findViewById(R.id.user_last_name_title);
        userNameInput = (TextInputEditText) rootView.findViewById(R.id.user_name_input);
        userLastNameInput = (TextInputEditText) rootView.findViewById(R.id.user_last_name_input);
        okBtn = (Button) rootView.findViewById(R.id.ok_btn);
        userNameLayout = (TextInputLayout) rootView.findViewById(R.id.user_name_layout);
        userLastNameLayout = (TextInputLayout) rootView.findViewById(R.id.user_last_name_layout);

        userNameInput.setText(mName);
        userLastNameInput.setText(mLastName);
        setViewsFont();
        setOnClick();
        return rootView;
    }

    public void onButtonPressed(String name, String lastName, View view) {
        if (mListener != null) {
            mListener.onNameCallback(name, lastName);
        }
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken() , 0);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Analytics.sendScreenName("user/set_name");
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onNameCallback(String name , String lastName);
    }
}
