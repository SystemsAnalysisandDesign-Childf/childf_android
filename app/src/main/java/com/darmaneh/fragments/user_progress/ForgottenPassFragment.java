package com.darmaneh.fragments.user_progress;

import android.content.Context;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.darmaneh.ava.App;
import com.darmaneh.ava.R;
import com.darmaneh.dialogs.DarmanehToast;
import com.darmaneh.requests.UserFlowV3;
import com.darmaneh.utilities.Analytics;
import com.darmaneh.utilities.Functions;
import com.darmaneh.utilities.Storage;
import com.victor.loading.rotate.RotateLoading;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ForgottenPassFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ForgottenPassFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ForgottenPassFragment extends Fragment {

    private TextInputLayout codeVerifyLayout, passLayout, passRepeatLayout;
    private TextInputEditText codeVerifyEditText, passEditText, passRepeatEditText;
    private Button okBtn;
    private TextView callButton;
    private LinearLayout mainContainer, loadingContainer;
    private RotateLoading rotateLoading;
    private CountDownTimer countDownTimer;

    private Context myCntx;

    private OnFragmentInteractionListener mListener;
    private static final String TAG = ForgottenPassFragment.class.getSimpleName();

    public ForgottenPassFragment() {
        // Required empty public constructor
    }


    public static ForgottenPassFragment newInstance() {
        ForgottenPassFragment fragment = new ForgottenPassFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_forgotten_pass, container, false);
        myCntx = rootView.getContext();
        initViews(rootView);
        setViewsFont(rootView);
        setOnClicks(rootView);
        showMainContainer();
        countDownTimer.start();
        return rootView;
    }


    private void initViews(View view) {
        codeVerifyEditText = (TextInputEditText) view.findViewById(R.id.code_verify_edit_text);
        codeVerifyLayout = (TextInputLayout) view.findViewById(R.id.code_verify_layout);
        passEditText = (TextInputEditText) view.findViewById(R.id.pass_edit_text);
        passLayout = (TextInputLayout) view.findViewById(R.id.pass_layout);
        passRepeatEditText = (TextInputEditText) view.findViewById(R.id.pass_repeat_edit_text);
        passRepeatLayout = (TextInputLayout) view.findViewById(R.id.pass_repeat_layout);
        okBtn = (Button) view.findViewById(R.id.ok_btn);
        callButton = (TextView) view.findViewById(R.id.counter_btn);
        mainContainer = (LinearLayout) view.findViewById(R.id.main_container);
        loadingContainer = (LinearLayout) view.findViewById(R.id.loading_container);
        rotateLoading = (RotateLoading) view.findViewById(R.id.rotate_loading);
    }

    private void setViewsFont(View view) {
        codeVerifyEditText.setTypeface(App.getFont(3));
        passEditText.setTypeface(App.getFont(3));
        passRepeatEditText.setTypeface(App.getFont(3));
        ((TextView) view.findViewById(R.id.code_verify_title)).setTypeface(App.getFont(5));
        ((TextView) view.findViewById(R.id.pass_title)).setTypeface(App.getFont(5));
        ((TextView) view.findViewById(R.id.pass_repeat_title)).setTypeface(App.getFont(5));
        okBtn.setTypeface(App.getFont(4));
        callButton.setTypeface(App.getFont(3));
        ((TextView) view.findViewById(R.id.wait_text)).setTypeface(App.getFont(4));
    }

    private void setOnClicks(final View rootview) {
        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String password = passEditText.getText().toString();
                String passwordRepeat = passRepeatEditText.getText().toString();
                String verifyCode = codeVerifyEditText.getText().toString();
                if (validatePass(password, passwordRepeat) && validateCode(verifyCode)) {
                    showLoadingContainer();
                    requestForgetPassword(view.getContext(), password, verifyCode);
                }
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken() , 0);
            }
        });
        final int grayColor = ContextCompat.getColor(rootview.getContext(), R.color.gray_text);
        final int whiteColor = ContextCompat.getColor(rootview.getContext(), R.color.colorWhite);
        countDownTimer = new CountDownTimer(100000, 1000) {
            public void onTick(long millisUntilFinished) {
                callButton.setEnabled(false);
                callButton.setTextColor(grayColor);
                callButton.setText(Functions.toPersian((int) (millisUntilFinished / 1000)));
            }

            public void onFinish() {
                callButton.setEnabled(true);
                callButton.setTextColor(whiteColor);
                if (Storage.getCodeNum().equals("98"))
                    callButton.setText("دریافت کد تایید از طریق تماس");
                else
                    callButton.setText("دریافت مجدد کد تایید");
            }
        }.start();
        callButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                showLoadingContainer();
//                if (Storage.getCodeNum().equals("98")) {
//                    UserFlowV3.patient_send_verification(myCntx, Storage.getPhoneNum(), Storage.getCodeNum(), "voice", new UserFlowV3.PatientSendVerification() {
//                        @Override
//                        public void onHttpResponse(Boolean success) {
//                            if (success) {
//                                Analytics.sendScreenName("login/phone_call");
//                                countDownTimer.start();
//                                DarmanehToast.makeText(myCntx, "کد تایید شما تا لحظاتی دیگر از طریق تماس صوتی به شما اطلاع داده خواهد شد", Toast.LENGTH_LONG);
//                            }
//                        }
//
//                        @Override
//                        public void onFinishResponse() {
//                            showMainContainer();
//                        }
//                    });
//                } else {
//                    UserFlowV3.patient_send_verification(myCntx, Storage.getPhoneNum(), Storage.getCodeNum(), "sms",
//                            new UserFlowV3.PatientSendVerification() {
//                                @Override
//                                public void onHttpResponse(Boolean success) {
//                                    if (success) {
//                                        countDownTimer.start();
//                                        DarmanehToast.makeText(myCntx, "کد تایید شما تا لحظاتی دیگر مجددا به شما اطلاع داده خواهد شد", Toast.LENGTH_LONG);
//                                    }
//                                }
//                                @Override
//                                public void onFinishResponse() {
//                                    showMainContainer();
//                                }
//                            });
//                }
            }
        });
    }

    private boolean validateCode(String verifyCode) {
        if (verifyCode.length() == 6)
            return true;
        else {
            codeVerifyLayout.setError(Functions.setFont("کد تایید وارد شده صحیح نیست", 3));
            return false;
        }
    }

    private boolean validatePass(String password, String passwordRepeat) {
        if (password.length() < 6) {
            passLayout.setError(Functions.setFont("رمز عبور باید حداقل ۶ کاراکتر باشد", 3));
            requestFocus(passEditText);
            passRepeatLayout.setErrorEnabled(false);
            return false;
        } else {
            passLayout.setErrorEnabled(false);
        }
        if (!password.equals(passwordRepeat)) {
            passRepeatLayout.setError(Functions.setFont("رمز عبور مطابقت ندارد", 3));
            requestFocus(passRepeatEditText);
            return false;
        } else {
            passRepeatLayout.setErrorEnabled(false);
        }
        return true;
    }


    public void onButtonPressed() {
        if (mListener != null) {
            mListener.onForgottenPassCallback();
        }
    }

    @Override
    public void onAttach(Context context) {
        Analytics.sendScreenName("user/forgotten_pass");
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    private void requestForgetPassword(final Context context, String password, String verifyCode) {
//        UserFlowV3.patient_set_or_change_pass(context, Storage.getPhoneNum(),Storage.getCodeNum(),
//                password, verifyCode, new UserFlowV3.PatientSetOrChangePass() {
//                    @Override
//                    public void onHttpResponse(Boolean success) {
//                        if (success) {
//                            onButtonPressed();
//                            countDownTimer.cancel();
//                        }
//                    }
//
//                    @Override
//                    public void onInvalidVerifyCode() {
//
//                    }
//
//                    @Override
//                    public void onExpiredVerificationCode() {
//                        DarmanehToast.makeText(context, "کد تایید منقضی شده است\n مجددا کد دیگری به شما ارسال خواهد شد"
//                                , Toast.LENGTH_LONG);
//                        countDownTimer.cancel();
//                        countDownTimer.start();
//                        requestSendVerificationCode(context);
//                        showLoadingContainer();
//                    }
//
//                    @Override
//                    public void onVerificationCodeMismatch() {
//                        codeVerifyLayout.setError(Functions.setFont("کد تایید اشتباه است", 3));
//                    }
//
//                    @Override
//                    public void onFinishResponse() {
//                        showMainContainer();
//                    }
//                });
    }

    private void requestSendVerificationCode(Context context) {
//        UserFlowV3.patient_send_verification(context, Storage.getPhoneNum(), Storage.getCodeNum(),
//                "sms", new UserFlowV3.PatientSendVerification() {
//            @Override
//            public void onHttpResponse(Boolean success) {
//
//            }
//
//            @Override
//            public void onFinishResponse() {
//                showMainContainer();
//            }
//        });
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    public interface OnFragmentInteractionListener {
        void onForgottenPassCallback();
    }

    private void showMainContainer() {
        mainContainer.setVisibility(View.VISIBLE);
        loadingContainer.setVisibility(View.GONE);
        rotateLoading.stop();
    }

    private void showLoadingContainer() {
        mainContainer.setVisibility(View.GONE);
        loadingContainer.setVisibility(View.VISIBLE);
        rotateLoading.start();
    }
}
