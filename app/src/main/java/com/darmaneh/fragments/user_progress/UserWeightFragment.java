package com.darmaneh.fragments.user_progress;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.darmaneh.ava.App;
import com.darmaneh.ava.R;
import com.darmaneh.dialogs.DarmanehToast;
import com.darmaneh.utilities.Analytics;
import com.darmaneh.utilities.Functions;

public class UserWeightFragment extends Fragment {
    private static final String ARG_PARAM1 = "WEIGHT";
    private static final String EMPTY_ERROR = "پر کردن این بخش اجباری است";

    private Integer weightParam;
    private OnFragmentInteractionListener mListener;
    TextView userWeightTitle;
    Button okBtn;
    TextInputEditText userWeightInput;
    TextInputLayout userWeightLayout;

    public UserWeightFragment() {
        // Required empty public constructor
    }

    public static UserWeightFragment newInstance(Integer param1) {
        UserWeightFragment fragment = new UserWeightFragment();
        if(param1 != null) {
            Bundle args = new Bundle();
            args.putInt(ARG_PARAM1, param1);
            fragment.setArguments(args);
        }
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            weightParam = getArguments().getInt(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_user_weight, container, false);
        userWeightTitle = (TextView) rootView.findViewById(R.id.user_weight_title);
        userWeightInput = (TextInputEditText) rootView.findViewById(R.id.user_weight_input);
        userWeightLayout = (TextInputLayout) rootView.findViewById(R.id.user_weight_layout);
        okBtn = (Button) rootView.findViewById(R.id.ok_btn);
        if(weightParam != null)
            userWeightInput.setText(String.valueOf(weightParam));
        setViewsFont();
        setOnClick();
        return rootView;
    }

    public void onButtonPressed(Integer weight, View view) {
        Log.d("weight" , String.valueOf(weight));
        if (mListener != null) {
            mListener.onWeightCallback(weight);
        }
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken() , 0);
    }

    private void setOnClick() {
        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(validateWeight(view.getContext())) {
                    String inputWeight = userWeightInput.getText().toString();
                    onButtonPressed(getValueOf(inputWeight) , view);
                }
            }
        });
    }

    private void setViewsFont() {
        userWeightTitle.setTypeface(App.getFont(5));
        userWeightInput.setTypeface(App.getFont(3));
        okBtn.setTypeface(App.getFont(4));
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Analytics.sendScreenName("user/set_weight");
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onWeightCallback(Integer weight);
    }

    private Integer getValueOf(String param){
        if(param.equals(""))
            return null;
        else
            return Integer.valueOf(param);
    }
    private boolean validateWeight(Context context) {
        if (!userWeightInput.getText().toString().trim().isEmpty()) {
            if (Integer.parseInt(userWeightInput.getText().toString()) < 1 || Integer.parseInt(userWeightInput.getText().toString()) > 400) {
                DarmanehToast.makeText(context,"وزن وارد شده صحیح نمی باشد.", Toast.LENGTH_SHORT);
                return false;
            }
        }else{
            userWeightLayout.setError(Functions.setFont(EMPTY_ERROR, 3));
            return false;
        }
        return true;
    }
}
