package com.darmaneh.fragments.user_progress;

import android.content.Context;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.darmaneh.ava.App;
import com.darmaneh.ava.R;
import com.darmaneh.dialogs.DarmanehToast;
import com.darmaneh.requests.UserFlowV3;
import com.darmaneh.utilities.Analytics;
import com.darmaneh.utilities.Functions;
import com.darmaneh.utilities.Storage;
import com.victor.loading.rotate.RotateLoading;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link CodeVerifyFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link CodeVerifyFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CodeVerifyFragment extends Fragment {
    private static final String ARG_PASS = "PASS";

    private OnFragmentInteractionListener mListener;
    CountDownTimer countDownTimer;
    Context myCntx;

    TextView userCodeTitle, userCodeBody;
    EditText codeInput;
    Button okBtn;
    TextView callButton;
    private LinearLayout mainContainer, loadingContainer;
    private RotateLoading rotateLoading;

    public CodeVerifyFragment() {
        // Required empty public constructor
    }

    public static CodeVerifyFragment newInstance(String pass) {
        CodeVerifyFragment fragment = new CodeVerifyFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PASS, pass);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_code_verify, container, false);
        userCodeTitle = (TextView) rootView.findViewById(R.id.user_code_title);
        userCodeBody = (TextView) rootView.findViewById(R.id.user_code_body);
        okBtn = (Button) rootView.findViewById(R.id.ok_btn);
        callButton = (TextView) rootView.findViewById(R.id.counter_btn);
        codeInput = (EditText) rootView.findViewById(R.id.code_input);
        myCntx = rootView.getContext();
        mainContainer = (LinearLayout) rootView.findViewById(R.id.main_container);
        loadingContainer = (LinearLayout) rootView.findViewById(R.id.loading_container);
        rotateLoading = (RotateLoading) rootView.findViewById(R.id.rotate_loading);
        setViewsFont(rootView);
        String phone_num = Storage.getCompletePhoneNum("fa");
        String text1text = "برای ادامه ثبت‌ نام ما یک کد تائید به ایمیل شما "
                + " ارسال کردیم. لطفا این کد را وارد نمائید. (دریافت آن ممکن است کمی طول بکشد)";
        userCodeBody.setText(text1text);
        userCodeBody.setGravity(Gravity.CENTER);
        setOnClick();
        showMainContainer();
        return rootView;
    }

    private void setOnClick() {
        codeInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (codeInput.getText().toString().length() == 6) {
                    okBtn.setEnabled(true);
                    okBtn.setTextColor(ContextCompat.getColor(myCntx, R.color.colorWhite));
                } else {
                    okBtn.setEnabled(false);
                    okBtn.setTextColor(ContextCompat.getColor(myCntx, R.color.gray_text));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String codeStr = codeInput.getText().toString();
                final Context context = view.getContext();
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                showLoadingContainer();
                UserFlowV3.patient_set_or_change_pass(view.getContext(), Storage.getPhoneNum(),
                        codeStr,
                        new UserFlowV3.PatientSetOrChangePass() {
                            @Override
                            public void onHttpResponse(Boolean success) {
                                if (success) {
                                    onButtonPressed(codeStr);
                                }
                            }

                            @Override
                            public void onInvalidVerifyCode() {

                            }

                            @Override
                            public void onExpiredVerificationCode() {
                                DarmanehToast.makeText(context, "کد تایید منقضی شده است\n مجددا کد دیگری به شما ارسال خواهد شد"
                                        , Toast.LENGTH_LONG);
                                countDownTimer.cancel();
                                countDownTimer.start();
                                requestSendVerificationCode(context);
                                showLoadingContainer();
                            }

                            @Override
                            public void onVerificationCodeMismatch() {
                                codeInput.setError("کد تایید وارد شده اشتباه است");
                            }

                            @Override
                            public void onFinishResponse() {
                                showMainContainer();
                            }
                        });
            }
        });

        countDownTimer = new CountDownTimer(100000, 1000) {
            public void onTick(long millisUntilFinished) {
                callButton.setEnabled(false);
                callButton.setTextColor(ContextCompat.getColor(myCntx, R.color.gray_text));
                callButton.setText(Functions.toPersian((int) (millisUntilFinished / 1000)));
            }

            public void onFinish() {
                callButton.setEnabled(true);
                callButton.setTextColor(ContextCompat.getColor(myCntx, R.color.colorWhite));
                callButton.setText("دریافت مجدد کد تایید");
            }
        }.start();

        callButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showLoadingContainer();
                    UserFlowV3.patient_send_verification_again(myCntx, Storage.getPhoneNum(),
                            new UserFlowV3.PatientSendVerificationAgain() {
                                @Override
                                public void onHttpResponse(Boolean success) {
                                    if (success) {
                                        countDownTimer.start();
//                                        DarmanehToast.makeText(myCntx, "کد تایید شما تا لحظاتی دیگر مجددا به شما اطلاع داده خواهد شد", Toast.LENGTH_LONG);
                                    }
                                }
                            });
                }
        });
    }

    private void requestSendVerificationCode(Context context) {
//        UserFlowV3.patient_send_verification(context, Storage.getPhoneNum(), Storage.getCodeNum(),
//                "sms", new UserFlowV3.PatientSendVerification() {
//                    @Override
//                    public void onHttpResponse(Boolean success) {
//
//                    }
//
//                    @Override
//                    public void onFinishResponse() {
//                        showMainContainer();
//                    }
//                });
    }

    private void setViewsFont(View view) {
        userCodeTitle.setTypeface(App.getFont(5));
        userCodeBody.setTypeface(App.getFont(3));
        okBtn.setTypeface(App.getFont(4));
        callButton.setTypeface(App.getFont(3));
        ((TextView) view.findViewById(R.id.wait_text)).setTypeface(App.getFont(4));
    }

    public void onButtonPressed(String code) {
        if (mListener != null) {
            mListener.onCodeCallback(code);
//            countDownTimer.cancel();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Analytics.sendScreenName("user/verify_code");
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnFragmentInteractionListener {
        void onCodeCallback(String code);
    }

    private void showMainContainer() {
        mainContainer.setVisibility(View.VISIBLE);
        loadingContainer.setVisibility(View.GONE);
        rotateLoading.stop();
    }

    private void showLoadingContainer() {
        mainContainer.setVisibility(View.GONE);
        loadingContainer.setVisibility(View.VISIBLE);
        rotateLoading.start();
    }
}
