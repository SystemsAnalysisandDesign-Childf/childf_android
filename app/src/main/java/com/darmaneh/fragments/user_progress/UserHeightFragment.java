package com.darmaneh.fragments.user_progress;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.darmaneh.ava.App;
import com.darmaneh.ava.R;
import com.darmaneh.dialogs.DarmanehToast;
import com.darmaneh.utilities.Analytics;
import com.darmaneh.utilities.Functions;


public class UserHeightFragment extends Fragment {

    private static final String ARG_PARAM1 = "height";
    private static final String EMPTY_ERROR = "پر کردن این بخش اجباری است";
    private Integer heightParam ;
    private OnFragmentInteractionListener mListener;
    TextView userHeightTitle ;
    Button okBtn;
    TextInputEditText userHeightInput;
    TextInputLayout userHeightLayout;

    public UserHeightFragment() {
        // Required empty public constructor
    }

    public static UserHeightFragment newInstance(Integer height) {
        UserHeightFragment fragment = new UserHeightFragment();
        if(height != null) {
            Bundle args = new Bundle();
            args.putInt(ARG_PARAM1, height);
            fragment.setArguments(args);
        }
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            heightParam = getArguments().getInt(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_user_height, container, false);
        userHeightTitle = (TextView) rootView.findViewById(R.id.user_height_title);
        userHeightInput = (TextInputEditText) rootView.findViewById(R.id.user_height_input);
        userHeightLayout = (TextInputLayout) rootView.findViewById(R.id.user_height_layout);
        okBtn = (Button) rootView.findViewById(R.id.ok_btn);
        if(heightParam != null)
            userHeightInput.setText(String.valueOf(heightParam));
        setViewsFont();
        setOnClick();
        return rootView;
    }

    private void setViewsFont() {
        userHeightTitle.setTypeface(App.getFont(5));
        userHeightInput.setTypeface(App.getFont(3));
        okBtn.setTypeface(App.getFont(4));
    }

    private void setOnClick() {
        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(validateHeight(view.getContext())) {
                    String inputHeight = userHeightInput.getText().toString();
                    onButtonPressed(getValueOf(inputHeight), view);
                }
            }
        });
    }

    public void onButtonPressed(Integer height, View view) {
        Log.d("height", String.valueOf(height));
        if (mListener != null) {
            mListener.onHeightCallback(height);
        }
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken() , 0);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Analytics.sendScreenName("user/set_height");
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onHeightCallback(Integer height);
    }

    private boolean validateHeight(Context context) {
        if (!userHeightInput.getText().toString().trim().isEmpty()) {
            if (Integer.parseInt(userHeightInput.getText().toString())<20 || Integer.parseInt(userHeightInput.getText().toString()) > 270){
                DarmanehToast.makeText(context,"قد وارد شده صحیح نمی باشد.", Toast.LENGTH_SHORT);
                return false;
            }
        }else{
            userHeightLayout.setError(Functions.setFont(EMPTY_ERROR, 3));
            return false;
        }
        return true;
    }
    private Integer getValueOf(String param){
        if(param.equals(""))
            return null;
        else
            return Integer.valueOf(param);
    }
}
