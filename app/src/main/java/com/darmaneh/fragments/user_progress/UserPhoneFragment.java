package com.darmaneh.fragments.user_progress;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.darmaneh.ava.App;
import com.darmaneh.ava.R;
import com.darmaneh.dialogs.DarmanehToast;
import com.darmaneh.requests.UserFlowV3;
import com.darmaneh.utilities.Analytics;
import com.darmaneh.utilities.Storage;
import com.country_code.CountryCodePicker;
import com.victor.loading.rotate.RotateLoading;

/**
 * A simple {@link Fragment} subclass.
 */
public class UserPhoneFragment extends Fragment {
    private static final String TAG = UserPhoneFragment.class.getSimpleName();
    private UserPhoneFragment.OnFragmentInteractionListener mListener;
    TextView userPhoneTitle, userPhoneBody, laterBtn;
    EditText phoneInput;
    Button okBtn;
    LinearLayout loadingContainer , mainContainer;
    Context myCntx;
    RotateLoading rotateLoading;

    public UserPhoneFragment() {
        // Required empty public constructor
    }

    public static UserPhoneFragment newInstance (){
        UserPhoneFragment fragment = new UserPhoneFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Analytics.sendScreenName("user/phone");
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_user_phone, container, false);
        userPhoneTitle = (TextView) rootView.findViewById(R.id.user_phone_title);
        userPhoneBody = (TextView) rootView.findViewById(R.id.user_phone_body);
//        laterBtn = (TextView) rootView.findViewById(R.id.later_btn);
        phoneInput = (EditText) rootView.findViewById(R.id.phone_input);
        phoneInput.requestFocus();
        okBtn = (Button) rootView.findViewById(R.id.ok_btn);
        myCntx = rootView.getContext();
        mainContainer = (LinearLayout) rootView.findViewById(R.id.main_container);
        loadingContainer = (LinearLayout) rootView.findViewById(R.id.loading_container);
        rotateLoading = (RotateLoading) rootView.findViewById(R.id.rotate_loading);
        ((TextView)rootView.findViewById(R.id.wait_text)).setTypeface(App.getFont(4));
        phoneInput.setText(Storage.getPhoneNum());
        showMainContainer();
        setViewsFont();
        setViewsOnClick();

        return rootView;
    }

    private void setViewsOnClick() {
        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String phoneStr = phoneInput.getText().toString();
//                phoneStr = phoneStr.replaceFirst("^0+(?!$)", "");
//                String codeStr = ccp.getSelectedCountryCode();
                if(phoneValidate(phoneStr)) {
                    Storage.setPhoneNum(phoneStr);
//                    Storage.setCodeNum(codeStr);
                    requestPhone(view.getContext());
                    showLoadingContainer();
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken() , 0);
                }
            }
        });
//        laterBtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Analytics.sendScreenName("user/late_register");
//                Intent returnIntent = new Intent();
//                getActivity().setResult(Activity.RESULT_OK, returnIntent);
//                getActivity().finish();
//            }
//        });
    }

    private void setViewsFont() {
        userPhoneTitle.setTypeface(App.getFont(5));
        userPhoneBody.setTypeface(App.getFont(3));
//        laterBtn.setTypeface(App.getFont(3));
        okBtn.setTypeface(App.getFont(4));
        phoneInput.setTypeface(App.getFont(3));
    }

    public void onButtonPressed(String phoneNum, boolean isRegistered) {
        if (mListener != null) {
            mListener.onPhoneCallback(phoneNum, isRegistered);
        }
    }

    public interface OnFragmentInteractionListener {
        void onPhoneCallback(String phoneNum, Boolean isRegistered);
    }

    private boolean phoneValidate(String phoneNumber) {
        if (phoneNumber.length() == 10)
            return true;
        else {
            DarmanehToast.makeText(getActivity(), "لطفا کد ملی خود را صحیح وارد کنید.", Toast.LENGTH_SHORT);
            return false;
        }
    }

    private void requestPhone(Context context) {
        UserFlowV3.patient_check_registered(context, Storage.getPhoneNum(),
                new UserFlowV3.PatientCheckRegistered() {
            @Override
            public void onHttpResponse(Boolean success, Boolean isRegistered) {
                if(success){
                    onButtonPressed(Storage.getPhoneNum(), isRegistered);
                }
            }

            @Override
            public void onFinishResponse() {
                showMainContainer();
            }
        });
    }

    private void showMainContainer() {
        mainContainer.setVisibility(View.VISIBLE);
        loadingContainer.setVisibility(View.GONE);
        rotateLoading.stop();
    }
    private void showLoadingContainer(){
        mainContainer.setVisibility(View.GONE);
        loadingContainer.setVisibility(View.VISIBLE);
        rotateLoading.start();
    }
}
