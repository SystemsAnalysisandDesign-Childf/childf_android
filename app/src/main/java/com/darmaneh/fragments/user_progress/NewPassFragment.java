package com.darmaneh.fragments.user_progress;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.darmaneh.ava.App;
import com.darmaneh.ava.R;
import com.darmaneh.requests.UserFlowV3;
import com.darmaneh.utilities.Analytics;
import com.darmaneh.utilities.Functions;
import com.darmaneh.utilities.Storage;
import com.victor.loading.rotate.RotateLoading;

public class NewPassFragment extends Fragment {

    private OnFragmentInteractionListener mListener;
    private TextInputLayout passLayout, passRepeatLayout, emailLayout, firstNameLayout, lastNameLayout, fatherNameLayout,
        phoneLayout;
    private TextInputEditText passEditText, passRepeatEditText, emailEditText, firstNameEditText,
            lastNameEditText, fatherNameEditText, phoneEditText;
    private Button okBtn;
    private LinearLayout mainContainer, loadingContainer;
    private RotateLoading rotateLoading;


    public NewPassFragment() {
        // Required empty public constructor
    }

    public static NewPassFragment newInstance() {
        NewPassFragment fragment = new NewPassFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_new_pass, container, false);
        initViews(rootView);
        setViewsFont(rootView);
        setOnClicks();
        showMainContainer();
        return rootView;
    }

    private void setOnClicks() {
        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String password = passEditText.getText().toString().trim();
                String passwordRepeat = passRepeatEditText.getText().toString().trim();
                String firstName = firstNameEditText.getText().toString().trim();
                String lastName = lastNameEditText.getText().toString().trim();
                String email = emailEditText.getText().toString().trim();
                String phone = phoneEditText.getText().toString().trim();
                String father = fatherNameEditText.getText().toString().trim();
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken() , 0);
                if (validatePass(password, passwordRepeat, firstName, lastName, email, phone, father)) {
                    showLoadingContainer();
                    UserFlowV3.patient_send_verification(view.getContext(), Storage.getPhoneNum(),
                            password,
                            email,
                            firstName,
                            lastName,
                            phone,
                            father,
                            new UserFlowV3.PatientSendVerification() {
                                @Override
                                public void onHttpResponse(Boolean success) {
                                    if (success) {
                                        onButtonPressed(password);
                                    }
                                }

                                @Override
                                public void onFinishResponse() {
                                    showMainContainer();
                                }
                            });
                }
            }
        });
    }

    private void setViewsFont(View view) {
        ((TextView) view.findViewById(R.id.fragment_title)).setTypeface(App.getFont(5));
        passEditText.setTypeface(App.getFont(3));
        passRepeatEditText.setTypeface(App.getFont(3));
        emailEditText.setTypeface(App.getFont(3));
        firstNameEditText.setTypeface(App.getFont(3));
        lastNameEditText.setTypeface(App.getFont(3));
        fatherNameEditText.setTypeface(App.getFont(3));
        phoneEditText.setTypeface(App.getFont(3));
        ((TextView) view.findViewById(R.id.pass_title)).setTypeface(App.getFont(4));
        ((TextView) view.findViewById(R.id.pass_repeat_title)).setTypeface(App.getFont(4));
        ((TextView) view.findViewById(R.id.email_title)).setTypeface(App.getFont(4));
        ((TextView) view.findViewById(R.id.father_name_title)).setTypeface(App.getFont(4));
        ((TextView) view.findViewById(R.id.first_name_title)).setTypeface(App.getFont(4));
        ((TextView) view.findViewById(R.id.last_name_title)).setTypeface(App.getFont(4));
        ((TextView) view.findViewById(R.id.phone_title)).setTypeface(App.getFont(4));
        okBtn.setTypeface(App.getFont(4));
        ((TextView) view.findViewById(R.id.wait_text)).setTypeface(App.getFont(4));
    }

    private void initViews(View view) {
        passEditText = (TextInputEditText) view.findViewById(R.id.pass_edit_text);
        passLayout = (TextInputLayout) view.findViewById(R.id.pass_layout);
        passRepeatEditText = (TextInputEditText) view.findViewById(R.id.pass_repeat_edit_text);
        passRepeatLayout = (TextInputLayout) view.findViewById(R.id.pass_repeat_layout);
        emailEditText = (TextInputEditText) view.findViewById(R.id.email_edit_text);
        emailLayout = (TextInputLayout) view.findViewById(R.id.email_layout);
        fatherNameEditText = (TextInputEditText) view.findViewById(R.id.father_name_edit_text);
        fatherNameLayout = (TextInputLayout) view.findViewById(R.id.father_name_layout);
        firstNameEditText = (TextInputEditText) view.findViewById(R.id.first_name_edit_text);
        firstNameLayout = (TextInputLayout) view.findViewById(R.id.first_name_layout);
        lastNameEditText = (TextInputEditText) view.findViewById(R.id.last_name_edit_text);
        lastNameLayout = (TextInputLayout) view.findViewById(R.id.last_name_layout);
        phoneEditText = (TextInputEditText) view.findViewById(R.id.phone_edit_text);
        phoneLayout = (TextInputLayout) view.findViewById(R.id.phone_layout);
        okBtn = (Button) view.findViewById(R.id.ok_btn);
        mainContainer = (LinearLayout) view.findViewById(R.id.main_container);
        loadingContainer = (LinearLayout) view.findViewById(R.id.loading_container);
        rotateLoading = (RotateLoading) view.findViewById(R.id.rotate_loading);

    }

    public void onButtonPressed(String password) {
        if (mListener != null) {
            mListener.onNewPassCallback(password);
        }
    }

    @Override
    public void onAttach(Context context) {
        Analytics.sendScreenName("user/set_pass");
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onNewPassCallback(String password);
    }

    private boolean validatePass(String password, String passwordRepeat, String first, String last, String email, String phone, String father) {
        if (password.length() < 6) {
            passLayout.setError(Functions.setFont("رمز عبور باید حداقل ۶ کاراکتر باشد", 3));
            requestFocus(passEditText);
            passRepeatLayout.setErrorEnabled(false);
            fatherNameLayout.setErrorEnabled(false);
            firstNameLayout.setErrorEnabled(false);
            lastNameLayout.setErrorEnabled(false);
            phoneLayout.setErrorEnabled(false);
            emailLayout.setErrorEnabled(false);
            return false;
        } else {
            passLayout.setErrorEnabled(false);
        }
        if (!password.equals(passwordRepeat)) {
            passRepeatLayout.setError(Functions.setFont("رمز عبور مطابقت ندارد", 3));
            requestFocus(passRepeatEditText);
            passLayout.setErrorEnabled(false);
            fatherNameLayout.setErrorEnabled(false);
            firstNameLayout.setErrorEnabled(false);
            lastNameLayout.setErrorEnabled(false);
            phoneLayout.setErrorEnabled(false);
            emailLayout.setErrorEnabled(false);
            return false;
        } else {
            passRepeatLayout.setErrorEnabled(false);
        }
        if (first.equals("")) {
            firstNameLayout.setError(Functions.setFont("نام نمی‌تواند خالی باشد", 3));
            requestFocus(firstNameEditText);
            passLayout.setErrorEnabled(false);
            fatherNameLayout.setErrorEnabled(false);
            passRepeatLayout.setErrorEnabled(false);
            lastNameLayout.setErrorEnabled(false);
            phoneLayout.setErrorEnabled(false);
            emailLayout.setErrorEnabled(false);
            return false;
        } else {
            firstNameLayout.setErrorEnabled(false);
        }
        if (last.equals("")) {
            lastNameLayout.setError(Functions.setFont("نام‌خانوادگی نمی‌تواند خالی باشد", 3));
            requestFocus(lastNameEditText);
            passLayout.setErrorEnabled(false);
            fatherNameLayout.setErrorEnabled(false);
            passRepeatLayout.setErrorEnabled(false);
            firstNameLayout.setErrorEnabled(false);
            phoneLayout.setErrorEnabled(false);
            emailLayout.setErrorEnabled(false);
            return false;
        } else {
            lastNameLayout.setErrorEnabled(false);
        }
        if (email.equals("")) {
            emailLayout.setError(Functions.setFont("ایمیل نمی‌تواند خالی باشد", 3));
            requestFocus(emailEditText);
            passLayout.setErrorEnabled(false);
            fatherNameLayout.setErrorEnabled(false);
            passRepeatLayout.setErrorEnabled(false);
            firstNameLayout.setErrorEnabled(false);
            phoneLayout.setErrorEnabled(false);
            lastNameLayout.setErrorEnabled(false);
            return false;
        } else {
            emailLayout.setErrorEnabled(false);
        }
        if (phone.equals("")) {
            phoneLayout.setError(Functions.setFont("شماره تلفن نمی‌تواند خالی باشد", 3));
            requestFocus(phoneEditText);
            passLayout.setErrorEnabled(false);
            fatherNameLayout.setErrorEnabled(false);
            passRepeatLayout.setErrorEnabled(false);
            firstNameLayout.setErrorEnabled(false);
            emailLayout.setErrorEnabled(false);
            lastNameLayout.setErrorEnabled(false);
            return false;
        } else {
            phoneLayout.setErrorEnabled(false);
        }
        if (father.equals("")) {
            fatherNameLayout.setError(Functions.setFont("نام پدر نمی‌تواند خالی باشد", 3));
            requestFocus(fatherNameEditText);
            passLayout.setErrorEnabled(false);
            phoneLayout.setErrorEnabled(false);
            passRepeatLayout.setErrorEnabled(false);
            firstNameLayout.setErrorEnabled(false);
            emailLayout.setErrorEnabled(false);
            lastNameLayout.setErrorEnabled(false);
            return false;
        } else {
            fatherNameLayout.setErrorEnabled(false);
        }
        return true;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private void showMainContainer() {
        mainContainer.setVisibility(View.VISIBLE);
        loadingContainer.setVisibility(View.GONE);
        rotateLoading.stop();
    }

    private void showLoadingContainer() {
        mainContainer.setVisibility(View.GONE);
        loadingContainer.setVisibility(View.VISIBLE);
        rotateLoading.start();
    }
}
