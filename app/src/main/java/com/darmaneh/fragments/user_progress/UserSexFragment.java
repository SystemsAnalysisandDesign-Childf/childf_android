package com.darmaneh.fragments.user_progress;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.darmaneh.ava.App;
import com.darmaneh.ava.R;
import com.darmaneh.utilities.Analytics;

public class UserSexFragment extends Fragment {
    private static final String ARG_PARAM1 = "SEX";
    private String mParam1;
    private OnFragmentInteractionListener mListener;
    public enum sexType {
        male , female , trans
    }
    private sexType sexParam;

    TextView userSexTitle;
    Button maleBtn, femaleBtn;
    public UserSexFragment() {
        // Required empty public constructor
    }

    public static UserSexFragment newInstance() {
        UserSexFragment fragment = new UserSexFragment();
//        Bundle args = new Bundle();
//        args.putSerializable(ARG_PARAM1, sex);
//        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            sexParam = (sexType) getArguments().getSerializable(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_user_sex, container, false);
        userSexTitle = (TextView) rootView.findViewById(R.id.user_sex_title);
        maleBtn = (Button) rootView.findViewById(R.id.male_btn);
        femaleBtn = (Button) rootView.findViewById(R.id.female_btn);
        setViewsFont();
        setOnClick();
        return rootView;
    }

    private void setViewsFont() {
        userSexTitle.setTypeface(App.getFont(5));
        maleBtn.setTypeface(App.getFont(4));
        femaleBtn.setTypeface(App.getFont(4));
    }

    private void setOnClick() {
        maleBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onButtonPressed(sexType.male);
            }
        });
        femaleBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onButtonPressed(sexType.female);
            }
        });
    }

    public void onButtonPressed(sexType sex) {
        if (mListener != null) {
            String sexStr;
            switch (sex){
                case male:
                    sexStr = "male";
                    break;
                case female:
                    sexStr = "female";
                    break;
                default:
                    sexStr = "male";
                    break;
            }
            mListener.onSexCallback(sexStr);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Analytics.sendScreenName("user/set_sex");
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onSexCallback(String sex);
    }
}
