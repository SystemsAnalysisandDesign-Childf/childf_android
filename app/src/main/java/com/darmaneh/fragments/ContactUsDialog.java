package com.darmaneh.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.CardView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.darmaneh.ava.App;
import com.darmaneh.ava.BuildConfig;
import com.darmaneh.ava.R;
import com.darmaneh.dialogs.DarmanehToast;
import com.darmaneh.requests.Utility;
import com.darmaneh.utilities.Analytics;
import com.darmaneh.utilities.Functions;
import com.darmaneh.utilities.Storage;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by pourya on 3/29/17.
 */

public class ContactUsDialog extends DialogFragment {

    String subject = "";

    public ContactUsDialog() {
        // Required empty public constructor
    }

    public static ContactUsDialog newInstance(String subject) {
        ContactUsDialog contactUsDialog = new ContactUsDialog();

        Bundle args = new Bundle();
        args.putString("subject", subject);
        contactUsDialog.setArguments(args);

        return contactUsDialog;
    }

    TextView emailHeader;
    EditText emailInput;
    TextView subjectHeader;
    EditText subjectInput;
    TextView contentHeader;
    EditText contentInput;

    CardView sendBtn;
    TextView sendText;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            subject = getArguments().getString("subject", "");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        WindowManager.LayoutParams wlp = getDialog().getWindow().getAttributes();
        wlp.windowAnimations = R.style.DialogAnimation;
        wlp.gravity = Gravity.BOTTOM;
        getDialog().getWindow().setAttributes(wlp);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        View rootView = inflater.inflate(R.layout.fragment_contact_us_dialog,
                container, false);

        emailHeader = (TextView) rootView.findViewById(R.id.email_header);
        emailHeader.setTypeface(App.getFont(4));
        emailInput = (EditText) rootView.findViewById(R.id.email_input);
        emailInput.setTypeface(App.getFont(3));

        subjectHeader = (TextView) rootView.findViewById(R.id.subject_header);
        subjectHeader.setTypeface(App.getFont(4));
        subjectInput = (EditText) rootView.findViewById(R.id.subject_input);

        if (!subject.equals("")) {
            subjectInput.setEnabled(false);
            subjectInput.setText(subject);
        }

        subjectInput.setTypeface(App.getFont(3));

        contentHeader = (TextView) rootView.findViewById(R.id.content_header);
        contentHeader.setTypeface(App.getFont(4));
        contentInput = (EditText) rootView.findViewById(R.id.content_input);
        contentInput.setTypeface(App.getFont(3));

        sendBtn = (CardView) rootView.findViewById(R.id.send_btn);
        sendText = (TextView) rootView.findViewById(R.id.send_text);
        sendText.setTypeface(App.getFont(4));
        sendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String patientMail = emailInput.getText().toString();
                String mailSubject = subjectInput.getText().toString();
                String mailContent = contentInput.getText().toString();

                if (validateMessage(patientMail, mailSubject, mailContent)) {

                    mailContent += "\n\n---------------";
                    // add user's phone number to email
                    String phone = Storage.getCompletePhoneNum("fa");
                    if (!phone.equals(""))
                        mailContent += "\n" + "phone: " + phone;

                    // add version name and version code
                    String versionCode = Integer.toString(BuildConfig.VERSION_CODE);
                    String versionName = BuildConfig.VERSION_NAME;
                    mailContent += "\n" + "version: " + versionCode + "(" + versionName + ")";

                    Utility.send_email(getContext(),
                            patientMail, mailSubject, mailContent,
                            new Utility.SendEmail() {
                                @Override
                                public void onHttpResponse(Boolean success) {
                                    if (success) {
                                        dismiss();
                                        Analytics.sendScreenName("contact_us/success_full");
                                        DarmanehToast.makeText(getContext(),
                                                "پیام با موفقیت ارسال شد.", Toast.LENGTH_SHORT);
                                    }
                                }
                            });
                }
            }
        });

        return rootView;
    }

    private boolean validateMessage(String studentEmail, String subject, String content) {
        if (!isEmailValid(studentEmail)) {
            return false;
        } else if (subject.length() < 4) {
            DarmanehToast.makeText(getContext(), "موضوع پیام باید بیش از ۴ حرف باشد.", Toast.LENGTH_SHORT);
            return false;
        } else if (content.length() < 15) {
            DarmanehToast.makeText(getContext(), "متن پیام باید بیش از ۱۵ حرف باشد.", Toast.LENGTH_SHORT);
            return false;
        } else
            return true;
    }

    private boolean isEmailValid(String email) {
        boolean isValid = false;

        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        } else {
            DarmanehToast.makeText(getContext(), "لطفا ایمیل را به درستی وارد کنید.", Toast.LENGTH_SHORT);
        }

        return isValid;
    }
}
