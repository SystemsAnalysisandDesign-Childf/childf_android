package com.darmaneh.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.darmaneh.adapters.BookmarkAdapter;
import com.darmaneh.ava.App;
import com.darmaneh.ava.R;
import com.darmaneh.models.patient_record.ConditionDetail;
import com.darmaneh.utilities.Analytics;

import java.util.ArrayList;
import java.util.List;

public class BookmarkFragment extends Fragment implements FragmentLifecycle, AdapterCallback{

    RecyclerView recyclerView;
    private List<ConditionDetail> conditionDetails =new ArrayList<>();
    BookmarkAdapter bookmarkAdapter;
    CardView noDataCard;
    private String TAG  = BookmarkFragment.class.getSimpleName();

    public BookmarkFragment() {
        // Required empty public constructor
    }

    public static BookmarkFragment newInstance() {
        return new BookmarkFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView =  inflater.inflate(R.layout.fragment_bookmark, container, false);
        rvInit(rootView);
        showCards();
        return rootView;
    }

    private void rvInit(View rootView) {
        recyclerView = (RecyclerView) rootView.findViewById(R.id.set_detail_card_list);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(rootView.getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(llm);
        bookmarkAdapter = new BookmarkAdapter(rootView.getContext(), conditionDetails, this);
        recyclerView.setAdapter(bookmarkAdapter);

        TextView noDataTxt = (TextView) rootView.findViewById(R.id.no_data_txt);
        noDataTxt.setTypeface(App.getFont(3));
        noDataCard = (CardView) rootView.findViewById(R.id.no_data_card);
    }

    private void showCards() {
        if(conditionDetails.isEmpty()) {
            recyclerView.setVisibility(View.GONE);
            noDataCard.setVisibility(View.VISIBLE);
        }
        else{
            recyclerView.setVisibility(View.VISIBLE);
            noDataCard.setVisibility(View.GONE);
            bookmarkAdapter.notifyDataSetChanged();
        }
    }

    public void setConditionDetails(List<ConditionDetail> conditionDetails) {
        this.conditionDetails = conditionDetails;
        if(isAdded()){
            bookmarkAdapter.swapData(conditionDetails);
            showCards();
        }
    }

    @Override
    public void onResumeFragment() {
        Analytics.sendScreenName("EMR/bookmark/screen");
    }

    @Override
    public void checkEmptyCallback() {
        showCards();
    }
}
