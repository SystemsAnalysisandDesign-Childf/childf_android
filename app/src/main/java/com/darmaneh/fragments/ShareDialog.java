package com.darmaneh.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.darmaneh.ava.App;
import com.darmaneh.ava.R;
import com.darmaneh.utilities.Functions;
import com.darmaneh.utilities.Storage;


/**
 * Created by pourya on 6/22/17.
 */

public class ShareDialog extends DialogFragment {

    public ShareDialog() {
        // Required empty public constructor
    }

    TextView title;
    TextView share;
    TextView skip;
    Context context;

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        Storage.setIsShared(true);
        context = getContext();
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        View rootView = inflater.inflate(R.layout.fragment_share_dialog, container, false);

        String titleText = "اگه از «درمانه» راضی بودی، میتونی الآن به بقیه معرفیش کنی." + "\n" +
                "یا بعدا از طریق دکمه به اشتراک‌گذاری بالای صفحه این کار رو انجام بدی :)";

        title = (TextView) rootView.findViewById(R.id.title);
        title.setTypeface(App.getFont(3));
        title.setText(titleText);

        share = (TextView) rootView.findViewById(R.id.share);
        share.setTypeface(App.getFont(4));
        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
                Functions.appShareFunction(view.getContext());
            }
        });

        skip = (TextView) rootView.findViewById(R.id.skip);
        skip.setTypeface(App.getFont(3));
        skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        return rootView;
    }
}
