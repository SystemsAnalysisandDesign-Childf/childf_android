package com.darmaneh.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.darmaneh.ava.App;
import com.darmaneh.ava.R;
import com.darmaneh.requests.Variable;
import com.darmaneh.utilities.Storage;

import java.util.Date;

/**
 * Created by pourya on 4/1/17.
 */

public class RateDialog extends DialogFragment{

    public RateDialog() {
        // Required empty public constructor
    }

    TextView title;
    TextView rate;
    TextView skip;
    TextView repeat;
    Context context;

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        context = getContext();
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        View rootView = inflater.inflate(R.layout.fragment_rate_dialog, container, false);

        String titleText = getResources().getString(R.string.rate_darmaneh1) +
                "\n" + getResources().getString(R.string.rate_darmaneh2) + "\n\n" +
                "همچنین میتونی از طریق دکمه به ‌اشتراک‌گذاری بالای صفحه «درمانه» رو به بقیه معرفی کنی.";

        title = (TextView) rootView.findViewById(R.id.title);
        title.setTypeface(App.getFont(3));
        title.setText(titleText);

        rate = (TextView) rootView.findViewById(R.id.rate);
        rate.setTypeface(App.getFont(4));
        rate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_EDIT);
                intent.setData(Uri.parse("bazaar://details?id=" + Variable.PACKAGE_NAME));
                intent.setPackage("com.farsitel.bazaar");

                // check if baazar is installed on the device or not
                if(intent.resolveActivity(context.getPackageManager()) != null) {
                    Storage.setIsRated(true);
                    context.startActivity(intent);
                } else {
                    Storage.setIsRated(false);
                    new AlertDialog.Builder(context)
                            .setTitle("توجه")
                            .setMessage("اول باید بازار و نصب کنی.")
                            .setPositiveButton("باشه", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    String url = "http://cafebazaar.ir/";
                                    Intent i = new Intent(Intent.ACTION_VIEW);
                                    i.setData(Uri.parse(url));
                                    context.startActivity(i);
                                }
                            })
                            .setNegativeButton("بیخیال", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
                }
                dismiss();
            }
        });

        skip = (TextView) rootView.findViewById(R.id.skip);
        skip.setTypeface(App.getFont(3));
        skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Storage.setIsRated(true);
                dismiss();
            }
        });

        repeat = (TextView) rootView.findViewById(R.id.repeat);
        repeat.setTypeface(App.getFont(3));
        repeat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Storage.setIsRated(false);
                Storage.setTimeRate((new Date(System.currentTimeMillis())).getTime());
                dismiss();
            }
        });

        return rootView;
    }
}
