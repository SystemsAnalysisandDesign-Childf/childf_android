package com.darmaneh.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.darmaneh.adapters.HistoryAdapter;
import com.darmaneh.ava.App;
import com.darmaneh.ava.R;
import com.darmaneh.models.patient_record.SymptomCheckerResult;
import com.darmaneh.models.patient_record.VisitResult;
import com.darmaneh.models.patient_record.VisitSymptomCheckerTime;
import com.darmaneh.utilities.Analytics;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link HistoryFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HistoryFragment extends Fragment implements FragmentLifecycle, AdapterCallback{

    private List<SymptomCheckerResult> symptomCheckerResults = new ArrayList<>();
    private List<VisitResult> visitResults = new ArrayList<>();
    private List<VisitSymptomCheckerTime> timeList = new ArrayList<>();
    RecyclerView recyclerView;
    CardView noDataCard;
    HistoryAdapter historyAdapter;

    public HistoryFragment() {
        // Required empty public constructor
    }

    public static HistoryFragment newInstance() {
        return new HistoryFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView =  inflater.inflate(R.layout.fragment_history, container, false);
        rvInit(rootView);
        showCards();
        return  rootView;
    }

    private void rvInit(View rootView) {
        recyclerView = (RecyclerView) rootView.findViewById(R.id.set_detail_card_list);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(rootView.getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(llm);
        historyAdapter = new HistoryAdapter(symptomCheckerResults,visitResults,timeList,this);
        recyclerView.setAdapter(historyAdapter);

        TextView noDataTxt = (TextView) rootView.findViewById(R.id.no_data_txt);
        noDataTxt.setTypeface(App.getFont(3));
        noDataCard = (CardView) rootView.findViewById(R.id.no_data_card);
    }

    private void showCards() {
        if(timeList.size() == 0) {
            recyclerView.setVisibility(View.GONE);
            noDataCard.setVisibility(View.VISIBLE);
        }
        else{
            recyclerView.setVisibility(View.VISIBLE);
            noDataCard.setVisibility(View.GONE);
            historyAdapter.notifyDataSetChanged();
        }
    }

    public void setSymptomCheckerResults(List<SymptomCheckerResult> symptomCheckerResults, List<VisitResult> visitResults) {
        this.symptomCheckerResults = symptomCheckerResults;
        this.visitResults = visitResults;
        Collections.sort(this.symptomCheckerResults, new Comparator<SymptomCheckerResult>() {
            @Override
            public int compare(SymptomCheckerResult result1, SymptomCheckerResult result2) {
                if(result1.getTimeStamp() < result2.getTimeStamp()){
                    return 1;
                }else {
                    return -1;
                }
            }
        });
        Collections.sort(this.visitResults, new Comparator<VisitResult>() {
            @Override
            public int compare(VisitResult result1, VisitResult result2) {
                if(result1.getTimeStamp() < result2.getTimeStamp())
                    return 1;
                else
                    return -1;
            }
        });
        List<VisitSymptomCheckerTime> timeList = getTimeList(this.visitResults,this.symptomCheckerResults);
        if(isAdded()){
            historyAdapter.swapData(this.symptomCheckerResults,this.visitResults,timeList);
            showCards();
        }
    }

    @Override
    public void onResumeFragment() {
        Analytics.sendScreenName("EMR/history/screen");
    }

    @Override
    public void checkEmptyCallback() {
        showCards();
    }

    private List<VisitSymptomCheckerTime> getTimeList(List<VisitResult> visitResults ,
                                                      List<SymptomCheckerResult> symptomCheckerResults){
        List<VisitSymptomCheckerTime> timeList = new ArrayList<>();
        for (int i=0 ; i<visitResults.size(); i++){
            VisitResult result = visitResults.get(i);
            timeList.add(new VisitSymptomCheckerTime(result.getTimeStamp(),true,i));
        }
        for (int i=0 ; i<symptomCheckerResults.size() ; i++){
            SymptomCheckerResult result = symptomCheckerResults.get(i);
            timeList.add(new VisitSymptomCheckerTime(result.getTimeStamp(),false,i));
        }
        Collections.sort(timeList, new Comparator<VisitSymptomCheckerTime>() {
            @Override
            public int compare(VisitSymptomCheckerTime result1, VisitSymptomCheckerTime result2) {
                if(result1.getTimeStamp() < result2.getTimeStamp()){
                    return 1;
                }else{
                    return -1;
                }
            }
        });

        int size = timeList.size();
        if(size>1){
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(timeList.get(0).getTimeStamp());
            int previousDay = calendar.get(Calendar.DAY_OF_YEAR);
            int currentDay;
            for (int i = 1; i < size; i++) {
                calendar.setTimeInMillis(timeList.get(i).getTimeStamp());
                currentDay = calendar.get(Calendar.DAY_OF_YEAR);
                if(currentDay == previousDay)
                    timeList.get(i).setVisibleDate(false);
                previousDay = currentDay;
            }
        }
        return timeList;
    }
}
