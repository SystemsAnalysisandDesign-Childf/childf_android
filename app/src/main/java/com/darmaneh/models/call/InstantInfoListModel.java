package com.darmaneh.models.call;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class InstantInfoListModel {

    @SerializedName("patient_conditions")
    @Expose
    private List<InstantInfo> patientConditions = null;
    @SerializedName("patient_medications")
    @Expose
    private List<InstantInfo> patientMedications = null;
    @SerializedName("patient_allergies")
    @Expose
    private List<InstantInfo> patientAllergies = null;
    @SerializedName("patient_surgeries")
    @Expose
    private List<InstantInfo> patientSurgeries = null;
    @SerializedName("patient_images_no")
    @Expose
    private Integer patinet_images_no = 0;


    public List<InstantInfo> getPatientConditions() {
        return patientConditions;
    }

    public void setPatientConditions(List<InstantInfo> patientConditions) {
        this.patientConditions = patientConditions;
    }

    public List<InstantInfo> getPatientMedications() {
        return patientMedications;
    }

    public void setPatientMedications(List<InstantInfo> patientMedications) {
        this.patientMedications = patientMedications;
    }

    public List<InstantInfo> getPatientAllergies() {
        return patientAllergies;
    }

    public void setPatientAllergies(List<InstantInfo> patientAllergies) {
        this.patientAllergies = patientAllergies;
    }

    public List<InstantInfo> getPatientSurgeries() {
        return patientSurgeries;
    }

    public void setPatientSurgeries(List<InstantInfo> patientSurgeries) {
        this.patientSurgeries = patientSurgeries;
    }

    public Integer getPatinet_images_no() {
        return patinet_images_no;
    }

    public void setPatinet_images_no(Integer patinet_images_no) {
        this.patinet_images_no = patinet_images_no;
    }
}