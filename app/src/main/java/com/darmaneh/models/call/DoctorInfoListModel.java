package com.darmaneh.models.call;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DoctorInfoListModel {


    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("prof_id")
    @Expose
    private Integer profId;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("is_available")
    @Expose
    private Boolean isAvailable;
    @SerializedName("waiting_no")
    @Expose
    private Integer waitingNo;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("profession")
    @Expose
    private List<ProfessionModel> profession = null;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Integer getProfId() {
        return profId;
    }

    public void setProfId(Integer profId) {
        this.profId = profId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Boolean getIsAvailable() {
        return isAvailable;
    }

    public void setIsAvailable(Boolean isAvailable) {
        this.isAvailable = isAvailable;
    }

    public Integer getWaitingNo() {
        return waitingNo;
    }

    public void setWaitingNo(Integer waitingNo) {
        this.waitingNo = waitingNo;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public List<ProfessionModel> getProfession() {
        return profession;
    }

    public void setProfession(List<ProfessionModel> profession) {
        this.profession = profession;
    }

}
