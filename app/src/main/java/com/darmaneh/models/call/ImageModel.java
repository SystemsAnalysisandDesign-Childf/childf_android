package com.darmaneh.models.call;

import android.graphics.Bitmap;

public class ImageModel {

    private String pack_name, send_date, description;
    private Bitmap image_bitmap;
    String base64;
    private byte[] imageByteArray;
    private int imageLength;

    public ImageModel(Bitmap bitmap, String pack_name, String send_date, String description, String base64){
        this.pack_name = pack_name;
        this.send_date = send_date;
        this.description = description;
        this.base64 = base64;
        this.image_bitmap = bitmap;

    }

    public Bitmap getImage_bitmap() {
        return image_bitmap;
    }

    public void setImage_bitmap(Bitmap image_bitmap) {
        this.image_bitmap = image_bitmap;
    }

    public String getPack_name() {
        return pack_name;
    }

    public void setPack_name(String pack_name) {
        this.pack_name = pack_name;
    }

    public String getSend_date() {
        return send_date;
    }

    public void setSend_date(String send_date) {
        this.send_date = send_date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public String getBase64() {
        return base64;
    }

    public void setBase64(String base64) {
        this.base64 = base64;
    }

    public byte[] getImageByteArray() {
        return imageByteArray;
    }

    public void setImageByteArray(byte[] imageByteArray) {
        this.imageByteArray = imageByteArray;
    }

    public int getImageLength() {
        return imageLength;
    }

    public void setImageLength(int imageLength) {
        this.imageLength = imageLength;
    }
}
