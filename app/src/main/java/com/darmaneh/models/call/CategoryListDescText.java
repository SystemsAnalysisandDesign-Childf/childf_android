package com.darmaneh.models.call;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CategoryListDescText {

    @SerializedName("text")
    @Expose
    private String text;
    @SerializedName("text_bazaar")
    @Expose
    private String text_bazaar;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getTextBazaar() {
        return text_bazaar;
    }

    public void setTextBazaar(String text_bazaar) {
        this.text_bazaar = text_bazaar;
    }

}
