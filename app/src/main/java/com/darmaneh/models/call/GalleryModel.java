package com.darmaneh.models.call;

/**
 * Created by alireza on 8/3/17.
 */

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GalleryModel {

    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("image_th")
    @Expose
    private String imageTh;
    @SerializedName("edit_url")
    @Expose
    private String editUrl;
    @SerializedName("category")
    @Expose
    private String category;
    @SerializedName("description")
    @Expose
    private String description;

    private boolean toBeHidden = false;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getImageTh() {
        return imageTh;
    }

    public void setImageTh(String imageTh) {
        this.imageTh = imageTh;
    }

    public String getEditUrl() {
        return editUrl;
    }

    public void setEditUrl(String editUrl) {
        this.editUrl = editUrl;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isToBeHidden() {
        return toBeHidden;
    }

    public void setToBeHidden(boolean toBeHidden) {
        this.toBeHidden = toBeHidden;
    }

    public void toggleToBeHidden(){
        this.toBeHidden = !(this.toBeHidden);
    }

    @Override
    public String toString() {
        return (new Gson()).toJson(this);
    }
}