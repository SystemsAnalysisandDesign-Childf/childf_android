package com.darmaneh.models.call;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DetailInfoModel {

    @SerializedName("about_me")
    @Expose
    private String aboutMe;
    @SerializedName("education")
    @Expose
    private String education;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("honors")
    @Expose
    private String honors;
    @SerializedName("societies")
    @Expose
    private String societies;

    public String getAboutMe() {
        return aboutMe;
    }

    public void setAboutMe(String aboutMe) {
        this.aboutMe = aboutMe;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getHonors() {
        return honors;
    }

    public void setHonors(String honors) {
        this.honors = honors;
    }

    public String getSocieties() {
        return societies;
    }

    public void setSocieties(String societies) {
        this.societies = societies;
    }

}
