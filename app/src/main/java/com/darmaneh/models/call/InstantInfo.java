package com.darmaneh.models.call;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

public class InstantInfo {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name_fa")
    @Expose
    private String nameFa;
    @SerializedName("name_en")
    @Expose
    private String nameEn;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNameFa() {
        return nameFa;
    }

    public void setNameFa(String nameFa) {
        this.nameFa = nameFa;
    }

    public String getNameEn() {
        return nameEn;
    }

    public void setNameEn(String nameEn) {
        this.nameEn = nameEn;
    }

    @Override
    public String toString() {
        return (new Gson()).toJson(this);
    }

    public static InstantInfo toJson(String strModel){
        return (new Gson()).fromJson(strModel,
                new TypeToken<InstantInfo>(){}.getType());
    }

}