package com.darmaneh.models.call;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProfessionListModel {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("professions")
    @Expose
    private List<ProfessionModel> professions = null;
    @SerializedName("max_minutes")
    @Expose
    private Integer maxMinutes;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ProfessionModel> getProfessions() {
        return professions;
    }

    public void setProfessions(List<ProfessionModel> professions) {
        this.professions = professions;
    }

    public Integer getMaxMinutes() {
        return maxMinutes;
    }

    public void setMaxMinutes(Integer maxMinutes) {
        this.maxMinutes = maxMinutes;
    }

}
