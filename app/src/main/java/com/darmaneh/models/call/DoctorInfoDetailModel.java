package com.darmaneh.models.call;

import java.util.List;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

public class DoctorInfoDetailModel {


    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("prof_id")
    @Expose
    private Integer profId;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("is_available")
    @Expose
    private Boolean isAvailable;
    @SerializedName("waiting_no")
    @Expose
    private Integer waitingNo;
    @SerializedName("profession")
    @Expose
    private List<ProfessionModel> profession = null;
    @SerializedName("price")
    @Expose
    private Integer price;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("info")
    @Expose
    private DetailInfoModel info;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getProfId() {
        return profId;
    }

    public void setProfId(Integer profId) {
        this.profId = profId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Boolean getIsAvailable() {
        return isAvailable;
    }

    public void setIsAvailable(Boolean isAvailable) {
        this.isAvailable = isAvailable;
    }

    public Integer getWaitingNo() {
        return waitingNo;
    }

    public void setWaitingNo(Integer waitingNo) {
        this.waitingNo = waitingNo;
    }

    public List<ProfessionModel> getProfession() {
        return profession;
    }

    public void setProfession(List<ProfessionModel> profession) {
        this.profession = profession;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public DetailInfoModel getInfo() {
        return info;
    }

    public void setInfo(DetailInfoModel info) {
        this.info = info;
    }

    @Override
    public String toString() {
        return (new Gson()).toJson(this);
    }

    public static DoctorInfoDetailModel toJson(String strModel){
        return (new Gson()).fromJson(strModel,
                new TypeToken<DoctorInfoDetailModel>(){}.getType());
    }

}
