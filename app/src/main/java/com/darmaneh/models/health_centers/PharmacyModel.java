package com.darmaneh.models.health_centers;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by pourya on 5/22/17.
 */

public class PharmacyModel {

    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("special_name")
    @Expose
    private String specialName;
    @SerializedName("address")
    @Expose
    private String address;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getSpecialName() {
        return specialName;
    }

    public void setSpecialName(String specialName) {
        this.specialName = specialName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return (new Gson()).toJson(this);
    }
}
