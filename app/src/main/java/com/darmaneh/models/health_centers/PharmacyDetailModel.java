package com.darmaneh.models.health_centers;

import com.darmaneh.models.location.Location;
import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by pourya on 5/22/17.
 */

public class PharmacyDetailModel {
    @SerializedName("pk")
    @Expose
    private Integer pk;
    @SerializedName("public_name")
    @Expose
    private String publicName;
    @SerializedName("special_name")
    @Expose
    private String specialName;
    @SerializedName("national_number")
    @Expose
    private Integer nationalNumber;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("founder")
    @Expose
    private String founder;
    @SerializedName("province")
    @Expose
    private String province;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("university")
    @Expose
    private String university;
    @SerializedName("location")
    @Expose
    private Location location;

    public String getPublicName() {
        return publicName;
    }

    public void setPublicName(String publicName) {
        this.publicName = publicName;
    }

    public String getSpecialName() {
        return specialName;
    }

    public void setSpecialName(String specialName) {
        this.specialName = specialName;
    }

    public Integer getNationalNumber() {
        return nationalNumber;
    }

    public void setNationalNumber(Integer nationalNumber) {
        this.nationalNumber = nationalNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getFounder() {
        return founder;
    }

    public void setFounder(String founder) {
        this.founder = founder;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getUniversity() {
        return university;
    }

    public void setUniversity(String university) {
        this.university = university;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    @Override
    public String toString() {
        return (new Gson()).toJson(this);
    }

    public Integer getPk() {
        return pk;
    }

    public void setPk(Integer pk) {
        this.pk = pk;
    }
}
