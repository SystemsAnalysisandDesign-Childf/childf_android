package com.darmaneh.models.bodypart_symptom;
import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by pourya on 3/25/17.
 */

public class Symptom {
    @SerializedName("model_id")
    @Expose
    private String modelId;
    @SerializedName("name_fa")
    @Expose
    private String nameFa;

    public String getModelId() {
        return modelId;
    }

    public void setModelId(String modelId) {
        this.modelId = modelId;
    }

    public String getNameFa() {
        return nameFa;
    }

    public void setNameFa(String nameFa) {
        this.nameFa = nameFa;
    }

    @Override
    public String toString() {
        return (new Gson()).toJson(this);
    }
}
