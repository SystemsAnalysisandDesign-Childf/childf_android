package com.darmaneh.models.bodypart_symptom;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by alireza on 7/29/17.
 */


public class SymptomCommon {

    @SerializedName("model_id")
    @Expose
    private String modelId;
    @SerializedName("name_fa")
    @Expose
    private String nameFa;
    @SerializedName("common_names")
    @Expose
    private List<String> commonNames = null;

    public String getModelId() {
        return modelId;
    }

    public void setModelId(String modelId) {
        this.modelId = modelId;
    }

    public String getNameFa() {
        return nameFa;
    }

    public void setNameFa(String nameFa) {
        this.nameFa = nameFa;
    }

    public List<String> getCommonNames() {
        return commonNames;
    }

    public void setCommonNames(List<String> commonNames) {
        this.commonNames = commonNames;
    }

}

