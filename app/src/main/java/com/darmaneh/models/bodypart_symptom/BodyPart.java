package com.darmaneh.models.bodypart_symptom;
import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by pourya on 3/25/17.
 */

public class BodyPart {
    @SerializedName("name_fa")
    @Expose
    private String nameFa;
    @SerializedName("symptoms")
    @Expose
    private List<Symptom> symptoms = null;

    public String getNameFa() {
        return nameFa;
    }

    public void setNameFa(String nameFa) {
        this.nameFa = nameFa;
    }

    public List<Symptom> getSymptoms() {
        return symptoms;
    }

    public void setSymptoms(List<Symptom> symptoms) {
        this.symptoms = symptoms;
    }

    @Override
    public String toString() {
        return (new Gson()).toJson(this);
    }
}
