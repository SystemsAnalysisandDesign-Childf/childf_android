package com.darmaneh.models;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by pourya on 5/17/17.
 */

public class ProvinceCityModel {
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("cities")
    @Expose
    private List<String> cities = null;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getCities() {
        return cities;
    }

    public void setCities(List<String> cities) {
        this.cities = cities;
    }

    @Override
    public String toString() {
        return (new Gson()).toJson(this);
    }
}
