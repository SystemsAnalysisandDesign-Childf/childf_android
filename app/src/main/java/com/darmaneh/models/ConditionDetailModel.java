package com.darmaneh.models;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by pourya on 3/28/17.
 */

public class ConditionDetailModel {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("bookmarked")
    @Expose
    private Boolean bookmarked;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("symptom")
    @Expose
    private String symptom;
    @SerializedName("cause")
    @Expose
    private String cause;
    @SerializedName("treatment")
    @Expose
    private String treatment;
    @SerializedName("diagnosis")
    @Expose
    private String diagnosis;
    @SerializedName("preventing")
    @Expose
    private String preventing;
    @SerializedName("risk_factor")
    @Expose
    private String riskFactor;
    @SerializedName("visit_doctor")
    @Expose
    private String visitDoctor;
    @SerializedName("ask_doctor")
    @Expose
    private String askDoctor;
    @SerializedName("expectation")
    @Expose
    private String expectation;
    @SerializedName("got_worse")
    @Expose
    private String gotWorse;
    @SerializedName("other")
    @Expose
    private String other;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Boolean getBookmarked() {
        return bookmarked;
    }

    public void setBookmarked(Boolean bookmarked) {
        this.bookmarked = bookmarked;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSymptom() {
        return symptom;
    }

    public void setSymptom(String symptom) {
        this.symptom = symptom;
    }

    public String getCause() {
        return cause;
    }

    public void setCause(String cause) {
        this.cause = cause;
    }

    public String getTreatment() {
        return treatment;
    }

    public void setTreatment(String treatment) {
        this.treatment = treatment;
    }

    public String getDiagnosis() {
        return diagnosis;
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    public String getPreventing() {
        return preventing;
    }

    public void setPreventing(String preventing) {
        this.preventing = preventing;
    }

    public String getRiskFactor() {
        return riskFactor;
    }

    public void setRiskFactor(String riskFactor) {
        this.riskFactor = riskFactor;
    }

    public String getVisitDoctor() {
        return visitDoctor;
    }

    public void setVisitDoctor(String visitDoctor) {
        this.visitDoctor = visitDoctor;
    }

    public String getAskDoctor() {
        return askDoctor;
    }

    public void setAskDoctor(String askDoctor) {
        this.askDoctor = askDoctor;
    }

    public String getExpectation() {
        return expectation;
    }

    public void setExpectation(String expectation) {
        this.expectation = expectation;
    }

    public String getGotWorse() {
        return gotWorse;
    }

    public void setGotWorse(String gotWorse) {
        this.gotWorse = gotWorse;
    }

    public String getOther() {
        return other;
    }

    public void setOther(String other) {
        this.other = other;
    }

    @Override
    public String toString() {
        return (new Gson()).toJson(this);
    }
}
