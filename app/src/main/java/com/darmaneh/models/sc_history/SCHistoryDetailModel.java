package com.darmaneh.models.sc_history;

import com.darmaneh.utilities.JalaliCalendar;
import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by pourya on 3/30/17.
 */

public class SCHistoryDetailModel {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("data")
    @Expose
    private Data data;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCreatedAt() {
        JalaliCalendar sc = new JalaliCalendar(createdAt);
        return sc.toString();
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return (new Gson()).toJson(this);
    }
}
