package com.darmaneh.models.sc_history;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by pourya on 3/30/17.
 */

public class Data {
    @SerializedName("sex")
    @Expose
    private String sex;
    @SerializedName("conditions")
    @Expose
    private List<Condition> conditions = null;
    @SerializedName("evidence")
    @Expose
    private List<Evidence> evidence = null;
    @SerializedName("age")
    @Expose
    private Integer age;

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public List<Condition> getConditions() {
        return conditions;
    }

    public void setConditions(List<Condition> conditions) {
        this.conditions = conditions;
    }

    public List<Evidence> getEvidence() {
        return evidence;
    }

    public void setEvidence(List<Evidence> evidence) {
        this.evidence = evidence;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return (new Gson()).toJson(this);
    }
}
