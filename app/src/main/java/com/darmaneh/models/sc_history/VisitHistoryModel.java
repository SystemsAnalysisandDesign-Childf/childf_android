package com.darmaneh.models.sc_history;

import com.darmaneh.utilities.JalaliCalendar;
import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Calendar;

/**
 * Created by alireza on 7/20/17.
 */

public class VisitHistoryModel {
    @SerializedName("time_stamp")
    @Expose
    private Long timeStamp;
    @SerializedName("ph_first_name")
    @Expose
    private String phFirstName;
    @SerializedName("ph_last_name")
    @Expose
    private String phLastName;
    @SerializedName("biography")
    @Expose
    private String biography;
    @SerializedName("advise")
    @Expose
    private String advise;
    @SerializedName("hpi")
    @Expose
    private String hpi;
    @SerializedName("prescription")
    @Expose
    private String prescription;

    public Long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getPhFirstName() {
        return phFirstName;
    }

    public void setPhFirstName(String phFirstName) {
        this.phFirstName = phFirstName;
    }

    public String getPhLastName() {
        return phLastName;
    }

    public void setPhLastName(String phLastName) {
        this.phLastName = phLastName;
    }

    public String getBiography() {
        return biography;
    }

    public void setBiography(String biography) {
        this.biography = biography;
    }

    public String getAdvise() {
        return advise;
    }

    public void setAdvise(String advise) {
        this.advise = advise;
    }

    public String getHpi() {
        return hpi;
    }

    public void setHpi(String hpi) {
        this.hpi = hpi;
    }

    public String getPrescription() {
        return prescription;
    }

    public void setPrescription(String prescription) {
        this.prescription = prescription;
    }

    @Override
    public String toString() {
        return (new Gson()).toJson(this);
    }

    public String getJalaliDate()
    {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(this.timeStamp);
        JalaliCalendar jalali = new JalaliCalendar(calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH)+1, calendar.get(Calendar.DATE));
        String date = jalali.getDay() + " " + jalali.getStrMonth() +" "+ jalali.getYear();
        return date;
    }
}