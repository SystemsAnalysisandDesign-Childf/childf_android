package com.darmaneh.models.Mail;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MailModel {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("sender_member")
    @Expose
    private SenderMember senderMember;
    @SerializedName("receiver_member")
    @Expose
    private ReceiverMember receiverMember;
    @SerializedName("attached_member")
    @Expose
    private Object attachedMember;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("content")
    @Expose
    private String content;
    @SerializedName("accepted")
    @Expose
    private Boolean accepted;
    @SerializedName("seen")
    @Expose
    private Boolean seen;
    @SerializedName("sent")
    @Expose
    private Boolean sent;
    @SerializedName("tag")
    @Expose
    private Integer tag;
    @SerializedName("message_file")
    @Expose
    private Object messageFile;
    @SerializedName("creation_datetime")
    @Expose
    private String creationDatetime;
    @SerializedName("last_update_datetime")
    @Expose
    private String lastUpdateDatetime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public SenderMember getSenderMember() {
        return senderMember;
    }

    public void setSenderMember(SenderMember senderMember) {
        this.senderMember = senderMember;
    }

    public ReceiverMember getReceiverMember() {
        return receiverMember;
    }

    public void setReceiverMember(ReceiverMember receiverMember) {
        this.receiverMember = receiverMember;
    }

    public Object getAttachedMember() {
        return attachedMember;
    }

    public void setAttachedMember(Object attachedMember) {
        this.attachedMember = attachedMember;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Boolean getAccepted() {
        return accepted;
    }

    public void setAccepted(Boolean accepted) {
        this.accepted = accepted;
    }

    public Boolean getSeen() {
        return seen;
    }

    public void setSeen(Boolean seen) {
        this.seen = seen;
    }

    public Boolean getSent() {
        return sent;
    }

    public void setSent(Boolean sent) {
        this.sent = sent;
    }

    public Integer getTag() {
        return tag;
    }

    public void setTag(Integer tag) {
        this.tag = tag;
    }

    public Object getMessageFile() {
        return messageFile;
    }

    public void setMessageFile(Object messageFile) {
        this.messageFile = messageFile;
    }

    public String getCreationDatetime() {
        return creationDatetime;
    }

    public void setCreationDatetime(String creationDatetime) {
        this.creationDatetime = creationDatetime;
    }

    public String getLastUpdateDatetime() {
        return lastUpdateDatetime;
    }

    public void setLastUpdateDatetime(String lastUpdateDatetime) {
        this.lastUpdateDatetime = lastUpdateDatetime;
    }

}
