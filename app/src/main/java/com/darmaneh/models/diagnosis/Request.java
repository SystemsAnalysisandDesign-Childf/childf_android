package com.darmaneh.models.diagnosis;
import com.darmaneh.utilities.Functions;
import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pourya on 3/20/17.
 */

public class Request {
    @SerializedName("sex")
    @Expose
    private String sex;
    @SerializedName("age")
    @Expose
    private Integer age;
    @SerializedName("weight")
    @Expose
    private Integer weight;
    @SerializedName("height")
    @Expose
    private Integer height;
    @SerializedName("req_num")
    @Expose
    private Integer reqNum;
    @SerializedName("evidence")
    @Expose
    private List<Evidence> evidence = null;

    private String interviewId;

    public Request () {
        evidence = new ArrayList<Evidence>();
        interviewId = Functions.generateInterviewId();
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public Integer getReqNum() {
        return reqNum;
    }

    public void setReqNum(Integer reqNum) {
        this.reqNum = reqNum;
    }

    public List<Evidence> getEvidence() {
        return evidence;
    }

    public void setEvidence(List<Evidence> evidence) {
        this.evidence = evidence;
    }

    public void increaseReqNum() {
        reqNum++;
    }

    public void decreaseReqNum() {
        reqNum--;
    }

    @Override
    public String toString() {
        return (new Gson()).toJson(this);
    }

    public String getInterviewId() {
        return interviewId;
    }
}
