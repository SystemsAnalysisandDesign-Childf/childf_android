package com.darmaneh.models.diagnosis;
import com.darmaneh.utilities.Functions;
import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by pourya on 3/20/17.
 */

public class Condition {
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("hint")
    @Expose
    private String hint;
    @SerializedName("colloquial")
    @Expose
    private String colloquial;
    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("probability")
    @Expose
    private Double probability;
    @SerializedName("id")
    @Expose
    private String id;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHint() {
        return hint;
    }

    public void setHint(String hint) {
        this.hint = hint;
    }

    public String getColloquial() {
        return colloquial;
    }

    public void setColloquial(String colloquial) {
        this.colloquial = colloquial;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getProbability() {
        Double temp = (probability * 100);
        return Functions.toPersian(temp.intValue());
    }

    public Integer getIntProbability() {
        Double temp = (probability * 100);
        return temp.intValue();
    }

    public void setProbability(Double probability) {
        this.probability = probability;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return (new Gson()).toJson(this);
    }
}
