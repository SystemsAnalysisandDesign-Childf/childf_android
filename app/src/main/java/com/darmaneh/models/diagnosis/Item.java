package com.darmaneh.models.diagnosis;
import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.List;

/**
 * Created by pourya on 3/20/17.
 */

public class Item {
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("choices")
    @Expose
    private List<Choice> choices = null;
    @SerializedName("id")
    @Expose
    private String id;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Choice> getChoices() {
        return choices;
    }

    public void setChoices(List<Choice> choices) {
        this.choices = choices;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return (new Gson()).toJson(this);
    }
}
