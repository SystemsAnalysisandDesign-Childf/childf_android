package com.darmaneh.models.diagnosis;

import com.darmaneh.requests.Variable;
import com.google.gson.Gson;

import java.util.ArrayList;

/**
 * Created by pourya on 5/24/17.
 */

public class History {
    private String[] requests = null;
    private String[] responses = null;

    public History() {
        // maximum number of request is 15
        requests = new String[Variable.MAX_REQ_NO];
        responses = new String[Variable.MAX_REQ_NO];
    }

    public void addHistoryRequest(int index, Request re) {
        requests[index] = re.toString();
    }

    public void addHistoryResponse(int index, Response re) {
        responses[index] = re.toString();
    }

    public Request getHistoryRequest(int index) {
        return (new Gson()).fromJson(requests[index], Request.class);
    }

    public Response getHistoryResponse(int index) {
        return (new Gson()).fromJson(responses[index], Response.class);
    }
}
