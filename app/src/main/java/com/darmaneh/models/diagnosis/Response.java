package com.darmaneh.models.diagnosis;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pourya on 3/20/17.
 */

public class Response {
    @SerializedName("conditions")
    @Expose
    private List<Condition> conditions = null;
    @SerializedName("question")
    @Expose
    private Question question;
    @SerializedName("sex")
    @Expose
    private String sex;
    @SerializedName("evidence")
    @Expose
    private List<Evidence> evidence = null;
    @SerializedName("age")
    @Expose
    private Integer age;
    @SerializedName("weight")
    @Expose
    private Integer weight;
    @SerializedName("height")
    @Expose
    private Integer height;

    public List<Condition> getConditions() {
        return conditions;
    }

    public List<Condition> getFilteredConditions() {
        // remove unnecessary conditions
        List<Condition> modifiedConditions = new ArrayList<>();
        for (Condition c : conditions) {
            if (c.getIntProbability() > 5)
                modifiedConditions.add(c);
        }
        return modifiedConditions;
    }

    public void setConditions(List<Condition> conditions) {
        this.conditions = conditions;
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public List<Evidence> getEvidence() {
        return evidence;
    }

    public void setEvidence(List<Evidence> evidence) {
        this.evidence = evidence;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    @Override
    public String toString() {
        return (new Gson()).toJson(this);
    }
}
