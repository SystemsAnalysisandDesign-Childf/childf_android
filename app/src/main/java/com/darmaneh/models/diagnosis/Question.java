package com.darmaneh.models.diagnosis;
import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.List;

/**
 * Created by pourya on 3/20/17.
 */

public class Question {
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("extras")
    @Expose
    private Extras extras;
    @SerializedName("items")
    @Expose
    private List<Item> items = null;
    @SerializedName("text")
    @Expose
    private String text;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Extras getExtras() {
        return extras;
    }

    public void setExtras(Extras extras) {
        this.extras = extras;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return (new Gson()).toJson(this);
    }
}
