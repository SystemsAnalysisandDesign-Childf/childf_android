package com.darmaneh.models.diagnosis;
import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;

/**
 * Created by pourya on 3/20/17.
 */

public class Evidence {
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("choice_id")
    @Expose
    private String choiceId;

    public String getName() {
        return name;
    }

    public void setName(String name){
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getChoiceId() {
        return choiceId;
    }

    public void setChoiceId(String choiceId) {
        this.choiceId = choiceId;
    }

    @Override
    public boolean equals(Object obj) {
        return this.getId().equals(((Evidence) obj).getId());
    }

    public static boolean isInList(List<Evidence> list, Evidence evd) {
        for (Evidence temp : list) {
            if (temp.equals(evd))
                return true;
        }
        return false;
    }

    @Override
    public String toString() {
        return (new Gson()).toJson(this);
    }
}
