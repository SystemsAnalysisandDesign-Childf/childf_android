package com.darmaneh.models.unadopted;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UnadoptedListModel {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("my_madadkar")
    @Expose
    private Object myMadadkar;
    @SerializedName("user")
    @Expose
    private User user;
    @SerializedName("my_hamyar")
    @Expose
    private Object myHamyar;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Object getMyMadadkar() {
        return myMadadkar;
    }

    public void setMyMadadkar(Object myMadadkar) {
        this.myMadadkar = myMadadkar;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Object getMyHamyar() {
        return myHamyar;
    }

    public void setMyHamyar(Object myHamyar) {
        this.myHamyar = myHamyar;
    }

}
