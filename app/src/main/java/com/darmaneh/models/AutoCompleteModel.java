package com.darmaneh.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by alireza on 6/5/17.
 */

public class AutoCompleteModel {

    public AutoCompleteModel(String modelId, String nameFa) {
        this.modelId = modelId;
        this.nameFa = nameFa;
    }

    private String modelId;

    private String nameFa;

    public String getModelId() {
        return modelId;
    }

    public void setModelId(String modelId) {
        this.modelId = modelId;
    }

    public String getNameFa() {
        return nameFa;
    }

    public void setNameFa(String nameFa) {
        this.nameFa = nameFa;
    }

}