package com.darmaneh.models.location;

/**
 * Created by alireza on 8/10/17.
 */

import java.util.List;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HealthCentersLocationModel {

    @SerializedName("hospitals")
    @Expose
    private List<Hospital> hospitals = null;

    public List<Hospital> getHospitals() {
        return hospitals;
    }

    public void setHospitals(List<Hospital> hospitals) {
        this.hospitals = hospitals;
    }

    @Override
    public String toString() {
        return (new Gson()).toJson(this);
    }

}