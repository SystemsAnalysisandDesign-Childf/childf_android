package com.darmaneh.models.location;

/**
 * Created by alireza on 8/10/17.
 */

import java.util.List;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Hospital {

    @SerializedName("pk")
    @Expose
    private Integer pk;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("province")
    @Expose
    private String province;
    @SerializedName("organization")
    @Expose
    private String organization;
    @SerializedName("expertise")
    @Expose
    private String expertise;
    @SerializedName("activity")
    @Expose
    private List<String> activity = null;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("bed_count")
    @Expose
    private Integer bedCount;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("tell")
    @Expose
    private String tell;
    @SerializedName("site")
    @Expose
    private String site;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("hospitalization")
    @Expose
    private String hospitalization;
    @SerializedName("with_star")
    @Expose
    private String withStar;
    @SerializedName("para_clinic")
    @Expose
    private String paraClinic;
    @SerializedName("clinical")
    @Expose
    private String clinical;
    @SerializedName("location")
    @Expose
    private Location location;

    public Integer getPk() {
        return pk;
    }

    public void setPk(Integer pk) {
        this.pk = pk;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public String getExpertise() {
        return expertise;
    }

    public void setExpertise(String expertise) {
        this.expertise = expertise;
    }

    public List<String> getActivity() {
        return activity;
    }

    public void setActivity(List<String> activity) {
        this.activity = activity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getBedCount() {
        return bedCount;
    }

    public void setBedCount(Integer bedCount) {
        this.bedCount = bedCount;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTell() {
        return tell;
    }

    public void setTell(String tell) {
        this.tell = tell;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getHospitalization() {
        return hospitalization;
    }

    public void setHospitalization(String hospitalization) {
        this.hospitalization = hospitalization;
    }

    public String getWithStar() {
        return withStar;
    }

    public void setWithStar(String withStar) {
        this.withStar = withStar;
    }

    public String getParaClinic() {
        return paraClinic;
    }

    public void setParaClinic(String paraClinic) {
        this.paraClinic = paraClinic;
    }

    public String getClinical() {
        return clinical;
    }

    public void setClinical(String clinical) {
        this.clinical = clinical;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }


    @Override
    public String toString() {
        return (new Gson()).toJson(this);
    }


}