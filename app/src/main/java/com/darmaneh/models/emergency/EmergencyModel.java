package com.darmaneh.models.emergency;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EmergencyModel {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("madadju")
    @Expose
    private Madadju madadju;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("type")
    @Expose
    private Integer type;
    @SerializedName("is_financial")
    @Expose
    private Boolean isFinancial;
    @SerializedName("cost")
    @Expose
    private Integer cost;
    @SerializedName("is_emergency")
    @Expose
    private Boolean isEmergency;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("is_done")
    @Expose
    private Boolean isDone;
    @SerializedName("creation_datetime")
    @Expose
    private String creationDatetime;
    @SerializedName("last_update_datetime")
    @Expose
    private String lastUpdateDatetime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Madadju getMadadju() {
        return madadju;
    }

    public void setMadadju(Madadju madadju) {
        this.madadju = madadju;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Boolean getIsFinancial() {
        return isFinancial;
    }

    public void setIsFinancial(Boolean isFinancial) {
        this.isFinancial = isFinancial;
    }

    public Integer getCost() {
        return cost;
    }

    public void setCost(Integer cost) {
        this.cost = cost;
    }

    public Boolean getIsEmergency() {
        return isEmergency;
    }

    public void setIsEmergency(Boolean isEmergency) {
        this.isEmergency = isEmergency;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getIsDone() {
        return isDone;
    }

    public void setIsDone(Boolean isDone) {
        this.isDone = isDone;
    }

    public String getCreationDatetime() {
        return creationDatetime;
    }

    public void setCreationDatetime(String creationDatetime) {
        this.creationDatetime = creationDatetime;
    }

    public String getLastUpdateDatetime() {
        return lastUpdateDatetime;
    }

    public void setLastUpdateDatetime(String lastUpdateDatetime) {
        this.lastUpdateDatetime = lastUpdateDatetime;
    }

}
