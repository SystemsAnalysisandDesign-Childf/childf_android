package com.darmaneh.models.patient_record;

import android.util.Log;

import com.darmaneh.utilities.JalaliCalendar;
import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by pourya on 3/29/17.
 */

public class PatientRecordModel {
    @SerializedName("condition_details")
    @Expose
    private List<ConditionDetail> conditionDetails = null;
    @SerializedName("symptom_checker_results")
    @Expose
    private List<SymptomCheckerResult> symptomCheckerResults = null;
    @SerializedName("phone_number")
    @Expose
    private String phoneNumber;
    @SerializedName("visit_results")
    @Expose
    private List<VisitResult> visitResults = null;
    @SerializedName("user_at")
    @Expose
    private String userAt;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("sex")
    @Expose
    private String sex;
    @SerializedName("birth_year")
    @Expose
    private Integer birthYear;
    @SerializedName("age")
    @Expose
    private Integer age;
    @SerializedName("height")
    @Expose
    private Integer height;
    @SerializedName("weight")
    @Expose
    private Integer weight;
    @SerializedName("province")
    @Expose
    private String province;
    @SerializedName("city")
    @Expose
    private String city;

    public List<ConditionDetail> getConditionDetails() {
        return conditionDetails;
    }

    public void setConditionDetails(List<ConditionDetail> conditionDetails) {
        this.conditionDetails = conditionDetails;
    }

    public List<VisitResult> getVisitResults() {
        return visitResults;
    }

    public void setVisitResults(List<VisitResult> visitResults) {
        this.visitResults = visitResults;
    }

    public List<SymptomCheckerResult> getSymptomCheckerResults() {
        return symptomCheckerResults;
    }

    public void setSymptomCheckerResults(List<SymptomCheckerResult> symptomCheckerResults) {
        this.symptomCheckerResults = symptomCheckerResults;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getUserAt() {
        JalaliCalendar sc = new JalaliCalendar(userAt);
        return sc.toString();
    }

    public void setUserAt(String userAt) {
        this.userAt = userAt;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public Integer getBirthYear() {
        return birthYear;
    }

    public void setBirthYear(Integer birthYear) {
        this.birthYear = birthYear;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Override
    public String toString() {
        return (new Gson()).toJson(this);
    }
}
