package com.darmaneh.models.patient_record;

import com.darmaneh.utilities.JalaliCalendar;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Calendar;

/**
 * Created by alireza on 7/18/17.
 */

public class VisitResult {

    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("time_stamp")
    @Expose
    private Long timeStamp;
    @SerializedName("ph_first_name")
    @Expose
    private String phFirstName;
    @SerializedName("ph_last_name")
    @Expose
    private String phLastName;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getPhFirstName() {
        return phFirstName;
    }

    public void setPhFirstName(String phFirstName) {
        this.phFirstName = phFirstName;
    }

    public String getPhLastName() {
        return phLastName;
    }

    public void setPhLastName(String phLastName) {
        this.phLastName = phLastName;
    }

    public String getJalaliDate()
    {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(this.timeStamp);
        JalaliCalendar jalali = new JalaliCalendar(calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH)+1, calendar.get(Calendar.DATE));
        String date = jalali.getDay() + " " + jalali.getStrMonth() +" "+ jalali.getYear();
        return date;
    }

    public String getMinute(){
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(this.timeStamp);
        String time = calendar.get(Calendar.HOUR_OF_DAY) + ":" + calendar.get(Calendar.MINUTE);
        return time;
    }

}
