package com.darmaneh.models.patient_record;

import com.darmaneh.utilities.JalaliCalendar;
import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Calendar;

/**
 * Created by pourya on 3/29/17.
 */

public class SymptomCheckerResult {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("time_stamp")
    @Expose
    private Long timeStamp;
    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("most_prob_condition")
    @Expose
    private String mostProbCondition;

    private Boolean visibleDate = true;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getMostProbCondition() {
        return mostProbCondition;
    }

    public void setMostProbCondition(String mostProbCondition) {
        this.mostProbCondition = mostProbCondition;
    }

    public String getCreatedAt() {
        JalaliCalendar sc = new JalaliCalendar(createdAt);
        return sc.toString();
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public Long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Long timeStamp) {
        this.timeStamp = timeStamp;
    }

    @Override
    public String toString() {
        return (new Gson()).toJson(this);
    }

    public Boolean getVisibleDate() {
        return visibleDate;
    }

    public void setVisibleDate(Boolean visibleDate) {
        this.visibleDate = visibleDate;
    }

    public String getJalaliDate()
    {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(this.timeStamp);
        JalaliCalendar jalali = new JalaliCalendar(calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH)+1, calendar.get(Calendar.DATE));
        String date = jalali.getDay() + " " + jalali.getStrMonth() +" "+ jalali.getYear();
        return date;
    }

    public String getMinute(){
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(this.timeStamp);
        String time = calendar.get(Calendar.HOUR_OF_DAY) + ":" + calendar.get(Calendar.MINUTE);
        return time;
    }
}
