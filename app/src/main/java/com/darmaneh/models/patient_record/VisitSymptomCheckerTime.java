package com.darmaneh.models.patient_record;

/**
 * Created by alireza on 7/20/17.
 */

public class VisitSymptomCheckerTime {
    private long timeStamp;
    private boolean isVisit;
    private int position;
    private boolean visibleDate = true;

    public VisitSymptomCheckerTime(long timeStamp, boolean isVisit, int pos) {
        this.timeStamp = timeStamp;
        this.isVisit = isVisit;
        this.position = pos;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public boolean isVisit() {
        return isVisit;
    }

    public void setVisit(boolean visit) {
        isVisit = visit;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public boolean isVisibleDate() {
        return visibleDate;
    }

    public void setVisibleDate(boolean visibleDate) {
        this.visibleDate = visibleDate;
    }
}
