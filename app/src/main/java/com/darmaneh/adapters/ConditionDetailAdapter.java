package com.darmaneh.adapters;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.darmaneh.ava.App;
import com.darmaneh.ava.ConditionDetailActivity;
import com.darmaneh.ava.R;
import com.darmaneh.models.ConditionDetailModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pourya on 3/28/17.
 */

public class ConditionDetailAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = ConditionDetailAdapter.class.getSimpleName();
    private ConditionDetailModel conditionDetailModel;
    private List<ConditionAdapterModel> cam = new ArrayList<>();
    private List<String> textBody = new ArrayList<>();
    private static final int TYPE_HEADER = 0;
    private static final int TYPE_BODY = 1;
    LinearLayoutManager llm;
    int previousHeaderPosition=0;

    public ConditionDetailAdapter(ConditionDetailModel conditionDetailModel) {
        this.conditionDetailModel = conditionDetailModel;
        makeList();
    }
    private void makeList() {
        int id = 0;
        if (!conditionDetailModel.getDescription().equals("")) {
            cam.add(new ConditionAdapterModel("توضیح", true, true,id));
            textBody.add(conditionDetailModel.getDescription());
            cam.add(new ConditionAdapterModel(conditionDetailModel.getDescription(), false, true, 0));
            id++;
        }
        if (!conditionDetailModel.getCause().equals("")) {
            cam.add(new ConditionAdapterModel("دلایل", true, false,id));
            textBody.add(conditionDetailModel.getCause());
            id++;
        }
        if (!conditionDetailModel.getSymptom().equals("")) {
            cam.add(new ConditionAdapterModel("علائم", true, false,id));
            textBody.add(conditionDetailModel.getSymptom());
            id++;
        }
        if (!conditionDetailModel.getDiagnosis().equals("")) {
            cam.add(new ConditionAdapterModel("تشخیص", true, false,id));
            textBody.add(conditionDetailModel.getDiagnosis());
            id++;
        }
        if (!conditionDetailModel.getTreatment().equals("")) {
            cam.add(new ConditionAdapterModel("درمان", true, false,id));
            textBody.add(conditionDetailModel.getTreatment());
            id++;
        }
        if (!conditionDetailModel.getPreventing().equals("")) {
            cam.add(new ConditionAdapterModel("پیشگیری", true, false,id));
            textBody.add(conditionDetailModel.getPreventing());
            id++;
        }
        if (!conditionDetailModel.getRiskFactor().equals("")) {
            cam.add(new ConditionAdapterModel("فاکتورهای ریسک", true, false,id));
            textBody.add(conditionDetailModel.getRiskFactor());
            id++;
        }
        if (!conditionDetailModel.getVisitDoctor().equals("")) {
            cam.add(new ConditionAdapterModel("زمان مراجعه به پزشک", true, false,id));
            textBody.add(conditionDetailModel.getVisitDoctor());
            id++;
        }
        if (!conditionDetailModel.getAskDoctor().equals("")) {
            cam.add(new ConditionAdapterModel("چه چیزهایی را از پزشک بپرسیم؟", true, false,id));
            textBody.add(conditionDetailModel.getAskDoctor());
            id++;
        }
        if (!conditionDetailModel.getExpectation().equals("")) {
            cam.add(new ConditionAdapterModel("انتظارات", true, false,id));
            textBody.add(conditionDetailModel.getExpectation());
            id++;
        }
        if (!conditionDetailModel.getGotWorse().equals("")) {
            cam.add(new ConditionAdapterModel("بدتر شدن", true, false,id));
            textBody.add(conditionDetailModel.getGotWorse());
            id++;
        }
        if (!conditionDetailModel.getOther().equals("")) {
            cam.add(new ConditionAdapterModel("توضیحات بیشتر", true, false,id));
            textBody.add(conditionDetailModel.getOther());
        }
    }

    @Override
    public int getItemViewType(int position) {
        if(cam.get(position).isLabel()){
            return TYPE_HEADER;
        }else{
            return TYPE_BODY;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        RecyclerView rv = (RecyclerView) parent.findViewById(R.id.condition_detail_items);
        llm = (LinearLayoutManager) rv.getLayoutManager();
        if(viewType == TYPE_HEADER) {
            View itemView = LayoutInflater.
                    from(context).
                    inflate(R.layout.item_condition_detail, parent, false);
            return new HeaderViewHolder(itemView);
        }else{
            View itemView = LayoutInflater.
                    from(context).
                    inflate(R.layout.item_condition_detail_body, parent, false);
            return new BodyViewHolder(itemView);
        }
    }

    private class HeaderViewHolder extends RecyclerView.ViewHolder{
        TextView conditionLabel;
        RelativeLayout conditionLabelLayout;
        ImageView arrow;

        public HeaderViewHolder(View view) {
            super(view);
            this.conditionLabel = (TextView) view.findViewById(R.id.condition_label);
            this.conditionLabel.setTypeface(App.getFont(4));
            this.arrow = (ImageView) view.findViewById(R.id.arrow_down);
            this.conditionLabelLayout = (RelativeLayout) view.findViewById(R.id.condition_label_layout);
            this.conditionLabelLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int newPosition = getAdapterPosition() +1 ;
                    int id = cam.get(getAdapterPosition()).getId();
                    if(!cam.get(getAdapterPosition()).isExpanded()) {
                        cam.get(getAdapterPosition()).setExpanded(true);
                        cam.add(newPosition, new ConditionAdapterModel(textBody.get(id), false, false, 0));
                        notifyItemChanged(getAdapterPosition());
                        notifyItemInserted(newPosition);
                        scrollPosition(getAdapterPosition());
                        removePreviousBody(getAdapterPosition());
                    }else{
                        cam.get(getAdapterPosition()).setExpanded(false);
                        cam.remove(newPosition);
                        previousHeaderPosition = -1;
                        notifyItemChanged(getAdapterPosition());
                        notifyItemRemoved(newPosition);
                    }
                }
            });
        }
        private void scrollPosition(int position) {
            if(previousHeaderPosition < position)   //It depends on which header has to be removed
                llm.scrollToPositionWithOffset(position - 1, 0);
            else
                llm.scrollToPositionWithOffset(position, 0);
        }

        private void removePreviousBody(int headerPosition) {
            Log.d(TAG, "new Position "+headerPosition+",\tprevious "+previousHeaderPosition);
            if(previousHeaderPosition == -1){       //no header is expanded
                previousHeaderPosition = headerPosition;
            }else{
                if(headerPosition < previousHeaderPosition){
                    //if new header clicked is before or after previous one, it affect on removing body
                    previousHeaderPosition++;
                    changeOpenBody();
                    previousHeaderPosition = headerPosition;
                }else{
                    changeOpenBody();
                    previousHeaderPosition = headerPosition-1;
                }
            }
        }

        private void changeOpenBody() {
            cam.remove(previousHeaderPosition+1);
            notifyItemRemoved(previousHeaderPosition+1);
            cam.get(previousHeaderPosition).setExpanded(false);
            notifyItemChanged(previousHeaderPosition);
        }
    }
    private class BodyViewHolder extends RecyclerView.ViewHolder{
        TextView conditionBody;
        public BodyViewHolder(View view) {
            super(view);
            this.conditionBody = (TextView) view.findViewById(R.id.condition_body);
            this.conditionBody.setTypeface(App.getFont(3));
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(holder.getItemViewType() == TYPE_BODY){
            BodyViewHolder bvh = (BodyViewHolder) holder;
            Spanned t;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                t = Html.fromHtml(cam.get(position).getText(), Html.FROM_HTML_MODE_LEGACY);
            } else {
                t = Html.fromHtml(cam.get(position).getText());
            }
            bvh.conditionBody.setText(t.toString().trim());
        }else {
            HeaderViewHolder hvh = (HeaderViewHolder) holder;
            hvh.conditionLabel.setText(cam.get(position).getText());
            if(cam.get(position).isExpanded()){ //TODO add animation for changing arrow
                hvh.arrow.setImageResource(R.drawable.arrow_up);
            }else{
                hvh.arrow.setImageResource(R.drawable.arrow_down);
            }
        }
    }

    @Override
    public int getItemCount() {
        return cam.size();
    }


    private class ConditionAdapterModel {
        private String text;
        private boolean isLabel;
        private boolean isExpanded;
        private int id;

        public ConditionAdapterModel(String text, boolean isLabel, boolean isExpanded , int id) {
            this.text = text;
            this.isLabel = isLabel;
            this.isExpanded = isExpanded;
            this.id = id;
        }

        public String getText() {
            return text;
        }

        public boolean isLabel() {
            return isLabel;
        }

        public boolean isExpanded() {
            return isExpanded;
        }

        public int getId() {
            return id;
        }

        public void setExpanded(boolean expanded) {
            isExpanded = expanded;
        }
    }

}
