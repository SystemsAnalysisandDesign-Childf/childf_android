package com.darmaneh.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import com.darmaneh.ava.App;
import com.darmaneh.ava.R;

import java.util.ArrayList;

/**
 * Created by pourya on 9/29/16.
 */
public class CustomSpinnerAdapter extends BaseAdapter implements SpinnerAdapter {

    private Context context;
    private ArrayList<String> asr;

    public CustomSpinnerAdapter(Context context, ArrayList<String> asr) {
        this.asr=asr;
        this.context = context;
    }

    public int getCount()
    {
        return asr.size();
    }

    public Object getItem(int i)
    {
        return asr.get(i);
    }

    public long getItemId(int i)
    {
        return (long)i;
    }



    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        TextView txt = new TextView(context);
        txt.setTypeface(App.getFont(3));
        txt.setTextSize(16);
        txt.setText(asr.get(position));
        txt.setPadding(0,0,24,0);
//        txt.setTextColor(ContextCompat.getColor(context, R.color.dark_gray));
        return  txt;
    }

    public View getView(int i, View view, ViewGroup viewgroup) {
        TextView txt = new TextView(context);
        txt.setTypeface(App.getFont(3));
        txt.setTextSize(16);
        txt.setPadding(0,0,16,0);
        txt.setText(asr.get(i));
        txt.setTextColor(ContextCompat.getColor(context, R.color.dark_gray));
        return  txt;
    }

}