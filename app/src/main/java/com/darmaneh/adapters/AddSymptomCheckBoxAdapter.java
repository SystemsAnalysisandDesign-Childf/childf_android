package com.darmaneh.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.darmaneh.ava.App;
import com.darmaneh.ava.R;
import com.darmaneh.models.diagnosis.Evidence;

import java.util.List;

/**
 * Created by pourya on 3/25/17.
 */

public class AddSymptomCheckBoxAdapter extends RecyclerView.Adapter<AddSymptomCheckBoxAdapter.SymptomListViewHolder> {
    Context context;

    @Override
    public SymptomListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        View itemView;
        itemView = LayoutInflater.
                from(context).
                inflate(R.layout.item_add_symptom_list, parent, false);
        return new SymptomListViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(SymptomListViewHolder holder, int position) {
        final Evidence evidence = App.diagnosis_req.getEvidence().get(position);
        holder.symptomName.setText(evidence.getName());
        holder.btnRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                List<Evidence> evidences = App.diagnosis_req.getEvidence();
                evidences.remove(evidence);
                App.diagnosis_req.setEvidence(evidences);
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return App.diagnosis_req.getEvidence().size();
    }

    class SymptomListViewHolder extends RecyclerView.ViewHolder {
        FrameLayout btnRemove;
        TextView symptomName;

        SymptomListViewHolder(View v) {
            super(v);
            this.btnRemove = (FrameLayout) v.findViewById(R.id.btn_remove);
            this.symptomName = (TextView) v.findViewById(R.id.symptom_name);

            this.symptomName.setTypeface(App.getFont(3));
        }
    }
}
