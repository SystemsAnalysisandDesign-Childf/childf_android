package com.darmaneh.adapters;

import android.content.ContentResolver;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.darmaneh.ava.App;
import com.darmaneh.ava.R;
import com.darmaneh.fragments.PhotoDescriptionFragment;
import com.darmaneh.models.call.ImageModel;

import java.util.List;

/**
 * Created by alireza on 7/28/17.
 */

public class PhotoDescriptionAdapter extends RecyclerView.Adapter<PhotoDescriptionAdapter.ViewHolder> {
    List<ImageModel> imageModels;
    Context context;

    public PhotoDescriptionAdapter(List<ImageModel> imageModelList, Context context) {
        this.imageModels = imageModelList;
        this.context = context;
    }


    @Override
    public PhotoDescriptionAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_photo_description, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(PhotoDescriptionAdapter.ViewHolder viewHolder, int i) {
        viewHolder.packName.setText(imageModels.get(i).getPack_name());
        viewHolder.description.setText(imageModels.get(i).getDescription());
        viewHolder.image.setImageBitmap(imageModels.get(i).getImage_bitmap());
    }

    @Override
    public int getItemCount() {
        return imageModels.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView packName, description;
        ImageView image;
        public ViewHolder(View view) {
            super(view);
            packName = (TextView) view.findViewById(R.id.pack_name);
            description = (TextView) view.findViewById(R.id.desc);
            packName.setTypeface(App.getFont(3));
            description.setTypeface(App.getFont(3));
            image = (ImageView) view.findViewById(R.id.selected_image);
        }
    }
}
