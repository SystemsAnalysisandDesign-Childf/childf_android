package com.darmaneh.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.darmaneh.ava.App;
import com.darmaneh.ava.R;

import com.darmaneh.dialogs.SweetAlertDialog;
import com.darmaneh.fragments.AdapterCallback;
import com.darmaneh.models.patient_record.SymptomCheckerResult;
import com.darmaneh.models.patient_record.VisitResult;
import com.darmaneh.models.patient_record.VisitSymptomCheckerTime;
import com.darmaneh.requests.SymptomCheckerHistory;
import com.darmaneh.requests.VisitHistory;
import com.darmaneh.utilities.Analytics;
import com.darmaneh.utilities.Functions;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by alireza on 3/30/17.
 */
public class HistoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<SymptomCheckerResult> checkerResults;
    private List<VisitResult> visitResults;
    private List<VisitSymptomCheckerTime> times;
    private static final int TYPE_VISIT = 0;
    private static final int TYPE_SYMPTOM = 1;

    private AdapterCallback myCallback;

    public HistoryAdapter(List<SymptomCheckerResult> checkerResults, List<VisitResult> visitResults,
                          List<VisitSymptomCheckerTime>times , AdapterCallback callback) {
        this.checkerResults = checkerResults;
        this.myCallback = callback;
        this.visitResults = visitResults;
        this.times = times;
    }

    public void swapData(List<SymptomCheckerResult> symptomCheckerResults,
                         List<VisitResult> visitResults, List<VisitSymptomCheckerTime> timeList){
        this.checkerResults.clear();
        this.checkerResults.addAll(symptomCheckerResults);
        this.visitResults.clear();
        this.visitResults.addAll(visitResults);
        this.times.clear();
        this.times.addAll(timeList);
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        if (times.get(position).isVisit())
            return TYPE_VISIT;
        else
            return TYPE_SYMPTOM;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;
        if(viewType == TYPE_SYMPTOM){
            itemView = LayoutInflater.
                    from(parent.getContext()).
                    inflate(R.layout.item_history_list, parent, false);
            return new SymptomCheckerHolder(itemView);
        }else{
            itemView = LayoutInflater.
                    from(parent.getContext()).
                    inflate(R.layout.item_visit_history_list, parent, false);
            return new VisitViewHolder(itemView);
        }
    }

    private class VisitViewHolder extends RecyclerView.ViewHolder{
        TextView timeTxt, dateTxt, doctorName, moreInfo;
        RelativeLayout headerCard;

        VisitViewHolder(View v) {
            super(v);
            this.timeTxt = (TextView) v.findViewById(R.id.time_txt);
            this.dateTxt = (TextView) v.findViewById(R.id.date);
            this.doctorName = (TextView) v.findViewById(R.id.doctor_name);
            this.moreInfo = (TextView) v.findViewById(R.id.more_info);
            this.headerCard = (RelativeLayout) v.findViewById(R.id.card2);
            this.doctorName.setTypeface(App.getFont(5));
            this.moreInfo.setTypeface(App.getFont(3));
            this.dateTxt.setTypeface(App.getFont(3));
            this.timeTxt.setTypeface(App.getFont(3));
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = times.get(getAdapterPosition()).getPosition();
                    VisitHistory.get_visit_history_detail(v.getContext(),visitResults.get(position).getUrl());
                }
            });
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int pos) {
        int position = times.get(pos).getPosition();
        if(getItemViewType(pos) == TYPE_SYMPTOM) {
            final SymptomCheckerResult scr = checkerResults.get(position);
            SymptomCheckerHolder sch = (SymptomCheckerHolder) holder;
            sch.date.setText(Functions.toPersian(scr.getJalaliDate()));
            sch.illness.setText(scr.getMostProbCondition());
            sch.timeTxt.setText(Functions.toPersian(scr.getMinute()));
            sch.timeTxt.setTypeface(App.getFont(4));
            sch.illnessTxt.setTypeface(App.getFont(4));
            sch.date.setTypeface(App.getFont(3));
            sch.illness.setTypeface(App.getFont(3));
            sch.moreInfo.setTypeface(App.getFont(3));

            if (times.get(pos).isVisibleDate()) {
                sch.card2.setVisibility(View.VISIBLE);
            } else {
                sch.card2.setVisibility(View.GONE);
            }
        }else{
            VisitViewHolder vvh = (VisitViewHolder) holder;
            final VisitResult vr = visitResults.get(position);
            String doctorNameStr = vr.getPhFirstName() + " " + vr.getPhLastName();
            vvh.doctorName.setText(doctorNameStr);
            vvh.dateTxt.setText(Functions.toPersian(vr.getJalaliDate()));
            vvh.timeTxt.setText(Functions.toPersian(vr.getMinute()));
            if(times.get(pos).isVisibleDate())
                vvh.headerCard.setVisibility(View.VISIBLE);
            else
                vvh.headerCard.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return times.size();
    }

    private class SymptomCheckerHolder extends RecyclerView.ViewHolder {
        TextView date, timeTxt, illness ,illnessTxt, moreInfo;
        ImageView closeBtn;
        RelativeLayout card2;
        SymptomCheckerHolder(View v) {
            super(v);
            date = (TextView) v.findViewById(R.id.date);
            timeTxt = (TextView) v.findViewById(R.id.time_txt);
            illness = (TextView) v.findViewById(R.id.illness);
            illnessTxt = (TextView) v.findViewById(R.id.illness_txt);
            closeBtn = (ImageView) v.findViewById(R.id.close);
            closeBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View view) {
                    final int position = times.get(getAdapterPosition()).getPosition();
                    new SweetAlertDialog(view.getContext(), SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("آیا مطمئن هستید؟")
                            .setContentText("پس از حذف قادر به بازیابی نیستید.")
                            .setConfirmText("بلی")
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(final SweetAlertDialog sDialog) {

                                    sDialog.dismiss();

                                    SymptomCheckerHistory.patient_remove_sc_history(view.getContext(),
                                            checkerResults.get(position).getId(), new SymptomCheckerHistory.PatientRemoveSymptomCheckerHistory() {
                                                @Override
                                                public void onHttpResponse(Boolean success) {
                                                    if(success) {
                                                        Analytics.sendScreenName("EMR/history/remove");
                                                        checkerResults.remove(position);
                                                        refreshTimeList();
                                                        notifyDataSetChanged();
                                                        myCallback.checkEmptyCallback();
                                                        sDialog.setTitleText("پاک شد!")
                                                                .setContentText(null)
                                                                .showContentText(false)
                                                                .setConfirmText("خُب")
                                                                .setConfirmClickListener(null)
                                                                .showCancelButton(false)
                                                                .setCancelClickListener(null)
                                                                .changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
                                                        sDialog.show();
                                                    }
                                                }
                                            });
                                }
                            })
                            .setCancelText("خیر")
                            .showCancelButton(true)
                            .setCancelClickListener(null)
                            .show();
                }
            });
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position = times.get(getAdapterPosition()).getPosition();
                    SymptomCheckerHistory.get_sc_history_detail(view.getContext(), checkerResults.get(position).getUrl());
                }
            });
            moreInfo = (TextView)v.findViewById(R.id.more_info);
            card2 = (RelativeLayout) v.findViewById(R.id.card2);
        }
    }


    private void refreshTimeList(){
        times.clear();
        for (int i=0 ; i<visitResults.size(); i++){
            VisitResult result = visitResults.get(i);
            times.add(new VisitSymptomCheckerTime(result.getTimeStamp(),true,i));
        }
        for (int i=0 ; i<checkerResults.size() ; i++){
            SymptomCheckerResult result = checkerResults.get(i);
            times.add(new VisitSymptomCheckerTime(result.getTimeStamp(),false,i));
        }
        Collections.sort(times, new Comparator<VisitSymptomCheckerTime>() {
            @Override
            public int compare(VisitSymptomCheckerTime result1, VisitSymptomCheckerTime result2) {
                if(result1.getTimeStamp() < result2.getTimeStamp()){
                    return 1;
                }else{
                    return -1;
                }
            }
        });

        int size = times.size();
        if(size>1){
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(times.get(0).getTimeStamp());
            int previousDay = calendar.get(Calendar.DAY_OF_YEAR);
            int currentDay;
            for (int i = 1; i < size; i++) {
                calendar.setTimeInMillis(times.get(i).getTimeStamp());
                currentDay = calendar.get(Calendar.DAY_OF_YEAR);
                if(currentDay == previousDay)
                    times.get(i).setVisibleDate(false);
                else
                    times.get(i).setVisibleDate(true);
                previousDay = currentDay;
            }
        }
    }
}
