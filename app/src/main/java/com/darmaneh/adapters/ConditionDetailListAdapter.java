package com.darmaneh.adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.darmaneh.ava.App;
import com.darmaneh.ava.R;
import com.darmaneh.models.ConditionDetailListModel;
import com.darmaneh.models.unadopted.UnadoptedListModel;
import com.darmaneh.requests.ConditionDetail;

import java.util.List;


public class ConditionDetailListAdapter extends RecyclerView.Adapter<ConditionDetailListAdapter.ConditionModelViewHolder>{

    private List<UnadoptedListModel> unadoptedListModels;
    Context context;

    public ConditionDetailListAdapter(List<UnadoptedListModel> conditionModels,
                                      Context context){
        this.unadoptedListModels = conditionModels;
        this.context = context;
    }

    @Override
    public int getItemCount() {
        return unadoptedListModels.size();
    }

    class ConditionModelViewHolder extends RecyclerView.ViewHolder {

        RelativeLayout cv;
        TextView conditionName;
        ImageView add;

        ConditionModelViewHolder(View itemView) {
            super(itemView);
            cv = (RelativeLayout) itemView.findViewById(R.id.cv);
            conditionName = (TextView)itemView.findViewById(R.id.condition_name);
            conditionName.setTypeface(App.getFont(4));
            add = (ImageView) itemView.findViewById(R.id.adopt);
            add.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ConditionDetail.get_condition_detail(view.getContext(),
                            unadoptedListModels.get(getAdapterPosition()).getId());

                }
            });
        }
    }

    @Override
    public ConditionModelViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_condition_detail_list, viewGroup, false);
        ConditionModelViewHolder avh = new ConditionModelViewHolder(v);
        return avh;
    }

    @Override
    public void onBindViewHolder(final ConditionModelViewHolder conditionModelViewHolder, final int i) {
        final UnadoptedListModel conditionDetailListModel = unadoptedListModels.get(i);
        conditionModelViewHolder.conditionName.setText(conditionDetailListModel.getUser().getFirstName() + ' ' + conditionDetailListModel.getUser().getLastName());
  }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

}
