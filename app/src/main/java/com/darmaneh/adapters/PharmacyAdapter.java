package com.darmaneh.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.darmaneh.ava.App;
import com.darmaneh.ava.R;
import com.darmaneh.models.health_centers.PharmacyModel;
import com.darmaneh.requests.HealthCenters;

import java.util.List;

/**
 * Created by alireza on 5/27/17.
 */

public class PharmacyAdapter extends RecyclerView.Adapter<PharmacyAdapter.MainViewHolder> {

    private List<PharmacyModel> results;

    public PharmacyAdapter(List<PharmacyModel> results) {
        this.results = results;
    }

    @Override
    public void onBindViewHolder(PharmacyAdapter.MainViewHolder holder, int position) {
        holder.name.setText(results.get(position).getSpecialName());
        holder.address.setText(results.get(position).getAddress());
        holder.name.setTypeface(App.getFont(4));
        holder.address.setTypeface(App.getFont(3));
    }

    @Override
    public int getItemCount() {
        return results.size();
    }
    @Override
    public MainViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;
        itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.item_hospital_list, parent, false);
        return new PharmacyAdapter.MainViewHolder(itemView);
    }

    class MainViewHolder extends RecyclerView.ViewHolder{
        TextView name, address;
        MainViewHolder(View v) {
            super(v);
            name = (TextView) v.findViewById(R.id.hospital_name);
            address = (TextView) v.findViewById(R.id.hospital_address);
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    HealthCenters.get_pharmacy_detail(view.getContext(), results.get(getAdapterPosition()).getUrl());
                }
            });
        }
    }
}
