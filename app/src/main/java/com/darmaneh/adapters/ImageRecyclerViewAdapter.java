package com.darmaneh.adapters;

/**
 * Created by Sepehr Abdous on 7/4/2017.
 */

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.support.design.widget.TextInputEditText;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.darmaneh.ava.App;
import com.darmaneh.ava.R;
import com.darmaneh.models.call.ImageModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ImageRecyclerViewAdapter extends RecyclerView.Adapter<ImageRecyclerViewAdapter.ViewHolder> {
    private ArrayList<ImageModel> imageModels;
    private Context context;
    private  OnDeleteRecyclerViewListener mListener;

    public ImageRecyclerViewAdapter(Context context,ArrayList<ImageModel> imageModels) {
        this.imageModels = imageModels;
        this.context = context;
        mListener = (OnDeleteRecyclerViewListener) context;
    }

    @Override
    public ImageRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.image_picker_layout, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ImageRecyclerViewAdapter.ViewHolder viewHolder, int i) {
        viewHolder.packName.setText(imageModels.get(i).getPack_name());
        viewHolder.description.setText(imageModels.get(i).getDescription());
        BitmapFactory.Options config = new BitmapFactory.Options();
        config.inPreferredConfig = Bitmap.Config.RGB_565;
        config.inSampleSize = 4;
//        Bitmap bm = BitmapFactory.decodeByteArray(imageModels.get(i).getImageByteArray(),0,
//                imageModels.get(i).getImageLength(),config);
//        Log.d("Image Adapter",String.valueOf(bm.getByteCount()));
//        viewHolder.image.setImageBitmap(bm);
        addListeners(viewHolder, i);
    }

    @Override
    public int getItemCount() {
        return imageModels.size();
    }

    private void addListeners(final ImageRecyclerViewAdapter.ViewHolder viewHolder, final int pos){

        viewHolder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imageModels.remove(pos);
                mListener.deleteImage(imageModels);
            }
        });

        viewHolder.edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                imageModels.remove(pos);
//                mListener.deleteImage(imageModels);
                viewHolder.packNameInput.setText(viewHolder.packName.getText().toString());
                viewHolder.descriptionInput.setText(viewHolder.description.getText().toString());

                viewHolder.packName.setVisibility(View.GONE);
                viewHolder.description.setVisibility(View.GONE);
                viewHolder.delete.setVisibility(View.GONE);
                viewHolder.edit.setVisibility(View.INVISIBLE);
                viewHolder.accept.setVisibility(View.VISIBLE);
                viewHolder.cancel.setVisibility(View.VISIBLE);
                viewHolder.packNameInput.setVisibility(View.VISIBLE);
                viewHolder.descriptionInput.setVisibility(View.VISIBLE);
            }
        });

        viewHolder.accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                imageModels.remove(pos);
//                mListener.deleteImage(imageModels);
                viewHolder.packName.setText(viewHolder.packNameInput.getText().toString());
                viewHolder.description.setText(viewHolder.descriptionInput.getText().toString());

                imageModels.get(pos).setPack_name(viewHolder.packNameInput.getText().toString());
                imageModels.get(pos).setDescription(viewHolder.descriptionInput.getText().toString());

                viewHolder.packNameInput.setText("");
                viewHolder.descriptionInput.setText("");

                viewHolder.packName.setVisibility(View.VISIBLE);
                viewHolder.description.setVisibility(View.VISIBLE);
                viewHolder.delete.setVisibility(View.VISIBLE);
                viewHolder.cancel.setVisibility(View.GONE);
                viewHolder.edit.setVisibility(View.VISIBLE);
                viewHolder.accept.setVisibility(View.GONE);
                viewHolder.cancel.setVisibility(View.GONE);
                viewHolder.packNameInput.setVisibility(View.GONE);
                viewHolder.descriptionInput.setVisibility(View.GONE);

                mListener.updateModel(imageModels);
            }
        });

        viewHolder.cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                imageModels.remove(pos);
//                mListener.deleteImage(imageModels);

                viewHolder.packNameInput.setText("");
                viewHolder.descriptionInput.setText("");

                viewHolder.packName.setVisibility(View.VISIBLE);
                viewHolder.description.setVisibility(View.VISIBLE);
                viewHolder.delete.setVisibility(View.VISIBLE);
                viewHolder.cancel.setVisibility(View.GONE);
                viewHolder.edit.setVisibility(View.VISIBLE);
                viewHolder.accept.setVisibility(View.GONE);
                viewHolder.cancel.setVisibility(View.GONE);
                viewHolder.packNameInput.setVisibility(View.GONE);
                viewHolder.descriptionInput.setVisibility(View.GONE);
            }
        });
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView packName, description;
        ImageView image, delete, edit, accept, cancel;
        TextInputEditText packNameInput, descriptionInput;
        public ViewHolder(View view) {
            super(view);

            packName = (TextView)view.findViewById(R.id.pack_name);
            description = (TextView)view.findViewById(R.id.desc);

            packName.setTypeface(App.getFont(3));
            description.setTypeface(App.getFont(3));

            image = (ImageView)view.findViewById(R.id.selected_image);
            delete = (ImageView)view.findViewById(R.id.deletePic);
            edit = (ImageView)view.findViewById(R.id.editPic);
            accept = (ImageView)view.findViewById(R.id.editAccept);
            cancel = (ImageView) view.findViewById(R.id.cancel_edit);

            packNameInput = (TextInputEditText) view.findViewById(R.id.pack_name_input);
            descriptionInput = (TextInputEditText) view.findViewById(R.id.desc_input);
        }
    }

    public interface OnDeleteRecyclerViewListener {
        void deleteImage(ArrayList<ImageModel> imageModels);
        void updateModel(ArrayList<ImageModel> imageModels);
    }
}