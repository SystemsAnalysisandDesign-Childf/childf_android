package com.darmaneh.adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.darmaneh.ava.App;
import com.darmaneh.ava.R;
import com.darmaneh.models.location.Hospital;
import com.darmaneh.models.location.Location;
import com.squareup.picasso.Picasso;

/**
 * Created by alireza on 6/18/17.
 */

public class HospitalDetailAdapter extends RecyclerView.Adapter<HospitalDetailAdapter.MainViewHolder> {

    private Hospital hospitalDetailModel ;
    private Context context;
    private final String STATIC_MAP_API_ENDPOINT =
            "http://maps.googleapis.com/maps/api/staticmap?size=640x400&zoom=15&markers=";

    public HospitalDetailAdapter(Hospital hospitalDetailModel, Context context) {
        this.hospitalDetailModel = hospitalDetailModel;
        this.context = context;
    }

    @Override
    public MainViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;
        itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.item_hospital_detail, parent, false);
        return new HospitalDetailAdapter.MainViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(HospitalDetailAdapter.MainViewHolder holder, int position) {
        holder.address.setText(hospitalDetailModel.getAddress());
        holder.tell.setText(hospitalDetailModel.getTell());
        holder.site.setText(hospitalDetailModel.getSite());
        holder.email.setText(hospitalDetailModel.getEmail());
        holder.activity.setText(hospitalDetailModel.getActivity().get(0));
        holder.expertise.setText(hospitalDetailModel.getExpertise());
        holder.organization.setText(hospitalDetailModel.getOrganization());
        holder.bedCount.setText(String.valueOf(hospitalDetailModel.getBedCount()));
        holder.hospitalization.setText(hospitalDetailModel.getHospitalization());
        holder.paraclinical.setText(hospitalDetailModel.getParaClinic());
        holder.clinical.setText(hospitalDetailModel.getClinical());
        holder.withStar.setText(hospitalDetailModel.getWithStar());

        if(hospitalDetailModel.getLocation().getLatitude() != 0 &&
                hospitalDetailModel.getLocation().getLongitude() != 0) {
            String locationStr = hospitalDetailModel.getLocation().getLatitude() + "," +
                    hospitalDetailModel.getLocation().getLongitude();
            String keyStr = "&key=" + context.getString(R.string.google_api_key);
            String url = STATIC_MAP_API_ENDPOINT + locationStr + keyStr;
            Log.d("url ", url);
            Picasso.with(context).load(url).placeholder(R.drawable.map_loading)
                    .fit().centerCrop()
                    .into(holder.map);
        }else{
            holder.map.setVisibility(View.GONE);
        }

    }

    @Override
    public int getItemCount() {
        return 1;
    }



    class MainViewHolder extends RecyclerView.ViewHolder{
        TextView name, address, tell,
                site, email, activity,
                expertise, organization, bedCount,
                hospitalization, clinical, paraclinical, withStar;
        ImageView map;
        public MainViewHolder(View itemView) {
            super(itemView);
            initViews(itemView);
            setViewsFont(itemView);
            setOnClick();
        }

        private void initViews(View view) {
            address = (TextView) view.findViewById(R.id.address);
            site = (TextView) view.findViewById(R.id.site);
            email = (TextView) view.findViewById(R.id.email);
            tell = (TextView) view.findViewById(R.id.tell);
            activity = (TextView) view.findViewById(R.id.activity);
            expertise = (TextView) view.findViewById(R.id.expertise);
            organization = (TextView) view.findViewById(R.id.organization);
            bedCount = (TextView) view.findViewById(R.id.bed_count);
            hospitalization = (TextView) view.findViewById(R.id.hospitalization);
            clinical = (TextView) view.findViewById(R.id.clinical);
            paraclinical =(TextView) view.findViewById(R.id.paraclinical);
            withStar = (TextView) view.findViewById(R.id.with_star);
            map = (ImageView) view.findViewById(R.id.map);
        }
        private void setViewsFont(View view) {
            address.setTypeface(App.getFont(4));
            ((TextView)view.findViewById(R.id.address_text)).setTypeface(App.getFont(3));
            tell.setTypeface(App.getFont(4));
            ((TextView)view.findViewById(R.id.tell_text)).setTypeface(App.getFont(3));
            site.setTypeface(App.getFont(4));
            ((TextView)view.findViewById(R.id.site_text)).setTypeface(App.getFont(3));
            email.setTypeface(App.getFont(4));
            ((TextView)view.findViewById(R.id.email_text)).setTypeface(App.getFont(3));
            activity.setTypeface(App.getFont(4));
            ((TextView)view.findViewById(R.id.activity_text)).setTypeface(App.getFont(3));
            expertise.setTypeface(App.getFont(4));
            ((TextView)view.findViewById(R.id.expertise_text)).setTypeface(App.getFont(3));
            organization.setTypeface(App.getFont(4));
            ((TextView)view.findViewById(R.id.organization_text)).setTypeface(App.getFont(3));
            bedCount.setTypeface(App.getFont(4));
            ((TextView)view.findViewById(R.id.bed_count_text)).setTypeface(App.getFont(3));
            hospitalization.setTypeface(App.getFont(4));
            ((TextView)view.findViewById(R.id.hospitalization_text)).setTypeface(App.getFont(3));
            paraclinical.setTypeface(App.getFont(4));
            ((TextView)view.findViewById(R.id.paraclinical_text)).setTypeface(App.getFont(3));
            clinical.setTypeface(App.getFont(4));
            ((TextView)view.findViewById(R.id.clinical_text)).setTypeface(App.getFont(3));
            withStar.setTypeface(App.getFont(4));
            ((TextView)view.findViewById(R.id.with_star_text)).setTypeface(App.getFont(3));
        }


        private void setOnClick() {
            map.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String locationStr = hospitalDetailModel.getLocation().getLatitude() + ","+
                            hospitalDetailModel.getLocation().getLongitude();
                    Uri uri = Uri.parse("geo:0,0?q="+locationStr+"(بیمارستان "+hospitalDetailModel.getName()+")");
                    Intent intent = new Intent(Intent.ACTION_VIEW,uri);
                    context.startActivity(intent);
                }
            });
        }

    }
}
