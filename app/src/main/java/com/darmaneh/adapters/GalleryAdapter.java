package com.darmaneh.adapters;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.icu.text.LocaleDisplayNames;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.darmaneh.ava.R;
import com.darmaneh.ava.call.FullSizeImageActivity;
import com.darmaneh.models.call.GalleryModel;
import com.darmaneh.utilities.Storage;
import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import com.squareup.picasso.Cache;
import com.squareup.picasso.Callback;
import com.squareup.picasso.LruCache;
import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by alireza on 8/3/17.
 */

public class GalleryAdapter extends RecyclerView.Adapter<GalleryAdapter.MainViewHolder> {
    private Context context;
    private List<GalleryModel> galleryModelList;
    private final String TAG = GalleryAdapter.class.getSimpleName();
    public boolean deleteMode = false;
    private Picasso picasso;

    public GalleryAdapter(Context context, List<GalleryModel> galleryModelList) {
        this.context = context;
        this.galleryModelList = galleryModelList;
        initPicasso();
    }

    private void initPicasso() {
        OkHttpClient client = new OkHttpClient();
        client.interceptors().add(new Interceptor() {
            @Override
            public Response intercept(Interceptor.Chain chain) throws IOException {
                Request request = chain.request().newBuilder()
                        .addHeader("Authorization", Storage.getToken())
                        .build();
                return chain.proceed(request);
            }
        });
        picasso = new Picasso.Builder(context)
                .memoryCache(new LruCache(25000000))
                .downloader(new OkHttpDownloader(client)).build();
    }


    @Override
    public MainViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;
        itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.item_gallery_list, parent, false);
        return new MainViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MainViewHolder holder, int position) {
        picasso.load(galleryModelList.get(position).getImageTh()).placeholder(R.drawable.gallery_loading)
                .into(holder.image, new Callback() {
                    @Override
                    public void onSuccess() {
                        Log.d(TAG,"success "+holder.getAdapterPosition());
                    }

                    @Override
                    public void onError() {
                        Log.d(TAG,"fail "+holder.getAdapterPosition());
                    }
                });
        if(deleteMode){
            holder.transLayout.setVisibility(View.VISIBLE);
            if(galleryModelList.get(position).isToBeHidden()) {
                holder.transLayout.setBackgroundColor(ContextCompat.getColor(context, R.color.float_transparent));
                holder.deleteCB.setChecked(true);
            }
            else {
                holder.transLayout.setBackgroundColor(ContextCompat.getColor(context, R.color.white_transparent));
                holder.deleteCB.setChecked(false);
            }
        }else{
            holder.transLayout.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return galleryModelList.size();
    }

    public class MainViewHolder extends RecyclerView.ViewHolder{
        private ImageView image;
        private FrameLayout transLayout;
        private CheckBox deleteCB;
        public MainViewHolder(final View itemView) {
            super(itemView);
            image = (ImageView) itemView.findViewById(R.id.image);
            image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, FullSizeImageActivity.class);
                    intent.putExtra("pic_model",galleryModelList.get(getAdapterPosition()).toString());
                    context.startActivity(intent);
                }
            });

            transLayout = (FrameLayout) itemView.findViewById(R.id.transparent_layout);
            deleteCB = (CheckBox) itemView.findViewById(R.id.delete_pic_cb);
            deleteCB.setClickable(false);
            transLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    deleteCB.toggle();
                    galleryModelList.get(getAdapterPosition()).toggleToBeHidden();
                    notifyItemChanged(getAdapterPosition());
                }
            });

//            deleteCB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//                @Override
//                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                    galleryModelList.get(getAdapterPosition()).setToBeHidden(isChecked);
//                    notifyItemChanged(getAdapterPosition());
//                }
//            });
        }
    }

    public List<String> getTobeHiddenList(){
        List<String> list = new ArrayList<>();
        for(GalleryModel model:galleryModelList){
            if(model.isToBeHidden())
                list.add(model.getEditUrl());
        }
        return list;
    }
}
