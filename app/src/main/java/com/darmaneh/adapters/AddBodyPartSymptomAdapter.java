package com.darmaneh.adapters;

import android.content.Context;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.darmaneh.ava.App;
import com.darmaneh.ava.R;
import com.darmaneh.ava.diagnosis.BodyPartSymptomList;
import com.darmaneh.models.bodypart_symptom.Symptom;

import java.util.List;

/**
 * Created by pourya on 3/26/17.
 */

public class AddBodyPartSymptomAdapter extends RecyclerView.Adapter<AddBodyPartSymptomAdapter.BodyPartSymptomViewHolder> {

    Context context;
    List<Symptom> symptoms;
    private final String TAG = AddBodyPartSymptomAdapter.class.getSimpleName();
    final Handler handler = new Handler();

    public AddBodyPartSymptomAdapter(List<Symptom> symptoms) {
        this.symptoms = symptoms;
    }

    @Override
    public BodyPartSymptomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        View itemView;
        itemView = LayoutInflater.
                from(context).
                inflate(R.layout.item_bodypart_symptom_list, parent, false);
        return new AddBodyPartSymptomAdapter.BodyPartSymptomViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final BodyPartSymptomViewHolder holder, int position) {
        final Symptom symptom = symptoms.get(position);
        holder.symptom_text.setText(symptom.getNameFa().trim());
        if (((BodyPartSymptomList) context).checkedSymptoms.contains(symptom)) {
            holder.symptom_cb.setChecked(true);
//            symptomLayoutToggle(holder.symptom_cb,holder.symptom_text,holder.symptom_layout);
//            holder.symptom_text.setTextColor(context.getResources().getColor(R.color.colorWhite));
//            holder.symptom_layout.setBackground(context.getResources().getDrawable(R.drawable.btn_bg_filled));
        } else {
            holder.symptom_cb.setChecked(false);
//            symptomLayoutToggle(holder.symptom_cb,holder.symptom_text,holder.symptom_layout);
//            holder.symptom_text.setTextColor(context.getResources().getColor(R.color.colorPrimary));
//            holder.symptom_layout.setBackground(context.getResources().getDrawable(R.drawable.btn_bg_blue));
        }
    }

    @Override
    public int getItemCount() {
        return symptoms.size();
    }

    private void symptomLayoutToggle(CheckBox symptom_cb, final TextView symptom_text, final FrameLayout symptom_layout){
        if (symptom_cb.isChecked()){
            symptom_layout.setBackgroundResource(R.drawable.btn_bg_filled);
            symptom_text.setTextColor(ContextCompat.getColor(context, R.color.colorWhite));
        }else{
            symptom_layout.setBackgroundResource(R.drawable.btn_bg_blue);
            symptom_text.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
        }
    }

    class BodyPartSymptomViewHolder extends RecyclerView.ViewHolder {
        CheckBox symptom_cb;
        TextView symptom_text;
        FrameLayout symptom_layout;

        BodyPartSymptomViewHolder(View view) {
            super(view);
            this.symptom_text = (TextView) view.findViewById(R.id.symptom_text);
            this.symptom_text.setTypeface(App.getFont(3));
            this.symptom_cb = (CheckBox) view.findViewById(R.id.symptom_cb);
            this.symptom_layout = (FrameLayout) view.findViewById(R.id.symptom_layout);
            this.symptom_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    symptom_cb.toggle();
                }
            });

            this.symptom_cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if (b) {
                        if(!((BodyPartSymptomList) context).checkedSymptoms.contains(symptoms.get(getAdapterPosition())))
                            ((BodyPartSymptomList) context).checkedSymptoms.add(
                                symptoms.get(getAdapterPosition())
                            );
                        Log.d(TAG , "add "+symptoms.get(getAdapterPosition()));
                    } else {
                        if(((BodyPartSymptomList) context).checkedSymptoms.contains(symptoms.get(getAdapterPosition())))
                            ((BodyPartSymptomList) context).checkedSymptoms.remove(
                                    symptoms.get(getAdapterPosition())
                            );
                        Log.d(TAG , "remove "+symptoms.get(getAdapterPosition()));
                    }
                    symptomLayoutToggle(symptom_cb,symptom_text,symptom_layout); //TODO find a solution to send it to onbind
//                    notifyItemChanged(getAdapterPosition());
                }
            });
        }
    }
}
