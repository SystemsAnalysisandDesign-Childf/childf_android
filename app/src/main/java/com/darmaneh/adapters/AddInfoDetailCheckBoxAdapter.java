package com.darmaneh.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.darmaneh.ava.App;
import com.darmaneh.ava.R;
import com.darmaneh.models.call.InstantInfo;

import java.util.ArrayList;

public class AddInfoDetailCheckBoxAdapter extends RecyclerView.Adapter<AddInfoDetailCheckBoxAdapter.InfoListViewHolder> {

    Context context;
    ArrayList<InstantInfo> instantInfos;
    private  OnDeleteRecyclerViewListener mListener;;

    public AddInfoDetailCheckBoxAdapter(Context context, ArrayList<InstantInfo> instantInfos){
        this.context = context;
        this.instantInfos = instantInfos;
        mListener = (OnDeleteRecyclerViewListener) context;
    }

    @Override
    public InfoListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;
        itemView = LayoutInflater.
                from(context).
                inflate(R.layout.item_add_info_list, parent, false);
        return new InfoListViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(InfoListViewHolder holder, int position) {
        final InstantInfo info = instantInfos.get(position);
        holder.infoName.setText(info.getNameFa());
        holder.btnRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                instantInfos.remove(info);
                mListener.deleteInfo(instantInfos);
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return instantInfos.size();
    }

    class InfoListViewHolder extends RecyclerView.ViewHolder {
        FrameLayout btnRemove;
        TextView infoName;

        InfoListViewHolder(View v) {
            super(v);
            this.btnRemove = (FrameLayout) v.findViewById(R.id.btn_remove);
            this.infoName = (TextView) v.findViewById(R.id.info_name);
            this.infoName.setTypeface(App.getFont(3));
        }
    }

    public interface OnDeleteRecyclerViewListener {
        void deleteInfo(ArrayList<InstantInfo> instantInfos);
    }
}
