package com.darmaneh.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.darmaneh.ava.App;
import com.darmaneh.ava.R;
import com.darmaneh.ava.call.AudioVideoCheck;
import com.darmaneh.ava.call.DoctorList;
import com.darmaneh.ava.call.PatientInfoList;
import com.darmaneh.models.call.ProfessionListModel;
import com.darmaneh.utilities.Analytics;
import com.darmaneh.utilities.Storage;

import java.util.List;

public class ProfessionListAdapter extends RecyclerView.Adapter<ProfessionListAdapter.ProfessionModelViewHolder>{

    private List<ProfessionListModel> professionModels;
    private RelativeLayout professionNameLayout;
    Context context;

    public ProfessionListAdapter(List<ProfessionListModel> professionModels,
                                      Context context){
        this.professionModels = professionModels;
        this.context = context;
    }

    @Override
    public int getItemCount() {
        return professionModels.size();
    }

    class ProfessionModelViewHolder extends RecyclerView.ViewHolder {

        TextView professionName;

        ProfessionModelViewHolder(View itemView) {
            super(itemView);
            professionName = (TextView)itemView.findViewById(R.id.profession_name);
            professionName.setTypeface(App.getFont(4));
            professionNameLayout = (RelativeLayout) itemView.findViewById(R.id.profession_name_layout);
            professionNameLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Analytics.sendScreenName("telemedicine/category/" + professionModels.get(getAdapterPosition()).getName());
                    Storage.setDoctorCategory(professionModels.get(getAdapterPosition()).getName());
                    Storage.setMaxWaitingTime(professionModels.get(getAdapterPosition()).getMaxMinutes());
                    Intent intent = new Intent(context, PatientInfoList.class);
                    context.startActivity(intent);
                }
            });
        }
    }

    @Override
    public ProfessionModelViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_profession_list, viewGroup, false);
        ProfessionModelViewHolder avh = new ProfessionModelViewHolder(v);
        return avh;
    }

    @Override
    public void onBindViewHolder(final ProfessionModelViewHolder conditionModelViewHolder, final int i) {
        final ProfessionListModel professionListModel = professionModels.get(i);
        conditionModelViewHolder.professionName.setText(professionListModel.getName());
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

}

