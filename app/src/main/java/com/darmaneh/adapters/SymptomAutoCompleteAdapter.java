package com.darmaneh.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.darmaneh.ava.App;
import com.darmaneh.ava.R;
import com.darmaneh.models.AutoCompleteModel;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Created by alireza on 6/28/17.
 */

public class SymptomAutoCompleteAdapter extends ArrayAdapter<AutoCompleteModel> {
    private static final String TAG = SymptomAutoCompleteAdapter.class.getSimpleName();
    final private List<AutoCompleteModel> symptoms;
    private List<AutoCompleteModel> filteredSymptoms = new ArrayList<>();

    public SymptomAutoCompleteAdapter(Context context, List<AutoCompleteModel> objects) {
        super(context, 0, objects);
        this.symptoms = objects;
    }

    @Nullable
    @Override
    public AutoCompleteModel getItem(int position) {
        return filteredSymptoms.get(position);
    }

    @NonNull
    @Override
    public Filter getFilter() {
        return new ArrayFilter(this,symptoms);
    }


    @Override
    public int getCount() {
        return filteredSymptoms.size();
    }


    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.item_sympthom_auto, parent, false);
        }
        TextView symptomText = (TextView)convertView.findViewById(R.id.text);
        AutoCompleteModel symptom = filteredSymptoms.get(position);
        if (symptom!= null){
            symptomText.setText(symptom.getNameFa());
            symptomText.setTypeface(App.getFont(3));
        }
        return convertView;
    }

    private class ArrayFilter extends Filter {

        SymptomAutoCompleteAdapter adapter;
        List<AutoCompleteModel> originalList;
        List<AutoCompleteModel> filteredList;

        public ArrayFilter(SymptomAutoCompleteAdapter adapter, List<AutoCompleteModel> originalList) {
            this.adapter = adapter;
            this.originalList = originalList;
            this.filteredList = new ArrayList<>();
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            filteredList.clear();
            final FilterResults results = new FilterResults();
            if (constraint == null || constraint.length() == 0){
                filteredList.addAll(originalList);
            } else{
                final String filterPatternStrict = "\\b"+constraint.toString().toLowerCase()+"\\b";
                final String filterPatternSoft = constraint.toString().toLowerCase();
                Pattern p = Pattern.compile(filterPatternStrict);
                for (final AutoCompleteModel model : originalList){
                    String item = model.getNameFa();
                    if (p.matcher(item.toLowerCase()).find()){
                        filteredList.add(0,model);
                    }else if (item.toLowerCase().contains(filterPatternSoft)) {
                        filteredList.add(model);
                    }
                }
            }
            results.values = filteredList;
            results.count = filteredList.size();
            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            adapter.filteredSymptoms.clear();
            adapter.filteredSymptoms.addAll((ArrayList<AutoCompleteModel>)results.values);
            if (results.count > 0) {
                adapter.notifyDataSetChanged();
            } else {
                adapter.notifyDataSetInvalidated();
            }
        }
    }
}
