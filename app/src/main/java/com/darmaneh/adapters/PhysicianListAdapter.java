package com.darmaneh.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.darmaneh.ava.App;
import com.darmaneh.ava.R;
import com.darmaneh.ava.health_centers.DoctorProfileActivity;
import com.darmaneh.models.health_centers.PhysicianListModel;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by alireza on 8/25/17.
 */

public class PhysicianListAdapter extends RecyclerView.Adapter<PhysicianListAdapter.MainViewHolder>{

    private List<PhysicianListModel> physicianListModels;
    Context context;
    private Picasso picasso;

    public PhysicianListAdapter(Context context, List<PhysicianListModel> physicianListModels) {
        this.physicianListModels = physicianListModels;
        this.context = context;
        this.picasso = Picasso.with(context);
    }

    @Override
    public MainViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;
        itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.item_physician_list, parent, false);
        return new PhysicianListAdapter.MainViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MainViewHolder holder, int position) {
        holder.name.setText(physicianListModels.get(position).getName());

        String professionStr = "";
        for(String prof : physicianListModels.get(position).getProfession()){
            professionStr += prof + ",";
        }
        holder.profession.setText(professionStr.substring(0,professionStr.length()-1)); // remove last ','

        if(physicianListModels.get(position).getImage() != null){
            picasso.load(physicianListModels.get(position).getImage()).into(holder.image);
        }else{
            holder.image.setImageResource(R.drawable.doctor_sample_profile);
        }
    }

    @Override
    public int getItemCount() {
        return physicianListModels.size();
    }

    public class MainViewHolder extends RecyclerView.ViewHolder{
        CircleImageView image;
        TextView name, profession;

        public MainViewHolder(View itemView) {
            super(itemView);
            image = (CircleImageView) itemView.findViewById(R.id.doctor_pic);
            name = (TextView) itemView.findViewById(R.id.doctor_name);
            profession = (TextView) itemView.findViewById(R.id.doctor_profession);
            name.setTypeface(App.getFont(4));
            profession.setTypeface(App.getFont(3));
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, DoctorProfileActivity.class);
                    intent.putExtra("url", physicianListModels.get(getAdapterPosition()).getUrl());
                    context.startActivity(intent);
                }
            });
        }
    }
}
