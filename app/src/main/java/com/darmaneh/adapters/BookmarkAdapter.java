package com.darmaneh.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.darmaneh.ava.App;
import com.darmaneh.ava.R;
import com.darmaneh.fragments.AdapterCallback;
import com.darmaneh.models.patient_record.ConditionDetail;
import com.darmaneh.utilities.Analytics;

import java.util.List;

/**
 * Created by alireza on 3/30/17.
 */
public class BookmarkAdapter extends RecyclerView.Adapter<BookmarkAdapter.MainViewHolder> {
    private List<ConditionDetail> conditionDetails;
    private Context thisCntx;
    private AdapterCallback myCallback;

    public BookmarkAdapter(Context thisCntx, List<ConditionDetail> conditionDetails, AdapterCallback callback) {
        this.thisCntx = thisCntx;
        this.conditionDetails = conditionDetails;
        this.myCallback = callback;
    }

    public void swapData (List<ConditionDetail> conditionDetailList){
        conditionDetails.clear();
        conditionDetails.addAll(conditionDetailList);
        notifyDataSetChanged();
    }

    @Override
    public BookmarkAdapter.MainViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.item_bookmark_list, parent, false);
        return new MainViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(BookmarkAdapter.MainViewHolder holder, int position) {
        ConditionDetail cd = conditionDetails.get(position);
        holder.illnessTxt.setTypeface(App.getFont(3));
        holder.illness.setTypeface(App.getFont(3));
        holder.illness.setText(cd.getName());
    }

    @Override
    public int getItemCount() {
        return conditionDetails.size();
    }

    public class MainViewHolder extends RecyclerView.ViewHolder{
        TextView illnessTxt , illness;
        ImageView arrow;
        FrameLayout closeBtn;
        public MainViewHolder(View itemView) {
            super(itemView);
            illnessTxt = (TextView) itemView.findViewById(R.id.illness_txt);
            illness = (TextView) itemView.findViewById(R.id.illness);
            closeBtn = (FrameLayout) itemView.findViewById(R.id.close);
            closeBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    com.darmaneh.requests.ConditionDetail.unbookmark_condition_detail(thisCntx,
                            conditionDetails.get(getAdapterPosition()).getId(),
                            new com.darmaneh.requests.ConditionDetail.UnbookmarkConditionDetail() {
                                @Override
                                public void onHttpResponse(Boolean success) {
                                    if(success) {
                                        Analytics.sendScreenName("EMR/bookmark/remove");
                                        conditionDetails.remove(getAdapterPosition());
                                        notifyDataSetChanged();
                                        myCallback.checkEmptyCallback();
                                    }
                                }
                            });
                }
            });
            arrow = (ImageView) itemView.findViewById(R.id.arrow_go);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
//                    com.darmaneh.requests.ConditionDetail.get_condition_detail(thisCntx,
//                            conditionDetails.get(getAdapterPosition()).getUrl());
                }
            });
            arrow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
//                    com.darmaneh.requests.ConditionDetail.get_condition_detail(thisCntx,
//                            conditionDetails.get(getAdapterPosition()).getUrl());
                }
            });

        }
    }
}
