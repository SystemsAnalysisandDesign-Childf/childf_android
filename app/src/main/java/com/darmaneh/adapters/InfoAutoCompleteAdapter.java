package com.darmaneh.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.darmaneh.ava.App;
import com.darmaneh.ava.R;
import com.darmaneh.models.AutoCompleteModel;
import com.darmaneh.models.call.InstantInfo;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Created by alireza on 6/28/17.
 */

public class InfoAutoCompleteAdapter extends ArrayAdapter<InstantInfo> {
    private static final String TAG = InfoAutoCompleteAdapter.class.getSimpleName();
    final private List<InstantInfo> infos;
    private List<InstantInfo> filteredInfos = new ArrayList<>();

    public InfoAutoCompleteAdapter(Context context, List<InstantInfo> objects) {
        super(context, 0, objects);
        this.infos = objects;
    }

    @Nullable
    @Override
    public InstantInfo getItem(int position) {
        return filteredInfos.get(position);
    }

    @NonNull
    @Override
    public Filter getFilter() {
        return new ArrayFilter(this,infos);
    }


    @Override
    public int getCount() {
        return filteredInfos.size();
    }


    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.item_info_auto, parent, false);
        }
        TextView infoText = (TextView)convertView.findViewById(R.id.text);
        InstantInfo info = filteredInfos.get(position);
        if (info!= null){
            infoText.setText(info.getNameFa());
            infoText.setTypeface(App.getFont(3));
        }
        return convertView;
    }

    private class ArrayFilter extends Filter {

        InfoAutoCompleteAdapter adapter;
        List<InstantInfo> originalList;
        List<InstantInfo> filteredList;

        public ArrayFilter(InfoAutoCompleteAdapter adapter, List<InstantInfo> originalList) {
            this.adapter = adapter;
            this.originalList = originalList;
            this.filteredList = new ArrayList<>();
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            filteredList.clear();
            final FilterResults results = new FilterResults();
            if (constraint == null || constraint.length() == 0){
                filteredList.addAll(originalList);
            } else{
                int lastStrictIndex = 0;
                final String filterPatternStrict = "\\b"+constraint.toString().toLowerCase()+"\\b";
                final String filterPatternSoft = constraint.toString().toLowerCase();
                Pattern p = Pattern.compile(filterPatternStrict);
                for (final InstantInfo model : originalList){
                    String item = model.getNameFa();
                    if (p.matcher(item.toLowerCase()).find()){
                        filteredList.add(0,model);
                        lastStrictIndex++;
                    }else if (item.toLowerCase().startsWith(filterPatternSoft)){
                        if (filteredList.size() == lastStrictIndex){
                            filteredList.add(model);
                        }else{
                            filteredList.add(lastStrictIndex, model);
                        }
                    }
                    else if (item.toLowerCase().contains(filterPatternSoft)) {
                        filteredList.add(model);
                    }
                }
            }
            results.values = filteredList;
            results.count = filteredList.size();
            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            adapter.filteredInfos.clear();
            adapter.filteredInfos.addAll((ArrayList<InstantInfo>)results.values);
            if (results.count > 0) {
                adapter.notifyDataSetChanged();
            } else {
                adapter.notifyDataSetInvalidated();
            }
        }
    }
}
