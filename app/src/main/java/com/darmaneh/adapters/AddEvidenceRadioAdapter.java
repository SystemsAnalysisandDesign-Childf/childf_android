package com.darmaneh.adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.darmaneh.ava.App;
import com.darmaneh.ava.R;
import com.darmaneh.ava.diagnosis.BodyPartSymptomList;
import com.darmaneh.ava.diagnosis.GroupSingleQuestion;
import com.darmaneh.models.bodypart_symptom.BodyPart;
import com.darmaneh.models.diagnosis.Evidence;
import com.darmaneh.models.diagnosis.Item;

import java.util.List;

/**
 * Created by pourya on 3/25/17.
 */

public class AddEvidenceRadioAdapter extends RecyclerView.Adapter<AddEvidenceRadioAdapter.EvidenceListViewHolder> {
    private List<Item> items;
    private Context context;
    private int selectedItem = -1;

    public AddEvidenceRadioAdapter(List<Item> items) {
        this.items = items;
    }
    private void evidenceLayoutColor(boolean isChecked, final TextView evidenceText, final FrameLayout evidenceLayout) {
        if (isChecked) {
            evidenceLayout.setBackgroundResource(R.drawable.btn_bg_filled);
            evidenceText.setTextColor(ContextCompat.getColor(context, R.color.colorWhite));
        } else {
            evidenceLayout.setBackgroundResource(R.drawable.btn_bg_blue);
            evidenceText.setTextColor(ContextCompat.getColor(context, R.color.bodypart_color));
        }
    }

    @Override
    public EvidenceListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        View itemView;
        itemView = LayoutInflater.
                from(context).
                inflate(R.layout.item_add_evidence_radio_list, parent, false);
        return new EvidenceListViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(EvidenceListViewHolder holder, int position) {
        Item item = items.get(position);
        boolean checked = position == selectedItem;

        holder.evidenceText.setText(item.getName());

        holder.evidenceRadio.setChecked(checked);
        if (checked) {
            ((GroupSingleQuestion) context).checkedItem = item;
        }
        evidenceLayoutColor(checked, holder.evidenceText, holder.evidenceLayout);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    class EvidenceListViewHolder extends RecyclerView.ViewHolder {
        RadioButton evidenceRadio;
        TextView evidenceText;
        FrameLayout evidenceLayout;

        EvidenceListViewHolder(View v) {
            super(v);
            this.evidenceText = (TextView) v.findViewById(R.id.evidence_text);
            this.evidenceText.setTypeface(App.getFont(3));
            this.evidenceLayout = (FrameLayout) v.findViewById(R.id.evidence_layout);
            this.evidenceRadio = (RadioButton) v.findViewById(R.id.evidence_radio);
            this.evidenceRadio.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    selectedItem = getAdapterPosition();
                    notifyItemRangeChanged(0, items.size());
                }
            });

            v.findViewById(R.id.evidence_layout).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    selectedItem = getAdapterPosition();
                    notifyItemRangeChanged(0, items.size());
                }
            });

        }
    }
}
