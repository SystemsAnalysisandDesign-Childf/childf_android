package com.darmaneh.adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.darmaneh.ava.App;
import com.darmaneh.ava.Manifest;
import com.darmaneh.ava.R;
import com.darmaneh.ava.health_centers.PharmacyDetailActivity;
import com.darmaneh.models.health_centers.PharmacyDetailModel;
import com.squareup.picasso.Picasso;

/**
 * Created by alireza on 6/18/17.
 */

public class PharmacyDetailAdapter  extends RecyclerView.Adapter<PharmacyDetailAdapter.MainViewHolder>{

    private PharmacyDetailModel pharmacyDetailModel;
    private Context context;
    private final String STATIC_MAP_API_ENDPOINT =
            "http://maps.googleapis.com/maps/api/staticmap?size=640x400&zoom=15&markers=";

    public PharmacyDetailAdapter(Context context, PharmacyDetailModel pharmacyDetailModel) {
        this.pharmacyDetailModel = pharmacyDetailModel;
        this.context = context;
    }

    @Override
    public MainViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;
        itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.item_pharmacy_detail, parent, false);
        return new PharmacyDetailAdapter.MainViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MainViewHolder holder, int position) {
        if(pharmacyDetailModel.getPublicName().equals(""))
            holder.publicNameLayout.setVisibility(View.GONE);
        else
            holder.publicName.setText(pharmacyDetailModel.getPublicName());

        if(pharmacyDetailModel.getAddress().equals(""))
            holder.addressLayout.setVisibility(View.GONE);
        else
            holder.address.setText(pharmacyDetailModel.getAddress());

        if(pharmacyDetailModel.getNationalNumber() == null)
            holder.nationalNumLayout.setVisibility(View.GONE);
        else
            holder.nationalNumber.setText(String.valueOf(pharmacyDetailModel.getNationalNumber()));

        if(pharmacyDetailModel.getFounder().equals(""))
            holder.founderLayout.setVisibility(View.GONE);
        else
            holder.founder.setText(pharmacyDetailModel.getFounder());

        if(pharmacyDetailModel.getUniversity()==null)
            holder.universityLayout.setVisibility(View.GONE);
        else
            holder.university.setText(pharmacyDetailModel.getUniversity());

        if(pharmacyDetailModel.getLocation().getLatitude() != 0 &&
                pharmacyDetailModel.getLocation().getLongitude() != 0) {
            String locationStr = pharmacyDetailModel.getLocation().getLatitude() + "," +
                    pharmacyDetailModel.getLocation().getLongitude();
            String keyStr = "&key=" + context.getString(R.string.google_api_key);
            String url = STATIC_MAP_API_ENDPOINT + locationStr + keyStr;
            Picasso.with(context).load(url).placeholder(R.drawable.map_loading)
                    .fit().centerCrop()
                    .into(holder.map);
        }else{
            holder.map.setVisibility(View.GONE);
        }

    }

    @Override
    public int getItemCount() {
        return 1;
    }

    class MainViewHolder extends RecyclerView.ViewHolder{
        TextView  publicName, address, nationalNumber, founder, university;
        LinearLayout publicNameLayout, addressLayout, nationalNumLayout, founderLayout, universityLayout;
        ImageView map;
        public MainViewHolder(View itemView) {
            super(itemView);
            initViews(itemView);
            setViewsFont(itemView);
            setOnClick();
        }
        private void initViews(View view) {
            publicName = (TextView) view.findViewById(R.id.public_name);
            address = (TextView) view.findViewById(R.id.address);
            nationalNumber = (TextView) view.findViewById(R.id.national_number);
            founder = (TextView) view.findViewById(R.id.founder);
            university = (TextView) view.findViewById(R.id.university);
            map = (ImageView) view.findViewById(R.id.map);

            publicNameLayout = (LinearLayout) view.findViewById(R.id.public_name_layout);
            addressLayout = (LinearLayout) view.findViewById(R.id.address_layout);
            nationalNumLayout = (LinearLayout) view.findViewById(R.id.national_number_layout);
            founderLayout = (LinearLayout) view.findViewById(R.id.founder_layout);
            universityLayout = (LinearLayout) view.findViewById(R.id.university_layout);
        }
        private void setViewsFont(View view) {
            address.setTypeface(App.getFont(4));
            ((TextView)view.findViewById(R.id.address_text)).setTypeface(App.getFont(3));
            publicName.setTypeface(App.getFont(4));
            ((TextView)view.findViewById(R.id.public_name_text)).setTypeface(App.getFont(3));
            nationalNumber.setTypeface(App.getFont(4));
            ((TextView)view.findViewById(R.id.national_number_text)).setTypeface(App.getFont(3));
            founder.setTypeface(App.getFont(4));
            ((TextView)view.findViewById(R.id.founder_text)).setTypeface(App.getFont(3));
            university.setTypeface(App.getFont(4));
            ((TextView)view.findViewById(R.id.university_text)).setTypeface(App.getFont(3));
        }
        private void setOnClick() {
            map.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String locationStr = pharmacyDetailModel.getLocation().getLatitude() + ","+
                            pharmacyDetailModel.getLocation().getLongitude();
                    Uri uri = Uri.parse("geo:0,0?q="+locationStr+"( داروخانه "+pharmacyDetailModel.getSpecialName()+")");
                    Intent intent = new Intent(Intent.ACTION_VIEW,uri);
                    context.startActivity(intent);
                }
            });
        }
    }
}
