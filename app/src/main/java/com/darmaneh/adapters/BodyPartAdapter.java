package com.darmaneh.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.darmaneh.ava.App;
import com.darmaneh.ava.R;
import com.darmaneh.ava.diagnosis.BodyPartSymptomList;
import com.darmaneh.models.bodypart_symptom.BodyPart;
import com.darmaneh.utilities.Statics;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pourya on 3/26/17.
 */

public class BodyPartAdapter extends RecyclerView.Adapter<BodyPartAdapter.BodyPartViewHolder> {

    private List<BodyPart> bodyParts;
    private Context context;

    public BodyPartAdapter(List<BodyPart> bodyParts) {
        this.bodyParts = new ArrayList<>();
        for (BodyPart bodyPart:bodyParts) {
            if (bodyPart.getSymptoms().size() != 0) {
                this.bodyParts.add(bodyPart);
            }
        }
    }

    @Override
    public BodyPartViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();

        View itemView = LayoutInflater.
                from(context).
                inflate(R.layout.item_bodypart_list, parent, false);

        return new BodyPartViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(BodyPartViewHolder holder, int position) {
        final BodyPart bodyPart;
        bodyPart = bodyParts.get(position);
        holder.bodyPartName.setText(bodyPart.getNameFa());
        holder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, BodyPartSymptomList.class);
                intent.putExtra("symptoms", bodyPart.getSymptoms().toString());
                intent.putExtra("bodypart", bodyPart.getNameFa());
                ((AppCompatActivity) context).startActivityForResult(intent, Statics.REQUEST_EXIT);
            }
        });
    }

    @Override
    public int getItemCount() {
        return bodyParts.size();
    }

    class BodyPartViewHolder extends RecyclerView.ViewHolder {

        TextView bodyPartName;
        FrameLayout container;

        BodyPartViewHolder(View view) {
            super(view);
            this.bodyPartName = (TextView) view.findViewById(R.id.btn_symptoms);
            this.bodyPartName.setTypeface(App.getFont(4));
            this.container = (FrameLayout) view.findViewById(R.id.container);
        }
    }
}
