package com.darmaneh.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.darmaneh.ava.App;
import com.darmaneh.ava.R;
import com.darmaneh.ava.call.DoctorDetail;
import com.darmaneh.models.emergency.EmergencyModel;
import com.darmaneh.utilities.Functions;

import java.util.List;

/**
 * Created by root on 2/5/18.
 */

public class EmergencyAdapter extends RecyclerView.Adapter<EmergencyAdapter.EmergencyModelViewHolder> {
    private List<EmergencyModel> emergencyModels;
    Context context;

    public EmergencyAdapter(List<EmergencyModel> emergencyModels,
                                      Context context){
        this.emergencyModels = emergencyModels;
        this.context = context;
    }

    @Override
    public int getItemCount() {
        return emergencyModels.size();
    }

    class EmergencyModelViewHolder extends RecyclerView.ViewHolder {

        RelativeLayout cv;
        TextView conditionName, price;

        EmergencyModelViewHolder(View itemView) {
            super(itemView);
            cv = (RelativeLayout) itemView.findViewById(R.id.cv);
            conditionName = (TextView)itemView.findViewById(R.id.condition_name);
            conditionName.setTypeface(App.getFont(4));
            price = (TextView)itemView.findViewById(R.id.price);
            price.setTypeface(App.getFont(4));
            cv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
//                    ConditionDetail.get_condition_detail(view.getContext(),
//                            unadoptedListModels.get(getAdapterPosition()).getId());
                    Intent intent = new Intent(context, DoctorDetail.class);
                    intent.putExtra("name", emergencyModels.get(getAdapterPosition()).getMadadju().getUser().getFirstName() + " " + emergencyModels.get(getAdapterPosition()).getMadadju().getUser().getLastName());
                    intent.putExtra("hamyar_name", emergencyModels.get(getAdapterPosition()).getMadadju().getMyHamyar().getUser().getFirstName() + " " + emergencyModels.get(getAdapterPosition()).getMadadju().getMyHamyar().getUser().getLastName());
                    intent.putExtra("hamyar_phone", emergencyModels.get(getAdapterPosition()).getMadadju().getMyHamyar().getPhone());
                    intent.putExtra("cost", String.valueOf(emergencyModels.get(getAdapterPosition()).getCost()));
                    intent.putExtra("email", emergencyModels.get(getAdapterPosition()).getMadadju().getUser().getEmail());
                    intent.putExtra("receiverID", emergencyModels.get(getAdapterPosition()).getMadadju().getId().toString());
                    intent.putExtra("requirementID", emergencyModels.get(getAdapterPosition()).getId().toString());
                    context.startActivity(intent);

                }
            });
        }
    }

    @Override
    public EmergencyModelViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_emergency_list, viewGroup, false);
        EmergencyModelViewHolder avh = new EmergencyModelViewHolder(v);
        return avh;
    }

    @Override
    public void onBindViewHolder(final EmergencyModelViewHolder emergencyModelViewHolder, final int i) {
        final EmergencyModel emergencyModel = emergencyModels.get(i);
        emergencyModelViewHolder.conditionName.setText(emergencyModel.getMadadju().getUser().getFirstName() + " " + emergencyModel.getMadadju().getUser().getLastName());
        emergencyModelViewHolder.price.setText("هزینه مورد نیاز: " + Functions.toPersian(emergencyModel.getCost()));
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }
}
