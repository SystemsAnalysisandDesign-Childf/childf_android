package com.darmaneh.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.darmaneh.ava.App;
import com.darmaneh.ava.R;
import com.darmaneh.ava.health_centers.DoctorListActivity;
import com.darmaneh.models.health_centers.ProfessionCategoryModel;

import java.util.List;

/**
 * Created by alireza on 8/24/17.
 */

public class ProfessionCategoryAdapter extends RecyclerView.Adapter<ProfessionCategoryAdapter.MainViewHolder> {

    private Context context;
    private List<ProfessionCategoryModel> professionList;
    private String city, province;

    public ProfessionCategoryAdapter(Context context, List<ProfessionCategoryModel> professionList,
                                     String province, String city) {
        this.context = context;
        this.professionList = professionList;
        this.city = city;
        this.province = province;
    }

    @Override
    public MainViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;
        itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.item_profession_list, parent, false);
        return new MainViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MainViewHolder holder, int position) {
        holder.name.setText(professionList.get(position).getName());
    }

    @Override
    public int getItemCount() {
        return professionList.size();
    }

    class MainViewHolder extends RecyclerView.ViewHolder{
        TextView name;
        public MainViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.profession_name);
            name.setTypeface(App.getFont(3));
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, DoctorListActivity.class);
                    intent.putExtra("city",city);
                    intent.putExtra("province",province);
                    intent.putExtra("category",professionList.get(getAdapterPosition()).getName());
                    context.startActivity(intent);
                }
            });
        }
    }
}
