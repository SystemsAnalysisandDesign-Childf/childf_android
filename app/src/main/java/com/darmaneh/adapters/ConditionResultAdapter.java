package com.darmaneh.adapters;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Handler;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.darmaneh.ava.App;
import com.darmaneh.ava.R;
import com.darmaneh.models.diagnosis.Condition;
import com.darmaneh.requests.ConditionDetail;
import com.darmaneh.utilities.Analytics;
import com.darmaneh.utilities.Functions;

import org.w3c.dom.Text;

import java.util.List;

/**
 * Created by pourya on 3/26/17.
 */

public class ConditionResultAdapter extends RecyclerView.Adapter<ConditionResultAdapter.ConditionViewHolder> {

    private List<Condition> conditions;
    private Context context;
    private boolean[] progressed;

    public ConditionResultAdapter(List<Condition> conditions) {
        this.conditions = conditions;
        progressed = new boolean[conditions.size()];
    }

    @Override
    public ConditionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();

        View itemView = LayoutInflater.
                from(context).
                inflate(R.layout.item_condition_result_list, parent, false);

        return new ConditionViewHolder(itemView);
    }

    private void startProgressing(final ConditionViewHolder holder, final Condition condition, int position){
        if (!progressed[position]) {
            progressed[position]=true;
            holder.conditionProgressBar.setProgress(0);
            ObjectAnimator animation = ObjectAnimator.ofInt(holder.conditionProgressBar, "progress", condition.getIntProbability());
            animation.setDuration((int)(((double)condition.getIntProbability())/100*3500)); // 0.5 second, 3500
            animation.setInterpolator(new DecelerateInterpolator());
            animation.start();
        } else{
            holder.conditionProgressBar.setProgress(condition.getIntProbability());
        }
    }

    @Override
    public void onBindViewHolder(final ConditionViewHolder holder, int position) {
        final Condition condition = conditions.get(position);

        String name = condition.getName();
        if (!condition.getColloquial().equals(""))
            name += "\n" + "(" + condition.getColloquial() + ")";

        holder.conditionName.setText(name);
        holder.conditionProb.setText(condition.getProbability() + "٪");
        holder.conditionProgressBar.setRotation(180);
        startProgressing(holder, condition, position);
        holder.conditionHint.setText(condition.getHint());
        holder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Analytics.sendScreenName("sc/diagnosis/condition_detail/" + condition.getName());
//                ConditionDetail.get_condition_detail(view.getContext(),
//                        condition.getUrl());
            }
        });

        if (condition.getUrl().equals("")) {
            holder.container.setClickable(false);
            holder.moreInfo.setVisibility(View.GONE);
        } else {
            holder.container.setClickable(true);
            holder.moreInfo.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return conditions.size();
    }

    class ConditionViewHolder extends RecyclerView.ViewHolder {

        TextView conditionName;
        TextView conditionProb;
        TextView conditionHint;
        TextView moreInfo;
        LinearLayout container;
        ProgressBar conditionProgressBar;

        ConditionViewHolder(View view) {
            super(view);
            this.conditionName = (TextView) view.findViewById(R.id.condition_name);
            this.conditionName.setTypeface(App.getFont(3));
            ((TextView) view.findViewById(R.id.condition_name_label)).setTypeface(App.getFont(4));

            this.conditionProb = (TextView) view.findViewById(R.id.condition_prob);
            this.conditionProb.setTypeface(App.getFont(3));
            ((TextView) view.findViewById(R.id.condition_prob_label)).setTypeface(App.getFont(4));

            this.conditionProgressBar = (ProgressBar) view.findViewById(R.id.condition_progress_bar);

            this.conditionHint = (TextView) view.findViewById(R.id.condition_hint);
            this.conditionHint.setTypeface(App.getFont(3));
            ((TextView) view.findViewById(R.id.condition_hint_label)).setTypeface(App.getFont(4));

            this.moreInfo = (TextView) view.findViewById(R.id.more_info);
            this.moreInfo.setTypeface(App.getFont(3));

            this.container = (LinearLayout) view.findViewById(R.id.container);
        }
    }
}
