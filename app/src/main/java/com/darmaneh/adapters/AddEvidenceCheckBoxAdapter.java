package com.darmaneh.adapters;

import android.content.Context;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.darmaneh.ava.App;
import com.darmaneh.ava.R;
import com.darmaneh.ava.diagnosis.BodyPartSymptomList;
import com.darmaneh.ava.diagnosis.GroupMultipleQuestion;
import com.darmaneh.models.diagnosis.Evidence;
import com.darmaneh.models.diagnosis.Item;

import org.w3c.dom.Text;

import java.util.List;

/**
 * Created by pourya on 3/25/17.
 */

public class AddEvidenceCheckBoxAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final String TAG = AddEvidenceCheckBoxAdapter.class.getSimpleName();
    private List<Item> items;
    private Context context;
    final Handler handler = new Handler();
    private static final int TYPE_CHECKBOX = 0;
    private static final int TYPE_NONE = 1;
    private boolean noneChecked = false;

    public AddEvidenceCheckBoxAdapter(List<Item> items) {
        this.items = items;
    }

    private void evidenceLayoutToggle(CheckBox evidenceCheckbox, final TextView evidenceText, final FrameLayout evidenceLayout) {
        if (evidenceCheckbox.isChecked()) {
            evidenceLayout.setBackgroundResource(R.drawable.btn_bg_filled);
            evidenceText.setTextColor(ContextCompat.getColor(context, R.color.colorWhite));
        } else {
            evidenceLayout.setBackgroundResource(R.drawable.btn_bg_blue);
            evidenceText.setTextColor(ContextCompat.getColor(context, R.color.bodypart_color));
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        View itemView;
        if (viewType == TYPE_CHECKBOX) {
            itemView = LayoutInflater.
                    from(context).
                    inflate(R.layout.item_add_evidence_checkbox_list, parent, false);
            return new EvidenceListViewHolder(itemView);
        } else {
            itemView = LayoutInflater.
                    from(context).
                    inflate(R.layout.item_none_of_them, parent, false);
            return new NoneViewHolder(itemView);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position < items.size()) {
            return TYPE_CHECKBOX;
        } else {
            return TYPE_NONE;
        }
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        if (holder.getItemViewType() == TYPE_CHECKBOX) {
            final Item item = items.get(position);
            final EvidenceListViewHolder elvh = (EvidenceListViewHolder) holder;
            elvh.evidenceText.setText(item.getName());
            if (noneChecked) {
                elvh.evidenceCheckbox.setChecked(false);
                elvh.evidenceCheckbox.setClickable(false);
                elvh.evidenceLayout.setClickable(false);
                elvh.evidenceLayout.setBackgroundResource(R.drawable.btn_bg_gray);
                elvh.evidenceText.setTextColor(ContextCompat.getColor(context, R.color.gray_text));
            } else {
                evidenceLayoutToggle(elvh.evidenceCheckbox, elvh.evidenceText, elvh.evidenceLayout);
                elvh.evidenceCheckbox.setClickable(true);
                elvh.evidenceLayout.setClickable(true);
            }
        }
    }

    @Override
    public int getItemCount() {
        return items.size() + 1;
    }

    private class EvidenceListViewHolder extends RecyclerView.ViewHolder {
        CheckBox evidenceCheckbox;
        TextView evidenceText;
        FrameLayout evidenceLayout;

        EvidenceListViewHolder(View v) {
            super(v);
            this.evidenceText = (TextView) v.findViewById(R.id.evidence_text);
            this.evidenceText.setTypeface(App.getFont(3));
            this.evidenceCheckbox = (CheckBox) v.findViewById(R.id.evidence_cb);
            this.evidenceLayout = (FrameLayout) v.findViewById(R.id.evidence_layout);

            evidenceLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    evidenceCheckbox.toggle();
                }
            });
            evidenceCheckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    Item item = items.get(getAdapterPosition());
                    if (b) {
                        ((GroupMultipleQuestion) context).checkedItem.add(item);
                        ((GroupMultipleQuestion) context).uncheckedItem.remove(item);
                    } else {
                        ((GroupMultipleQuestion) context).checkedItem.remove(item);
                        ((GroupMultipleQuestion) context).uncheckedItem.add(item);
                    }
                    evidenceLayoutToggle(evidenceCheckbox, evidenceText, evidenceLayout);
                }
            });
        }
    }

    private class NoneViewHolder extends RecyclerView.ViewHolder {
        CheckBox checkbox;
        TextView text;
        FrameLayout layout;

        NoneViewHolder(View itemView) {
            super(itemView);
            this.checkbox = (CheckBox) itemView.findViewById(R.id.evidence_cb);
            this.text = (TextView) itemView.findViewById(R.id.evidence_text);
            this.text.setTypeface(App.getFont(3));
            this.layout = (FrameLayout) itemView.findViewById(R.id.evidence_layout);
            layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    checkbox.toggle();
                }
            });
            checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        ((GroupMultipleQuestion) context).checkedItem.clear();
                        ((GroupMultipleQuestion) context).uncheckedItem.clear();        //add other items to checked
                        ((GroupMultipleQuestion) context).uncheckedItem.addAll(items);
                        ((GroupMultipleQuestion) context).noneIsChecked = true;
                        noneChecked = true;
                    } else {
                        ((GroupMultipleQuestion) context).noneIsChecked = false;
                        noneChecked = false;
                    }
                    evidenceLayoutToggle(checkbox, text, layout);
                    notifyDataSetChanged();
                }
            });
        }
    }


}
