package com.darmaneh.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.darmaneh.ava.App;
import com.darmaneh.ava.R;
import com.darmaneh.models.sc_history.Condition;
import com.darmaneh.models.sc_history.Data;
import com.darmaneh.models.sc_history.Evidence;
import com.darmaneh.requests.ConditionDetail;

import java.util.ArrayList;
import java.util.List;

public class SCHistoryDetailAdapter extends RecyclerView.Adapter<SCHistoryDetailAdapter.MainViewHolder>{

    private static final int TYPE_HEADER_SYMPTOM = 0;
    private static final int TYPE_HEADER_CONDITION = 1;
    private static final int TYPE_SYMPTOMS = 2;
    private static final int TYPE_CONDITIONS = 3;

    private List<Evidence> symptoms;
    private List<Condition> conditions;
    private Context context;

    public SCHistoryDetailAdapter(Data data) {
        this.conditions = data.getConditions();
        this.symptoms = new ArrayList<>();
        for (Evidence evidence:data.getEvidence()) {
            if (evidence.getId().startsWith("s_") && evidence.getChoiceId().equals("present")) {
                this.symptoms.add(evidence);
            }
        }
    }

    @Override
    public int getItemViewType(int pos) {
        if (pos == 0) {
            return TYPE_HEADER_SYMPTOM;
        } else if (pos <= symptoms.size()) {
            return TYPE_SYMPTOMS;
        } else if (pos == symptoms.size() + 1) {
            return TYPE_HEADER_CONDITION;
        } else {
            return TYPE_CONDITIONS;
        }
    }

    @Override
    public MainViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        View itemView;
        if (viewType == TYPE_HEADER_SYMPTOM || viewType == TYPE_HEADER_CONDITION) {
            itemView = LayoutInflater.from(context).
                    inflate(R.layout.item_sc_history_header_list, parent, false);
            return new HeaderViewHolder(itemView);
        }
        else if (viewType == TYPE_SYMPTOMS){
            itemView = LayoutInflater.from(context).
                    inflate(R.layout.item_sc_history_symptoms_list, parent, false);
            return new SymptomsViewHolder(itemView);
        }
        else if (viewType == TYPE_CONDITIONS){
            itemView = LayoutInflater.from(context).
                    inflate(R.layout.item_sc_history_conditions_list, parent, false);
            return new ConditionsViewHolder(itemView);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(MainViewHolder holder, int position) {
        if(holder.getItemViewType() == TYPE_HEADER_SYMPTOM){
            HeaderViewHolder hvh = (HeaderViewHolder) holder;
            hvh.headerTitle.setText("علائم");
        }
        else if(holder.getItemViewType() == TYPE_HEADER_CONDITION){
            HeaderViewHolder hvh = (HeaderViewHolder) holder;
            hvh.headerTitle.setText("مشکلات پزشکی");
        }
        else if(holder.getItemViewType() == TYPE_SYMPTOMS) {
            SymptomsViewHolder svh = (SymptomsViewHolder) holder;
            String name = "\t" + "\u2022 " + symptoms.get(position - 1).getName();
            svh.title.setText(name);
        }
        else if(holder.getItemViewType() == TYPE_CONDITIONS) {
            ConditionsViewHolder cvh = (ConditionsViewHolder) holder;
            final Condition condition = conditions.get(position - 2 - symptoms.size());
            String name = "\t" + "\u2022 " + condition.getName()
                    + " (" + condition.getProbability() + "%)";
            cvh.title.setText(name);
            if(condition.getUrl().equals("")){
                cvh.goToBtn.setVisibility(View.INVISIBLE);
            }
        }
    }

    @Override
    public int getItemCount() {
        return conditions.size() + symptoms.size() + 2;
    }

    class MainViewHolder extends RecyclerView.ViewHolder {
        MainViewHolder(View view) {
            super(view);
        }
    }

    private class HeaderViewHolder extends MainViewHolder {
        private TextView headerTitle;

        HeaderViewHolder(View itemView) {
            super(itemView);
            this.headerTitle = (TextView) itemView.findViewById(R.id.header_title);
            this.headerTitle.setTypeface(App.getFont(4));
        }
    }

    private class SymptomsViewHolder extends MainViewHolder {
        private TextView title;

        SymptomsViewHolder(View itemView) {
            super(itemView);
            this.title = (TextView) itemView.findViewById(R.id.header_title);
            this.title.setTypeface(App.getFont(3));
        }
    }

    private class ConditionsViewHolder extends MainViewHolder {
        private TextView title;
        private ImageView goToBtn;
        private LinearLayout layout;

        ConditionsViewHolder(View itemView) {
            super(itemView);
            this.title = (TextView) itemView.findViewById(R.id.header_title);
            this.title.setTypeface(App.getFont(3));
            this.goToBtn = (ImageView) itemView.findViewById(R.id.go_to_btn);
            this.layout = (LinearLayout) itemView.findViewById(R.id.header_layout);
            this.layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
//                    ConditionDetail.get_condition_detail(view.getContext(),
//                            conditions.get(getAdapterPosition()- 2 - symptoms.size()).getUrl());
                }
            });
        }
    }

}