package com.darmaneh.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.darmaneh.ava.App;
import com.darmaneh.ava.R;
import com.darmaneh.ava.health_centers.NearestHealthCenterActivity;
import com.darmaneh.models.health_centers.NearestListModel;
import com.darmaneh.requests.HealthCenters;
import com.darmaneh.utilities.Functions;

import java.util.List;

/**
 * Created by alireza on 9/12/17.
 */

public class NearestCentersAdapter extends RecyclerView.Adapter<NearestCentersAdapter.MainViewHolder>{
    private Context context;
    private List<NearestListModel> nearestModels;
    private NearestHealthCenterActivity.healthCenterType type;

    public NearestCentersAdapter(Context context, List<NearestListModel> nearestModels,
                                 NearestHealthCenterActivity.healthCenterType type) {
        this.context = context;
        this.nearestModels = nearestModels;
        this.type =type;
    }

    @Override
    public MainViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_nearest_list, parent, false);
        return new MainViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MainViewHolder holder, int position) {
        if(type == NearestHealthCenterActivity.healthCenterType.HOSPITAL)
            holder.centerName.setText(nearestModels.get(position).getName());
        else
            holder.centerName.setText(nearestModels.get(position).getSpecial_name());
        holder.centerAddress.setText(nearestModels.get(position).getAddress());
        int distanceInt = nearestModels.get(position).getDistance();
        String distanceKm = distanceInt>1000 ? Functions.toPersian((double)(distanceInt/100)/10) + " کیلومتر":
                Functions.toPersian(distanceInt) + " متر";
        String distanceStr = ("فاصله از شما " +"<b>" + distanceKm + "</b>");
        Spanned t;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            t = Html.fromHtml(distanceStr, Html.FROM_HTML_MODE_COMPACT);
        } else {
            t = Html.fromHtml(distanceStr);
        }
        holder.centerDistance.setText(t);
    }

    @Override
    public int getItemCount() {
        return nearestModels.size();
    }

    public class MainViewHolder extends RecyclerView.ViewHolder{
        private TextView centerName, centerAddress, centerDistance;
        public MainViewHolder(View itemView) {
            super(itemView);
            centerName = (TextView) itemView.findViewById(R.id.center_name);
            centerAddress = (TextView) itemView.findViewById(R.id.center_address);
            centerDistance = (TextView) itemView.findViewById(R.id.center_distance);
            centerName.setTypeface(App.getFont(4));
            centerAddress.setTypeface(App.getFont(3));
            centerDistance.setTypeface(App.getFont(2));
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(type == NearestHealthCenterActivity.healthCenterType.HOSPITAL) {
                        HealthCenters.get_hospital_detail(context, nearestModels.get(getAdapterPosition()).getUrl());
                    }
                    else{
                        HealthCenters.get_pharmacy_detail(context,nearestModels.get(getAdapterPosition()).getUrl());
                    }
                }
            });
        }
    }
}
