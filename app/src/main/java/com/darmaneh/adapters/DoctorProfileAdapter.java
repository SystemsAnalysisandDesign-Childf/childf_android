package com.darmaneh.adapters;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.TypedArray;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.darmaneh.ava.App;
import com.darmaneh.ava.R;
import com.darmaneh.models.physician.Address;
import com.squareup.picasso.Picasso;
import com.darmaneh.utilities.Functions;
import java.util.List;

/**
 * Created by alireza on 8/26/17.
 */

public class DoctorProfileAdapter extends RecyclerView.Adapter<DoctorProfileAdapter.MainViewHolder> {

    private List<Address> addressList;
    private String name;
    private final String STATIC_MAP_API_ENDPOINT =
            "http://maps.googleapis.com/maps/api/staticmap?size=640x400&zoom=15&markers=";
    private Context context;

    public DoctorProfileAdapter(Context context, List<Address> addressList, String name) {
        this.addressList = addressList;
        this.context = context;
        this.name = name;
    }

//    @Override
//    public int getItemViewType(int position) {
//        if(addressList.get(position).getContacts().size() <= 1){
//            return SINGLE_PHONE_MODE;
//        }else{
//            return MULTI_PHONE_MODE;
//        }
//    }

    @Override
    public MainViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;
        itemView = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.item_addresses_doctor, parent, false);
        return new MainViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MainViewHolder holder, int position) {

        Address addressModel = addressList.get(position);
        holder.address.setText(addressModel.getAddress());
        int phoneCount = 0;
        if(addressModel.getContacts().size()==1){
            String phoneNum = addressModel.getContacts().get(0);
            holder.phone.setText(phoneNum);
            holder.phoneTitle.setText("شماره تماس :");
            callClick(phoneNum,holder.firstPhoneLayout);
        }else {
            holder.firstPhoneLayout.setVisibility(View.GONE);
            holder.phoneTitle.setVisibility(View.GONE);
            for (final String phoneNum : addressModel.getContacts()) {
                phoneCount = phoneCount + 1;
                int padding_in_dp = 16;  // 6 dps
                final float scale = context.getResources().getDisplayMetrics().density;
                int padding_in_px_right = (int) (padding_in_dp * scale + 0.5f);
                int padding_in_px_bottom = (int) (16 * scale + 0.5f);

                LinearLayout newPhoneLayout = new LinearLayout(context);
                LinearLayout inPhoneLayout = new LinearLayout(context);
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams
                        (LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

                newPhoneLayout.setLayoutParams(params);
                newPhoneLayout.setClickable(true);
                LinearLayout.LayoutParams txtP = new LinearLayout.LayoutParams
                        (LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);

                LinearLayout.LayoutParams imgP = new LinearLayout.LayoutParams
                        (LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

                TextView phonenumView = new TextView(context);
                TextView phoneNumSeq = new TextView(context);
                ImageView phoneIcon = new ImageView(context);

                holder.phoneLayout.addView(newPhoneLayout);
                newPhoneLayout.setOrientation(LinearLayout.VERTICAL);

                phoneNumSeq.setText("شماره تماس " + Functions.toPersian(phoneCount) + ":");
                phoneNumSeq.setTextColor(context.getResources().getColor(R.color.colorBlack));
                phoneNumSeq.setLayoutParams(params);
                newPhoneLayout.addView(phoneNumSeq);
                phoneNumSeq.setGravity(Gravity.RIGHT);
                phoneNumSeq.setPadding(0, 0, padding_in_px_right, 0);
                //phoneNumSeq.setPadding();
                phoneNumSeq.setTypeface(App.getFont(3));
                newPhoneLayout.addView(inPhoneLayout);
                inPhoneLayout.setLayoutParams(params);
                inPhoneLayout.setOrientation(LinearLayout.HORIZONTAL);

                phoneIcon.setImageResource(R.drawable.ic_chevron_left);
                phoneIcon.setLayoutParams(imgP);
                phoneIcon.setBaselineAlignBottom(true);
                inPhoneLayout.addView(phoneIcon);


                inPhoneLayout.addView(phonenumView);
                phonenumView.setText(phoneNum);
                phonenumView.setLayoutParams(txtP);
                phonenumView.setPadding(0, 0, padding_in_px_right, padding_in_px_bottom);
                phonenumView.setGravity(Gravity.RIGHT | Gravity.CENTER);


                callClick(phoneNum, newPhoneLayout);
            }
        }

        if(addressModel.getLocation().getLongitude() == 0 && addressModel.getLocation().getLatitude() == 0 )
            holder.map.setVisibility(View.GONE);
        else{
            String locationStr = addressModel.getLocation().getLatitude() + ","+
                    addressModel.getLocation().getLongitude();
            String keyStr = "&key=" + context.getString(R.string.google_api_key);
            String url = STATIC_MAP_API_ENDPOINT + locationStr + keyStr;
            Picasso.with(context).load(url).placeholder(R.drawable.map_loading)
                    .fit().centerCrop()
                    .into(holder.map);
        }
    }

    @Override
    public int getItemCount() {

        return addressList.size();
    }

    public class MainViewHolder extends RecyclerView.ViewHolder{
        private TextView address;
        private ImageView map;
        private LinearLayout phoneLayout;
        private LinearLayout firstPhoneLayout;
        private TextView phone;
        private TextView phoneTitle;

        public MainViewHolder(View itemView) {
            super(itemView);
            phoneLayout = (LinearLayout) itemView.findViewById(R.id.doctor_phone_layout);
            firstPhoneLayout = (LinearLayout) itemView.findViewById(R.id.first_phone_layout);
            phone = (TextView) itemView.findViewById(R.id.phone_num);
            address = (TextView) itemView.findViewById(R.id.doctor_address);
            map = (ImageView) itemView.findViewById(R.id.map);
            phoneTitle = (TextView) itemView.findViewById(R.id.phone_title) ;

            //phone.setTypeface(App.getFont(3));
            address.setTypeface(App.getFont(3));
            phoneTitle.setTypeface(App.getFont(3));
            ((TextView)itemView.findViewById(R.id.address_title)).setTypeface(App.getFont(3));
            setOnClick();
        }

        private void setOnClick() {
            map.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String locationStr = addressList.get(getAdapterPosition()).getLocation().getLatitude() + ","+
                            addressList.get(getAdapterPosition()).getLocation().getLongitude();
                    Uri uri = Uri.parse("geo:0,0?q="+locationStr+"( "+name+")");
                    Intent intent = new Intent(Intent.ACTION_VIEW,uri);
                    context.startActivity(intent);
                }

            });
        }

    }
    private void callClick(final String callnum,  final LinearLayout layout ){
        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which){
                            case DialogInterface.BUTTON_POSITIVE:
                                String phoneNumber = "tel:" + callnum;
                                Log.e("key","onclickTest");
                                Intent callIntent = new Intent(Intent.ACTION_DIAL, Uri.parse(phoneNumber));
                                context.startActivity(callIntent);
                                //Yes button clicked
                                break;

                            case DialogInterface.BUTTON_NEGATIVE:
                                //No button clicked
                                break;
                        }
                    }
                };
                AlertDialog dialog= new AlertDialog.Builder(context)
                        .setMessage("آیا از برقراری تماس خود اطمینان دارید؟")
                        .setPositiveButton("بله", dialogClickListener)
                        .setNegativeButton("خیر", dialogClickListener)
                        .show();
                TextView textMessage = (TextView) dialog.findViewById(android.R.id.message);
                textMessage.setTypeface(App.getFont(3));
                TextView textPositive = (TextView) dialog.findViewById(android.R.id.button1);
                textPositive.setTypeface(App.getFont(3));
                textPositive.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
                TextView textNegative = (TextView) dialog.findViewById(android.R.id.button2);
                textNegative.setTypeface(App.getFont(3));
                textNegative.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
            }
        });

    }


}
