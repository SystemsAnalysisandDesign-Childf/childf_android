package com.darmaneh.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.darmaneh.ava.App;
import com.darmaneh.ava.R;
import com.darmaneh.ava.call.ChooseCallType;
import com.darmaneh.ava.call.DoctorDetail;
import com.darmaneh.fragments.RequestFailureDialog;
import com.darmaneh.models.adopted.AdoptedModel;
import com.darmaneh.models.call.DoctorInfoDetailModel;
import com.darmaneh.models.call.ProfessionModel;
import com.darmaneh.requests.DoctorDetailRequest;
import com.darmaneh.utilities.Analytics;
import com.darmaneh.utilities.Functions;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class DoctorListRecyclerViewAdapter extends RecyclerView.Adapter<DoctorListRecyclerViewAdapter.ViewHolder>{

    Context context;
    private ArrayList<AdoptedModel> adoptedModels;

    public DoctorListRecyclerViewAdapter(Context context, ArrayList<AdoptedModel> adoptedModels) {
        this.adoptedModels = adoptedModels;
        this.context = context;
    }

    @Override
    public int getItemCount() {
        return adoptedModels.size();
    }

    private void changeFonts(View view){
        ((TextView)view.findViewById(R.id.doctor_name_info)).setTypeface(App.getFont(3));
    }

    private void addListeners(final DoctorListRecyclerViewAdapter.ViewHolder view, final AdoptedModel info, final int position){

        view.card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, DoctorDetail.class);
                intent.putExtra("name", info.getUser().getFirstName() + " " + info.getUser().getLastName());
                intent.putExtra("hamyar_name", info.getMyHamyar().getUser().getFirstName() + " " + info.getMyHamyar().getUser().getLastName());
                intent.putExtra("hamyar_phone", info.getMyHamyar().getPhone());
                intent.putExtra("cost", "10000");
                intent.putExtra("email", info.getUser().getEmail());
                intent.putExtra("receiverID", info.getId().toString());
                intent.putExtra("requirementID", info.getUser().getId().toString());
                context.startActivity(intent);
            }
        });

    }

    @Override
    public DoctorListRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.doctor_list_layout, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        AdoptedModel info = adoptedModels.get(position);
        holder.doctorNameInfo.setText(info.getUser().getFirstName() + " " + info.getUser().getLastName());
        addListeners(holder, info, position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView doctorNameInfo;
        CircleImageView doctorImage;
        LinearLayout card;
        public ViewHolder(View view) {
            super(view);

            doctorNameInfo = (TextView)view.findViewById(R.id.doctor_name_info);
            card = (LinearLayout) view.findViewById(R.id.card);
            changeFonts(view);

            doctorImage = (CircleImageView) view.findViewById(R.id.doctor_pic);

        }
    }

}
