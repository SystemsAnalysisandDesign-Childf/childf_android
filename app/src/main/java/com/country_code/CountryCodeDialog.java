package com.country_code;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Typeface;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.darmaneh.ava.R;
import com.futuremind.recyclerviewfastscroll.FastScroller;

import java.util.List;

/**
 * Created by hbb20 on 11/1/16.
 */
class CountryCodeDialog {
    public static Typeface font;
    public static void openCountryCodeDialog(CountryCodePicker codePicker) {
        Context context=codePicker.getContext();
        final Dialog dialog = new Dialog(context);
        codePicker.refreshCustomMasterList();
        codePicker.refreshPreferredCountries();
        font = Typeface.createFromAsset(dialog.getContext().getAssets(), "IRANSansMobile.ttf");;
        List<Country> masterCountries = Country.getCustomMasterCountryList(context, codePicker);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setContentView(R.layout.layout_picker_dialog);
        RecyclerView recyclerView_countryDialog = (RecyclerView) dialog.findViewById(R.id.recycler_countryDialog);
        final TextView textViewTitle=(TextView) dialog.findViewById(R.id.textView_title);
//        textViewTitle.setText(codePicker.getDialogTitle());
        textViewTitle.setText("کشور خود را انتخاب کنید");
//        textViewTitle.setTypeface(App.getFont(3));
        final EditText editText_search = (EditText) dialog.findViewById(R.id.editText_search);
//        editText_search.setHint(codePicker.getSearchHintText());
        textViewTitle.setTypeface(font);
        editText_search.setTypeface(font);
        editText_search.setHint("جستجو ...");
        TextView textView_noResult = (TextView) dialog.findViewById(R.id.textView_noresult);
//        textView_noResult.setText(codePicker.getNoResultFoundText());
        textView_noResult.setText("کشوری یافت نشد");
        textView_noResult.setTypeface(font);
        final CountryCodeAdapter cca = new CountryCodeAdapter(context, masterCountries, codePicker, editText_search, textView_noResult, dialog);
        //this will make dialog compact
        if (!codePicker.isSelectionDialogShowSearch()) {
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) recyclerView_countryDialog.getLayoutParams();
            params.height = RecyclerView.LayoutParams.WRAP_CONTENT;
            recyclerView_countryDialog.setLayoutParams(params);
        }

        recyclerView_countryDialog.setLayoutManager(new LinearLayoutManager(context));
        recyclerView_countryDialog.setAdapter(cca);

        //fast scroller
        FastScroller fastScroller = (FastScroller) dialog.findViewById(R.id.fastscroll);
        fastScroller.setRecyclerView(recyclerView_countryDialog);
        if (codePicker.isShowFastScroller()) {
            if (codePicker.getFastScrollerBubbleColor() != -1) {
                fastScroller.setBubbleColor(codePicker.getFastScrollerBubbleColor());
            }

            if (codePicker.getFastScrollerHandleColor() != -1) {
                fastScroller.setHandleColor(codePicker.getFastScrollerHandleColor());
            }

            if (codePicker.getFastScrollerBubbleTextAppearance() != -1) {
                try {
                    fastScroller.setBubbleTextAppearance(codePicker.getFastScrollerBubbleTextAppearance());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        } else {
            fastScroller.setVisibility(View.GONE);
        }
        dialog.show();
    }
}
